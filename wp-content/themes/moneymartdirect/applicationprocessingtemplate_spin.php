<?php
/*
Template Name: APPLICATION PROCESSING W/SPIN
 */
get_header();

require_once $_SERVER['DOCUMENT_ROOT'] . BASE_DIR . "/wp-content/themes/moneymartdirect/include/mm_system_config.php";

// Check if user is logged in
if (!is_user_logged_in()) {
    $post_id = 923;
    $queried_post = get_post($post_id);
    $content = $queried_post->post_content;
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    echo $content;
} else {
    //Attempt to restore a session
    global $current_user;
    wp_get_current_user();
    $email_address = $current_user->user_email;
    mm_restore_session($email_address);
    ?>

    <script type="text/javascript">
        //Should ultimately move to the master js file.
        function replace_dropdown_values(object_name, values_arr) {
            let select = document.getElementById(object_name);

            //Clear the element first in case there are values
            while (select.options.length) select.remove(0);

            //let option = document.createElement("option");
            for (let i = 0; i < values_arr.length; i++) {
                let option = document.createElement("option");
                option.text = values_arr[i];
                select.add(option);
            }
        }

        var lookup = [];
        var state = '<?php $temp_value = isset($_SESSION["app_state"]) ? $_SESSION["app_state"] : "Unknown"; echo "$temp_value";?>';

        function convert_to_payfreq_code(pay_frequency) {
            if (pay_frequency === "Paid Twice Monthly") {
                return 'S';
            } else if (pay_frequency === "Paid Weekly" || pay_frequency === "Bi-Weekly") {
                return 'B';
            } else if (pay_frequency === "Paid Monthly") {
                return 'M';
            } else {
                //The pay frequency was unexpected so display nothing and log an error
                return '';
            }
        }

        function display_payment() {
            if (Object.keys(lookup).length === 1) {
                jQuery('#field_17_1 > label').text('Approved loan amount');
            }

            let selected_value = document.getElementById("input_17_1").value;
            let estimated_pmt = 0, term = 0;
            if (lookup.hasOwnProperty(selected_value)) {
                estimated_pmt = lookup[selected_value][0];
                term = lookup[selected_value][1];
            }
            let payment_amt = document.getElementById("display_pmt_amt");
            let loan_term = document.getElementById("display_pmt_term");
            let payment_frequency = document.getElementById("display_pay_frequency");
            let pay_frequency = "<?php $temp_value = isset($_SESSION["app_pay_frequency_1"]) ? $_SESSION["app_pay_frequency_1"] : "Unknown";  echo "$temp_value";?>";

            let pay_frequency_display = '';
            if (pay_frequency === "Paid Twice Monthly") {
                pay_frequency_display = "Payment Frequency: Twice A Month";
            } else if (pay_frequency === "Paid Weekly" || pay_frequency === "Bi-Weekly") {
                pay_frequency_display = "Payment Frequency: Every Other Week";
            } else if (pay_frequency === "Paid Monthly") {
                pay_frequency_display = "Payment Frequency: Once A Month";
            }
            let payment_amt_display = "Estimated Payment: $" + estimated_pmt;
            let term_display = "Estimated Number of Payments: " + term;
            payment_amt.innerHTML = payment_amt_display;
            loan_term.innerHTML = term_display;
            payment_frequency.innerHTML = pay_frequency_display;
        }
    </script>

    <div class="resultContent approved" id="app_approved" style="display:none">
        <?php
        $post_id = 575;
        $queried_post = get_post($post_id);
        $content = $queried_post->post_content;
        $content = apply_filters('the_content', $content);
        $content = str_replace(']]>', ']]&gt;', $content);
        echo $content;
        ?>
    </div>

    <div class="resultContent approved" id="app_refresh" style="display:none">
        <?php
        $post_id = 1521;
        $queried_post = get_post($post_id);
        $content = $queried_post->post_content;
        $content = apply_filters('the_content', $content);
        $content = str_replace(']]>', ']]&gt;', $content);
        echo $content;
        ?>
    </div>

    <div class="resultContent declined" id="app_declined" style="display:none">
        <?php
        $post_id = 577;
        $queried_post = get_post($post_id);
        $content = $queried_post->post_content;
        $content = apply_filters('the_content', $content);
        $content = str_replace(']]>', ']]&gt;', $content);
        echo $content;
        ?>
    </div>
    <div class="resultContent declined" id="app_error" style="display:none">
        <?php
        $post_id = 579;
        $queried_post = get_post($post_id);
        $content = $queried_post->post_content;
        $content = apply_filters('the_content', $content);
        $content = str_replace(']]>', ']]&gt;', $content);
        echo $content;
        ?>
    </div>
    <?php
    if (!isset($_SESSION["app_uw_decision"])) {
        error_log("Mark: In Application-Processing:  starting IF section\n");
        //Insert Processing Overlay and AJAX CALL FOR THIS SCENARIO
        //Set the uw decision to prevent multiple requests from being submitted
        $_SESSION["app_uw_decision"] = "Draft";
        ?>
        <!--This is displays the overlay on page load.  You can modify this however you see fit.   -->
        <div id="overlay" align="center">
            <div class="loaderContainer">
                <div class="loader"></div>
                <p style="color:white"><b>Please wait while we process your application</b></p>
                <p style="color:white"><b>This may take up to 60 seconds</b></p>
            </div>
        </div>
        <script type="text/javascript">
            <?php $lead_id = (isset($_SESSION['app_gf_lead_id'])) ? $_SESSION['app_gf_lead_id'] : '';

            global $current_user;
            get_currentuserinfo();
            $email_address = $current_user->user_email;
            ?>
            var lead_id = <?php echo "$lead_id"; ?>;
            var data = {
                "gf_lead_id": lead_id
            };
            data = jQuery(this).serialize() + "&" + jQuery.param(data);
            jQuery.ajax({
                url: "<?php echo baseUrl(); ?>/wp-content/themes/moneymartdirect/include/process_application.php",
                type: "POST",
                data: data,
                //content-type: 'application/json',
                dataType: "text json",
                success: function (data) {
                    let uw_decision = data.uw_decision;
                    let approved_amt = data.approved_amt;
                    let state_cd = data.state_cd;
                    let contract_apr = data.contract_apr;
                    let tmp = "<?php $temp_value = isset($_SESSION["app_pay_frequency_1"]) ? $_SESSION["app_pay_frequency_1"] : "Unknown";  echo "$temp_value";?>";
                    let payment_frequency = convert_to_payfreq_code(tmp);
                    let app_nbr = data.mm_application_nbr;

                    if (uw_decision === "Approved") {
                        let parms = {
                            "state_cd": state_cd,
                            "approved_amt": approved_amt,
                            "mm_application_nbr": app_nbr
                        };
                        parms = jQuery(this).serialize() + "&" + jQuery.param(parms);
                        jQuery.ajax({
                            url: "<?php echo baseUrl(); ?>/wp-content/themes/moneymartdirect/include/get_approved_values.php",
                            type: "POST",
                            data: parms,
                            dataType: "text json",
                            success: function (data) {
                                //let return_value = data.return_value;
                                //let return_message = data.return_message;
                                let display_values = data.display_values;
                                let data2 = {
                                    "state_cd": state_cd,
                                    "approved_amt": approved_amt,
                                    "pay_freq": payment_frequency,
                                    "contract_apr": contract_apr,
                                    "mm_application_nbr": app_nbr
                                };

                                jQuery.ajax({
                                    url: "<?php echo baseUrl(); ?>/wp-content/themes/moneymartdirect/include/get_pricing_values.php",
                                    type: 'POST',
                                    dataType: "text json",
                                    data: data2,
                                    success: function (data) {
                                        lookup = data.pricing_values;
                                        replace_dropdown_values("input_17_1", display_values);
                                        display_payment();
                                        let select_object = document.getElementById("input_17_1");
                                        select_object.onchange = function () {
                                            display_payment();
                                        };
                                        jQuery('#app_approved').show();
                                        jQuery('#overlay').hide();
                                    },
                                    error: function (xhr, status, errorThrown) {
                                        jQuery("#app_error").show();
                                        jQuery("#overlay").hide();
                                    }
                                });
                            },
                            error: function (xhr, status, errorThrown) {
                                jQuery('#app_error').show();
                                jQuery('#overlay').hide();
                            }
                        });
                    } else if (uw_decision === "Declined") {
                        jQuery('#app_declined').show();
                        jQuery('#overlay').hide();
                    } else {
                        jQuery('#app_error').show();
                        jQuery('#overlay').hide();
                    }
                },
                error: function (xhr, status, errorThrown) {
                    jQuery('#overlay').hide();
                    jQuery('#app_error').show();
                }
            });

        </script>
        <?php
    } else {
        error_log("Mark: In Application-Processing:  starting Else section\n");
        $uw_decision = $_SESSION["app_uw_decision"];
        if ($uw_decision == "Draft") {
            error_log("Mark: In Application-Processing:  starting IF section, first If\n");
            //The user must have refreshed the page before a decision could be levied so display an opps message asking them to go to their my account page for a status update
            ?>
            <script type="text/javascript">
                let x = document.getElementById('app_refresh');
                x.style.display = 'block';</script>
            <?php

        } else if ($uw_decision == "Approved") {
            error_log("Mark: In Application-Processing:  starting IF section, first Else If\n");
            $state_cd = isset($_SESSION["app_state"]) ? $_SESSION["app_state"] : '';
            $approved_amt = isset($_SESSION["app_approved_amt"]) ? $_SESSION["app_approved_amt"] : 0;
            ?>
            <script type="text/javascript">
                let state_cd = <?php echo "'$state_cd'"; ?> ;
                let approved_amt = <?php echo "$approved_amt"; ?>;
                let app_nbr = <?php echo $_SESSION["app_application_nbr"]; ?>;
                let parms = {
                    "state_cd": state_cd,
                    "approved_amt": approved_amt,
                    "mm_application_nbr": app_nbr
                };
                parms = jQuery(this).serialize() + "&" + jQuery.param(parms);
                jQuery.ajax({
                    url: "<?php echo baseUrl(); ?>/wp-content/themes/moneymartdirect/include/get_approved_values.php",
                    type: "POST",
                    data: parms,
                    dataType: "text json",
                    success: function (data) {
                        //let return_value = data.return_value;
                        //let return_message = data.return_message;
                        let display_values = data.display_values;
                        let tmp = "<?php $temp_value = isset($_SESSION["app_pay_frequency_1"]) ? $_SESSION["app_pay_frequency_1"] : "Unknown";  echo "$temp_value";?>";
                        let payment_frequency = convert_to_payfreq_code(tmp);
                        let tmp1 = <?php echo $_SESSION["app_approved_apr"]; ?>;

                        let data2 = {
                            "state_cd": state_cd,
                            "approved_amt": approved_amt,
                            "pay_freq": payment_frequency,
                            "contract_apr": tmp1,
                            "mm_application_nbr": app_nbr
                        };
                        (async function () {
                            let result;
                            try {
                                result = await jQuery.ajax({
                                    url: "<?php echo baseUrl(); ?>/wp-content/themes/moneymartdirect/include/get_pricing_values.php",
                                    type: 'POST',
                                    dataType: "text json",
                                    data: data2
                                });
                                return result;
                            }
                            catch (error) {
                                jQuery("#app_error").show();
                                jQuery("#overlay").hide();
                            }
                        })().then((data) => {
                            lookup = data.pricing_values;
                            replace_dropdown_values("input_17_1", display_values);
                            display_payment();
                            let select_object = document.getElementById("input_17_1");
                            select_object.onchange = function () {
                                display_payment();
                            };
                            jQuery('#app_approved').show();
                            jQuery('#overlay').hide();
                        });
                    },
                    error: function (xhr, status, errorThrown) {
                        jQuery('#app_error').show();
                        jQuery('#overlay').hide();
                    }
                });
            </script>
            <script type="text/javascript">    let x = document.getElementById('app_approved');
                x.style.display = 'block';</script>
            <?php
        } else if ($uw_decision == "Declined") {
            error_log("Mark: In Application-Processing:  starting IF section, second Else If\n");
            mm_unset_application_session();

            ?>
            <script type="text/javascript">
                let x = document.getElementById('app_declined');
                x.style.display = 'block';</script>
            <?php
        } else {
            ?>
            <script type="text/javascript">    var x = document.getElementById('app_error');
                x.style.display = 'block';</script>
            <?php
        }

    }
    ?>
    <?php
}
?>
<!--<script type="text/javascript">display_payment();</script>-->

<?php

get_footer();
?>



