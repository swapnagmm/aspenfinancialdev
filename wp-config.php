<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mmdtcusr_dev');

/** MySQL database username */
// mark: this is only temporary
//define('DB_USER', 'mmdtcusr_devadmin');
//define('DB_USER', 'mmdtcusr_temp');
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'bFy{;:-Z;9&Lv5CK*E/%{ZM<Azy7RQ]L N{rP+ZAdCw24;C9$@lBgq45T54&:6=-');
define('SECURE_AUTH_KEY',  '2-WOt<wPV,SiSh}8X)3T$I@!=dq,!oSgY%Na^U!<^%b^`?`JjeN-~9.iJwKvs/{)');
define('LOGGED_IN_KEY',    '@/ H30YWlff|nog)R$E$+Iaz;eDA@8r,bWk&*PIgb`g<*c$&kU+$gsoEsXNt|QsE');
define('NONCE_KEY',        'EQ0,V,YMwf,toM42bq3:xvQ_-Ru|fy`~7&0V1HRdem7=UOc/Rms^p%PIA.a-0.=;');
define('AUTH_SALT',        'cLG3z PT<=S2Dj;;Wl9}!F kZ%v=$yH3Xu8+d|wU+J}]C(XIe5)J=X%w1SvsDeRD');
define('SECURE_AUTH_SALT', 'N@ww*V2?rTh.o$y|97)C>>%4/{ig*8MFo( 5&~RIp(7jpN0In>Q5$G+NMBR23ab*');
define('LOGGED_IN_SALT',   '/{#|9#vn7{y&LYUE^P&nJ~#rX!xn>w?HW}Fmmz,v4FS3EA`zP0tJI]xk&zM;x7-+');
define('NONCE_SALT',       'H/62|Y{E65rX+wmB.+MuC}f+{h]-h%PnlCjgC2yVI]1!`YwFD;=]o-DRb-Qht.Ws');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
 
//error_reporting(E_ALL); ini_set('display_errors', 1);
 
//define('WP_DEBUG', false);
define('WP_DEBUG', true);


// Enable Debug logging to the /wp-content/debug.log file
//define( 'WP_DEBUG_LOG', false );
define( 'WP_DEBUG_LOG', true );
//define( 'WP_HOME', 'https://dev.moneymartdirect.com')
//define( 'WP_SITEURL', 'https://dev.moneymartdirect.com')

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
