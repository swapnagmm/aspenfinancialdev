<?php
/*
Template Name: CUSTOMER ACCOUNT
 */

get_header();
?>
<div class="accountPage">
    <?php
    error_log("mark: in accounttemplate.php\n");
    // Check if user is logged in, if not present login form and error handling
    if (!is_user_logged_in()) {
        $redirect_url = home_url('member-login');
        wp_redirect($redirect_url);
        exit;
    } else {

    //Atempt to restore a session
    global $current_user;
    $user_info = wp_get_current_user();
    $email_address = $user_info->user_email;
    //echo "EMAIL: $email_address<br>";
    mm_restore_session($email_address);

    //The customer is logged in.  Display a Welcome header message "Welcome First_name Last_Name".  Almost like a banner

    //Check to see if there ia an action passed as part of the query string i.e. aspenfinancialdirect.com/my-account?action=APPHISTORY
    if (isset($_GET["action"])) {
        //If the URL passed a query parameter of action then it will be used to determine what content to display

        //Display a button/link that navigates back to the home my account page

        //Get the query string value for action.  NEED TO ADD CLEANSING TO PREVENT CODE INJECTION
        $action = $_GET["action"];
        if ($action == "APPHISTORY") {
            //Insert code here to get the application history and display the content.  Debbie to define content, Jason to provide data routine, Bent to output it.
        } else if ($action == "LOANHISTORY") {
            //Insert code here to get the loan history and display the content.  Debbie to define content, Jason to provide data routine, Ben to output it.

        } else if ($action == "CUSTSETTINGS") {
            //Insert code here to display the form for the customer to have the ability to update their personal settings.

        } else if ($action == "MAKEPMT") {
            //The customer has selected to make a payment so we need to display the payment options portal
        } else {
            //Received an action that we didn't expect.  Should only happen if the user manually changed the value.
            //We can either disply a message stating the action doesn't exists or just display the base my account page content
        }
    } else {
    //No action is currently selected so therefore get the account status and determine what available actions to show
    $account_status_arr = mm_get_account_status($email_address);
    // print_r($account_status_arr);


    if ($account_status_arr["return_value"] > 0) {
        //There was an issue getting the account status.  Log a message and display an error message
        $return_message = $account_status_arr["return_message"];
        mm_log_error('my account page', "Unable to get account status.  Error is: $return_message");
        echo "There was an error retrieving information about your account.  Please check back later.  If the problem persists, please reach out to us through our Contact Us page.";
    } else {
    $account_status = $account_status_arr["account_status"];
    $mm_account_nbr = $account_status_arr["account_nbr"];
    $first_name = $account_status_arr["first_name"];
    $last_name = $account_status_arr["last_name"];
    $loan_status = $account_status_arr["loan_status"];
    $loan_substatus = $account_status_arr['loan_substatus'];
    $app_state = $account_status_arr["application_state"];
    echo $loan_nbr = $account_status_arr["loan_nbr"];

    //Code changes for story 315 sprint 6 Application Statuses eligible to Reapply
    //get most recent appln no from db
    $customer_recent_application_arr = mm_get_customer_recent_application($mm_account_nbr);
    /*echo "<pre>";
    print_r($customer_recent_application_arr);
    echo "</pre>";*/
    $custonmer_recent_loan_nbr = $customer_recent_application_arr['results'][0]['loan_nbr'];
    $custonmer_recent_app_status = $customer_recent_application_arr['results'][0]['application_status'];
    $custonmer_recent_app_substatus = $customer_recent_application_arr['results'][0]['application_sub_status'];
    $app_status = $account_status_arr["application_status"];
    //

    if ($custonmer_recent_loan_nbr != $loan_nbr && $custonmer_recent_app_status == '7' && $custonmer_recent_app_substatus == '1') {
        echo "3";
        $post_id = 1249; //Account Page Status Active - Pending Funding
        $queried_post = get_post($post_id);
        $content = $queried_post->post_content;
        $content = apply_filters('the_content', $content);
        $content = str_replace(']]>', ']]&gt;', $content);
        echo $content;

    }
    else if ($custonmer_recent_loan_nbr == $loan_nbr && ( ($account_status == "IDLE" && $app_status != 7) || $app_status == 8 || $app_status == 10 || $app_status == 11 || ($app_status == 7 && $loan_status == 'PAID OFF' && ($loan_substatus == 'Paid Off' || $loan_substatus == 'Funding Returned' || $loan_substatus == 'Rescinded')))) {
        //The account is idle so therefore show a link/button to display the apply now feature.  The url to the form page should prepopulate as much of the data as possible.
        $post_id = 1047;
        $queried_post = get_post($post_id);
        $content = $queried_post->post_content;
        $content = apply_filters('the_content', $content);
        $content = str_replace(']]>', ']]&gt;', $content);

        //code changes made by Swapna on 6th Aug For story point 315 logic haandled for declined applications as it was storing draft in session uw decision, we had to reset it.
        if($app_status == 8){
            if(isset($_SESSION["app_uw_decision"])){
                unset($_SESSION["app_uw_decision"]);
            }
        }
        echo $content;
    } else if ($account_status == "ACTIVEAPP") {
        //The account has an active account so depending on the status of the application display a resume action link/button
        //Get the application status of the active application

        if ($app_status == 1) {
            //The application is considered in draft - Should never happen, but if it does then display a link/button to take the customer to the application-processing page
            $post_id = 966;
            $queried_post = get_post($post_id);
            $content = $queried_post->post_content;
            $content = apply_filters('the_content', $content);
            $content = str_replace(']]>', ']]&gt;', $content);
            echo $content;
        } else if ($app_status == 2) {
            //The customer is approved pending acceptance so display a link to take the customer to the application-processing page
            $post_id = 1019;
            $queried_post = get_post($post_id);
            $content = $queried_post->post_content;
            $content = apply_filters('the_content', $content);
            $content = str_replace(']]>', ']]&gt;', $content);
            echo $content;
        } else if ($app_status == 3) {
            //The customer has accepted the requested amount and deposit method.  Display a resume app link/button that takes the customer to the verification-processing page
            $post_id = 1029;
            $queried_post = get_post($post_id);
            $content = $queried_post->post_content;
            $content = apply_filters('the_content', $content);
            $content = str_replace(']]>', ']]&gt;', $content);
            echo $content;
        } else if ($app_status == 4) {
            //The customer has been approved pending customer documents.  Display the message for them to send in their documents
            if ($app_state == 'IL') {
                $post_id = 2221;
                if (SITE_URI_PREFIX == '') {
                    // prod
                    $post_id = 2215;
                }
            } else {
                $post_id = 1031;
            }
            $queried_post = get_post($post_id);
            $content = $queried_post->post_content;
            $content = apply_filters('the_content', $content);
            $content = str_replace(']]>', ']]&gt;', $content);
            echo $content;
        } else if ($app_status == 5) {
            //The customer has been approved pending agent review.  Display the message that we have everything we need and are working on verifying their information.
            $post_id = 1037;
            $queried_post = get_post($post_id);
            $content = $queried_post->post_content;
            $content = apply_filters('the_content', $content);
            $content = str_replace(']]>', ']]&gt;', $content);
            echo $content;
        } else if ($app_status == 6) {
            //The customer is approved pending Customer Loan Documents. The resume application button/link should redirect them to the verification-processing page
            $post_id = 1039;
            $queried_post = get_post($post_id);
            $content = $queried_post->post_content;
            $content = apply_filters('the_content', $content);
            $content = str_replace(']]>', ']]&gt;', $content);
            echo $content;
        } else if ($app_status == 9) {
            //The customer has an application that is in stuck status.  Display the error message that we use through the application process.
            $post_id = 968;
            $queried_post = get_post($post_id);
            $content = $queried_post->post_content;
            $content = apply_filters('the_content', $content);
            $content = str_replace(']]>', ']]&gt;', $content);
            echo $content;
        } else {
            //There shouldn't be any other application statuses that appear when the customer status is ACTIVEAPP.  Capture them as errors here and display the error messaage
            mm_log_error('my account page', "customer status is ACTIVEAPP and received unexpected application status $app_status for account $mm_account_nbr");
            $post_id = 968;
            $queried_post = get_post($post_id);
            $content = $queried_post->post_content;
            $content = apply_filters('the_content', $content);
            $content = str_replace(']]>', ']]&gt;', $content);
            echo $content;

        }
    } else if ($account_status == "ACTIVELOAN") {
    // echo "2";
    //The account has an active loan so depending on the status display information about the loan as well the ability to make a payment
    $loan_status = $account_status_arr["loan_status"];
    if ($loan_status == "PENDING FUND") {
        //echo "3";
        $post_id = 1249; //Account Page Status Active - Pending Funding
        $queried_post = get_post($post_id);
        $content = $queried_post->post_content;
        $content = apply_filters('the_content', $content);
        $content = str_replace(']]>', ']]&gt;', $content);
        echo $content;

    } else if ($loan_status == "ACTIVE") {
    //echo "Your loan is currently active.<br>";
    ?>
    <!-- welcome -->
    <script type="text/javascript">
        /*document.getElementById('download').addEventListener('click', function () {
        var content = document.getElementById('content').value;
        var request = new XMLHttpRequest();
        request.open('POST', '../server/', true);
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        request.responseType = 'blob';

        request.onload = function() {
          // Only handle status code 200
          if(request.status === 200) {
            // Try to find out the filename from the content disposition `filename` value
            var disposition = request.getResponseHeader('content-disposition');
            var matches = /"([^"]*)"/.exec(disposition);
            var filename = (matches != null && matches[1] ? matches[1] : 'file.pdf');

            // The actual download
            var blob = new Blob([request.response], { type: 'application/pdf' });
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = filename;

            document.body.appendChild(link);

            link.click();

            document.body.removeChild(link);
          }

          // some error handling should be done here...
        };

        request.send('content=' + content);
      });*/

        function download_agreement(account_no,loan_no){
            var data = {
                "account_nbr": account_no,
                "loan_nbr": loan_no
            };
            jQuery.ajax({
                url: "<?php echo baseUrl(); ?>/wp-content/themes/moneymartdirect/include/download_agreement.php",
                type: "POST",
                data: data,
                //content-type: 'application/json',
                dataType: "text json",
                success: function (data) {
                    // return_value = data.return_value;

                },
                error: function (xhr) {
                    //jQuery("#app_error").show();
                    //jQuery("#overlay").hide();
                }
            });
        }
    </script>
    <div class="loanSummaryContainer">
        <div class="loanSummaryWelcome">Welcome, <span
                    class="customerName"><!-- customer name --><?php echo "$first_name"; ?></span></div>
        <ul class="loanDataContainer">
            <div class="loadDataContainerLabel">Loan Information</div>
            <li class="loanDataItem">
                <div class="loanDataLabel">Status:</div>
                <div class="loanData">
                    <?php
                    $days_past_due = $account_status_arr["days_past_due"];
                    if ($days_past_due > 0) {
                        echo "<span class='pastDue'>Past Due</span>";
                    } else {
                        echo "<span class='current'>Current</span>";
                    }
                    ?>
                </div>
            </li>
            <li class="loanDataItem">
                <div class="loanDataLabel">Next payment:</div>
                <div class="loanData">
                    <div class="loanPaymentDate">
                        <!-- next payment date -->
                        <?php
                        $next_payment_dt = $account_status_arr["next_payment_dt"];
                        echo "$next_payment_dt";
                        ?>
                    </div>
                    <div class="loanPaymentAmt">
                        <!-- next payment amount -->
                        <?php
                        $next_payment_amt = $account_status_arr["next_payment_amt"];
                        setlocale(LC_MONETARY, 'en_US');
                        $next_payment_amt = money_format('%.2n', $account_status_arr["next_payment_amt"]);
                        echo "$next_payment_amt";
                        ?>
                    </div>
                </div>
            </li>
            <li class="loanDataItem">
                <div class="loanDataLabel">Origination:</div>
                <div class="loanData">
                    <!-- origination date -->
                    <?php
                    $contract_dt = $account_status_arr["contract_dt"];
                    echo "$contract_dt";
                    ?>
                </div>
            </li>
            <li class="loanDataItem">
                <div class="loanDataLabel">Maturity:</div>
                <div class="loanData">
                    <!-- maturity date -->
                    <?php
                    $maturity_dt = $account_status_arr["maturity_dt"];
                    echo "$maturity_dt";
                    ?>
                </div>
            </li>
            <li class="loanDataItem">
                <div class="loanDataLabel">Balance:</div>
                <div class="loanData">
                    <!-- balance -->
                    <?php
                    setlocale(LC_MONETARY, 'en_US');
                    $principal_balance = money_format('%.2n', $account_status_arr["principal_balance"]);
                    echo "$principal_balance";
                    ?>
                </div>
            </li>
            <li class="loanDataItem">
                <div class="loanDataLabel">Loan #:</div>
                <div class="loanData">
                    <!-- loan number here -->
                    <?php
                    $loan_nbr = $account_status_arr["loan_nbr"];
                    echo "$loan_nbr";
                    ?>
                </div>
            </li>
            <li class="loanDataItem">
                <div class="additionalContactInfo" style="text-align:center"><a target="_blank" href="<?php echo baseUrl(); ?>/wp-content/themes/moneymartdirect/include/download_agreement.php?loan_nbr=<?php echo base64_encode($loan_nbr); ?>&account_nbr=<?php echo base64_encode($mm_account_nbr);?>">View Loan Agreement</a></div>

            </li>
        </ul>
        <div class="additionalContactInfo">
            <p>To request your payoff amount, to make an unscheduled payment or for other account or payment questions,
                please contact <a href="mailto:support@aspenfinancialdirect.com">support@aspenfinancialdirect.com</a> or
                call your Loan Specialist at <a href="tel:(877) 293-2987">(877) 293-2987</a></p>
        </div>

        <?php
        /* onclick="download_agreement('<?php echo $mm_account_nbr; ?>','<?php echo $account_status_arr["loan_nbr"]; ?>')"*/
        }

        } else if ($account_status == "CHARGEDOFF") {
            $post_id = 1257; //Account Page Status - Charged Off
            $queried_post = get_post($post_id);
            $content = $queried_post->post_content;
            $content = apply_filters('the_content', $content);
            $content = str_replace(']]>', ']]&gt;', $content);
            echo $content;

        }
        //Display links/buttons for the functionality that is always available regardless of account and application status.  I believe we landed on application history, loan history and customer settings

        }
        }
        }
        ?>
    </div>
    <?php
    get_footer();
    ?>



