<?php
/* 
 * file: UWDecision.php
 * 
 * model to work with mm_uw_decision table data
 */

class UWDecision
{
    public function __construct()
    {
        require_once 'mm_system_config.php';
        $conn = mm_get_db_connection();
        $this->conn = $conn;
        $this->table = "mm_uw_decision";
    }
    
    /**
     * function get_all_records
     * @desc get all records, indexed by decision_nbr
     * @return array
     */
    public function get_all_records()
    {
        $records = array();
        $query = "SELECT * FROM " . $this->table;
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $rows = $stmt->get_result();
        
        while ($row = $rows->fetch_assoc())
        {
            $records[$row['decision_nbr']] = $row;
        }
        
        $this->close_db_conn();
        
        return $records;
    }

    /**
     * function get_fico
     * @desc get the fico score for an application number. NOTE: there may be multiple records for an application number! (is this a testing issue?
     * @param int $app_num
     * @return array
     */
    public function get_fico($app_num)
    {
        $ret_val = array();
        $query = "SELECT fico FROM " . $this->table . " WHERE application_nbr = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bind_param('i', $app_num);
        $stmt->execute();
        $rows = $stmt->get_result();
        $ret_val['count'] = $rows->num_rows;
        $ret_val['records'] = array();
        
        while ($row = $rows->fetch_assoc())
        {
            $ret_val['records'][] = $row;
        }
        
        $this->close_db_conn();

        return $ret_val;
    }


    /**
     * function get_flag_clarity_tu
     * @desc get the is_clarity_tu value. NOTE: the string will need to be exploded on ","
     * @param int $app_num
     * @return array
     */
    public function get_flag_clarity_tu($app_num)
    {
        $ret_val = array();
        $query = "SELECT is_clarity_tu FROM " . $this->table . " WHERE application_nbr = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bind_param('i', $app_num);
        $stmt->execute();
        $rows = $stmt->get_result();
        $ret_val['count'] = $rows->num_rows;
        $ret_val['records'] = array();

        while ($row = $rows->fetch_assoc())
        {
            $ret_val['records'][] = $row;
        }
        
        $this->close_db_conn();

        return $ret_val;
    }
    
    
    /**
     * function get_decision_details
     * @desc get an individual record by decision_nbr. Should always get only 1 record (decision_nbr is primary key)
     * @param int $decision_num
     * @return array
     */
    public function get_decision_details($decision_num)
    {
        $ret_val = array();
        $query = "SELECT * FROM " . $this->table . " WHERE decision_nbr = ? LIMIT 1";
        $stmt = $this->conn->prepare($query);
        $stmt->bind_param('i', $decision_num);
        $stmt->execute();
        $rows = $stmt->get_result();
        $ret_val['count'] = $rows->num_rows;
        $ret_val['records'] = array();

        while ($row = $rows->fetch_assoc())
        {
            $ret_val['records'][] = $row;
        }
        
        $this->close_db_conn();

        return $ret_val;
    }
    
    public function check_is_organic_customer($app_num)
    {
        $is_organic = false;
        // because of duplicates, also look for min decision_nbr
        $query = "SELECT MIN(decision_nbr) AS decision_nbr, segment 
                  FROM  " . $this->table . " 
                  WHERE application_nbr=?
                      AND segment='New Customer - Organic' 
                  GROUP BY application_nbr";
        $stmt = $this->conn->prepare($query);
        $stmt->bind_param('i', $app_num);
        $stmt->execute();
        $rows = $stmt->get_result();
        $num_rows = $rows->num_rows;
        
        if  ($num_rows > 0)
        {
            $is_organic = true;
        }

        $this->close_db_conn();

        return $is_organic;
    }


/*
 *         $data = array(
            'orig_appid'                        => array('val' => $orig_appid, 'data_type'                          => 'i'),
            'app_status_cd'                     => array('val' => $app_status_cd, 'data_type'                       => 's'),
            'app_status_desc'                   => array('val' => $app_status_desc, 'data_type'                     => 's'),
            'approved_amt'                      => array('val' => $approved_amt, 'data_type'                        => 'd'),
            'is_approved'                       => array('val' => $is_approved, 'data_type'                         => 's'),
            'product'                           => array('val' => $product, 'data_type'                             => 's'),
            'application_nbr'                   => array('val' => $reference_id, 'data_type'                        => 'i'),
            'reject_reason_1'                   => array('val' => $reject_reason_1, 'data_type'                     => 's'),
            'reject_reason_2'                   => array('val' => $reject_reason_2, 'data_type'                     => 's'),
            'reject_reason_3'                   => array('val' => $reject_reason_3, 'data_type'                     => 's'),
            'reject_reason_4'                   => array('val' => $reject_reason_4, 'data_type'                     => 's'),
            'reject_reason_cd'                  => array('val' => $reject_reason_cd, 'data_type'                    => 'i'),
            'segment'                           => array('val' => $segment, 'data_type'                             => 's'),
            'ssn'                               => array('val' => $ssn, 'data_type'                                 => 's'),
            'strategy'                          => array('val' => $strategy, 'data_type'                            => 's'),
            'transaction_dt'                    => array('val' => $transaction_dt, 'data_type'                      => 's'),
            'create_dt'                         => array('val' => $create_dt, 'data_type'                           => 's'),
            'transaction_id'                    => array('val' => $transaction_id, 'data_type'                      => 'i'),
            'approved_apr'                      => array('val' => $approved_apr, 'data_type'                        => 'd'),
            'approved_term'                     => array('val' => $approved_term, 'data_type'                       => 'i'),
            'clr_action'                        => array('val' => $clr_action, 'data_type'                          => 's'),
            'clr_exp_auto_most_neg_last_12'     => array('val' => $clr_exp_auto_most_neg_last_12, 'data_type'       => 'i'),
            'clr_exp_bankcard_total_bal_last_6' => array('val' => $clr_exp_bankcard_total_bal_last_6, 'data_type'   => 'd'),
            'clr_exp_bankcard_most_neg_last_6'  => array('val' => $clr_exp_bankcard_most_neg_last_6, 'data_type'    => 'i'),
            'clr_exp_mtg_most_neg_last_6'       => array('val' => $clr_exp_mtg_most_neg_last_6, 'data_type'         => 'i'),
            'clr_exp_cbb_num_ssns'              => array('val' => $clr_ccb_num_ssns, 'data_type'                    => 'i'),
            'clr_cca_days_since_last_inq'       => array('val' => $clr_cca_days_since_last_inq, 'data_type'         => 'i'),
            'clr_cca_num_total_loans'           => array('val' => $clr_cca_num_total_loans, 'data_type'             => 'i'),
            'clr_cca_days_since_last_loan_open' => array('val' => $clr_cca_days_since_last_loan_open, 'data_type'   => 'i'),
            'clr_cca_num_loans_current'         => array('val' => $clr_cca_num_loans_current, 'data_type'           => 'i'),
            'clr_cca_amt_loans_current'         => array('val' => $clr_cca_amt_loans_current, 'data_type'           => 'd'),
            'clr_cca_amt_loan_past_due'         => array('val' => $clr_cca_amt_loans_past_due, 'data_type'          => 'd'),
            'clr_cca_num_loans_past_due'        => array('val' => $clr_cca_num_loans_past_due, 'data_type'          => 'i'),
            'clr_cca_num_loans_paid_off'        => array('val' => $clr_cca_num_loans_paid_off, 'data_type'          => 'i'),
            'clr_cca_days_since_last_loan_paid' => array('val' => $clr_cca_days_since_last_loan_paid, 'data_type'   => 'i'),
            'clr_cca_days_since_last_loan_co'   => array('val' => $clr_cca_days_since_last_loan_co, 'data_type'     => 'i'),
            'clr_cca_worst_payment_rating'      => array('val' => $clr_cca_worst_payment_rating, 'data_type'        => 's'),
            'clr_cca_active_duty_status'        => array('val' => $clr_cca_active_duty_status, 'data_type'          => 's'),
            'fico'                              => array('val' => $fico, 'data_type'                                => 's'),
            'is_clarity_tu'                     => array('val' => $clarity_tu_flag, 'data_type'                     => 's')
        );

 */
    /**
     * function create_record
     * @desc inserts a new record. NOTE: any time the table structure is changed, this method probably needs to be changed as well
     * @param array $data
     * @return array
     * 
     * TODO: finish this method!!!!!!!!!!!!!
     */
    public function create_record($data)
    {
        $ret_val = array();
        $param_types = "";
        // build a string of data types like issdssissssisssssidisidiiiiiiiddiiiissss
        foreach ($data as $key => $value)
        {
            $param_types .= $value['data_type'];
        }
        
//        $query = "INSERT INTO " . $this->table . "(
//                      orig_appid, 
//                      app_status_cd, 
//                      app_status_desc, 
//                      approved_amt, 
//                      is_approved, 
//                      product,
//                      application_nbr, 
//                      reject_reason_1, 
//                      reject_reason_2, 
//                      reject_reason_3, 
//                      reject_reason_4, 
//                      reject_reason_cd,
//                      segment, 
//                      ssn, 
//                      strategy, 
//                      transaction_dt, 
//                      create_dt, 
//                      transaction_id, 
//                      approved_apr, 
//                      approved_term, 
//                      clr_action,
//                      clr_exp_auto_most_neg_last_12, 
//                      clr_exp_bankcard_total_bal_last_6, 
//                      clr_exp_bankcard_most_neg_last_6, 
//                      clr_exp_mtg_most_neg_last_6,
//                      clr_exp_cbb_num_ssns, 
//                      clr_cca_days_since_last_inq, 
//                      clr_cca_num_total_loans, 
//                      clr_cca_days_since_last_loan_open,
//                      clr_cca_num_loans_current, 
//                      clr_cca_amt_loans_current, 
//                      clr_cca_amt_loan_past_due, 
//                      clr_cca_num_loans_past_due,
//                      clr_cca_num_loans_paid_off, 
//                      clr_cca_days_since_last_loan_paid, 
//                      clr_cca_days_since_last_loan_co,
//                      clr_cca_worst_payment_rating, 
//                      clr_cca_active_duty_status, 
//                      fico, 
//                      is_clarity_tu
//                  )
//                  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
//        $stmt = $this->conn->prepare($query);
//        $stmt->execute();
//        $ret_val['decision_nbr'] = $stmt->insert_id;
//        
//        $this->close_db_conn();

        //return $ret_val;
    }













    
    private function close_db_conn()
    {
        if (is_resource($this->conn))
        {
            $this->conn->close();
        }

    }
    
}