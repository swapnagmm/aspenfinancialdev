<?php
$results_array = array();
$pdf_dir = 'PDFs';

echo "<pre>";

if ($handle = opendir($pdf_dir))
{
    while ( ($file = readdir($handle)) !== false)
    {
        if ($file != "." && $file != "..")
        {
            $results_array[] = $file;
        }
    }


    
    if (count($results_array) == 0)
    {
        echo "No pdf files to delete.";
    }
    else
    {
        print_r($results_array);
        
        foreach ($results_array as $val)
        {
            if (file_exists($pdf_dir . "/" . $val))
            {
                echo "About to delete " . $val . "...";
            }

            unlink($pdf_dir . "/" . $val);

            if (!file_exists($pdf_dir . "/" . $val))
            {
                echo $val . " has been deleted.<br>";
            }
            else
            {
                echo "Failed to delete " . $val . "<br>";
            }
        }
    }
    
    closedir($handle);
}
