<?php
/*
Template Name: LOAN PROCESSING W/SPIN
*/

get_header();
?>

<?php
require_once $_SERVER['DOCUMENT_ROOT'] . BASE_DIR . "/wp-content/themes/moneymartdirect/include/mm_system_config.php";

// Check if user is logged in 
if ( !is_user_logged_in() ){
	//$post_id = 646;
	$post_id = 923;
	$queried_post = get_post($post_id);
	//$title = $queried_post->post_title;
	//echo $title;
	$content = $queried_post->post_content;
	$content = apply_filters('the_content', $content);
	$content = str_replace(']]>', ']]&gt;', $content);
	echo $content;
	
}else{
     //Atempt to restore a session
    global $current_user;
      get_currentuserinfo();
    $email_address = $current_user->user_email;
    mm_restore_session($email_address);
    
    ?>
    
    <div id="overlay" align="center">
	        <div class="loaderContainer">
		        <div class="loader"> </div>
		        <p style="color:white"><b>Please wait while we process your application</b></p>
		        <p style="color:white"><b>This may take up to 60 seconds</b></p>
        	</div>
            </div>
   
   <div class="loanAgreement congratulations" id="app_complete" style="display:none">
			<?php
		    $post_id = 855;
			$queried_post = get_post($post_id);
			$content = $queried_post->post_content;
			$content = apply_filters('the_content', $content);
			$content = str_replace(']]>', ']]&gt;', $content);
			echo $content;
			?>	
		</div>
	    <div class="loanAgreement loanError" id="app_error" style="display:none">
			<?php
		    $post_id = 858;
			$queried_post = get_post($post_id);
			$content = $queried_post->post_content;
			$content = apply_filters('the_content', $content);
			$content = str_replace(']]>', ']]&gt;', $content);
			echo $content;
			?>	
		</div>
   <div class="resultContent approved" id="app_refresh_verif" style="display:none">
		<?php
		   // $postid = url_to_postid( $url );
			$post_id = 1521;
			$queried_post = get_post($post_id);
			$content = $queried_post->post_content;
			$content = apply_filters('the_content', $content);
			$content = str_replace(']]>', ']]&gt;', $content);
			echo $content;
		?>
		</div>
   
   
   <script type="text/javascript">
   <?php 
    $application_nbr=(isset($_SESSION['app_application_nbr']))?$_SESSION['app_application_nbr']:0;
    $_SESSION["TESTING APP NUM"] = $application_nbr;
    ?>
    
    
        var application_nbr = <?php echo "$application_nbr";?>;
        
        if(application_nbr == 0){
            jQuery('#app_refresh_verif').show();
              jQuery('#overlay').hide();
        }else{
    
        
            var data2 = {
                "application_nbr" : application_nbr
            }

            jQuery.ajax({
                url: "<?php echo baseUrl(); ?>/wp-content/themes/moneymartdirect/include/process_loan_booking.php",
                type: "POST",
                data: data2,
                //content-type: 'application/json',
                dataType: "text json",
                success: function(data) {
                    return_value = data.return_value;
                    if(return_value == 0){
                        jQuery("#app_complete").show();
                        <?php mm_unset_application_session();?>
                    }else{
                         jQuery("#app_error").show();
                    }
                    jQuery('#overlay').hide();
                },
                error: function(xhr,textStatus,errorThrown){
                 //alert("An error occured: " + xhr.status + " " + xhr.statusText + " " + xhr.responseText + textStatus);
                    jQuery("#app_error").show();
                    jQuery("#overlay").hide();
                            
                }
            });
        }
        </script>
<?php
}
?>

<?php
get_footer(); 
?>
	
	
	
