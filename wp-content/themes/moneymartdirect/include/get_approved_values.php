<?php

require "mm_middleware.php";

if (is_ajax()) {
    if (isset($_POST["state_cd"]) && !empty($_POST["state_cd"]) &&
        isset($_POST["approved_amt"]) && !empty($_POST["approved_amt"]) &&
        isset($_POST["mm_application_nbr"]) && !empty($_POST["mm_application_nbr"])) { //Checks if needed values are set
        $approved_amt = $_POST["approved_amt"];
        $state_cd = $_POST["state_cd"];
        $mm_application_nbr = $_POST["mm_application_nbr"];
        
        $application_data = mm_get_application_details($mm_application_nbr);
        //Get Application specific state details like gross income for Illinois
        //Code start - added by Swapna on july 18th 2018 for story 320
        if($application_data["app_data"]['approved_amt'] != '' && $application_data["app_data"]['state'] == 'IL'){
            $approved_amt = $application_data["app_data"]['approved_amt'];
        }
        
        $results = mm_get_approval_amt_display_values($state_cd, $approved_amt, $mm_application_nbr);
        $tmp = $results["return_value"];

        if ($tmp == 0)
        {
            header('Content-Type: application/json');
            echo json_encode($results);
        }
        else
        {
            error_log("mark: an error is being returned from get_approved_values to the AJAX caller: tmp: $tmp\n");
            header('HTTP/1.1 500 Internal Server');
            header('Content-Type: application/json; charset=UTF-8');
            die(json_encode(array('message' => "An Error occurred, check log.")));
        }
        //echo json_encode($results);
    }
}

//Function to check if the request is an AJAX request
function is_ajax() {
    return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}
