<?php

if (!defined('ENVIRONMENT'))
{
    if ($_SERVER['SERVER_NAME'] == "dev.moneymartdirect.com")
    {
        define('ENVIRONMENT', 'DEV');
    }
    elseif ($_SERVER['SERVER_NAME'] == "localhost")
    {
        define('ENVIRONMENT', 'LOCAL');
    }
    else
    {
        die("Internal Server Error: invalid ENVIRONMENT");
    }
}

/* ADD TO THE TOP */
define('MY_DEBUG_EMAIL', 'Tim.Poovey@MoneyMart.com'); // insert your own work email address if you like
define('BASE_DIR',       '/dev');                     // Each Dev project's "root" directory in C:\xampp\htdocs

define('DEBUG', 1);

// these should match database values in mm_lender table
define('SITE_NAME', 'Aspen Financial Direct');


// Replace the following with production define statements or add into PROD section below
if (ENVIRONMENT == "DEV")
{
    define('SITE_URI',          'dev.moneymartdirect.com');
    define('SITE_URI_PREFIX',   'dev.');
    define('SITE_HOME_PATH',    'dev.moneymartdirect.com');
    define('MM_DB_USER_NAME',   'mmdtcusr_devadmin');
    define('MM_DB_PASSWORD',    'Direct2Con!18');
    define('MM_DB_NAME',        "mmdtcusr_dev");
    define('MM_LOG_PATH',       "/home/mmdtcusr/dev.moneymartdirect.com/mm_logs/");
    define('MM_ACH_DOCS_PATH',  "/home/mmdtcusr/dev_documents/ach_documents/outbound_files/");
    define('MM_CUST_DOCS_PATH', "/home/mmdtcusr/dev_documents/customer_documents/");

    define('ERROR_MONITOR_ADDRESSES',        MY_DEBUG_EMAIL);
    define('ADMIN_EMAIL_ADDRESSES',          MY_DEBUG_EMAIL);
    define('AFTER_SUBMISSION_TEST_LIST',     MY_DEBUG_EMAIL);
    define('ACH_PROCESS_RETURNS_EMAIL_LIST', MY_DEBUG_EMAIL);
    define('EMAIL_SUBJECT',                  'DEV TEST: ');
}
elseif (ENVIRONMENT == "LOCAL")
{
    define('SITE_URI',          'localhost/dev');
    define('SITE_URI_PREFIX',   'dev.');
    define('SITE_HOME_PATH',    'C:\xampp\htdocs\dev');
    define('MM_DB_USER_NAME',   'root');
    define('MM_DB_PASSWORD',    '');
    define('MM_DB_NAME',        "mmdtcusr_dev");
    define('MM_LOG_PATH',       "C:/xampp/htdocs/dev/mm_logs/");
    define('MM_ACH_DOCS_PATH',  "C:/xampp/htdocs/dev_documents/ach_documents/outbound_files/");
    define('MM_CUST_DOCS_PATH', "C:/xampp/htdocs/dev_documents/customer_documents/");

    define('ERROR_MONITOR_ADDRESSES',        MY_DEBUG_EMAIL);
    define('ADMIN_EMAIL_ADDRESSES',          MY_DEBUG_EMAIL);
    define('AFTER_SUBMISSION_TEST_LIST',     MY_DEBUG_EMAIL);
    define('ACH_PROCESS_RETURNS_EMAIL_LIST', MY_DEBUG_EMAIL);
    define('E MAIL_SUBJECT',                 'DEV TEST: ');
} elseif (ENVIRONMENT == "PROD") {
    // Drop in production "define" statements
    define('SITE_URI',          '');
    define('SITE_URI_PREFIX',   '');
    define('SITE_HOME_PATH',    '');

    define('MM_DB_USER_NAME',   '');
    define('MM_DB_PASSWORD',    '');
    define('MM_DB_NAME',        "");
    define('MM_LOG_PATH',       "");
    define('MM_ACH_DOCS_PATH',  "");
    define('MM_CUST_DOCS_PATH', "");

    define('ERROR_MONITOR_ADDRESSES',        'jason.bumgarner@moneymart.com, mark.bench@moneymart.com');
    define('ADMIN_EMAIL_ADDRESSES',          'jason.bumgarner@moneymart.com, rex.northen@moneymart.com');
    define('AFTER_SUBMISSION_TEST_LIST',     'jason.bumgarner@moneymart.com, rex.northen@moneymart.com, mark.bench@moneymart.com');
    define('ACH_PROCESS_RETURNS_EMAIL_LIST', 'jason.bumgarner@moneymart.com, jessica.esparza@moneymart.com, brian.miller@moneymart.com,chantel.jones@moneymart.com');
    define('EMAIL_SUBJECT',                  '');
}

define('SUPPORT_PHONE', '(877) 293-2987');
define('SUPPORT_FAX', '(888) 252-2298');
define('SUPPORT_EMAIL', 'support@aspenfinancialdirect.com');
define('SUPPORT_ADDRESS', 'PO Box 882533');
define('SUPPORT_CITY', 'Dallas');
define('SUPPORT_STATE', 'TX');
define('SUPPORT_ZIP', '75380');
define('PHYSICAL_ADDRESS', '74 E. Swedesford Road, Suite 150');
define('PHYSICAL_CITY', 'Malvern');
define('PHYSICAL_STATE', 'PA');
define('PHYSICAL_ZIP', '19355');
define('APPROVED_APRS', array(116.13, 132.73, 213.27, 266.34, 96.73 ));
define('FADIR', 'fa_dev_ftp');
