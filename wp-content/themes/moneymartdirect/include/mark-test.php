<?php

$arr = array ( 'DaysSinceDecline' => '10000',
'IsReturningCustomer' => 'N',
'HasActiveLoan' => 'N',
'HasChargedOffLoan' => 'N',
'CollectionDifficulty' => 'N',
'TotalMonthlyIncome' => '1820',
'UserName' => 'vivacetest1',
'Password' => 'Password123',
'ApplicationSource' => 'TestSource',
'AuthToken' => '123ABCDEF',
'PortfolioID' => '1',
'LoanReason' => 'Personal Loan',
'FirstName' => 'Robyn',
'MiddleName' => '',
'LastName' => 'Hayes',
'CellPhone' => '3235966211',
'Email' => 'Rehayes52791@gmail.com',
'DOB' => '05271991',
'SSN' => '620466075',
'HomeAddress1' => '7406 E 109 Terrace',
'HomeAddress2' => '',
'HomeCity' => 'Kansas City',
'HomeState' => 'MO',
'HomeZip' => '64134',
'HomePhone' => '3235088163',
'HomeType' => 'Rent',
'EmploymentStatus' => 'Employed',
'EmployerName' => 'Ronnies child care',
'PayFrequency' => 'WKLY',
'OtherIncomeSource' => 'OTHR',
'MonthlyHousePayment' => '$800 - $999',
'NextPayDate1' => '05112018',
'NextPayDate2' => '01012017',
'DirectDeposit' => 'true',
'ReferenceID' => '10048',
'PerformDataValidation' => 'true',
'BankAccountType' => 'Checking',
'BankAccountNumber' => '002780420443',
'BankABA' => '121000358',
'MonthlyIncome' => '1820',
'OtherIncome' => '0',
'CustomFields' => [["Name" => "AffordabilityLineAmount","Value" => "10000","Type" => "int"],["Name" => "FormerAmount","Value" => "0","Type" => "int"]]);
$json = json_encode($arr);

echo $json . PHP_EOL;

$arr2 = array_map('htmlentities', $arr);
$json2 = json_encode($arr2);

echo $json2 . PHP_EOL;

$arr3 = array_map('utf8_encode', $arr);
$json3 = json_encode($arr3);

echo $json3;
