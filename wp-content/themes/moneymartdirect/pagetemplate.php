<?php
/*
Template Name: Page Template
*/

get_header();
?>

<?php
 //   $current_user = wp_get_current_user();
 //   if ( !($current_user instanceof WP_User) ){
//	    return;
 //   }
        
    /**
     * @example Safe usage: $current_user = wp_get_current_user();
     * if ( !($current_user instanceof WP_User) )
     *     return;
     */
  //  echo 'Username: ' . $current_user->user_login . '<br />';
  //  echo 'User email: ' . $current_user->user_email . '<br />';
  //  echo 'User first name: ' . $current_user->user_firstname . '<br />';
  //  echo 'User last name: ' . $current_user->user_lastname . '<br />';
  //  echo 'User display name: ' . $current_user->display_name . '<br />';
  //  echo 'User ID: ' . $current_user->ID . '<br />';
?>
<?php
	
	
// Check if user is logged in 
if ( !is_user_logged_in() ){
	echo "Sign in to see the content!";
	wp_login_form( array( 'echo' => true ) );
}else{
	
	//http://dev.firstfederalcredit.com/application-processing/?leadIDEntryID=1
	//http://dev.firstfederalcredit.com/application-processing/?leadIDEntryID=2
	//http://dev.firstfederalcredit.com/application-processing/?leadIDEntryID=3
	
	// Display application processing message
	?>
	<div class="resultContent processing">
	<?php
	    $post_id = 566;
		$queried_post = get_post($post_id);
		$content = $queried_post->post_content;
		$content = apply_filters('the_content', $content);
		$content = str_replace(']]>', ']]&gt;', $content);
		echo $content;
	?>
	</div>
	<?php
	$uw_results = process_application();
	$uw_decision = $uw_results["uw_decision"];
	if($uw_decision == "Approved"){
	    //Insert code here to display the congratulations message and the form for selecting loan amount and deposit method.
	    ?>
	    <script>
			jQuery('.resultContent.processing').remove();    
		</script>
		<div class="resultContent approved">
		<?php
			$post_id = 575;
			$queried_post = get_post($post_id);
			$content = $queried_post->post_content;
			$content = apply_filters('the_content', $content);
			$content = str_replace(']]>', ']]&gt;', $content);
			echo $content;
		?>
		</div>
		<?php
	}else if($uw_decision == "Declined"){
	    //Insert code here to display a generic decline message
	    ?>
	    <script>
			jQuery('.resultContent.processing').remove();    
		</script>
		<div class="resultContent declined">
		<?php
		    $post_id = 577;
			$queried_post = get_post($post_id);
			$content = $queried_post->post_content;
			$content = apply_filters('the_content', $content);
			$content = str_replace(']]>', ']]&gt;', $content);
			echo $content;
		?>
		</div>
		<?php
	}else{
		
	   //Insert code here to display a message saying we're sorry about the error
	    ?>
	    <script>
			jQuery('.resultContent.processing').remove();    
		</script>
		<div class="resultContent errorprocessing">
		<?php
		    $post_id = 579;
			$queried_post = get_post($post_id);
			$content = $queried_post->post_content;
			$content = apply_filters('the_content', $content);
			$content = str_replace(']]>', ']]&gt;', $content);
			echo $content;
		?>
		</div>
		<?php
	}
}
?>

<?php
get_footer(); 
?>
	
	
	