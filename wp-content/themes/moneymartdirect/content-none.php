<?php
/*
Template Name: No Results Template
*/

get_header();
?>


<div class="entry">
<!--If no results are found-->
	<h1><?php esc_html_e('No Results Found','Divi'); ?></h1>
	<p><?php esc_html_e('The page you requested could not be found.','Divi'); ?></p>
</div>
<!--End if no results are found-->


<?php
get_footer(); 
?>