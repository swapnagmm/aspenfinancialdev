CREATE TABLE `mm_securitization_log` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`loan_agreement_nbr` VARCHAR(50) NOT NULL,
	`new_value` VARCHAR(100) NOT NULL,
	`old_value` VARCHAR(100) NOT NULL,
	`create_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;




alter table mm_loan_agreement add fa_pending_file_path varchar(255);
