<?php
/*
Template Name: TEMP NEW APPLICATION
 */

get_header();
?>

<?php
//error_log("mark: in newapptemplate.php\n");
$account_status = '';
$application_status = '';
// Check if user is logged in, if logged in the make sure the customer is in a status to be able to fill out an application.

if (is_user_logged_in()) {
    $user_info = wp_get_current_user();
    $account_status_arr = mm_get_account_status($user_info->user_login);
    if ($account_status_arr["return_value"] > 0) {
        //There was an issue getting the account status.  Log a message and display an error message
        $return_message = $account_status_arr["return_message"];
        mm_log_error('my account page', "Unable to get account status.  Error is: $return_message");

    } else {
        $account_status = $account_status_arr["account_status"];
    }
}

if (isset($form)) {
    $current_page = rgpost('gform_source_page_number_' . $form['id']) ? rgpost('gform_source_page_number_' . $form['id']) : 1;
    ?><script>var current_state='<?php echo $_SESSION["app_state"] ?>';</script><?php
}

//error_log("mark: in newapptemplate.php. account_status=$account_status\n");
if ($account_status == 'ACTIVEAPP' || $account_status == "ACTIVELOAN" || $account_status == "CHARGEDOFF" || $account_status == "ERROR") {
    //Enter your code here for what to do when it’s empty*/
    //             $post_id = 1542;
    //             $queried_post = get_post($post_id);
    //             $content = $queried_post->post_content;
    //             $content = apply_filters('the_content', $content);
    //             $content = str_replace(']]>', ']]&gt;', $content);
    //             echo $content;
    //The customer is approved pending acceptance so display a link to take the customer to the application-processing page
    $post_id = 2149;
    $queried_post = get_post($post_id);
    $content = $queried_post->post_content;
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    echo $content;
} else {
    //The customer status is ok so display the application form
    gravity_form(14, true, true, true, '', true);
}

?>

<?php
get_footer();
?>



