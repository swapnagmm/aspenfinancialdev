

<?php




require "mm_middleware.php";

if (is_ajax()) {
  if (isset($_POST["application_nbr"]) && !empty($_POST["application_nbr"])) { //Checks if application_nbr value exists
        $application_nbr = $_POST["application_nbr"];
       // $lp_results = mm_process_loan_agreement($application_nbr);
        $lp_results = mm_schedule_loan_funding($application_nbr);
        if($lp_results["return_value"] == 0){
            	mm_unset_application_session();
            	mm_update_database_value("mm_application", 'application_status', 7, 'i', 'application_nbr', $application_nbr);
            	mm_update_database_value("mm_application", 'application_sub_status', 1, 'i', 'application_nbr', $application_nbr);
        }
        echo json_encode($lp_results);
  }
}

//Function to check if the request is an AJAX request
function is_ajax() {
  return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}

?>
