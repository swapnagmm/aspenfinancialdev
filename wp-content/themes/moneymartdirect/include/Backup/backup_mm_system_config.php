<?php

function baseUrl(){
    return sprintf("%s://%s",
        isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
        $_SERVER['SERVER_NAME']
    );
}

function mm_get_db_credentials(){
    $return_array = array();
    $return_array["db_server_name"] = "localhost";
    $return_array["db_user_name"] = "mmdtcusr_devadmin";
    $return_array["db_password"] = "Direct2Con!18";
    $return_array["db_name"] = "mmdtcusr_dev";

    return $return_array;
}

function mm_get_lp_credentials(){
    $return_array = array();
    $return_array["token"] = "Bearer f1c9d078d3828e7fbffcc8944cad2f00b93f109b";
    $return_array["tenant_id"] = "5200206";

    return $return_array;
}

function mm_get_lp_environment_portfolio(){
    //For test return the value 4 and for production return the value 5
    return "4";
}

function mm_get_pci_credentials(){
    $return_array = array();
    $return_array["username"] = "jbumgarner@firstfederalcredit.com";
    $return_array["password"] = "FFC2018q1!2";

    return $return_array;
}

function mm_get_ftp_details(){
    $return_array = array();
    $return_array["server"] = "moneymartdirect.com";
    $return_array["password"] = "gk3EQ3yJWaN7";
    $return_array["username"]  = "mm_web_documents_dev@moneymartdirect.com";
    $return_array["port"] = "21";

    return $return_array;
}

function mm_get_pdo_connection(){
    $credentials = mm_get_db_credentials();
    $db_server_name = $credentials["db_server_name"];
    $db_user_name = $credentials["db_user_name"];
    $db_password = $credentials["db_password"];
    $db_name = $credentials["db_name"];        $conn = new PDO("mysql:host=$db_server_name;dbname=$db_name", $db_user_name, $db_password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    return $conn;
}

function mm_get_db_connection(){
    $credentials = mm_get_db_credentials();
    $db_server_name = $credentials["db_server_name"];
    $db_user_name = $credentials["db_user_name"];
    $db_password = $credentials["db_password"];
    $db_name = $credentials["db_name"];
    $conn = new mysqli($db_server_name, $db_user_name, $db_password, $db_name);

    return $conn;
}

function get_lms_connection_details(){
}

function mm_get_log_path(){
    $log_path = "/home/mmdtcusr/dev.moneymartdirect.com/mm_logs/";
	return $log_path;
}

function mm_get_document_path(){
    $log_path = "/home/mmdtcusr/dev.moneymartdirect.com/mm_logs/";
	return $log_path;
}

function get_origination_system_connection_details(){
	$return_array = array();
	$header_array = array();
	$return_array["url"] = "http://kleverlend-dev-app.azurewebsites.net/api/";
	$return_array["UserName"] = "vivacetest1";
	$return_array["Password"] = "Password123";
	$return_array["ApplicationSource"] = 'TestSource';
	$return_array["AuthToken"] = '123ABCDEF';
    $return_array["PortfolioID"] = '1';
	$header_array = array("Content-type: application/json", "key: Authorization", "Authorization: Basic dGVzdDp0ZXN0", "Description: \"\"");
	$return_array["header_array"] = $header_array;

	return $return_array;
}

function mm_get_ach_server_details(){
    $return_array = array();
    $return_array["server"] = "transfer.greenbank.com";
    $return_array["username"] = "FirstFedCred";
    $return_array["password"] = "Ffc2017!";
    $return_array["port"] = "992";

    return $return_array;
}

function mm_get_ach_path(){
    return "/home/mmdtcusr/dev_documents/ach_documents/outbound_files/";
}

function mm_get_document_storage_path(){
    return "/home/mmdtcusr/dev_documents/customer_documents/";
}

function mm_create_cache() {
    $cache = new Memcached();
    $cache->addServer('127.0.0.1', 11211);
    return $cache;
}

function mm_set_cache_value($key, $value, $expiration=60) {
    mm_create_cache()->set($key, $value, $expiration);
}

function mm_get_cache_value($key) {
    return mm_create_cache()->get($key);
}

