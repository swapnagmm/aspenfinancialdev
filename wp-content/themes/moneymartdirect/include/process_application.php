<?php

require "mm_middleware.php";

if (is_ajax()) {
    if (isset($_POST["gf_lead_id"]) && !empty($_POST["gf_lead_id"])) { //Checks if gf_lead_id value exists
        $lead_id = $_POST["gf_lead_id"];
        $uw_results = process_application($lead_id);
        $tmp = $uw_results["return_value"];
        error_log("mark: in process_application from Ajax request: tmp: $tmp\n");
        if ($tmp == 0)
        {
            header('Content-Type: application/json');
            echo json_encode($uw_results);
        }
        else
        {
            header('HTTP/1.1 500 Internal Server');
            header('Content-Type: application/json; charset=UTF-8');
            die(json_encode(array('message' => $uw_results["return_message"])));
        }
        //header('Content-Type: application/json');
        //echo json_encode($uw_results);
    }
} else {
    $uw_results[0] = "ajax failed";
    echo json_encode($uw_results);
}

//Function to check if the request is an AJAX request
function is_ajax() {
    return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}

