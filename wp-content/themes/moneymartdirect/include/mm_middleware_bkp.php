<?php
date_default_timezone_set('America/Chicago');
require "mm_system_config.php";
require __DIR__ . "/vendor/autoload.php";
require "pdf.php";
error_reporting(E_ALL);
ini_set('display_errors', 0);
ini_set("include_path", '/home/mmdtcusr/php:' . ini_get("include_path"));
ini_set("include_path", '/home/mmdtcusr/php/phpseclib1:' . ini_get("include_path"));
include "phpseclib1/Net/SFTP.php";

use Simnang\LoanPro\LoanProSDK as LPSDK;
use \Simnang\Loanpro\Constants\ADDRESS as ADDRESS;
use \Simnang\Loanpro\Constants\BASE_ENTITY as BASE_ENTITY;
use \Simnang\LoanPro\Constants\CUSTOMERS as CUSTOMERS;
use \Simnang\LoanPro\Constants\LOAN as LOAN;
use \Simnang\LoanPro\Constants\LOAN_SETTINGS as LOAN_SETTINGS;
use \Simnang\LoanPro\Constants\LOAN_SETUP as LOAN_SETUP;
use \Simnang\LoanPro\Constants\LOAN_TRANSACTIONS as LOAN_TRANSACTIONS;
use \Simnang\Loanpro\Constants\PHONES as PHONES;
use \Simnang\Loanpro\Constants\PHONES\PHONES_TYPE__C as PHONES_TYPE_C;
use \Simnang\Loanpro\Constants\STATUS_ARCHIVE as STATUS_ARCHIVE;

function mm_insert_state_specific_WI($application_nbr, $specific_name, $blob_type, $blob) {
    //error_log_dump_associative_array("agreement_data:", $agreement_data);
    error_log("flow: mm_insert_state_specific_WI, application_nbr: $application_nbr, name: $specific_name, blob_type: $blob_type, blob: $blob\n");
    //error_log_dump_associative_array("agreement_data:", $agreement_data);
    $return_array = array();

    $sql_string = "insert into mm_state_specific_data (`application_nbr`, `state`, `name`, `blob_type`, `blob`)
                    values (?,?,?,?,?)";
    try {
        $conn = mm_get_pdo_connection();
        $stmt = $conn->prepare($sql_string);
        if($specific_name == "Illinois_Gross_Income"){
            $statename= 'IL';
        }
        if($specific_name == "Marital_Notice_Info"){
            $statename= 'WI';
        }
        $parms = [$application_nbr, $statename, $specific_name, $blob_type, $blob];
        $stmt->execute($parms);
        $loan_agreement_nbr = $conn->lastInsertId();

    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "$e";
        $return_array["sql_string"] = $sql_string;
        error_log("mark: error in sql: $sql_string\n");
        error_log("mark: error in sql: $e\n");
        return $return_array;
    }

    if (is_resource($conn)) {
        $conn = null;
    }
    
    //Need to add defaulting and handle errors
    $return_array["return_value"] = 0;

    return $return_array;
}

/************************************************************************************************
 * Function mm_get_lp_customer_data($loan_data)
 ************************************************************************************************/
function mm_get_lp_customer_data($customer_id){
    error_log("flow: mm_get_lp_customer_data: ($customer_id)\n");
	$return_array = array();
	 $lp_credentials = mm_get_lp_credentials();
        $account_token = $lp_credentials["token"];
        $auto_pal_id = $lp_credentials["tenant_id"];
        $header_array = array("Content-Type: application/json", 'Accept: application/json', "Authorization: $account_token", "Autopal-Instance-ID: $auto_pal_id");
        $url = "https://loanpro.simnang.com/api/public/api/1/odata.svc/Customers($customer_id)?\$expand=Phones,PrimaryAddress,MailAddress,PaymentAccounts/CheckingAccount";
	echo "URL $url\n";
	$curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER,$header_array );
        curl_setopt($curl, CURLOPT_POST, false);
        curl_setopt($curl, CURLOPT_HTTPGET, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);


        $json_response = curl_exec($curl);
	echo " Get Customer Response: $json_response\n";
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($status != 201 && $status != 200) {
            //There was an error in the message sending  Add error handling
            $return_value = 1;
            $return_message = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);
		$data = '';
        } else {
            //The delete command was successful
            $return_value = 0;
            $return_message = '';
	    $data = json_decode($json_response,1);
        }

	$return_array["return_value"] = $return_value;
	$return_array["return_message"] = $return_message;
	$return_array["data"] = $data;

	return $return_array;
	

}

function mm_create_lp_customer($customer_data){
    error_log("flow: mm_create_lp_customer: \n");
    //NEED TO HANDLE THAT THE CUSTOMER EXISTS AND ADD SOME ERROR HANDLING
    $return_array = array();
    $lp_credentials = mm_get_lp_credentials();
    $account_token = $lp_credentials["token"];
    $auto_pal_id = $lp_credentials["tenant_id"];
    $header_array = array("Content-Type: application/json", 'Accept: application/json', "Authorization: $account_token", "Autopal-Instance-ID: $auto_pal_id");
    $first_name = $customer_data["first_name"];
    $account_nbr = $customer_data["account_nbr"];
    echo "Account Number: $account_nbr\n";
    $last_name = $customer_data["last_name"];
    $email_address = $customer_data["email_address"];
    $dob = new DateTime($customer_data["dob"]);
    $birth_date = $dob->format('Y-m-d');
    $ssn = $customer_data["ssn"];
    //$ssn_formatted = substr($customer_data["ssn"], 0, 3) . "-" . substr($customer_data["ssn"], 3, 2) . "-" . substr($customer_data["ssn"], -4);
    $street_address_1 = $customer_data["street_address"];
    $street_address_2 = $customer_data["appt_suite"];
    $city = $customer_data["city"];
    $state = 'geo.state.' . $customer_data["state"];
    $zip_code = $customer_data["zip_code"];
    $mobile_phone_nbr = $customer_data["mobile_phone_nbr"];
    $home_phone_nbr = $customer_data["home_phone_nbr"];
    $customer_id = isset($customer_data["lp_customer_id"]) ? $customer_data["lp_customer_id"] : 0;
    echo "Customer ID is: $customer_id\n";
   
    $request_arr = array();
    $phones_arr = array(); 
    $phones_results_arr = array();
    $primary_address_arr = array();
    $mail_address_arr = array();
    $phones_results_arr[0]["__ignoreWarnings"] = true;
    $phones_results_arr[0]["phone"] = $mobile_phone_nbr;
    $phones_results_arr[0]["isPrimary"] = "1";
    $phones_results_arr[0]["isSecondary"] = "0";
    $phones_results_arr[0]["type"] = "customer.phoneType.cell";
    $phones_results_arr[1]["__ignoreWarnings"] = true;
    $phones_results_arr[1]["phone"] = $home_phone_nbr;
    $phones_results_arr[1]["isPrimary"] = "0";
    $phones_results_arr[1]["isSecondary"] = "1";
    $phones_results_arr[1]["type"] = "customer.phoneType.home";
    $phones_arr["results"] = $phones_results_arr;
   
    $primary_address_arr["__ignoreWarnings"] = true;
    $primary_address_arr["country"] = "company.country.usa";
    $primary_address_arr["address1"] = $street_address_1 . " " . $street_address_2;
    $primary_address_arr["zipcode"] = $zip_code;
    $primary_address_arr["city"] = $city;
    $primary_address_arr["state"] = $state;
   
    $mail_address_arr["__ignoreWarnings"] = true;
    $mail_address_arr["country"] = "company.country.usa";
    $mail_address_arr["address1"] = $street_address_1 . " " . $street_address_2;
    $mail_address_arr["zipcode"] = $zip_code;
    $mail_address_arr["city"] = $city;
    $mail_address_arr["state"] = $state;
   
    $request_arr["status"] = "Active";
    $request_arr["customerType"] = "customer.type.individual";
    $request_arr["customerIdType"] = "customer.idType.ssn";
    $request_arr["gender"] = "customer.gender.unknown";
    $request_arr["generationCode"] = "customer.generationCode.none";
    $request_arr["__ignoreWarnings"] = true;
    $request_arr["birthDate"] = $birth_date;
    $request_arr["firstName"] = $first_name;
    $request_arr["lastName"] = $last_name;
    $request_arr["ssn"] = $ssn;
    $request_arr["email"] = $email_address;
    $request_arr["Phones"] =  $phones_arr;
    $request_arr["PrimaryAddress"] = $primary_address_arr;
    $request_arr["MailAddress"] = $mail_address_arr;
    
   
    //Check to see if the customer already exists
    $url = "https://loanpro.simnang.com/api/public/api/1/odata.svc/Customers";
	//echo "Cust Id: $customer_id\n";
    if($customer_id >0){
        $lp_customer_data = mm_get_lp_customer_data($customer_id);
        if($lp_customer_data['return_value'] ==0){
            $data = $lp_customer_data["data"];
            $d = $data["d"];
            //echo "d $d\n";
            $phones = $d["Phones"];
            $mail_address = $d["MailAddress"];
            $primary_address = $d["PrimaryAddress"];
            $mail_address_id = $mail_address["id"];
            $primary_address_id = $primary_address["id"];
            //Process Phones
            $phone_results = $phones["results"];
            $phone_1_id = $phone_results[0]["id"];
            $phone_1_type = $phone_results[0]["type"];
            $phone_1_primary = $phone_results[0]["isPrimary"];
            $phone_1_secondary = $phone_results[0]["isSecondary"];
            $phone_2_id = $phone_results[1]["id"];
            $phone_2_type = $phone_results[1]["type"];
            $phone_2_primary = $phone_results[1]["isPrimary"];
            $phone_2_secondary = $phone_results[1]["isSecondary"];
   
            //Not lasting logic, but opdating for now to allow formers to apply
            $request_arr["Phones"]["results"][0]["__id"] = $phone_1_id;
            $request_arr["Phones"]["results"][0]["__update"] = "true";
            $request_arr["Phones"]["results"][1]["__id"] = $phone_2_id;
            $request_arr["Phones"]["results"][1]["__update"] = "true";
   
   
            //Update json request
            $request_arr["MailAddress"]["__id"] = $mail_address_id;
            $request_arr["MailAddress"]["__update"] = "true";
            $request_arr["PrimaryAddress"]["__id"] = $primary_address_id;
            $request_arr["PrimaryAddress"]["__update"] = "true";
   
            $url = $url. "($customer_id)";
        }else{
            //Add Erro Handling because the customer lookup failed
        }
    }else{
           //The customer doesn't exists so submit this as a post
   
    }
    echo "Url: $url\n";
    $json_message = json_encode($request_arr);	
    echo "Adding Cusotmer Request: $json_message\n";
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header_array);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $json_message);
    if($customer_id >0){
        //The customer id is populated so this must be an update and not an add
        echo "Customer exists so calling a put statement\n";
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
    }
   
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
   
    $json_response = curl_exec($curl);
    echo "$json_response\n";
    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
   
    if ($status != 201 && $status != 200) {
        //There was an error in the message sending  Add error handling
        $return_value = 1;
        $return_message = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno      " . curl_errno($curl);
        $lp_customer_id = '';
    } else {
        $response_array = json_decode($json_response, true);
        $lp_customer_id = $response_array["d"]["id"];
        $return_value = 0;
        $return_message = '';
    }
   
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["lp_customer_id"] = $lp_customer_id;
    return $return_array;
}
   

/************************************************************************************************
 * Function mm_create_lp_customer($customer_data) - This function takes an array of data and will attempt to create a customer in LoanPro.
 ************************************************************************************************/
// function mm_create_lp_customer($customer_data) {
//     error_log("flow: mm_create_lp_customer: \n");
//     //NEED TO HANDLE THAT THE CUSTOMER EXISTS AND ADD SOME ERROR HANDLING
//     $first_name = $customer_data["first_name"];
//     $account_nbr = $customer_data["account_nbr"];
//     $last_name = $customer_data["last_name"];
//     $email_address = $customer_data["email_address"];
//     $dob = new DateTime($customer_data["dob"]);
//     $birth_date = $dob->format('Y-m-d');
//     $ssn = $customer_data["ssn"];
//     //$ssn_formatted = substr($customer_data["ssn"], 0, 3) . "-" . substr($customer_data["ssn"], 3, 2) . "-" . substr($customer_data["ssn"], -4);
//     $street_address_1 = $customer_data["street_address"];
//     $street_address_2 = $customer_data["appt_suite"];
//     $city = $customer_data["city"];
//     $state = 'geo.state.' . $customer_data["state"];
//     $zip_code = $customer_data["zip_code"];
//     $mobile_phone_nbr = $customer_data["mobile_phone_nbr"];
//     $home_phone_nbr = $customer_data["home_phone_nbr"];
//     $lp_customer_id = 0;

//     $sdk = LPSDK::GetInstance();
//     $customers = $sdk->GetCustomers_RAW([CUSTOMERS::PHONES, CUSTOMERS::PRIMARY_ADDRESS], $sdk->CreateNoPagingParam(), $sdk->CreateFilterParams_Logic(CUSTOMERS::EMAIL . " ='$email_address'"));
//     $num_customers = sizeof($customers);

//     try {
//         $address = $sdk->CreateAddress($state, $zip_code)
//             ->Set([ADDRESS::ADDRESS_1 => $street_address_1, ADDRESS::ADDRESS_2 => $street_address_2, ADDRESS::CITY => $city]);
//     } catch (Exception $e) {
//         $return_array["return_value"] = 1;
//         $return_array["return_message"] = "Failed Creating Address For Customer with email $email_address.  Exception is $e";
//         return $return_array;
//     }
//     try {
//         $primary_phone = $sdk->CreatePhoneNumber($mobile_phone_nbr)->Set([PHONES::TYPE__C => PHONES_TYPE_C::CELL, PHONES::IS_PRIMARY => "1", PHONES::IS_SECONDARY => "0", "__ignoreWarnings" => true]);
//         $phones_arr = array();
//         $phones_arr[0] = $primary_phone;
//     } catch (Exception $e) {
//         $return_array["return_value"] = 1;
//         $return_array["return_message"] = "Failed creating primary phone number for customer with email $email_address.  Exception is $e";
//         return $return_array;
//     }
//     try {
//         if ($home_phone_nbr != '' && $home_phone_nbr != '0000000000') {
//             $secondary_phone = $sdk->CreatePhoneNumber($home_phone_nbr)->Set([PHONES::TYPE__C => PHONES_TYPE_C::HOME, PHONES::IS_SECONDARY => "1", PHONES::IS_PRIMARY => "0", "__ignoreWarnings" => true]);
//             $phones_arr[1] = $secondary_phone;
//         }
//     } catch (Exception $e) {
//         $return_array["return_value"] = 1;
//         $return_array["return_message"] = "Failed creating secondary (home) phone number for customer with email $email_address.  Exception is $e";
//         return $return_array;
//     }
//     try {
//         if ($num_customers == 1) {
//             $customer = $customers[0];
//             $lp_customer_id = $customer->Get(BASE_ENTITY::ID);
//             //There were customers in the system so need to update the information rather than add from scratch
//             $address->Set(BASE_ENTITY::ID, $customer->Get(CUSTOMERS::PRIMARY_ADDRESS)->Get(BASE_ENTITY::ID));
//             $customer = $customer->Set([CUSTOMERS::FIRST_NAME => $first_name, CUSTOMERS::LAST_NAME => $last_name, CUSTOMERS::EMAIL => $email_address, CUSTOMERS::PRIMARY_ADDRESS => $address, CUSTOMERS::BIRTH_DATE => $birth_date, CUSTOMERS::SSN => $ssn, "gender" => "customer.gender.unknown"]);
//         } else if ($num_customers == 0) {
//             //There are no matching customers in the system so just add a new one.
//             $customer = $sdk->CreateCustomer($first_name, $last_name)
//                 ->Set([CUSTOMERS::EMAIL => $email_address, CUSTOMERS::PRIMARY_ADDRESS => $address, CUSTOMERS::MAIL_ADDRESS => $address, CUSTOMERS::BIRTH_DATE => $birth_date, CUSTOMERS::SSN => $ssn, CUSTOMERS::STATUS => "Active", CUSTOMERS::PHONES => $phones_arr, "gender" => "customer.gender.unknown"]);
//             //Update the account record to store the ID of the customer on Loan Pro
//         } else {
//             //There must have been more than 1 customer returned which shouldn't happen.  Throw an error and return
//             $return_array["return_value"] = 1;
//             $return_array["return_message"] = "More than 1 customer exists with the proposed email addres: $email_address or SSN $ssn";
//             return $return_array;

//         }
//     } catch (Exception $e) {
//         $return_array["return_value"] = 1;
//         $return_array["return_message"] = "Failed creating the customer with email $email_address.  Exception is: $e";
//         return $return_array;
//     }
//     try {
//         $lp_customer_id = $customer->Save()->Get('id');
//         mm_update_database_value('mm_account', 'lp_customer_id', $lp_customer_id, 'i', 'account_nbr', $account_nbr);

//     } catch (Exception $e) {
//         //Failed, but log the error and try to add the customer by bypassing warnings.
//         $customer = $customer->Set(["__ignoreWarnings" => true]);
//         try {
//             $lp_customer_id = $customer->Save()->Get('id');
//             mm_update_database_value('mm_account', 'lp_customer_id', $lp_customer_id, 'i', 'account_nbr', $account_nbr);
//             mm_log_error('mm_create_lp_customer', "Received an error adding customer but was able to successfully add after bypassing warnings.  The error received was: $e");
//             $return_array["return_value"] = 0;
//             $return_array["return_message"] = '';
//             $return_array["lp_customer_id"] = $lp_customer_id;
//         } catch (Exception $e) {
//             $return_array["return_value"] = 1;
//             $return_array["return_message"] = "Failed to save the customer record for email address $email_address.  Exception is $e";
//         }

//         return $return_array;
//     }

//     $return_array["return_value"] = 0;
//     $return_array["return_message"] = "Customer was successfully added.";
//     $return_array["lp_customer_id"] = $lp_customer_id;

//     return $return_array;

// }

function mm_determine_term_for_state($state, $pay_frequency, $requested_amt, $apr) {
    error_log("flow: mm_determine_term_for_state: $state, $pay_frequency, $requested_amt, $apr\n");
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    
    $sql_string = "select number_of_payments from mm_pricing_detail where state = ? and payment_freq = ? and loan_amt = ? and apr = ?";
    try {
        $conn = mm_get_pdo_connection();
        $stmt = $conn->prepare($sql_string);
        $params = [$state, $pay_frequency, $requested_amt, $apr];
        $stmt->execute($params);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $return_array["term"] = $row['number_of_payments'];

    } catch (PDOException $e) {
        //$return_array["return_value"] = 1;
        //$return_array["return_message"] = "$e";
        $return_value = 1;
        $return_message = "$e";
        $return_array["sql_string"] = $sql_string;
        //return $return_array;
    }

    if (is_resource($conn)) {
        $conn = null;
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    
    return $return_array;
}

/****************************************************************************************************
 * Function mm_create_lp_loan($loan_data)
 ****************************************************************************************************/
function mm_create_lp_loan($loan_data, $custom_fields = null) {
    error_log("flow: mm_create_lp_loan\n");
    error_log_dump_associative_array("array:", $loan_data);

    $lp_credentials = mm_get_lp_credentials();
    $account_token = $lp_credentials["token"];
    $auto_pal_id = $lp_credentials["tenant_id"];
    $url = "https://loanpro.simnang.com/api/public/api/1/odata.svc/Loans";
    $header_array = array("Content-Type: application/json", 'Accept: application/json', "Authorization: $account_token", "Autopal-Instance-ID: $auto_pal_id");
    $request_arr = array();

    //Perform some validation on the data that needs to be set for
    $application_nbr = $loan_data["application_nbr"];
    //$loan_agreement_nbr = $loan_data["loan_agreement_nbr"];
    //	echo "3.  Loan Agreement Number: $loan_agreement_nbr\n";
    $state = $loan_data["state"];
    $requested_deposit_type = $loan_data["requested_deposit_type"];
    $pay_frequency = $loan_data["pay_frequency"];
    $temp_dt = new DateTime($loan_data["effective_dt"]);
    $effective_dt = $temp_dt->format('Y-m-d');
    $temp_dt = new DateTime($loan_data["orig_dt"]);
    //$orig_dt = $temp_dt->format('Y-m-d');
    $temp_dt = new DateTime($loan_data["first_due_dt"]);
    $first_due_dt = $temp_dt->format('Y-m-d');
    $last_day_of_month = date("Y-m-t", strtotime($first_due_dt));
    $requested_amt = $loan_data["requested_amt"];
    $apr = $loan_data["apr"];
    //Reformat Some of the data as Needed
    $display_id = str_pad($application_nbr, 8, '0', STR_PAD_LEFT);
    $display_id = "L" . $display_id;
    $loan_setup_arr = array();
    $loan_setup_arr["loanClass"] = "loan.class.consumer";
    $loan_setup_arr["loanType"] = "loan.type.installment";
    $loan_setup_arr["loanAmount"] = $requested_amt;
    $loan_setup_arr["loanRate"] = $apr;
    $loan_setup_arr["loanRateType"] = "loan.rateType.annually";

    if ($pay_frequency == "Bi-Weekly" || $pay_frequency == "Paid Weekly") {
        $loan_setup_arr["paymentFrequency"] = "loan.frequency.biWeekly";
        $tmp_pay_frequency = 'B';
    } else if ($pay_frequency == "Paid Monthly") {
        $loan_setup_arr["paymentFrequency"] = "loan.frequency.monthly";
        //Check to see if the date selected is the last day of the month and if it is set the Last Day of Month Flag.  This will force a payment schedule so that all payments are on the last day of the month
        if ($first_due_dt == $last_day_of_month) {
            $loan_setup_arr["dueDateOnLastDOM"] = "1";
        }
        $tmp_pay_frequency = 'M';
    } else if ($pay_frequency == "Paid Twice Monthly") {
        $loan_setup_arr["paymentFrequency"] = "loan.frequency.semiMonthly";
        $tmp_pay_frequency = 'S';
    }

    $term_result = mm_determine_term_for_state($state, $tmp_pay_frequency, $requested_amt, $apr);
    if ($term_result['return_value'] == 0) {
        $loan_setup_arr["loanTerm"] = $term_result['term'];
    } else {
        // error happened
        $term_return_message = $term_result['return_message'];
        error_log("Failed Getting Loan Term for state: $state, pay frequency: $pay_frequency, requested amount: $requested_amt, apr: $apr with error: $term_return_message");
    }

    $loan_setup_arr["contractDate"] = $effective_dt;
    $loan_setup_arr["firstPaymentDate"] = $first_due_dt;
    $loan_setup_arr["calcType"] = "loan.calcType.simpleInterest";
    $loan_setup_arr["daysInYear"] = "loan.daysInYear.actual";
    $loan_setup_arr["interestApplication"] = "loan.interestApplication.betweenTransactions";
    $loan_setup_arr["begEnd"] = "loan.begend.end";
    $loan_setup_arr["firstPeriodDays"] = "loan.firstPeriodDays.actual";
    $loan_setup_arr["firstDayInterest"] = "loan.firstdayinterest.yes";
    $loan_setup_arr["discountCalc"] = "loan.discountCalc.straightLine";
    $loan_setup_arr["diyAlt"] = "loan.diyAlt.no";
    $loan_setup_arr["roundDecimals"] = "5";
    $loan_setup_arr["lastAsFinal"] = "loan.lastasfinal.yes";
    $loan_setup_arr["curtailPercentBase"] = "loan.curtailpercentbase.loanAmount";
    $loan_setup_arr["nddCalc"] = "loan.nddCalc.standard";
    $loan_setup_arr["endInterest"] = "loan.endInterest.no";
    $loan_setup_arr["feesPaidBy"] = "loan.feesPaidBy.date";
    $loan_setup_arr["graceDays"] = $loan_data["late_fee_grace_period"];
    if ($loan_data["late_fee_calc"] == "GT") {
        $loan_setup_arr["lateFeeType"] = "loan.lateFee.4";
    } else if ($loan_data["late_fee_calc"] == "LT") {
        $loan_setup_arr["lateFeeType"] = "loan.lateFee.5";
    } else if ($loan_data["late_fee_calc"] == "AMT") {
        $loan_setup_arr["lateFeeType"] = "loan.lateFee.2";
    } else if ($loan_data["late_fee_calc"] == "PCT") {
        $loan_setup_arr["lateFeeType"] = "loan.lateFee.3";
    } else if ($loan_data["late_fee_calc"] == "STD") {
        $loan_setup_arr["lateFeeType"] = "loan.lateFee.2";
    }
    $loan_setup_arr["lateFeeAmount"] = $loan_data["late_fee_amt"];
    $loan_setup_arr["lateFeePercent"] = $loan_data["late_fee_pct"];
    $loan_setup_arr["lateFeeCalc"] = "loan.lateFeeCalc.standard";
    $loan_setup_arr["lateFeePercentBase"] = "loan.latefeepercentbase.regular";
    $loan_setup_arr["paymentDateApp"] = "loan.pmtdateapp.actual";

    $portfolio_arr = array();
    $portfolio_results_arr = array();
    $portfolio_metadata_arr = array();

    //Assign the state level portfolio
    if ($state == "MO") {
        $portfolio_value = 27;
    } else if ($state == "UT") {
        $portfolio_value = 28;
    } else if ($state == "NM") {
        $portfolio_value = 30;
    } else if ($state == "MS") {
        $portfolio_value = 31;
    } else if ($state == "CA") {
        $portfolio_value = 29;
    } else if ($state == "WI") {
        $portfolio_value = 34;
    } else if ($state == "SC") {
        $portfolio_value = 32;
    } else if ($state == "AL") {
        $portfolio_value = 33;
    } else if ($state == 'DE') {
        $portfolio_value = 37;
    } else if ($state == 'ID') {
        $portfolio_value = 42;
    } else {
        $portfolio_value = 36;
        mm_log_error('mm_create_lp_loan', "Non-Critical Added a loan for State $state and was assigned to the other portfolio");
    }

    $portfolio_metadata_arr["type"] = "Entity.Portfolio";
    $portfolio_metadata_arr["uri"] = '/api/1/odata.svc/Portfolios(id=25)';
    $portfolio_results_arr[0]["__metadata"] = $portfolio_metadata_arr;


    //Assign a portfolio based on the deposit preference of the customer
    if ($requested_deposit_type == "ACH") {
        //Customer signed up for ACH Authorization
        $portfolio_value = 6;
    } else {
        //Customer signed up for Manual Check
        $portfolio_value = 7;
    }

    $portfolio_metadata_arr["type"] = "Entity.Portfolio";
    $portfolio_metadata_arr["uri"] = '/api/1/odata.svc/Portfolios(id=6)';
    $portfolio_results_arr[1]["__metadata"] = $portfolio_metadata_arr;

    $portfolio_arr["results"] = $portfolio_results_arr;

    //Add sub-portfolios

    $subportfolio_arr = array();
    $subportfolio_metadata_arr = array();
    $subportfolio_results_arr = array();

    //Add stater sub-portfolio type

    //Assign the state level portfolio
    if ($state == "MO") {
        $portfolio_value = 27;
    } else if ($state == "UT") {
        $portfolio_value = 28;
    } else if ($state == "NM") {
        $portfolio_value = 30;
    } else if ($state == "MS") {
        $portfolio_value = 31;
    } else if ($state == "CA") {
        $portfolio_value = 29;
    } else if ($state == "WI") {
        $portfolio_value = 34;
    } else if ($state == "SC") {
        $portfolio_value = 32;
    } else if ($state == "AL") {
        $portfolio_value = 33;
    } else if ($state == 'DE') {
        $portfolio_value = 37;
    } else if ($state == 'ID') {
        $portfolio_value = 42;
    } else {
        $portfolio_value = 36;
        mm_log_error('mm_create_lp_loan', "Non-Critical Added a loan for State $state and was assigned to the other portfolio");
    }

    $subportfolio_metadata_arr["type"] = "Entity.SubPortfolio";
    $subportfolio_metadata_arr["uri"] = '/api/1/odata.svc/SubPortfolios(id=' . $portfolio_value . ')';
    $subportfolio_results_arr[0]["__metadata"] = $subportfolio_metadata_arr;


    if ($requested_deposit_type == "ACH") {
        //Customer signed up for ACH Authorization
        $portfolio_value = 40;
    } else {
        //Customer signed up for Manual Check
        $portfolio_value = 41;
    }

    $subportfolio_metadata_arr["type"] = "Entity.SubPortfolio";
    $subportfolio_metadata_arr["uri"] = '/api/1/odata.svc/SubPortfolios(id=' . $portfolio_value . ')';
    $subportfolio_results_arr[1]["__metadata"] = $subportfolio_metadata_arr;

    $subportfolio_arr["results"] = $subportfolio_results_arr;
    
    //Austin W - Setup for setting custom fields for new values
    $results = array();
    if (isset($custom_fields)) {
        foreach($custom_fields as $key => $value) {
            switch ($key) {
                case 'FICO Score':
                    $tmp = array("customFieldId" => 18, "customFieldValue" => $value);
                    break;
                case 'Gross Monthly Income':
                    $tmp = array("customFieldId" => 19, "customFieldValue" => $value);
                    break;
                case 'ACH Authorization Acknowledgement':
                    $tmp = array("customFieldId" => 20, "customFieldValue" => $value);
                    break;
            }
            array_push($results, $tmp);
        }
    }

    $loan_settings_arr = array();

    //Check to see if the loan already exists
    $loan_agreement_nbr = $loan_data["loan_agreement_nbr"];
    if ($loan_agreement_nbr == 0 || $loan_agreement_nbr == '') {
        //The loan should already exist on LoanPro
        $loan_settings_arr["loanStatusId"] = "1";
        $loan_settings_arr["loanSubStatusId"] = "1";
        $loan_settings_arr["autopayEnabled"] = "1";
        if (isset($custom_fields)) {
            $loan_settings_arr["CustomFieldValues"] = array("results"=>$results);
        }
    } else {
        $la_details = mm_get_loan_agreement_details($loan_agreement_nbr);
        $lp_loan_settings_id = $la_details["lp_loan_settings_id"];
        $lp_loan_setup_id = $la_details["lp_loan_setup_id"];
        $lp_loan_id = $la_details["lp_loan_id"];
        $loan_settings_arr["__id"] = $lp_loan_settings_id;
        $loan_settings_arr["__update"] = true;
        $loan_setup_arr["__id"] = $lp_loan_setup_id;
        $loan_setup_arr["__update"] = true;
        $url = $url . "(id=$lp_loan_id)";
    }


    //finalize the request array
    $request_arr["displayId"] = $display_id;
    $request_arr["title"] = 'Personal Loan';
    $request_arr["LoanSettings"] = $loan_settings_arr;
    $request_arr["LoanSetup"] = $loan_setup_arr;
    $request_arr["Portfolios"] = $portfolio_arr;
    $request_arr["SubPortfolios"] = $subportfolio_arr;

    $json_request = json_encode($request_arr);
    error_log("Create Loan: Json Request = $json_request");

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header_array);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $json_request);
    curl_setopt($curl, CURLOPT_HTTPGET, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    if ($loan_agreement_nbr > 0) {
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
    }

    $json_response = curl_exec($curl);
    error_log("Create Loan: Json Response = $json_response");

    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    if ($status != 201 && $status != 200) {
        //There was an error in the message sending  Add error handling
        $return_value = 1;
        $return_message = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);
        //echo "$return_message\n";
    } else {
        $response_array = json_decode($json_response, true);
        $d = $response_array["d"];
        $display_id = $d["displayId"];
        $return_value = 0;
        $return_message = '';
    }
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["display_id"] = $display_id;
    return $return_array;

}

/*******************************************************************************************
 * Function mm_process_loan_agreement()
 *
 * NOTE: Called by external Chron job:  process_loan_booking.php
 *******************************************************************************************/

function mm_process_loan_agreement($application_nbr){
    error_log("flow: mm_process_loan_agreement\n");
    $return_array = array();
    $return_message = '';
    $return_value = 0;

    $app_data_arr = mm_get_application_details($application_nbr);
    $app_data = $app_data_arr["app_data"];

    $application_nbr = $app_data["application_nbr"];
    $account_nbr = $app_data["account_nbr"];
    $account_details = mm_get_account_details($account_nbr);
    $loan_agreement_nbr = $app_data["loan_agreement_nbr"];
    $requested_amt = $app_data["requested_amt"];
    $state = $app_data["state"];
    $requested_deposit_type = $app_data["requested_deposit_type"];
    $pay_frequency = $app_data["pay_frequency_1"];

    //NEED to add error handling.  Also should pull la details from session but need to be stored there when created in a different function
    $lender_details = mm_get_lender_details($state);
    $la_details = mm_get_loan_agreement_details($loan_agreement_nbr);
    $payment_schedule_details = mm_get_payment_schedule($loan_agreement_nbr);

    //Get the loan agreement details - assumes that this function is only called from processing on the website and the session variables will contain the necessary information
    $lp_data = array();
    $lp_data["application_nbr"] = $application_nbr;
    $lp_data["loan_agreement_nbr"] = $loan_agreement_nbr;
    $lp_data["state"] = $state;
    $lp_data["requested_deposit_type"] = $requested_deposit_type;
    $lp_data["pay_frequency"] = $pay_frequency;
    $lp_data["effective_dt"] = $la_details["effective_dt"];
    $lp_data["first_due_dt"] = $la_details["first_due_dt"];
    $lp_data["orig_dt"] = $la_details["origination_dt"];
    $lp_data["lp_loan_id"] = $la_details["lp_loan_id"];
    $lp_data["lp_loan_settings_id"] = $la_details["lp_loan_settings_id"];
    $lp_data["lp_loan_setup_id"] = $la_details["lp_loan_setup_id"];
    $lp_data["requested_amt"] = $requested_amt;
    //error_log_dump_associative_array("mark: dumping app_data in mm_process_loan_agreement", $app_data);
    //error_log_dump_associative_array("mark: dumping la_details in mm_process_loan_agreement", $la_details);
    // todo: Mark to fix when Kleverlend passes back APR
    //$lp_data["apr"] = 132.73;
    $lp_data["apr"] = $app_data["approved_apr"];
    $lp_data["late_fee_amt"] = $lender_details["late_fee_amt"];
    $lp_data["late_fee_pct"] = $lender_details["late_fee_pct"];
    $lp_data["late_fee_calc"] = $lender_details["late_fee_calc"];
    $lp_data["returned_item_fee_amt"] = $lender_details["returned_item_fee_amt"];
    $lp_data["late_fee_grace_period"] = $lender_details["late_fee_grace_period"];

    $customer_data = array();
    $customer_data["first_name"] = $app_data["first_name"];
    $customer_data["account_nbr"] = $app_data["account_nbr"];
    $customer_data["last_name"] = $app_data["last_name"];
    $customer_data["email_address"] = $app_data["email_address"];
    $customer_data["dob"] = $app_data["dob"];
    $customer_data["ssn"] = $app_data["ssn"];
    $customer_data["street_address"] = $app_data["street_address"];
    $customer_data["appt_suite"] = isset($app_data["appt_suite"]) ? $app_data["appt_suite"] : '';
    $customer_data["city"] = $app_data["city"];
    $customer_data["state"] = $app_data["state"];
    error_log("mark: In mm_process_loan_agreement and state is: {$customer_data['state']}\n");
    $customer_data["zip_code"] = $app_data["zip_code"];
    $customer_data["mobile_phone_nbr"] = $app_data["mobile_phone_nbr"];
    $customer_data["home_phone_nbr"] = $app_data["home_phone_nbr"];
    $customer_data["lp_customer_id"] = $account_details["lp_customer_id"];


    //Generate the signed loan agreement document - ADD ERROR HANDLING
    $payment_schedule = mm_get_payment_schedule($loan_agreement_nbr);
    //Generate the Loan Agreement PDF
    $pdf = new PDF;
    //$pdf_results = mm_generate_loan_agreement_pdf($application_nbr);
    $pdf_results = $pdf->mm_generate_loan_agreement_pdf($application_nbr,
                                                        $app_data_arr,
                                                        $la_details,
                                                        $payment_schedule_details,
                                                        $lender_details,
                                                        mm_get_document_path(), 1);
    //$document_results = mm_generate_loan_agreement_pdf($application_nbr, 1);

    // Mark was here, creating these variables in outer scope as they are used throughout this routine
    $lp_customer_id = "";
    $payment_profile_return_value = "";

        $lp_loan_id = $la_details["lp_loan_id"];
        $lp_display_id = $la_details["loan_nbr"];
        $customer_results = mm_create_lp_customer($customer_data);
        $customer_return_value = $customer_results["return_value"];
        $customer_return_message = $customer_results["return_message"];
        //ADD ERROR HANDLING
        if ($customer_return_value != 0) {
            mm_log_error('mm_process_loan_agreement', "Unable to add the customer to loan pro for application number $application_nbr.  Error from Loan Pro is $customer_return_message");
            $return_value = 1;
            $return_message = "Unable to add the customer to loan pro for application $application_nbr.  Error from Loan Pro is $customer_return_message";
        } else {
            // Mark was here:  Commenting out the original code and then refactoring $lp_customer_id and $payment_profile_return_value to a more outer scope, since they are needed later in this routine
            //The customer was added successfully so create the payment profile.  This code needs to be updated to update the payment profile, not just create a new one.
            /*
            $lp_customer_id = $customer_results["lp_customer_id"];
            $payment_profile_results = mm_create_lp_payment_profile($lp_display_id);
            $payment_profile_return_value = $payment_profile_results["return_value"];
            $payment_profile_return_message = $payment_profile_results["return_message"];
             */
            $lp_customer_id = $customer_results["lp_customer_id"];
		mm_update_database_value('mm_account', 'lp_customer_id', $lp_customer_id, 'i', 'account_nbr', $account_nbr);

            $payment_profile_results = mm_create_lp_payment_profile($lp_display_id);
            $payment_profile_return_value = $payment_profile_results["return_value"];
            $payment_profile_return_message = $payment_profile_results["return_message"];
            if ($payment_profile_return_value != 0) {
                mm_update_database_value("mm_application", 'application_status', 9, 'i', 'application_nbr', $application_nbr);
                mm_log_error('mm_process_loan_agreement', "Unable to add the payment profile for loan $lp_display_id.  The error message is $payment_profile_return_message");
                $return_value = 1;
                $return_message = $payment_profile_return_message;
            }
        }

        //Update the application status based on the result
        if ($customer_return_value == 0 && $payment_profile_return_value == 0) {
            //Create the link between the two loans
            $link_results = mm_create_lp_customer_loan_link($lp_customer_id, $lp_loan_id);
            $link_results_return_value = $link_results["return_value"];
            $link_results_return_message = $link_results["return_message"];
            if ($link_results_return_value != 0) {
                mm_update_database_value("mm_application", 'application_status', 9, 'i', 'application_nbr', $application_nbr);
                mm_log_error('mm_process_loan_agreement', "Unable to link the customer to the loan in loan pro for application number $application_nbr.  Error from Loan Pro is $link_results_return_message.");
                $return_value = 1;
                $return_message = $link_results_return_message;
            } else {
                $return_value = 0;
                $return_message = "The loan was successfully added and linked to the customer.";
            }
        }
        //If the loan was booked successfully then go ahead and send the welcome email.
            //Update the email to approved and schedule the welcome email to be sent.
            //mm_update_database_value("mm_application", 'application_status', 7, 'i', 'application_nbr', $application_nbr);
            $email_data = array();
            $email_data["to"] = $app_data["email_address"];
            $email_data["from"] = SUPPORT_EMAIL;
            $email_data["from_name"] = SITE_NAME;
            $email_data["subject"] = "Welcome To ".SITE_NAME;
            $tmp_first_name = ucwords(strtolower($app_data["first_name"]));

            //Build Payment Schedule Strings
            $payment_schedule = $payment_schedule_details["payment_schedule"];
            $payment_schedule_string = 'Payment Schedule\n';
            $payment_schedule_string_html = '<p><b>Payment Schedule</b></p>';
            foreach ($payment_schedule as $payment) {
                $pay_dt = new DateTime($payment["payment_dt"]);
                $payment_dt = $pay_dt->format("m/d/Y");
                $payment_amt = $payment["payment_amt"];
                $payment_schedule_string .= "$payment_dt:          \$$payment_amt\n";
                $payment_schedule_string_html .= "<p>$payment_dt:<span>          </span>\$$payment_amt</p>";

            }

            $tmp = "Hello $tmp_first_name,\n\nWelcome to ".SITE_NAME.", where we make borrowing better!\n\nWe're glad you chose us for your loan, and your funds are on the way.  Depending on your bank, the funds could be available as soon as the next business day.  We appreciate your business and value you as a customer - so please call us if there's anything we can do better, or if you have a question about your loan or account at ".SUPPORT_PHONE." or email us at ".SUPPORT_EMAIL.".\n\nBelow you'll find a complete list of scheduled payments including due dates and amounts, and if you have selected to make payments from your bank account via ACH, your payments will occur automatically.  With ".SITE_NAME." you can always pay your loan off early with no penalties, fees or additional charges.\n\n$payment_schedule_string\n\n";
            if ($app_data["state"] == 'SC') {
                $tmp .= "Please review the South Carolina Department of Consumer Affairs Written Pamphlet located here: https://".SITE_URI."/wp-content/uploads/2018/05/SC-Consumer-Affairs-Lender-Rights.pdf\n\n";
            }
            $tmp .= "Welcome Aboard,\n\n".SITE_NAME."\n";
            $email_data["txt_body"] = $tmp;
            $tmp = "<html><head></head><body><p>Hello $tmp_first_name,</p><p>Welcome to ".SITE_NAME.", where we make borrowing better!</p><p>We're glad you chose us for your loan, and your funds are on the way.  Depending on your bank, the funds could be available as soon as the next business day.  We appreciate your business and value you as a customer - so please call us if there's anything we can do better, or if you have a question about your loan or account at ".SUPPORT_PHONE." or email us at ".SUPPORT_EMAIL.".</p>Below you'll find a complete list of scheduled payments including due dates and amounts, and if you have selected to make payments from your bank account via ACH, your payments will occur automatically.  With ".SITE_NAME." you can always pay your loan off early with no penalties, fees or additional charges.</p><p>$payment_schedule_string_html</p><p>";
            if ($app_data["state"] == 'SC') {
                $tmp .= "Please review the South Carolina Department of Consumer Affairs Written Pamphlet located <a href='https://".SITE_URI."/wp-content/uploads/2018/05/SC-Consumer-Affairs-Lender-Rights.pdf' target='_blank'>here</a></p><p>";
            }
            $tmp .= "Welcome Aboard,</p><p>".SITE_NAME."</p></body></html>";
            $email_data["html_body"] = $tmp;
            $tmp_dt = new DateTime();
            $tmp_dt = $tmp_dt->modify("+1 Day");
            $scheduled_dt = $tmp_dt->format("Y-m-d H:i:s");
            $email_data["scheduled_dt"] = $scheduled_dt;
            $email_message = json_encode($email_data);
            // todo:  do we need error handling here?
            $email_results = mm_schedule_email_message($email_message);

    //}

    //Set the return values of the function
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;

    //return the return array
    return $return_array;




}
/***********************************************************************************************
 * Function mm_create_lp_customer_loan_link($customer_id, $loan_id)
 ***********************************************************************************************/
function mm_create_lp_customer_loan_link($customer_id, $loan_id) {
    error_log("flow: mm_create_lp_customer_loan_link\n");
    $return_array = array();

    try {
        $sdk = LPSDK::GetInstance();
        //Check to see if the loan exists and if it does then just update the data otherwise add a new loan
        $loan = $sdk->GetLoan($loan_id, [LOAN::LOAN_SETUP], $nopageProps = true);
        $disp_id = $loan->Get(LOAN::DISP_ID);
        if (is_null($loan)) {
            //There was no matching loan record
            $return_array["return_value"] = 1;
            $return_array["return_message"] = "There were no loans with the loan id $loan_id";
        } else {
            $customer = $sdk->GetCustomer($customer_id);
            $loan->AddCustomer($customer, \Simnang\LoanPro\Constants\CUSTOMER_ROLE::PRIMARY);
            $return_array["return_value"] = 0;
            $return_array["return_message"] = "Success";
        }

    } catch (Exception $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "There was a problem linking the loan: $e";
    }

    return $return_array;

}

/***********************************************************************************************
 * Function mm_unset_application_session()
 ***********************************************************************************************/
function mm_unset_application_session() {
    error_log("flow: mm_unset_application_session\n");
    if (isset($_SESSION)) {
        foreach ($_SESSION as $key => $value) {
            if (substr($key, 0, 4) == "app_") {
                unset($_SESSION["$key"]);
            }
        }
    }
}

/******************************************************************************************
 * Function mm_restore_session($email_address)
 ******************************************************************************************/
function mm_restore_session($email_address) {
    error_log("flow: mm_restore_session: $email_address\n");
    //Need to add error handling
    $return_array = array();

    //If session isn't started then start it?
    if (!session_id()) {
        session_start();
    }

    $account_details = mm_get_account_details_from_email_address($email_address);
    $account_nbr = $account_details["account_nbr"];
    if ($account_nbr != 0) {
        $temp1 = mm_get_most_recent_application_details($account_nbr);
        $app_data = $temp1["app_data"];
        $num_apps = $temp1["num_apps"];
        error_Log("mark: after call to mm_get_most_recent_application_details, num_apps: $num_apps\n");
        if ($num_apps == 0) {
            //There are no applications so set default values\
            error_Log("mark: error, zero apps found!!!\n");
        } else {
            $app_status = $app_data["application_status"];
            error_log("mark: the application status is: $app_status\n");
            if ($app_status == 1 || $app_status == 2 || $app_status == 3 || $app_status == 4 || $app_status == 5 || $app_status == 6) {
                error_Log("mark: in first if statement\n");
                //The application is either in draft, approved pending acceptance, approved pending customer docs, approved pending agent review, or approved pending customer loan doc therefore setup the appropriate session variables.
                foreach ($app_data as $key => $value) {
                    //prepend all of the application variables with the app_ prefix
                    if ($key == "return_value" || $key == "return_message" || $key == "num_apps") {
                        //Do nothing because we don't want to add these to the application array
                    } else {
                        $_SESSION["app_$key"] = $value;
                    }
                }

                if ($app_status == 2 || $app_status == 3 || $app_status == 4 || $app_status == 5 || $app_status == 6) {
                    error_Log("mark: in second if statement\n");
                    //Get the uw_decision data
                    //Need to add error handling for these queries
                    $uw_decision_nbr = $app_data["uw_decision_nbr"];
                    $uw_decision_arr = mm_get_uw_decision_details($uw_decision_nbr);
                    // Mark was here: commenting out the following 2 lines as they are not being used
                    /*
                    $uw_decision_return_value = $uw_decision_arr["return_value"];
                    $uw_decision_return_message = $uw_decision_arr["return_message"];
                     */
                    $decision_data = $uw_decision_arr["decision_data"];
                    //$_SESSION["app_uw_decision"] = $decision_data["app_status_cd"];
                    $_SESSION["app_uw_decision"] = array_key_exists('app_status_cd', $decision_data) ? $decision_data["app_status_cd"] : null;
                }

                if ($app_status == 3 || $app_status == 4 || $app_status == 5 || $app_status == 6) {
                    //Get the Verifications Decision Data
                    //Need to add Error Handling
                    $preverif_decision_nbr = $app_data["preverif_decision_nbr"];
                    $preverif_decision_arr = mm_get_preverif_decision_details($preverif_decision_nbr);
                    // Mark was here: commenting out the following 2 lines as they are not being used
                    /*
                    $preverif_decision_return_value = $preverif_decision_arr["return_value"];
                    $preverif_decision_return_message = $preverif_decision_arr["return_message"];
                     */
                    $preverif_data = $preverif_decision_arr["preverif_data"];
                    $_SESSION["app_vf_decision"] = array_key_exists('app_status_cd', $preverif_data) ? $preverif_data["app_status_cd"] : null;
                }

                //The customer is approved pending customer loan docs so set the session variables related to the loan agreement information in the database
                if ($app_status == 6) {
                    $app_loan_agreement_nbr = $app_data["loan_agreement_nbr"];
                    $la_results = mm_get_loan_agreement_details($app_loan_agreement_nbr);
                    foreach ($la_results as $key => $value) {
                        //APPEND ALL OF THE DATA TO SESSION ARRAY EXCEPT A FEW FIELDS
                        if ($key == "loan_agreement_nbr" || $key == "application_nbr" || $key == "return_value" || $key == "return_message") {
                            //These keys are already output so ignore them
                        } else {
                            $_SESSION["app_$key"] = $value;
                        }
                    }
                }
            }
        }
    } else {
        // todo: do we need error handling here?
        //No MM Account Record exists
        error_log("mark: no account details found after call to mm_get_account_details_from_email_address\n");
    }

    //Set Return Values
    $return_array["return_value"] = 0;
    $return_array["return_message"] = "Success, but no error handling added";

    return $return_array;
}

/***************************************************************************************
 * Function mm_create_loan_agreement($agreement_data) - Takes an array of data and generates the loan agreement record in mm_loan_agreement
 ***************************************************************************************/
function mm_create_loan_agreement($agreement_data) {
    //error_log_dump_associative_array("agreement_data:", $agreement_data);
    error_log("flow: mm_create_loan_agreement\n");
    //error_log_dump_associative_array("agreement_data:", $agreement_data);
    $return_array = array();

    $temp_dt = new DateTime($agreement_data["orig_dt"]);
    $origination_dt = $temp_dt->format('Y/m/d');
    $temp_dt = new DateTime($agreement_data["effective_dt"]);
    $effective_dt = $temp_dt->format('Y/m/d');
    $apr = $agreement_data["apr"];
    $temp_dt = new DateTime($agreement_data["first_due_dt"]);
    $first_due_dt = $temp_dt->format('Y/m/d');
    $temp_dt = new DateTime($agreement_data["maturity_dt"]);
    $maturity_dt = $temp_dt->format('Y/m/d');
    $payment_freq = $agreement_data["payment_freq"];
    $number_of_payments = $agreement_data["num_payments"];
    $total_finance_charge = $agreement_data["total_finance_charge"];
    $principal_amt = $agreement_data["principal_amt"];
    $total_amt_due = $agreement_data["total_amt_due"];
    $application_nbr = $agreement_data["application_nbr"];
    $lender_nbr = $agreement_data["lender_nbr"];
    $temp_dt = new DateTime($agreement_data["create_dt"]);
    $create_dt = $temp_dt->format('Y/m/d H:i:s');
    $late_fee_pct = $agreement_data["late_fee_pct"];
    $loan_nbr = $agreement_data["loan_nbr"];
    $late_fee_amt = $agreement_data["late_fee_amt"];
    $late_fee_calc = $agreement_data["late_fee_calc"];
    $late_fee_grace_period = $agreement_data["late_fee_grace_period"];
    $lp_loan_id = $agreement_data["loan_id"];
    $returned_item_fee_amt = $agreement_data["returned_item_fee_amt"];
    $lp_loan_settings_id = $agreement_data["loan_settings_id"];
    $lp_loan_setup_id = $agreement_data["loan_setup_id"];

    $sql_string = "insert into mm_loan_agreement (origination_dt, effective_dt, apr, first_due_dt, maturity_dt, payment_freq, number_of_payments,
                    total_finance_charge, principal_amt, total_amt_due, application_nbr, lender_nbr, create_dt, late_fee_amt, late_fee_pct,
                    late_fee_calc, late_fee_grace_period, returned_item_fee_amt, loan_nbr, lp_loan_id, lp_loan_settings_id, lp_loan_setup_id)
                    values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    try {
        $conn = mm_get_pdo_connection();
        $stmt = $conn->prepare($sql_string);
        $parms = [$origination_dt, $effective_dt, $apr, $first_due_dt, $maturity_dt, $payment_freq, $number_of_payments, $total_finance_charge,
            $principal_amt, $total_amt_due, $application_nbr, $lender_nbr, $create_dt, $late_fee_amt, $late_fee_pct, $late_fee_calc, $late_fee_grace_period,
            $returned_item_fee_amt, $loan_nbr, $lp_loan_id, $lp_loan_settings_id, $lp_loan_setup_id];
        $stmt->execute($parms);
        $loan_agreement_nbr = $conn->lastInsertId();

    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "$e";
        $return_array["sql_string"] = $sql_string;
        return $return_array;
    }

    if (is_resource($conn)) {
        $conn = null;
    }
    //Need to add defaulting and handle errors
    $return_array["loan_agreement_nbr"] = $loan_agreement_nbr;
    $return_array["return_value"] = 0;

    return $return_array;

}

/************************************************************************************
 * Function mm_load_payment_schedule($loan_agreement_nbr, $payment_schedule)
 ************************************************************************************/
function mm_load_payment_schedule($loan_agreement_nbr, $payment_schedule) {
    error_log("flow: mm_load_payment_schedule\n");
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    try {
        $conn = mm_get_pdo_connection();
        $sql_query = "insert into mm_payment_schedule (payment_nbr, payment_dt, payment_amt, loan_agreement_nbr, payment_int, payment_prin) values (?,?,?,?, ?, ?)";
        $stmt = $conn->prepare($sql_query);
        foreach ($payment_schedule as $payment) {
            $payment_nbr = $payment["payment_nbr"];
            $temp_dt = new DateTime($payment["payment_dt"]);
            $payment_dt = $temp_dt->format('Y-m-d');
            $payment_amt = $payment["payment_amt"];
            $payment_int = $payment["payment_int"];
            $payment_prin = $payment["payment_prin"];
            $param_arr = [$payment_nbr, $payment_dt, $payment_amt, $loan_agreement_nbr, $payment_int, $payment_prin];
            $stmt->execute($param_arr);
        }
    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "$e";
        return $return_array;
    }

    if (is_resource($conn)) {
        $conn = null;
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    return $return_array;
}

/************************************************************************************
 * Function mm_get_lp_custom_fields($application_nbr)
 ************************************************************************************/
function mm_get_lp_custom_fields($application_nbr) {
    error_log("flow: mm_get_lp_custom_fields\n");
    $return_array = array();
    
    $conn = mm_get_pdo_connection();
    // FICO
    try {
        $sql_query = "select fico from mm_uw_decision where application_nbr = ?";
        $stmt = $conn->prepare($sql_query);
        $params = [$application_nbr];
        $stmt->execute($params);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $return_array['FICO Score'] = $row['number_of_payments'];
    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "$e";
        return $return_array;
    }
    
    // Gross Monthly Income and ACH Authorization Acknowledgement
    try {
        $sql_query = "select requested_deposit_type, total_monthly_income_amt from mm_application where application_nbr = ?";
        $stmt = $conn->prepare($sql_query);
        $params = [$application_nbr];
        $stmt->execute($params);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $return_array['Gross Monthly Income'] = $row['total_monthly_income_amt'];
        $return_array['ACH Authorization Acknowledgement'] = $row['requested_deposit_type'];
    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "$e";
        return $return_array;
    }

    if (is_resource($conn)) {
        $conn = null;
    }
    
    error_log_dump_associative_array("return_array:", $return_array);
    return $return_array;
}

/*************************************************************************************
 * Function mm_generate_loan_agreement($application_nbr)
 * called externally from create_loan_agreement.php
 *************************************************************************************/
function mm_generate_loan_agreement($application_nbr, $timestamp = '') {
    error_log("flow: mm_generate_loan_agreement: ($application_nbr, $timestamp)\n");
    $return_array = array();
    $document_path = '';
    $loan_agreement_nbr = '';

    if ($timestamp == '') {
        $curr_timestamp = new DateTime();
    } else {
        $curr_timestamp = new DateTime($timestamp);
    }
    $curr_timestamp_string = $curr_timestamp->Format('m/d/Y H:i:s');

    $orig_dt = $curr_timestamp->format('m/d/Y');

    $return_data = mm_get_application_details($application_nbr);
    //Add error handling in case it fails
    $app_data = $return_data["app_data"];
    $app_state = $app_data["state"];
    $loan_agreement_nbr = $app_data["loan_agreement_nbr"];
    $requested_deposit_type = $app_data["requested_deposit_type"];
    $lender_details = mm_get_lender_details($app_state);
    $next_pay_dt = $app_data["next_pay_dt_1"];
    $direct_deposit_ind_1 = $app_data["direct_deposit_ind_1"];
    $pay_frequency = $app_data["pay_frequency_1"];
    $requested_amt = $app_data["requested_amt"];
    if ($pay_frequency == 'Bi-Weekly') {
        $pay_freq = 'B';
    } else if ($pay_frequency == 'Paid Monthly') {
        $pay_freq = 'M';
    } else if ($pay_frequency == 'Paid Twice Monthly') {
        $day_paid_twice_monthly = $app_data["day_paid_twice_monthly_1"];
        if ($day_paid_twice_monthly == "1st and 15th") {
            $pay_freq = 'S1';
        } else if ($day_paid_twice_monthly == "15th and 30th") {
            $pay_freq = 'S2';
        }

    } else if ($pay_frequency == 'Paid Weekly') {
        $pay_freq = 'W';
    } else {
        //Default to bi-weekly and log an error
        $pay_freq = 'B';
    }

    //Insert logic here to calculate first pay date, etc
    $effective_dt = mm_calculate_contract_date($curr_timestamp_string, $requested_deposit_type);
    $date1 = new DateTime($next_pay_dt);
    $date2 = new DateTime($effective_dt);
    $date_diff = $date2->diff($date1)->format("%r%a");
    //error_log("mark: state: " . print_r($app_state, true));
    if ($app_state == 'CA' || $app_state == 'WI') {
        if ($date_diff >= 15) {
            // The first due date is already 15 days out.
            $first_due_dt = $next_pay_dt;
        } else {
            $first_due_dt = mm_calculate_future_pay_dt_CA_or_WI($pay_freq, $next_pay_dt, $effective_dt);
        }
    } else if ($date_diff >= 5) {
        error_log("mark: date_diff >= 5");
        $first_due_dt = $next_pay_dt;
    } else {
        error_log("mark: date_diff < 5");
        $first_due_dt = mm_calculate_future_pay_dt($pay_freq, $next_pay_dt, 1);
    }

    //Determine if there is an adjustment needed based on the deposit method - Manual check needs an extra day added to allow time for the customer to deposit the check
    if ($direct_deposit_ind_1 == "N") {
        $temp_dt = new DateTime($first_due_dt);
        $first_due_dt = $temp_dt->Modify('+1 Day')->format('m/d/Y');

    }

    $app_data["first_due_dt"] = $first_due_dt;
    $app_data["effective_dt"] = $effective_dt;
    $app_data["orig_dt"] = $orig_dt;

    //Insert logic here to call LoanPro quick quote to get the information needed to generate the loan document
    $lp_data = array();
    $lp_data["application_nbr"] = $application_nbr;
    $lp_data["loan_agreement_nbr"] = $loan_agreement_nbr;
    $lp_data["state"] = $app_state;
    $lp_data["requested_deposit_type"] = $requested_deposit_type;
    $lp_data["pay_frequency"] = $pay_frequency;
    $lp_data["effective_dt"] = $effective_dt;
    $lp_data["first_due_dt"] = $first_due_dt;
    $lp_data["orig_dt"] = $orig_dt;
    $lp_data["requested_amt"] = $requested_amt;
    // this in contract_apr
    $lp_data["apr"] = $app_data["approved_apr"];
    $lp_data["late_fee_amt"] = $lender_details["late_fee_amt"];
    $lp_data["late_fee_pct"] = $lender_details["late_fee_pct"];
    $lp_data["late_fee_calc"] = $lender_details["late_fee_calc"];
    $lp_data["returned_item_fee_amt"] = $lender_details["returned_item_fee_amt"];
    $lp_data["late_fee_grace_period"] = $lender_details["late_fee_grace_period"];

    $_SESSION["LP_DATA"] = $lp_data;
    $custom_fields = mm_get_lp_custom_fields($application_nbr);
    $loan_array = mm_create_lp_loan($lp_data, $custom_fields);
    $return_value = $loan_array["return_value"];
    if ($return_value != 0) {
        $return_message = $loan_array["return_message"];
        mm_log_error('mm_generate_loan_agreement', "Failed Creating the Loan in Loan Pro: $return_message");
    } else {
        $display_id = $loan_array["display_id"];

        //Get the loan details
        $loan_details = mm_get_lp_loan_details($display_id);
        $return_value = $loan_details["return_value"];
        if ($return_value != 0) {
            $return_message = $loan_details["return_message"];
            mm_log_error('mm_generate_loan_agreement', "Failed Getting Loan Details: $return_message");
        } else {
            $agreement_data = array();
            $agreement_data["orig_dt"] = $orig_dt;
            $agreement_data["effective_dt"] = $loan_details["contract_dt"];
            // this is TILA apr
            $agreement_data["apr"] = $loan_details["apr"];
            $agreement_data["first_due_dt"] = $first_due_dt;
            $agreement_data["maturity_dt"] = $loan_details["maturity_dt"];
            $agreement_data["payment_freq"] = $pay_frequency;
            $agreement_data["num_payments"] = $loan_details["num_payments"];
            $agreement_data["total_finance_charge"] = $loan_details["total_int"];
            $agreement_data["total_amt_due"] = $loan_details["total_due"];
            $agreement_data["principal_amt"] = $loan_details["total_prin"];
            $agreement_data["late_fee_amt"] = $loan_details["late_fee_amt"];
            $agreement_data["late_fee_pct"] = $loan_details["late_fee_pct"];
            $agreement_data["late_fee_calc"] = $lender_details["late_fee_calc"]; //Not an error that it's calc vs. type
            $agreement_data["returned_item_fee_amt"] = $lender_details["returned_item_fee_amt"];
            $agreement_data["late_fee_grace_period"] = $loan_details["late_fee_grace_period"];
            $agreement_data["lender_nbr"] = $lender_details["lender_nbr"];
            $agreement_data["application_nbr"] = $application_nbr;
            $agreement_data["loan_nbr"] = $display_id;
            $agreement_data["loan_id"] = $loan_details["loan_id"];
            $agreement_data["loan_settings_id"] = $loan_details["loan_settings_id"];
            $agreement_data["loan_setup_id"] = $loan_details["loan_setup_id"];
            $agreement_data["create_dt"] = $curr_timestamp_string;

            //Create an mm_loan_agreement record
            $la_details = mm_create_loan_agreement($agreement_data);
            $return_value = $la_details["return_value"];
            if ($return_value != 0) {
                $return_message = $loan_details["return_message"];
                mm_log_error('mm_create_loan_agreement', "Failed Creating the Loan Agreement Record: $return_message");
            } else {
                $loan_agreement_nbr = $la_details["loan_agreement_nbr"];
		//echo "Loan Agreemnet # is $loan_agreement_nbr\n";
                //Insert the payment schedule records
                $payment_sched_result = mm_load_payment_schedule($loan_agreement_nbr, $loan_details["payment_schedule"]);
                $return_value = $payment_sched_result["return_value"];
                if ($return_value != 0) {
                    //$return_message = "Failed Creating the Payment Schedule";
                    mm_log_error('mm_generate_loan_agreement', "Failed Creating the Loan Agreement Payment Schedule");
                } else {
                    //update the application record to point to this loan agreement
                    mm_update_database_value('mm_application', 'loan_agreement_nbr', $loan_agreement_nbr, 'i', 'application_nbr', $application_nbr);

                    $loan_agreement_details = mm_get_loan_agreement_details($loan_agreement_nbr);
                    $payment_schedule = mm_get_payment_schedule($loan_agreement_nbr);

                    error_log_dump_associative_array("mark: dumping app_details", $return_data);
                    error_log_dump_associative_array("mark: dumping lender_details", $lender_details);
                    error_log_dump_associative_array("mark: dumping la_details", $loan_agreement_details);

                    //Generate the Loan Agreement PDF
                    $pdf = new PDF;
                    //$pdf_results = mm_generate_loan_agreement_pdf($application_nbr);
                    $pdf_results = $pdf->mm_generate_loan_agreement_pdf($application_nbr,
                                                                        $return_data,
                                                                        $loan_agreement_details,
                                                                        //$la_details,
                                                                        $payment_schedule,
                                                                        //$payment_sched_result,
                                                                        $lender_details,
                                                                        mm_get_document_path());

                    $return_value = $pdf_results["return_value"];
                    if ($return_value != 0) {
                        //$return_message = "Failed creating the loan agreement pdf";
                        mm_log_error('mm_generate_loan_agreement', "Failed Creating the Loan Agreement PDF");
                    } else {
                        $document_path = $pdf_results["document_path"];
                    }

                }
            }
        }
    }

    if ($return_value != 0) {
        mm_update_database_value('mm_application', 'application_status', 9, 'i', "application_nbr", $application_nbr);
    }
    //constuct return mesage
    $return_array["document_path"] = $document_path;
    $return_array["loan_agreement_nbr"] = $loan_agreement_nbr;
    $return_array["return_value"] = $return_value;
    //$return_array["return_message"] = $return_message;
    //send return array back

    return $return_array;
}

/**********************************************************************************************
 * Function mm_get_lp_loan_details($display_id);
 **********************************************************************************************/
function mm_get_lp_loan_details($display_id) {
    error_log("flow: mm_get_lp_loan_details\n");
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    try {
        $sdk = LPSDK::GetInstance();
        $loans = $sdk->GetLoans_RAW([LOAN::LOAN_SETUP, LOAN::TRANSACTIONS, LOAN::LOAN_SETTINGS], $sdk->CreateNoPagingParam(), $sdk->CreateFilterParams_Logic(LOAN::DISP_ID . " = '$display_id'"));

        // $loans is an iterator so you can simply use a foreach loop
        foreach ($loans as $loan) {
            $loan_setup = $loan->get(LOAN::LOAN_SETUP);
            $loan_settings = $loan->get(LOAN::LOAN_SETTINGS);
            $active_ind = $loan->get(LOAN::ACTIVE);
            $loan_setup_active_ind = $loan_setup->get(LOAN::ACTIVE);
            $apr = $loan_setup->get(LOAN_SETUP::APR);
            $id = $loan->get(BASE_ENTITY::ID);
            $loan_settings_id = $loan_settings->get(BASE_ENTITY::ID);
            $loan_setup_id = $loan_setup->get(BASE_ENTITY::ID);
            $loan_status = $loan_settings->get(LOAN_SETTINGS::LOAN_STATUS_ID);
            $loan_sub_status = $loan_settings->get(LOAN_SETTINGS::LOAN_SUB_STATUS_ID);
            $maturity_dt = DateTime::createFromFormat('U', $loan_setup->get(LOAN_SETUP::ORIG_FINAL_PAY_DATE))->format('m/d/Y');
            $total_due = $loan_setup->get(LOAN_SETUP::TIL_TOTAL_OF_PAYMENTS);
            $total_prin = $loan_setup->get(LOAN_SETUP::TIL_LOAN_AMOUNT);
            $total_int = $loan_setup->get(LOAN_SETUP::TIL_FINANCE_CHARGE);
            $num_payments = $loan_setup->get(LOAN_SETUP::LOAN_TERM);
            $contract_dt = DateTime::createFromFormat('U', $loan_setup->get(LOAN_SETUP::CONTRACT_DATE))->format('m/d/Y');
            $late_fee_amount = $loan_setup->get(LOAN_SETUP::LATE_FEE_AMT);
            $late_fee_percent = $loan_setup->get(LOAN_SETUP::LATE_FEE_PERCENT);
            $late_fee_type = $loan_setup->get(LOAN_SETUP::LATE_FEE_TYPE__C);
            $late_fee_calc = $loan_setup->get(LOAN_SETUP::LATE_FEE_CALC__C);
            $late_fee_percent_base = $loan_setup->get(LOAN_SETUP::LATE_FEE_PERC_BASE__C);
            $late_fee_grace_period = $loan_setup->get(LOAN_SETUP::GRACE_DAYS);

            $transactions = $loan->get(LOAN::TRANSACTIONS);
            // we can now iterate over the array and gather our total numbers and prep our output
            $payment_schedule = array(array());
            foreach ($transactions as $trans) {
                $type = $trans->get(LOAN_TRANSACTIONS::TYPE);
                if ($type == "scheduledPayment") {
                    $payment_nbr = $trans->get(LOAN_TRANSACTIONS::PERIOD) + 1;
                    $payment_period = $trans->get(LOAN_TRANSACTIONS::PERIOD);
                    $amt = $trans->get(LOAN_TRANSACTIONS::CHARGE_AMOUNT);
                    $prin = $trans->get(LOAN_TRANSACTIONS::CHARGE_PRINCIPAL);
                    $int = $trans->get(LOAN_TRANSACTIONS::CHARGE_INTEREST);
                    $dt = DateTime::createFromFormat('U', $trans->get(LOAN_TRANSACTIONS::DATE))->format('m/d/Y');
                    $payment_schedule[$payment_period]["payment_dt"] = $dt;
                    $payment_schedule[$payment_period]["payment_amt"] = $amt;
                    $payment_schedule[$payment_period]["payment_int"] = $int;
                    $payment_schedule[$payment_period]["payment_prin"] = $prin;
                    $payment_schedule[$payment_period]["payment_nbr"] = $payment_nbr;
                }
            }

        }

    } catch (Exception $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "There was a problem getting the loan details for $display_id.  Exception is $e";
        return $return_array;
    }
    //Prepare Return Array
    $return_array = array();
    $return_array["return_value"] = isset($return_value) ? $return_value : 1;
    $return_array["return_message"] = isset($return_message) ? $return_message : 'Error: No Return Message Set';
    $return_array["total_due"] = isset($total_due) ? $total_due : 0;
    $return_array["total_prin"] = isset($total_prin) ? $total_prin : 0;
    $return_array["total_int"] = isset($total_int) ? $total_int : 0;
    $return_array["apr"] = isset($apr) ? $apr : 0;
    $return_array["num_payments"] = isset($num_payments) ? $num_payments : 0;
    $return_array["late_fee_amt"] = isset($late_fee_amount) ? $late_fee_amount : 0;
    $return_array["late_fee_calc"] = isset($late_fee_calc) ? $late_fee_calc : 0;
    $return_array["late_fee_pct"] = isset($late_fee_percent) ? $late_fee_percent : 0;
    $return_array["late_fee_type"] = isset($late_fee_type) ? $late_fee_type : 0;
    $return_array["returned_item_fee_amt"] = isset($returned_item_fee_amt) ? $returned_item_fee_amt : 0;
    $return_array["late_fee_percent_base"] = isset($late_fee_percent_base) ? $late_fee_percent_base : 0;
    $return_array["late_fee_grace_period"] = isset($late_fee_grace_period) ? $late_fee_grace_period : 0;
    $return_array["contract_dt"] = isset($contract_dt) ? $contract_dt : '';
    $return_array["maturity_dt"] = isset($maturity_dt) ? $maturity_dt : '';
    $return_array["payment_schedule"] = isset($payment_schedule) ? $payment_schedule : '';
    $return_array["loan_id"] = isset($id) ? $id : '';
    $return_array["loan_settings_id"] = isset($loan_settings_id) ? $loan_settings_id : '';
    $return_array["loan_setup_id"] = isset($loan_setup_id) ? $loan_setup_id : '';
    $return_array["loan_status"] = isset($loan_status) ? $loan_status : '';
    $return_array["loan_sub_status"] = isset($loan_sub_status) ? $loan_sub_status : '';
    $return_array["active_ind"] = isset($active_ind) ? $active_ind : '';
    $return_array["loan_setup_active_ind"] = isset($loan_setup_active_ind) ? $loan_setup_active_ind : '';

    return $return_array;

}

/*****************************************************************************************************
 * Function mm_get_db_records($table_name, $key_name, $key_value)
 *****************************************************************************************************/
function mm_get_db_records($table_name, $key_name, $key_value) {
    error_log("flow: mm_get_db_records\n");
    $return_array = array();
    $bind_params = array();

    $bind_params[0] = $key_value;
    try {
        $sql_string = "Select * from $table_name where $key_name = ?";
        $conn = mm_get_pdo_connection();
        $stmt = $conn->prepare($sql_string);
        $stmt->execute($bind_params);
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $row_count = $stmt->rowCount();
    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "There was an issue executing the query $sql_string.  Error: $e";
        return $return_array;
    }

    $return_array["return_value"] = 0;
    $return_array["return_message"] = "The query was executed successfully";
    $return_array["results"] = $results;
    $return_array["result_count"] = $row_count;

    return $return_array;
}

/********************************************************************************************************
 * Function mm_get_loan_agreement_details($loan_agreement_nbr)
 ********************************************************************************************************/
function mm_get_loan_agreement_details($loan_agreement_nbr) {
    error_log("flow: mm_get_loan_agreement_details (loan_agreement_nbr: $loan_agreement_nbr)\n");
    //Need to add error handling
    $return_array = array();
    $sql_string = "select * from mm_loan_agreement where loan_agreement_nbr = ?";
    $conn = mm_get_db_connection();
    $stmt = $conn->prepare($sql_string);
    $stmt->bind_param('i', $loan_agreement_nbr);
    $stmt->execute();
    $results = $stmt->get_result();
    $row = $results->fetch_assoc();

    foreach ($row as $key => $value) {
        $return_array["$key"] = $value;
    }

    $return_array["return_value"] = 0;
    return $return_array;

}

/*****************************************************************************************
 * Function mm_get_account_details($account_nbr)
 *****************************************************************************************/
function mm_get_account_details($account_nbr) {
    error_log("flow: mm_get_account_details\n");
    $return_array = array();
    $sql_string = "select * from mm_account where account_nbr = ?";
    $conn = mm_get_db_connection();
    $stmt = $conn->prepare($sql_string);
    $stmt->bind_param('i', $account_nbr);
    $stmt->execute();
    $results = $stmt->get_result();
    $row = $results->fetch_assoc();

    foreach ($row as $key => $value) {
        $return_array["$key"] = $value;
    }

    $return_array["return_value"] = 0;
    return $return_array;

}

/****************************************************************************************
 * Function mm_get_payment_schedule($loan_agreement_nbr)
 ****************************************************************************************/
function mm_get_payment_schedule($loan_agreement_nbr) {
    error_log("flow: mm_get_payment_schedule\n");
    //Need to add error handling
    $return_array = array();
    $payment_schedule = array(array());
    $sql_string = "select * from mm_payment_schedule where loan_agreement_nbr = ? order by payment_nbr asc";
    $conn = mm_get_db_connection();
    $stmt = $conn->prepare($sql_string);
    $stmt->bind_param('i', $loan_agreement_nbr);
    $stmt->execute();
    $results = $stmt->get_result();
    $i = 0;
    while ($row = $results->fetch_assoc()) {
        foreach ($row as $key => $value) {
            $payment_schedule[$i][$key] = $value;
        }
        $i += 1;
    }

    $return_array["payment_schedule"] = $payment_schedule;
    $return_array["return_value"] = 0;

    return $return_array;
}

/******************************************************************************************
 * Function mm_get_account_details_from_email_address($email_address)
 ******************************************************************************************/
function mm_get_account_details_from_email_address($email_address) {
    error_log("flow: mm_get_account_details_from_email_address ($email_address)\n");
    //NEED TO ADD ERROR HANDLING TO THIS FUNCTION
    $return_array = array();
    //$account_nbr = 0;
    $sql_string = "select * from mm_account where email_address = ?";
    $conn = mm_get_db_connection();
    $stmt = $conn->prepare($sql_string);
    $stmt->bind_param('s', $email_address);
    $stmt->execute();
    $result = $stmt->get_result();
    $num_rows = $result->num_rows;
    if ($num_rows == 0) {
        //Need to add default value setting for accounts with mm account
        $return_array["account_nbr"] = 0;
    } else {
        $row = $result->fetch_assoc();
        foreach ($row as $key => $value) {
            $return_array["$key"] = $value;
        }
    }

    $return_array["return_value"] = 0;
    $return_array["return_message"] = "Account Lookup Successful";

    return $return_array;
}

/*******************************************************************************************
 * Function mm_get_most_recent_application_details($account_nbr);
 *******************************************************************************************/

function mm_get_most_recent_application_details($account_nbr) {
    error_log("flow: mm_get_most_recent_application_details ($account_nbr)\n");
    // todo: NEED TO ADD ERROR HANDLING
    //Excludes voided applications (status = 11)
    $sql_string = "select * from mm_application where application_nbr = (select max(application_nbr) as application_nbr from mm_application where account_nbr = ? and application_status <> 11)";
    $conn = mm_get_db_connection();
    $stmt = $conn->prepare($sql_string);
    $stmt->bind_param('i', $account_nbr);
    $stmt->execute();
    $result = $stmt->get_result();
    $num_rows = $result->num_rows;
    $row = $result->fetch_assoc();
    $app_data = array();
    if ($num_rows == 0) {
        error_log("mark: most recent application not found.\n");
    } else {
        foreach ($row as $key => $value) {
            $app_data["$key"] = $value;
        }
    }
    $return_array = array();
    $return_array["return_value"] = 0;
    $return_array["return_message"] = "Success";
    $return_array["app_data"] = $app_data;
    $return_array["num_apps"] = $num_rows;

    error_Log("mark: returning from mm_get_most_recent_application_details, $num_rows\n");
    return $return_array;

}

/**
 * @param string $pay_freq
 * @param string $first_pay_dt
 * @param String $contract_dt
 * @return string
 */
function mm_calculate_future_pay_dt_CA_or_WI(string $pay_freq, string $first_pay_dt, String $contract_dt) {
    error_log("flow: mm_calculate_future_pay_dt_CA_or_WI($pay_freq, $first_pay_dt, $contract_dt)\n");
    $dt_first_pay_date = new DateTime($first_pay_dt);
    $dt_answer_date = clone $dt_first_pay_date;
    $dt_contract_date = new DateTime($contract_dt);
    $dt_next_pay_date = clone $dt_contract_date;
    $last_day_of_month_of_contract_dt = cal_days_in_month(CAL_GREGORIAN, $dt_contract_date->format('n'), $dt_contract_date->format('Y'));
    switch (strtoupper($pay_freq)) {
        case 'W':
        case 'B':
            // loop to find the next pay date that is >= 15 days
            $tmp = $dt_answer_date->diff($dt_contract_date)->days;
            while ($tmp < 15) {
                $dt_answer_date->modify("+14 day");
                $tmp = $dt_answer_date->diff($dt_contract_date)->days;
            }
            $dt_next_pay_date = $dt_answer_date;
            break;
        case 'M':
            // test current month
            $dom_paid = $dt_first_pay_date->format('j');
            $tmp = $dt_first_pay_date->diff($dt_contract_date)->days;
            $dt_answer_date = $dt_first_pay_date;
            if ($tmp <= 15) {
                $dt_first_pay_date->modify("+1 month");
                $dt_answer_date = $dt_first_pay_date;
            }
            $dt_next_pay_date = $dt_answer_date;
            break;
        case 'S1':
            $dom = $dt_next_pay_date->format('j');
            if ($dom <= 15) {
                $dt_next_pay_date->modify('first day of next month');
            } else if (($last_day_of_month_of_contract_dt - $dom) >= 15) {
                $dt_next_pay_date->modify('first day of next month');
            } else {
                // can't be the first day of the next month, so must be the 15th of the next month
                $dt_next_pay_date->modify('first day of next month');
                $dt_next_pay_date->modify('+14 day');
            }
            break;
        case 'S2':
            $dom = $dt_next_pay_date->format('j');
            if ($dom <= 15) {
                $dt_next_pay_date->modify('last day of this month');
            } else if (($last_day_of_month_of_contract_dt - $dom) >= 15) {
                $dt_next_pay_date->modify('last day of this month');
            } else {
                $dt_next_pay_date->modify('first day of next month');
                $dt_next_pay_date->modify('+14 day');
            }
            break;
        default:
    }

    $tmp = $dt_next_pay_date->format("m/d/Y");
    error_log("mark: returning the future CA pay date: $tmp");
    return $tmp;
}

/*************************************************************
 * mm_calculate_future_pay_dt($pay_freq, $first_pay_dt, $payment_nbr)
 * pay_freq - 'W' - Weekly, 'B' - Bi-Weekly, 'M' - Monthly, 'S1' Semi-Monthly 1st and 15th, 'S2' - Semi-Montly 15th and last day of the month.
 *
 * Returns a date in the format MM/DD/YYYY which represents the next pay date from the date provided.  The current date is excluded if it is a paydate.
 *************************************************************/
function mm_calculate_future_pay_dt($pay_freq, $first_pay_dt, $payment_nbr) {
    error_log("flow: mm_calculate_future_pay_dt($pay_freq, $first_pay_dt, $payment_nbr)\n");
    //error_log("mark: starting mm_calculate_future_pay_dt, pay_freq: ".print_r($pay_freq, true).", first_pay_date: ".print_r($first_pay_dt, true).", payment_nbr: ".print_r($payment_nbr, true));
    $date = new DateTime($first_pay_dt);

    // mark was here: adjusting for only bi-weekly payments
    /* if ($pay_freq == 'W') {
    $num_days_to_add = $payment_nbr * 7;
    $date->modify("+{$num_days_to_add} day");
    } else */

    if ($pay_freq == 'W' || $pay_freq == 'B') {
        $num_days_to_add = $payment_nbr * 14;
        $date->modify("+{$num_days_to_add} day");
    } else if ($pay_freq == 'M') {
        //$date = new DateTime($first_pay_dt);
        $start_day = $date->format('j');
        $date->modify("+{$payment_nbr} month");
        $end_day = $date->format('j');
        if ($start_day != $end_day) {
            $date->modify('last day of last month');
        }
    } else if ($pay_freq == 'S1') {
        $i = 0;
        while ($i < $payment_nbr) {
            $dom = $date->format('j');
            if ($dom == 1) {
                $date->modify("+14 day");
            } else {
                $date->modify('first day of next month');
            }
            $i = $i + 1;
        }
    } else if ($pay_freq == 'S2') {
        $i = 0;
        while ($i < $payment_nbr) {
            $dom = $date->format('j');
            if ($dom == 15) {
                $date->modify('last day of this month');
            } else {
                $date->modify('+ 15 day');
            }

            $i = $i + 1;
        }

    }

    $next_pay_dt = $date->format("m/d/Y");

    return $next_pay_dt;
}

/***************************************************************************************
 * Function mm_calculate_contract_date($curr_timestamp) - Takes a current time stamp and returns a date in the format mm/dd/YYYY which represents the proposed contract date.
 **************************************************************************************
 * @param $curr_timestamp
 * @param string $deposit_type
 * @return string
 */
function mm_calculate_contract_date($curr_timestamp, $deposit_type = "ACH") {
    error_log("flow: mm_calculate_contract_date: ($curr_timestamp, $deposit_type)\n");
    $cutoff_time = 17;  // 5pm local time
    $date = new DateTime($curr_timestamp);
    $curr_date = $date->format('m/d/Y');
    $curr_hour = $date->format('H');
    $curr_dow = $date->format('D');

    if ($deposit_type == "Check") {
        $date->modify('+10 days');
        $date->setTime(0, 0, 0);
        $curr_date = $date->format('m/d/Y');
        $curr_dow = $date->format('D');
    } else {
        if (($curr_dow == 'Fri' && $curr_hour >= $cutoff_time) || $curr_dow == 'Sat' || ($curr_dow == 'Sun' && $curr_hour < $cutoff_time)) {
            //The date is between Friday after cutoff and before Sunday's cutoff so make the effective date Monday
            while ($curr_dow != 'Mon') {
                $date->modify('+1 day');
                $curr_date = $date->format('m/d/Y');
                $curr_dow = $date->format('D');
            }
        } else if (($curr_dow == "Sun" && $curr_hour >= $cutoff_time) || ($curr_dow == 'Mon' && $curr_hour < $cutoff_time)) {
            //The date is between Sunday after cutoff and before Monday's cutoff so make the effective date Tuesday
            while ($curr_dow != 'Tue') {
                $date->modify('+1 day');
                $curr_date = $date->format('m/d/Y');
                $curr_dow = $date->format('D');
            }
        } else if (($curr_dow == 'Mon' && $curr_hour >= $cutoff_time) || ($curr_dow == 'Tue' && $curr_hour < $cutoff_time)) {
            //The date is between Monday after cutoff and Tuesday before cutoff so make the effective date Wednesday
            while ($curr_dow != 'Wed') {
                $date->modify('+1 day');
                $curr_date = $date->format('m/d/Y');
                $curr_dow = $date->format('D');
            }
        } else if (($curr_dow == 'Tue' && $curr_hour >= $cutoff_time) || ($curr_dow == 'Wed' && $curr_hour < $cutoff_time)) {
            //The date is between Tuesday after cutoff and Wednesday before cutoff so make the effective date Thursday
            while ($curr_dow != 'Thu') {
                $date->modify('+1 day');
                $curr_date = $date->format('m/d/Y');
                $curr_dow = $date->format('D');
            }
        } else if (($curr_dow == 'Wed' && $curr_hour >= $cutoff_time) || ($curr_dow == 'Thu' && $curr_hour < $cutoff_time)) {
            //The date is between Wednesday after cutoff and Thursday before cutoff so make the effective date Friday
            while ($curr_dow != 'Fri') {
                $date->modify('+1 day');
                $curr_date = $date->format('m/d/Y');
                $curr_dow = $date->format('D');
            }
        } else if (($curr_dow == 'Thu' && $curr_hour >= $cutoff_time) || ($curr_dow == 'Fri' && $curr_hour < $cutoff_time)) {
            //The date is between Thursday after cutoff and Friday before cutoff so make the effective date Monday
            while ($curr_dow != 'Mon') {
                $date->modify('+1 day');
                $curr_date = $date->format('m/d/Y');
                $curr_dow = $date->format('D');
            }
        }
    }

    //Now that the BAU effective date is set.  Check to make sure that the date isn't a holiday.  If it is then keep moving the date until you reach the next business day.
    if (mm_is_bank_holiday($curr_date)) {
        if ($curr_dow == 'Sat') {
            $date->modify('+2 days');
            $curr_date = $date->format('m/d/Y');
        } else if ($curr_dow == 'Fri') {
            $date->modify('+3 days');
            $curr_date = $date->format('m/d/Y');
        } else {
            $date->modify('+1 days');
            $curr_date = $date->format('m/d/Y');
        }

    }

    return $curr_date;
}

/****************************************************************************************************************************************
 * Function mm_process_verifications()
 ****************************************************************************************************************************************/

function mm_process_verifications($preverif_lead_id_, $mm_application_nbr, $requested_amt, $requested_deposit_type) {
    error_log("flow: mm_process_verifications\n");
    $return_array = array();

    $preverif_results = mm_submit_preverifications($mm_application_nbr);
    $vf_decision = $preverif_results["vf_decision"];
    $vf_state = $preverif_results["state"];

    $return_array["vf_decision"] = $vf_decision;
    $return_array["vf_state"] = $vf_state;

    return $return_array;

}

/********************************************************************
 * Function process_application - Wrapper function called by the website to handle a customer submitting an application
 ********************************************************************/
function process_application($form_lead_id = 0) {
    error_log("flow: process_application ($form_lead_id)\n");

    $app_data = mm_get_lead_details($form_lead_id);
    $app_email_address = isset($app_data["email_address"]) ? $app_data["email_address"] : '';

    $return_array = mm_submit_application($app_data);
    if ($return_array["return_value"] == 0) {
        // this is for subsequent mm_get_application_details
        error_log_dump_associative_array("mark: in process_application after submit_application", $return_array);
        $tmp = $return_array["mm_account_nbr"];
        mm_set_cache_value('app_data_' . $tmp, $app_data);
    } else {
        $reason = isset($return_array['return_message']) ? $return_array['return_message'] : '';
        mm_log_error('process_application', "The return_value was not 0 for gf_lead_id: $form_lead_id, email: $app_email_address, reason: $reason");
    }

    return $return_array;
}

/**********************************************************************************************
 * Function mm_get_lender_details - Function takes a state code and returns the information needed
 **********************************************************************************************/
function mm_get_lender_details($state_cd) {
    error_log("flow: mm_get_lender_details\n");
    // NEED TO ADD ERROR HANDLING TO THIS FUNCTION
    // $return_array = array();

    $sql_string = "select * from mm_lender where state_cd = ?";
    $conn = mm_get_db_connection();
    $stmt = $conn->prepare($sql_string);
    $stmt->bind_param('s', $state_cd);
    $stmt->execute();
    $results = $stmt->get_result();
    $num_rows = $results->num_rows;
    if ($num_rows == 0) {
        //There are no rows returned so set the variables very conservatively.
        $row["lender_nbr"] = 0;
        $row["legal_name"] = SITE_NAME . " LLC";
        $row["dba_name"] = SITE_NAME . "LLC";
        $row["state_cd"] = "$state_cd";
        $row["display_state"] = mm_state_cd_to_display($state_cd);
        $row["mailing_street_address"] = SUPPORT_ADDRESS;
        $row["mailing_city"] = SUPPORT_CITY;
        $row["mailing_state_cd"] = SUPPORT_STATE;
        $row["mailing_zip_code"] = SUPPORT_ZIP;
        $row["physical_street_address"] = PHYSICAL_ADDRESS;
        $row["physical_city"] = PHYSICAL_CITY;
        $row["physical_state_cd"] = PHYSICAL_STATE;
        $row["physical_zip_code"] = PHYSICAL_ZIP;
        $row["phone_nbr"] = SUPPORT_PHONE;
        $row["fax_nbr"] = SUPPORT_FAX;
        $row["email_address"] = SUPPORT_EMAIL;
        $row["late_fee_amt"] = 0;
        $row["late_fee_pct"] = 0;
        $row["late_fee_calc"] = "AMT";
        $row["late_fee_grace_period"] = 10;
        $row["returned_item_fee_amt"] = 0;

    } else {
        $row = $results->fetch_assoc();
    }
    $return_array = $row;
    $return_array["return_value"] = 0;

    return $return_array;
}

/******************************************************************************************************************
 * Function mm_get_noaa_text($noaa_cd) - Returns the text needed for the
 ******************************************************************************************************************/
function mm_get_noaa_text($noaa_cd) {
    error_log("flow: mm_get_noaa_text\n");
    // Mark was here:  what is the purpose of this function?
    $return_string = '';
    switch ($noaa_cd) {
        case 10:
            $return_string = "Some NOAA Text";
            break;

    }
    //Temp Code
    $return_string = "This is some text that should go in the dynamic section of the NOAA.";

    return $return_string;
}

/*****************************************************************************************************************************
 * Function mm_generate_noaa_pdf($application_nbr, $noaa_cd)
 ****************************************************************************************************************************
 * @param $application_nbr
 * @param $noaa_cd
 * @param string $score_data
 * @return array
 */
function mm_generate_noaa_pdf($application_nbr, $noaa_cd, $score_data = '') {
    error_log("flow: mm_generate_noaa_pdf ($application_nbr, $noaa_cd, $score_data)\n");
    //Initialize Variables
    $return_array = array();
    //$customer_info = array();
    //$noaa_text = array();
    //$header = array();
    //$lender_info = array();
    $return_value = 0;
    $return_message = "";
    //Get Customer data from mm database
    $customer_array = mm_get_application_details($application_nbr);
    $customer_info = $customer_array["app_data"];
    $account_nbr = $customer_info["account_nbr"];
    $customer_name = $customer_info["first_name"] . " " . $customer_info["last_name"];
    $customer_street = $customer_info["street_address"] . " " . $customer_info["appt_suite"];
    $customer_city_state_zip = $customer_info["city"] . ", " . $customer_info["state"] . " " . $customer_info["zip_code"];
    $time = strtotime($customer_info["create_dt"]);
    $application_dt = date('m/d/Y', $time);

    //Get Noaa text
    $noaa_text = mm_get_noaa_text($noaa_cd);
    if ($noaa_text == '') {
        //Insert error handling of Missing NOAA Text
    }

    //Get Lender Information
    $lender_info = mm_get_lender_details($customer_info["state"]);
    $lender_legal_name = $lender_info["legal_name"];
    $lender_dba_name = $lender_info["dba_name"];
    $lender_address_line_1 = $lender_info["mailing_street_address"];
    $lender_address_line_2 = $lender_info["mailing_city"] . ", " . $lender_info["mailing_state_cd"] . " " . $lender_info["mailing_zip_code"];
    $lender_phone_nbr = $lender_info["phone_nbr"];
    $lender_fax_nbr = $lender_info["fax_nbr"];
    $title_name = "$lender_legal_name d/b/a $lender_dba_name";

    //Build the NOAA
    $pdf = new PDF('P', 'mm', 'Letter');
    $pdf->SetMargins(4, 4, 4);
    $pdf->AliasNbPages();
    $pdf->AddPage();

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(0, 10, $title_name, 0, 0, 'C');
    $pdf->Ln(4);
    $pdf->Cell(0, 10, $lender_address_line_1, 0, 0, 'C');
    $pdf->Ln(4);
    $pdf->Cell(0, 10, $lender_address_line_2, 0, 0, 'C');
    $pdf->Ln(4);
    $pdf->Cell(0, 10, "Phone: $lender_phone_nbr   Fax:$lender_fax_nbr ", 0, 0, 'C');
    $pdf->Ln(4);
    $pdf->Cell(0, 10, "Attn: Legal Department", 0, 0, 'C');
    $pdf->Ln(10);
    $pdf->Cell(0, 10, "Notice of Adverse Action", 0, 0, 'C');
    $pdf->Ln(10);
    $pdf->setFont('Arial', '', 10);
    $pdf->Cell(30, 10, "Application Date: ", 0, 0, 'L');
    $pdf->Cell(0, 10, "$application_dt", 0, 0, 'L');
    $pdf->Ln(10);
    $pdf->Cell(30, 10, "Applicant's Name:", 0, 0, 'L');
    $pdf->Cell(0, 10, "$customer_name", 0, 0, 'L');
    $pdf->Ln(5);
    $pdf->Cell(30, 10, "Address:", 0, 0, 'L');
    $pdf->Cell(0, 10, "$customer_street", 0, 0, 'L');
    $pdf->Ln(5);
    $pdf->Cell(30, 10, "", 0, 0, 'L');
    $pdf->Cell(0, 10, "$customer_city_state_zip", 0, 0, 'L');
    $pdf->Ln(10);
    $pdf->Cell(0, 10, "Dear Applicant,", 0, 0, 'L');
    $pdf->Ln(10);
    $pdf->Cell(0, 10, "Thank you for applying to $lender_dba_name for a loan.", 0, 0, 'L');
    $pdf->Ln(10);
    $pdf->MultiCell(0, 4, "After carefully reviewing your application, we are sorry to advise you that we cannot grant a loan to you at this time.  If you would like a statement of specific reasons why your application was denied, please contact us at the address below within SIXTY (60) days of the date of this letter.  We will provide you with the statement of reasons within THIRTY (30) days after receiving your request.", 0, 'L');
    $pdf->Ln(4);
    $pdf->Cell(0, 10, $title_name, 0, 0, 'C');
    $pdf->Ln(4);
    $pdf->Cell(0, 10, $lender_address_line_1, 0, 0, 'C');
    $pdf->Ln(4);
    $pdf->Cell(0, 10, $lender_address_line_2, 0, 0, 'C');
    $pdf->Ln(4);
    $pdf->Cell(0, 10, "Phone: $lender_phone_nbr   Fax:$lender_fax_nbr ", 0, 0, 'C');
    $pdf->Ln(4);
    $pdf->Cell(0, 10, "Attn: Legal Department", 0, 0, 'C');
    $pdf->Ln(12);

    $pdf->MultiCell(0, 4, "If we obtained information from a consumer reporting agency as part of our consideration of your application, its name, address, and toll-free telephone number is shown below.  The reporting agency played no part in our decision and is unable to supply specific reasons why we have denied credit to you.  You have a right under the Fair Credit Reporting Act to know the information contained in your credit file at the consumer reporting agency.  You have a right to a free copy of your report from the reporting agency, if you request it no later than SIXTY (60) days after you receive this notice.  In addition, if you find that any information contained in the report you received is inaccurate or incomplete, you have the right to dispute the matter with the reporting agency.  You can find out about the information contained in your file (if one was used) by contacting:", 0, 'L');

    $pdf->Ln(5);
    $pdf->Cell(0, 10, "Clarity Services, Inc.", 0, 0, 'C');
    $pdf->Ln(4);
    $pdf->Cell(0, 10, "PO BOX 5717", 0, 0, 'C');
    $pdf->Ln(4);
    $pdf->Cell(0, 10, "Clearwater, FL 33758", 0, 0, 'C');
    $pdf->Ln(4);
    $pdf->Cell(0, 10, "Phone: (866) 390-3118", 0, 0, 'C');
    $pdf->Ln(10);

    if (isset($score_data) && is_array($score_data) && count($score_data) > 0) {
        $pdf->MultiCell(0, 4, "We also obtained your credit score from the consumer reporting agency and used it in making our credit decision.  Your credit score is a number that reflects the information in your consumer report.  Your credit score can change, depending on how the information in your consumer report changes.", 0, 'L');
        $pdf->Ln(2);
        $pdf->Cell(0, 8, "Your credit score: {$score_data['credit_score_value']}", 0, 1, 'L');
        $pdf->Cell(0, 8, "Date: " . date("m/d/y"), 0, 1, 'L');
        $pdf->Cell(0, 8, "Scores range from a low of {$score_data['credit_score_minimum']} to a high of {$score_data['credit_score_maximum']}.", 0, 1, 'L');
        $pdf->Cell(0, 8, "Key factors that adversely affected your credit score:", 0, 1, 'L');
        $pdf->Cell(0, 8, "{$score_data['credit_score_key_factor_1']}", 0, 1, 'L');
        $pdf->Cell(0, 8, "{$score_data['credit_score_key_factor_2']}", 0, 1, 'L');
        $pdf->Cell(0, 8, "{$score_data['credit_score_key_factor_3']}", 0, 1, 'L');
        $pdf->Cell(0, 8, "{$score_data['credit_score_key_factor_4']}", 0, 1, 'L');
        //$pdf->MultiCell(0, 8, "[In addition to the reasons listed above, insert \"Number of recent inquiries on consumer report\" if it was a key factor]", 0, 'L');
        $pdf->MultiCell(0, 8, "If you have any questions regarding your credit score, you should contact {$score_data['credit_reporting_agency_name']} at:", 0, 'L');
        $pdf->Cell(0, 8, "Address: {$score_data['credit_reporting_agency_address']}", 0, 1, 'L');
        $pdf->Cell(0, 8, "Telephone number: {$score_data['credit_reporting_agency_phone_nbr']}", 0, 1, 'L');
        $pdf->MultiCell(0, 8, "Our credit decision was based in whole or in part on information obtained from an affiliate or from an outside source other than a consumer reporting agency. Under the Fair Credit Reporting Act, you have the right to make a written request, no later than 60 days after you receive this notice, for disclosure of the nature of this information.", 0, 'L');
        $pdf->Cell(0, 8, "$lender_legal_name", 0, 1, 'L');
        $pdf->Cell(0, 8, "$lender_address_line_1", 0, 1, 'L');
        $pdf->Cell(0, 8, "$lender_address_line_2", 0, 1, 'L');
        $pdf->Cell(0, 8, "$lender_phone_nbr", 0, 1, 'L');
    }
    $pdf->Ln(10);
    $pdf->Cell(0, 5, "Sincerely,", 0, 0, 'L');
    $pdf->Ln(10);
    $pdf->Cell(0, 5, "$title_name", 0, 0, 'L');
    $pdf->Ln(15);
    $pdf->setFont('Arial', 'I', 10);
    $pdf->MultiCell(0, 4, "Notice: The federal Equal Credit Opportunity Act prohibits creditors from discriminating against credit applicants on the basis of race, color, religion, national origin, sex, marital status, age (provided the applicant has the capacity to enter into a binding contract); because all or part of the applicant's income derives from any public assistance program; or because the applicant has in good faith exercised any right under the Consumer Credit Protection Act.  The federal agency that administers compliance with this law concerning this creditor is Federal Trade Commission, Equal Credit Opportunity, Washington, D.C. 20580", 0, 'L');

    //Store the NOAA
    $doc_path = mm_get_document_path();
    $doc_path = $doc_path . "$account_nbr/";
    if (!file_exists($doc_path)) {
        //Doc Path doesn't exist so create it
        mkdir($doc_path);
    }
    $doc_path = $doc_path . "$application_nbr/";
    if (!file_exists($doc_path)) {
        //Doc Path doesn't exist so create it
        mkdir($doc_path);
    }
    $doc_path = $doc_path . "noaa_$application_nbr.pdf";
    $pdf->Output('F', $doc_path);

    //Send the document to our secure location
    $temp = mm_send_ftp_document($doc_path, "noaa_$application_nbr.pdf", "customer_documents/$account_nbr/$application_nbr/", "B");

    //Delete the temporary document - log an error if it doesn't work

    if (!unlink($doc_path)) {
        mm_log_error('mm_generate_noaa_pdf', "Unable to delete the file $doc_path");
    } else {
        //The file was deleted successfully no need for action
    }

    //set the doc path to the storage location not the temp location
    $doc_path = mm_get_document_storage_path();
    $doc_path = $doc_path . "$account_nbr/$application_nbr/noaa_$application_nbr.pdf";

    //Build the return values
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["document_path"] = $doc_path;

    //Return NOAA information
    return $return_array;

}

/**********************************************************************************
 * Function mm_schedule_email_message()
 **********************************************************************************/
function mm_schedule_email_message($json_message) {
    error_log("flow: mm_schedule_email_message\n");
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    //$parm_array = array();
    $create_dt = new DateTime();
    $create_dt_string = $create_dt->format('Y-m-d H:i:s');
    $parm_array = json_decode($json_message, true);
    $scheduled_dt = isset($parm_array["scheduled_dt"]) ? $parm_array["scheduled_dt"] : $create_dt_string;
    $to = isset($parm_array["to"]) ? $parm_array["to"] : '';
    $from = isset($parm_array["from"]) ? $parm_array["from"] : '';
    $to_name = isset($parm_array["to_name"]) ? $parm_array["to_name"] : '';
    $from_name = isset($parm_array["from_name"]) ? $parm_array["from_name"] : '';
    $subject = isset($parm_array["subject"]) ? $parm_array["subject"] : '';
    $txt_body = isset($parm_array["txt_body"]) ? $parm_array["txt_body"] : '';
    $attachment = isset($parm_array["attachment"]) ? $parm_array["attachment"] : '';
    $account_results = mm_get_account_details_from_email_address($to);
    $account_nbr = $account_results["account_nbr"];
    $html_body = isset($parm_array["html_body"]) ? $parm_array["html_body"] : "<html><body><p>$txt_body</body></html>";
    $status = "READY TO SEND";

    //Build Insert Query
    $sql_string = "insert into mm_email_message_queue(email_to, email_to_name, email_from, email_from_name, subject, txt_body, html_body, attachment,  scheduled_dt, status, create_dt, account_nbr) values (?,?,?,?,?,?,?,?,?,?,?,?)";

    try {
        $conn = mm_get_pdo_connection();
        $stmt = $conn->prepare($sql_string);
        $parms = [$to, $to_name, $from, $from_name, $subject, $txt_body, $html_body, $attachment, $scheduled_dt, $status, $create_dt_string, $account_nbr];
        $stmt->execute($parms);
        $email_message_nbr = $conn->lastInsertId();

    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "$e";
        $return_array["sql_string"] = $sql_string;
        return $return_array;
    }

    if (is_resource($conn)) {
        $conn = null;
    }

    //Setup Return Array
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["email_message_nbr"] = isset($email_message_nbr) ? $email_message_nbr : 0;

    return $return_array;

}

/**********************************************************************************************************************************************************
 * Function mm_log_error() - Generic function to handle the processing of an error.  The function will try to add the error to the database, add it to a log file on the disk drive, and send an notification that there was an error.
 **********************************************************************************************************************************************************/
function mm_log_error($source, $message, $error_cd = 0) {
    error_log("flow: mm_log_error\n");
    $log_path = mm_get_log_path();
    $create_dt = date("Y-m-d H:i:s");
    $create_dt_file_name = date("Y-m-dH:i:s");
    $microtime = microtime(true);
    $log_filename = "mm_error_log" . $create_dt_file_name . "." . $microtime . ".csv";
    $log_filepath = $log_path . "errors/";
    $log_file = $log_filepath . $log_filename;

    $return_value = 0;

    //Add a record to the log file
    $logfile_message = $create_dt . "," . $source . "," . $error_cd . "," . $message . "\n";
    $myfile = fopen($log_file, "w");
    fwrite($myfile, $logfile_message);
    fclose($myfile);

    // todo: handle $result or get rid of it
    $result = mm_send_ftp_document($log_file, $log_filename, "errors/", "A");
    unlink($log_file);

    //Add a record to the mm database documenting the error

    //Send an email to technical support notifying them of the error
    $to = 'jason.bumgarner@moneymart.com,mark.bench@moneymart.com';
    $from = 'MM Direct Monitoring <' . SUPPORT_EMAIL . '>';
    $subject = SITE_URI_PREFIX.'******MM ERROR*********';
    $body = "There was an error with the site: $logfile_message";
    $html_body = "<html><head></head><body><p>$logfile_message</p></body></html>";
    mm_send_email($to, $from, $subject, $body, $html_body);

    return $return_value;

}

/*****************************************************************************************************************************************
 * Function mm_log_uw_request($message)
 *****************************************************************************************************************************************/
function mm_log_uw_request($json_message, $application_nbr) {
    error_log("flow: mm_log_uw_request\n");

    $log_path = mm_get_log_path();
    //$create_dt = date("Y-m-d H:i:s");
    $create_dt_file_name = date("Y-m-dH:i:s");
    $microtime = microtime(true);
    $log_filename = "mm_uw_request_$application_nbr - " . $create_dt_file_name . "." . $microtime . ".json";
    $log_filepath = $log_path . "uw_requests/";
    $log_file = $log_filepath . $log_filename;
    $return_value = 0;

    //Add a record to the log file
    $logfile_message = $json_message;
    $myfile = fopen($log_file, "w");
    fwrite($myfile, $logfile_message);
    fclose($myfile);

    // todo: handle $result or get rid of it
    $result = mm_send_ftp_document($log_file, $log_filename, "uw_requests/", "A");
    if (!unlink($log_file)) {
        mm_log_error('mm_log_uw_request', "Error Deleting UW Request for Application $application_nbr");
    }

    //Add a record to the mm database documenting the error

    //Send an email to technical support notifying them of the error

    return $return_value;

}

/*****************************************************************************************************************************************
 * Function mm_log_uw_response($message, $app_id)
 *****************************************************************************************************************************************/
function mm_log_uw_response($json_message, $application_nbr) {
    error_log("flow: mm_log_uw_response\n");
    $log_path = mm_get_log_path();
    //$create_dt = date("Y-m-d H:i:s");
    $create_dt_file_name = date("Y-m-dH:i:s");
    $microtime = microtime(true);
    $log_filename = "mm_uw_response_$application_nbr - " . $create_dt_file_name . "." . $microtime . ".json";
    //$log_filepath = $log_path . "uw_responses/";
    $log_file = $log_path . $log_filename;
    $return_value = 0;

    //Add a record to the log file
    $logfile_message = $json_message;
    $myfile = fopen($log_file, "w");
    fwrite($myfile, $logfile_message);
    fclose($myfile);

    // todo: handle $result or get rid of it
    $result = mm_send_ftp_document($log_file, $log_filename, "uw_responses/", "A");
    if (!unlink($log_file)) {
        mm_log_error('mm_log_uw_responses', "Error Deleting UW Response for Application $application_nbr");
    }
    //Add a record to the mm database documenting the error

    //Send an email to technical support notifying them of the error

    return $return_value;

}

/*****************************************************************************************************************************************
 * Function mm_log_preverif_response($message, $application_nbr)
 *****************************************************************************************************************************************/
function mm_log_preverif_response($json_message, $application_nbr) {
    error_log("flow: mm_log_preverif_response\n");
    $log_path = mm_get_log_path();
    //$create_dt = date("Y-m-d H:i:s");
    $create_dt_file_name = date("Y-m-dH:i:s");
    $microtime = microtime(true);
    $log_filename = "mm_preverif_response_$application_nbr - " . $create_dt_file_name . "." . $microtime . ".json";
    //$log_filepath = $log_path . "preverif_responses/";
    $log_file = $log_path . $log_filename;
    $return_value = 0;

    //Add a record to the log file
    $logfile_message = $json_message;
    $myfile = fopen($log_file, "w");
    fwrite($myfile, $logfile_message);
    fclose($myfile);

    // todo: handle $result or get rid of it
    $result = mm_send_ftp_document($log_file, $log_filename, "preverif_responses/", "A");
    if (!unlink($log_file)) {
        mm_log_error('mm_log_uw_responses', "Error Deleting Preverification Response for Application $application_nbr");
    }
    //Add a record to the mm database documenting the error

    //Send an email to technical support notifying them of the error

    return $return_value;

}

/*****************************************************************************************************************************************
 * Function mm_log_preverif_request($message, $application_nbr)
 *****************************************************************************************************************************************/
function mm_log_preverif_request($json_message, $application_nbr) {
    error_log("flow: mm_log_preverif_request\n");
    $log_path = mm_get_log_path();
    //$create_dt = date("Y-m-d H:i:s");
    $create_dt_file_name = date("Y-m-dH:i:s");
    $microtime = microtime(true);
    $log_filename = "mm_preverif_request_$application_nbr - " . $create_dt_file_name . "." . $microtime . ".json";
    //$log_filepath = $log_path . "preverif_requests/";
    $log_file = $log_path . $log_filename;
    $return_value = 0;

    //Add a record to the log file
    $logfile_message = $json_message;
    $myfile = fopen($log_file, "w");
    fwrite($myfile, $logfile_message);
    fclose($myfile);

    // todo: handle $result or get rid of it
    $result = mm_send_ftp_document($log_file, $log_filename, "preverif_requests/", "A");
    if (!unlink($log_file)) {
        mm_log_error('mm_log_uw_responses', "Error Deleting Preverification Requests for Application $application_nbr");
    }

    //Add a record to the mm database documenting the error

    //Send an email to technical support notifying them of the error

    return $return_value;

}

/*****************************************************************************************
 * Function mm_build_preverif_request
 ****************************************************************************************
 * @param $app_data
 * @return array
 */
function mm_build_preverif_request(array $app_data): array {
    error_log("flow: mm_build_preverif_request\n");
    $return_array = array();
    $temp_array = array();
    $header_data = get_origination_system_connection_details();

    //Reformat Paydate from YYYY-MM-DD to MM
    $time = strtotime($app_data["next_pay_dt_1"]);
    $payment_date = date('mdY', $time);
    $state = strtoupper($app_data["state"]);
    $street_address = $app_data["street_address"];
    $income_source_1 = $app_data["income_source_1"];

    $temp_array["UserName"] = $header_data["UserName"];
    $temp_array["Password"] = $header_data["Password"];
    $temp_array["ApplicationSource"] = $header_data["ApplicationSource"];
    $temp_array["AuthToken"] = $header_data["AuthToken"];
    $temp_array["PortfolioID"] = $header_data["PortfolioID"];
    $temp_array["AcceptedLoanAmount"] = intval($app_data["requested_amt"]);
    $temp_array["ApplicationID"] = $app_data["orig_appid"];
    $temp_array["IPAddress"] = '1.1.1.1';
    $temp_array["PaymentDate"] = $payment_date;
    $temp_array["PaymentMode"] = $app_data["requested_deposit_type"];
    $temp_array["ReferenceID"] = $app_data["application_nbr"];

    $tmp_dt = new DateTime($app_data["create_dt"]);

    // Central Time Zone states are excluded from tests below as their time does not need to be modified.
    if ($state === 'HI') {
        $tmp_dt = $tmp_dt->modify("-5 Hours");
    } else if ($state === 'AK') {
        $tmp_dt = $tmp_dt->modify("-3 Hours");
    } else if (in_array($state, ["WA", "OR", "CA", "NV"])) {
        $tmp_dt = $tmp_dt->modify("-2 Hours");
    } else if (in_array($state, ["MT", "ID", "WY", "UT", "CO", "AZ", "NM"])) {
        $tmp_dt = $tmp_dt->modify("-1 Hours");
    } else if (in_array($state, ["CT", "DE", "MD", "FL", "GA", "IN", "KY", "ME", "MD", "MA", "MI", "NH", "NJ", "NY", "NC", "OH", "PA", "RI", "SC", "TN", "VT", "VA", "WV"])) {
        $tmp_dt = $tmp_dt->modify("+1 Hours");
    }

    /*if ($state == "CA") {
    $tmp_dt = $tmp_dt->modify("-2 Hours");
    } else if ($state == "NM" || $state == "UT") {
    $tmp_dt = $tmp_dt->modify("-1 Hours");
    } else if ($state == "TX" || $state == "MO" || $state == "WI" || $state == "IL" || $state == "AL" || $state == "MS") {
    //The customer is in central time so no modification is needed to the time stamp
    } else if ($state == "SC" || $state == "GA" || $state == "OH") {
    $tmp_dt = $tmp_dt->modify("+1 Hours");
    } else {
    mm_log_error('mm_build_uw_request', "Warning: Unable to check for after hours application due to unexpected State: $state");
    }*/

    $application_hour = $tmp_dt->format('H');
    if ($street_address == "TIME FAIL") {
        $temp_array["RequireFullVerification"] = "true";
        $note_data = array();
        $note_data["application_nbr"] = $app_data["application_nbr"];
        $note_data["category"] = "Automated Decisioning";
        $note_data["sub_category"] = "Pre-Verifications";
        $note_data["txt_body"] = "Application outsorted due to application time";
        $note_data["create_dt"] = date("Y-m-d H:i:s");
        $note_data["username"] = "System";
        $note_json = json_encode($note_data);
        $note_results = mm_add_application_note($note_json);
        $note_message = $note_results["return_message"];
        if ($note_results["return_value"] != 0) {
            //Failed to add note so log an error
            mm_log_error('mm_build_preverif_request', "Failed Adding an application note: $note_message");
        }
    } else if ($application_hour >= 1 && $application_hour < 5) {
        // uncommented this line as we should not pass null to KL in this field.
        $temp_array["RequireFullVerification"] = "false";
        $note_data = array();
        $note_data["application_nbr"] = $app_data["application_nbr"];
        $note_data["category"] = "Automated Decisioning";
        $note_data["sub_category"] = "Pre-Verifications";
        $note_data["txt_body"] = "Application outsorted due to application time";
        $note_data["create_dt"] = date("Y-m-d H:i:s");
        $note_data["username"] = "System";
        $note_json = json_encode($note_data);
        $note_results = mm_add_application_note($note_json);
        $note_message = $note_results["return_message"];
        if ($note_results["return_value"] != 0) {
            //Failed to add note so log an error
            mm_log_error('mm_build_preverif_request', "Failed Adding an application note: $note_message");
        }
    } else if ($income_source_1 == 'Other / not listed') {
        $temp_array["RequireFullVerification"] = "true";
        $note_data = array();
        $note_data["application_nbr"] = $app_data["application_nbr"];
        $note_data["category"] = "Automated Decisioning";
        $note_data["sub_category"] = "Pre-Verifications";
        $note_data["txt_body"] = "Application set to require customer documents due to income type of Other / not listed";
        $note_data["create_dt"] = date("Y-m-d H:i:s");
        $note_data["username"] = "System";
        $note_json = json_encode($note_data);
        $note_results = mm_add_application_note($note_json);
        $note_message = $note_results["return_message"];
        if ($note_results["return_value"] != 0) {
            //Failed to add note so log an error
            mm_log_error('mm_build_preverif_request', "Failed Adding an application note: $note_message");
        }

    } else {
        $temp_array["RequireFullVerification"] = "false";
    }

    //Check to see if there are any other applications that are approved/funded from the same household
    $address_data = array();
    $address_data["street_address"] = $app_data["street_address"];
    $address_data["appt_suite"] = $app_data["appt_suite"];
    $address_data["first_name"] = $app_data["first_name"];
    $address_data["application_nbr"] = $app_data["application_nbr"];
    $address_json = json_encode($address_data);
    $household_data = mm_get_household_matches($address_json);
    $household_return_value = $household_data["return_value"];
    $household_return_message = $household_data["return_message"];
    $household_result_count = $household_data["row_count"];
    $household_results = $household_data["results"];
    if ($household_return_value != 0) {
        //Log an error but continue processing
        mm_log_error('mm_build_preverif_request', "Failed getting the household information with message $household_return_message");
    } else if ($household_result_count > 0) {
        //There were household matches to outsort for full verifications and add a note to the application
        $temp_array["RequireFullVerification"] = "true";
        //Add an application note
        $note_data = array();
        $note_data["application_nbr"] = $app_data["application_nbr"];
        $note_data["category"] = "Automated Decisioning";
        $note_data["sub_category"] = "Pre-Verifications";
        $note_data["txt_body"] = "Application outsorted for verifications because of an existing approval/loan in the household Matches: ";
        foreach ($household_results as $household) {
            $app_nbr = $household["application_nbr"];
            $note_data["txt_body"] .= "$app_nbr, ";
        }
        $note_data["create_dt"] = date("Y-m-d H:i:s");
        $note_data["username"] = "System";

        $note_json = json_encode($note_data);
        $note_results = mm_add_application_note($note_json);
        $note_message = $note_results["return_message"];
        if ($note_results["return_value"] != 0) {
            //Failed to add note so log an error
            mm_log_error('mm_build_preverif_request', "Failed Adding an application note: $note_message");
        }
    } else {
        // todo: what needs to happen here?
        //There were no matches so continue as normal
    }

    //For now default to success
    $return_value = 0;
    $return_message = '';
    $json_message = json_encode($temp_array);

    //Prepare return array
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["json_message"] = $json_message;

    //Send return_array
    return $return_array;
}

/*************************************************************************************
 * Function mm_send_preverif_request($app_data)
 ************************************************************************************
 * @param array $app_data
 * @return array
 */
function mm_send_preverif_request(array $app_data): array {
    error_log("flow: mm_send_preverif_request\n");
    $return_array = array();
    $uw_response = array();
    $server_details = get_origination_system_connection_details();
    $application_nbr = $app_data["application_nbr"];
    $preverif_request_array = mm_build_preverif_request($app_data);
    $preverif_request_value = $preverif_request_array["return_value"];
    //$preverif_request_message = $preverif_request_array["return_message"];
    $preverif_request_json = $preverif_request_array["json_message"];

    $return_value = 0;
    $return_message = '';
    $url = $server_details["url"];
    $url = $url . "AcceptLoan";
    $curl = curl_init($url);

    if ($preverif_request_value != 0) {
        //Handle an error building the message
    } else {
        mm_log_preverif_request($preverif_request_json, $application_nbr);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $server_details["header_array"]);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $preverif_request_json);

        $json_response = curl_exec($curl);
        mm_log_preverif_response($json_response, $application_nbr);
        $response_array = json_decode($json_response, true);
        $response_array["application_nbr"] = $app_data["application_nbr"];

        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($status != 201 && $status != 200) {
            //There was an error in the message sending  Add error handling
            $return_value = 1;
            $return_message = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);
        } else {
            //The request worked.  Process the request, update the database application status
            $uw_response = json_decode($json_response, true);

            // todo: how to handle the return of the call?
            $insert_results = mm_create_preverif_decision($response_array);
        }
    }
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["preverif_response"] = $uw_response;

    curl_close($curl);

    return $return_array;
}

/********************************************************************************
 * Function mm_process_customer_acceptance($entry, $form) - Take action after the customer has accepted the form.  Update the database with the customers selections, submit the customer data to the originations system for preverificaitons determination.  Respond with the result of preverifications. For now, assumes that this is called directly from the gravity forms form.
 *******************************************************************************
 * @param $application_nbr
 * @return array
 */
function mm_submit_preverifications($application_nbr) {
    error_log("flow: mm_submit_preverifications\n");
    //Initialize Variables
    $return_array = array();
    $request_array = array();
    $return_value = 0;
    $return_message = '';

    //Get the data you need from the entry form to continue processing
    //EDIT - Come back and get the details from the session variable if it exists....
    $app_array = mm_get_application_details($application_nbr);
    $app_data = $app_array["app_data"];
    $request_array["orig_appid"] = $app_data["orig_appid"];
    $request_array["application_nbr"] = $application_nbr;
    $request_array["next_pay_dt_1"] = $app_data["next_pay_dt_1"];
    $request_array["requested_amt"] = $app_data["requested_amt"];
    $request_array["requested_deposit_type"] = $app_data["requested_deposit_type"];
    $request_array["create_dt"] = $app_data["create_dt"];
    $request_array["state"] = $app_data["state"];
    $request_array["street_address"] = $app_data["street_address"];
    $request_array["first_name"] = $app_data["first_name"];
    $request_array["appt_suite"] = $app_data["appt_suite"];
    $request_array["income_source_1"] = $app_data["income_source_1"];

    //Update the application status

    //Submit the data using a preverifications message
    $preverif_result_array = mm_send_preverif_request($request_array);

    //Process the preverifications response
    $preverif_return_value = $preverif_result_array["return_value"];
    //$preverif_return_message = $preverif_result_array["return_message"];
    $preverif_response = $preverif_result_array["preverif_response"];
    $preverif_result = $preverif_response["AppStatusCode"];
    if ($preverif_return_value != 0) {
        //There was an issue set the app status to stuck and the appropriate return values for display to the customer
        $return_value = 1;
        $return_message = "There was an issue processing the preverifications request";
        $vf_decision = "Error";
        mm_update_database_value('mm_application', 'application_status', 9, 'i', 'application_nbr', $application_nbr);

    } else {
        //The request was fine determine if documents are needed or not.  Update the application status appropriately and set the return information
        if ($preverif_result == "Accepted") {
            mm_update_database_value('mm_application', 'application_status', 6, 'i', 'application_nbr', $application_nbr);
        } else if ($preverif_result == "Error") {
            //Need to handle the difference between Error and Need Documents
            mm_update_database_value('mm_application', 'application_status', 9, 'i', 'application_nbr', $application_nbr);
        } else if ($preverif_result == "ACCEPTED PENDING AGENT REVIEW") {
            mm_update_database_value('mm_application', 'application_status', 5, 'i', 'application_nbr', $application_nbr);
        } else if ($preverif_result == "ACCEPTED PENDING CUSTOMER DOCUMENTS") {
            mm_update_database_value('mm_application', 'application_status', 4, 'i', 'application_nbr', $application_nbr);
        } else {
            // todo: what to do here?
            //Need to handle the difference between Error and Need Documents

        }
        $vf_decision = $preverif_result;
    }

    //Build the function response array
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["vf_decision"] = $vf_decision;
    $return_array["state"] = $app_data["state"];


    //Return the data from the function
    return $return_array;
}

/******************************************************************************************************************************************
 * Function mm_get_preverif_decision_details($decision_nbr)
 ******************************************************************************************************************************************/
function mm_get_preverif_decision_details($preverif_decision_nbr) {
    error_log("flow: mm_get_preverif_decision_details\n");
    $return_array = array();
    //$decision_data = array();
    $preverif_data = array();

    $conn = mm_get_db_connection();
    $sql_string = "Select * from mm_preverif_decision where preverif_decision_nbr = ?";
    if (!$stmt = $conn->prepare($sql_string)) {
        mm_log_error('mm_get_preverif_decision_details', "$conn->error", $conn->errno);
    }
    $stmt->bind_param('i', $preverif_decision_nbr);
    if (!$stmt->execute()) {
        $return_value = $stmt->errno;
        $return_message = $stmt->error;
        //Need to log an error here as well
    } else {
        $rows = $stmt->get_result();
        $num_rows = $rows->num_rows;
        if ($num_rows == 0) {
            //The preverif decision record didn't exist
            $return_value = 1;
            $return_message = "There was no associated preverif record with a preverif number of $preverif_decision_nbr";
        } else {
            $preverif_data = $rows->fetch_assoc();
            $return_value = 0;
            $return_message = '';
        }
    }

    if (is_resource($conn)) {
        $conn->close();
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["preverif_data"] = $preverif_data;
    return $return_array;

}

/******************************************************************************************************************************************
 * Function mm_get_uw_decision_details($decision_nbr)
 ******************************************************************************************************************************************/
function mm_get_uw_decision_details($decision_nbr) {
    error_log("flow: mm_get_uw_decision_details\n");
    $return_array = array();
    $decision_data = array();

    $conn = mm_get_db_connection();
    $sql_string = "Select * from mm_uw_decision where decision_nbr = ?";
    if (!$stmt = $conn->prepare($sql_string)) {
        mm_log_error('mm_get_uw_decision_details', "$conn->error", $conn->errno);
    }
    $stmt->bind_param('i', $decision_nbr);
    if (!$stmt->execute()) {
        $return_value = $stmt->errno;
        $return_message = $stmt->error;
        //Need to log an error here as well
    } else {
        $rows = $stmt->get_result();
        $num_rows = $rows->num_rows;
        if ($num_rows == 0) {
            //The uw decision record didn't exist
            $return_value = 1;
            $return_message = "There was no associated decision with a decision number $decision_nbr";
        } else {
            $decision_data = $rows->fetch_assoc();
            $return_value = 0;
            $return_message = '';
        }
    }

    if (is_resource($conn)) {
        $conn->close();
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["decision_data"] = $decision_data;
    return $return_array;

}

/***************************************************************************************************
 * Function mm_get_application_history($account_nbr)
 ***************************************************************************************************/
function mm_get_application_history($account_nbr) {
    error_log("flow: mm_get_application_history\n");
    $return_array = array();
    $app_history = array(array());

    $conn = mm_get_db_connection();
    $sql_string = "Select * from mm_application where account_nbr = ?";
    if (!$stmt = $conn->prepare($sql_string)) {
        mm_log_error('mm_get_application_history', "$conn->error", $conn->errno);
    }
    $stmt->bind_param('i', $account_nbr);
    if (!$stmt->execute()) {
        //$return_value = $stmt->errno;
        //$return_message = $stmt->error;
        // todo: Need to log an error here as well
    } else {
        $rows = $stmt->get_result();
        $num_rows = $rows->num_rows;
        if ($num_rows == 0) {
            //The application id didn't exist so default all of the return values
            $return_value = 0;
            $return_message = "There were no applications associated with this account";
        } else {
            $i = 0;
            while ($row = $rows->fetch_assoc()) {
                foreach ($row as $key => $value) {
                    $app_history[$i]["$key"] = $value;
                }
                $i += 1;
            }
            $return_value = 0;
            $return_message = '';
        }

        if (is_resource($conn)) {
            $conn->close();
        }

        $return_array["return_value"] = $return_value;
        $return_array["return_message"] = $return_message;
        $return_array["app_history"] = $app_history;
        $return_array["num_apps"] = $num_rows;
        return $return_array;

    }

}

/******************************************************************************************************************************************
 * Function mm_get_application_details($application_nbr)
 ******************************************************************************************************************************************/
function mm_get_application_details($application_nbr) {
    error_log("flow: mm_get_application_details\n");
    $return_array = array();
    $app_data = array();
    $return_value = 0;
    $return_message = '';

    // mark was here: try cache first
    $app_data = mm_get_cache_value('app_data_' . $application_nbr);

    if (!$app_data) {
        // cache did not work, so fall back
        error_log("mark: Cache did not work, falling back to db query");
        $conn = mm_get_db_connection();
        $sql_string = "Select * from mm_application where application_nbr = ?";
        if (!$stmt = $conn->prepare($sql_string)) {
            mm_log_error('mm_get_application_details', "$conn->error", $conn->errno);
        }
        $stmt->bind_param('i', $application_nbr);
        if (!$stmt->execute()) {
            $return_value = $stmt->errno;
            $return_message = $stmt->error;
            //Need to log an error here as well
        } else {
            $rows = $stmt->get_result();
            $num_rows = $rows->num_rows;
            if ($num_rows == 0) {
                //The application id didn't exist so default all of the return values
                $return_value = 1;
                $return_message = "There was no associated application with that application number.";
            } else {
                $app_data = $rows->fetch_assoc();

            }
        }

        if (is_resource($conn)) {
            $conn->close();
        }
    } else {
        error_log("cache value found for app_data");
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["app_data"] = $app_data;
    return $return_array;

}

/******************************************************************************************************************************************
 * Function mm_get_application_state_specific_details($application_nbr)
 ******************************************************************************************************************************************/
function mm_get_application_state_specific_details($application_nbr) {
    error_log("flow: mm_get_application_state_specific_details\n");
    $return_array = array();
    $app_data = array();
    $return_value = 0;
    $return_message = '';

    $conn = mm_get_db_connection();
    $sql_string = "Select * from mm_state_specific_data where application_nbr = ?";
    if (!$stmt = $conn->prepare($sql_string)) {
        mm_log_error('mm_get_application_state_specific_details', "$conn->error", $conn->errno);
    }
    $stmt->bind_param('i', $application_nbr);
    if (!$stmt->execute()) {
        $return_value = $stmt->errno;
        $return_message = $stmt->error;
        //Need to log an error here as well
    } else {
        $rows = $stmt->get_result();
        $num_rows = $rows->num_rows;
        if ($num_rows == 0) {
            //The application id didn't exist so default all of the return values
            $return_value = 1;
            $return_message = "There was no associated state specific data with that application number.";
        } else {
            $app_data = $rows->fetch_assoc();

        }
    }

    if (is_resource($conn)) {
        $conn->close();
    }


    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    error_log("flow: application specific data ".$app_data['blob']);
    $return_array["app_data"] = json_decode($app_data['blob']);
    return $return_array;

}

/************************************************************************************************
 * Function mm_get_preverif_lead_details($lead_id)
 ************************************************************************************************/
function mm_get_preverif_lead_details($lead_id) {
    error_log("flow: mm_get_preverif_lead_details\n");
    $app_data = array();
    $return_array = array();
    $requested_amt = 0;
    $deposit_method = "";

    $conn = mm_get_db_connection();
    $sql_string = "select * from wp_rg_lead_detail where lead_id = ? and form_id = 17";
    $stmt = $conn->prepare($sql_string);
    $stmt->bind_param('i', $lead_id);
    $stmt->execute();
    $rows = $stmt->get_result();
    foreach ($rows as $row) {
        $field_number = $row["field_number"];
        $value = $row["value"];
        if ($field_number == 1) {
            $requested_amt = $value;
        } else if ($field_number == 4) {
            $deposit_method = $value;
        } else if ($field_number == 3) {
            $application_nbr = $value;

        } else {
            //Log Error Because Received Unexpected Field
        }

    }

    $app_data["requested_amt"] = $requested_amt;
    $app_data["application_nbr"] = $requested_amt;
    $app_data["deposit_method"] = $deposit_method;

    if (is_resource($conn)) {
        $conn->close();
    }
    //Set the return array value
    $return_array["app_data"] = $app_data;

    return $return_array;

}

function app_data_map() {
    error_log("flow: app_data_map\n");
    return [
        '1' => 'income_source_1',
        '2' => 'secondary_income_ind',
        '3' => 'income_source_2',
        '4' => 'company_name_1',
        '6' => 'wage_type_1',
        '7' => 'hourly_rate_1',
        '8' => 'hours_per_week_1',
        '9' => 'pay_frequency_1',
        '11' => 'day_paid_month_1',
        '12' => 'last_paycheck_amt_1',
        '13' => 'direct_deposit_ind_1',
        '14' => 'next_pay_dt_1',
        '17' => 'day_paid_twice_monthly_1',
        '21' => 'day_paid_weekly_1',
        '23' => 'monthly_income_amt_1',
        '25' => 'company_name_1',
        '33' => 'annual_salary_1',
        '34' => 'monthly_income_amt_1',
        '51.1' => 'street_address',
        '51.2' => 'appt_suite',
        '51.3' => 'city',
        '51.4' => 'state',
        '51.5' => 'zip_code',
        '51.6' => 'country',
        '52' => 'state',
        '53.3' => 'first_name',
        '53.6' => 'last_name',
        '54' => 'email_address',
        '56' => 'own_or_rent',
        '57' => 'monthly_rent_amt',
        '58' => 'monthly_mtg_amt',
        '59' => 'mobile_phone_nbr',
        '60' => 'home_phone_nbr',
        '63' => 'routing_nbr',
        '64' => 'bank_acct_nbr',
        '68.1' => 'bank_acct_standing_ind',
        '70' => 'dob',
        '73.1' => 'contact_agreement_agreement',
        '75.1' => 'electronic_disclosure_agreement',
        '76' => 'last_4_ssn',
        '77.1' => 'credit_bureau_inquiry_agreement',
        '78' => 'ssn',
        '80' => 'wage_type_2',
        '81' => 'hourly_rate_2',
        '82' => 'hours_per_week_2',
        '83' => 'monthly_income_amt_2',
        '84' => 'annual_salary_2',
        '86' => 'company_name_2',
        '87' => 'monthly_income_amt_2',
        '91' => 'pay_frequency_2',
        '93' => 'day_paid_month_2',
        '95' => 'day_paid_twice_monthly_2',
        '97' => 'day_paid_weekly_2',
        '99' => 'last_paycheck_amt_2',
        '101' => 'direct_deposit_ind_2',
        '102' => 'next_pay_dt_2',
        '103' => 'company_name_1',
        '104' => 'company_name_2',
        '109' => "not used", // confirm routing number
        '110' => "not used", // confirm account number
        '113' => "bank_acct_type",
        '114' => "not used", // confirm SSN
        '115' => "bank_acct_age_years",
        '116' => "bank_acct_age_months",
        '119.1' => "delaware_schedule_agreement", // delaware agreement
        '122.1' => "california_schedule_acknowledgement",
        '129' =>'gross_income_1',
        '134' =>'gross_income_2',
        '136.1' => 'illinois_agreement'
    ];
}

/**************************************************************************************************************************************************************************
 * Function mm_get_lead_details($lead_id)
 * Description: The purpose of this plugin is to take a lead id, query the gravity forms database and convert the information from the form into an associative array of value from the application
 * Work Still Needing Completed: Handling of default values for missing/not applicable fields, error handling for database connectivity, handling when a lead id has no data in the database
 **************************************************************************************************************************************************************************/
function mm_get_lead_details($lead_id) {
    error_log("flow: mm_get_lead_details\n");
    $app_data = array();
    $app_data["gf_lead_id"] = $lead_id;
    $conn = mm_get_db_connection();
    $sql_string = "select lead_id, form_id, field_number, value from wp_rg_lead_detail where lead_id = ? and form_id = 14";
    if (!$stmt = $conn->prepare($sql_string)) {
        mm_log_error('mm_get_lead_details', "$conn->error", $conn->errno);
    }
    $stmt->bind_param('i', $lead_id);
    if (!$stmt->execute()) {
        // todo: what to do here?
        //$return_value = $stmt->errno;
        //$return_message = $stmt->error;
    } else {
        $rows = $stmt->get_result();
        $tmp_map = app_data_map();
        foreach ($rows as $row) {
            $field_number = $row["field_number"];
            $tmp = $tmp_map[strval($field_number)];
            //error_log("mark:  fieldnumber, tmp: $field_number, $tmp\n");
            if (isset($tmp)) {
                $app_data[$tmp] = $row["value"];
            } else {
                if ($field_number != 119.1 || $field_number != 122.1) {
                    mm_log_error('mm_get_lead_details', "received a field that I wasn't expecting: $field_number");
                }
            }
        }

        //Set default values for all of the fields that may not be set
        $app_data["wage_type_2"] = isset($app_data["wage_type_2"]) ? $app_data["wage_type_2"] : '';
        $app_data["hourly_rate_2"] = isset($app_data["hourly_rate_2"]) ? $app_data["hourly_rate_2"] : 0;
        $app_data["hours_per_week_2"] = isset($app_data["hours_per_week_2"]) ? $app_data["hours_per_week_2"] : 0;
        $app_data["annual_salary_1"] = isset($app_data["annual_salary_1"]) ? $app_data["annual_salary_1"] : 0;
        $app_data["annual_salary_2"] = isset($app_data["annual_salary_2"]) ? $app_data["annual_salary_2"] : 0;
        $app_data["pay_frequency_2"] = isset($app_data["pay_frequency_2"]) ? $app_data["pay_frequency_2"] : '';
        $app_data["day_paid_month_2"] = isset($app_data["day_paid_month_2"]) ? $app_data["day_paid_month_2"] : '';
        $app_data["day_paid_weekly_2"] = isset($app_data["day_paid_weekly_2"]) ? $app_data["day_paid_weekly_2"] : '';
        $app_data["day_paid_biweekly_2"] = isset($app_data["day_paid_biweekly_2"]) ? $app_data["day_paid_biweekly_2"] : '';
        $app_data["day_paid_twice_monthly_2"] = isset($app_data["day_paid_twice_monthly_2"]) ? $app_data["day_paid_twice_monthly_2"] : '';
        $app_data["direct_deposit_ind_2"] = isset($app_data["direct_deposit_ind_2"]) ? $app_data["direct_deposit_ind_2"] : '';
        $app_data["next_pay_dt_2"] = isset($app_data["next_pay_dt_2"]) ? $app_data["next_pay_dt_2"] : '';
        $app_data["monthly_income_amt_2"] = isset($app_data["monthly_income_amt_2"]) ? $app_data["monthly_income_amt_2"] : 0;
        $app_data["company_name_2"] = isset($app_data["company_name_2"]) ? $app_data["company_name_2"] : '';
        $app_data["company_name_1"] = isset($app_data["company_name_1"]) ? $app_data["company_name_1"] : 'N/A';
         $app_data["gross_income_1"] = isset($app_data["gross_income_1"]) ? $app_data["gross_income_1"] : 0;
        $app_data["gross_income_2"] = isset($app_data["gross_income_2"]) ? $app_data["gross_income_2"] : 0;

        //perform some form validation and cleansing
        $app_data["home_phone_nbr"] = isset($app_data["home_phone_nbr"]) ? clean_phone_nbr($app_data["home_phone_nbr"]) : 0;
        $app_data["mobile_phone_nbr"] = clean_phone_nbr($app_data["mobile_phone_nbr"]);
        $app_data["ssn"] = clean_phone_nbr($app_data["ssn"]);
        $app_data["state"] = mm_state_to_cd($app_data["state"]);
        $app_data["direct_deposit_ind_1"] = ($app_data["direct_deposit_ind_1"] == "Yes") ? 'Y' : 'N';
        $app_data["direct_deposit_ind_2"] = ($app_data["direct_deposit_ind_2"] == "Yes") ? 'Y' : 'N';

    }

    //Close Database connection
    if (is_resource($conn)) {
        $conn->close();
    }

    //Send Return Data
    return $app_data;
}

/*************************************************************************************************************************************************
 * Function mm_account_exists($key, $value) - Returns 1 if the account exists, 0 if it doesn't and > 1 if there was an error.
 *************************************************************************************************************************************************/
function mm_account_exists($key, $value) {
    error_log("flow: mm_account_exists: (\$key: $key, \$value = $value)\n");
    $return_array = array();
    $conn = mm_get_db_connection();
    $sql_string = "select account_nbr from mm_account where $key = ?";
    if (!$stmt = $conn->prepare($sql_string)) {
        mm_log_error("mm_account_exists($key, $value)", "$conn->error", "$conn->errorno");
        $return_value = $conn->errno;
        $return_message = $conn->error;
    } else {
        $stmt->bind_param('s', $value);
        if (!$stmt->execute()) {
            $return_value = $stmt->errno;
            $return_message = $stmt->error;
        } else {
            $rows = $stmt->get_result();
            $num_rows = $rows->num_rows;
            if ($num_rows == 0) {
                //There is no match in the database
                $return_value = 0;
                $return_message = "No Match";
                $account_nbr = 0;
            } else if ($num_rows == 1) {
                //There is an existing record in the database
                $row = $rows->fetch_assoc();
                $return_value = 1;
                $return_message = "Match";
                $account_nbr = $row["account_nbr"];
            } else {
                //There was more than 1 match.  This should never happen, but wanted to capture the scenario just in case
                $return_value = 2;
                $return_message = "Multiple Matches";
                $account_nbr = 0;
            }
        }
    }

    //Assign values to return array
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    if (isset($account_nbr)) {
        $return_array["account_nbr"] = $account_nbr;
    }

    //close db connection if it exists
    if (is_resource($conn)) {
        $conn->close();
    }

    //return data
    return $return_array;
}

/************************************************************************************************************************************************
 * Function clean_phone_nbr($phone_nbr) - Removes all non-numbers from a phone number
 ************************************************************************************************************************************************/
function clean_phone_nbr($phone_nbr) {
    error_log("flow: clean_phone_nbr\n");
    $clean_phone_nbr = preg_replace("/[^0-9,.]/", "", $phone_nbr);
    return $clean_phone_nbr;

}

/*******************************************************************************************************************************************
 * Function mm_create_account($account_data)
 *******************************************************************************************************************************************/
function mm_create_account($account_data) {
    error_log("flow: mm_create_account\n");
    //Initialize Variables
    $return_array = array();
    $first_name = $account_data["first_name"];
    $last_name = $account_data["last_name"];
    $email_address = $account_data["email_address"];
    $ssn = $account_data["ssn"];
    $account_status = 'A';
    $mobile_phone_nbr = $account_data["mobile_phone_nbr"];
    $home_phone_nbr = $account_data["home_phone_nbr"];
    $create_dt = date("Y-m-d H:i:s");
    $agree_to_communications = isset($account_data["contact_agreement_agreement"]) ? true : false;
    $Y='Y';
    $N='N';

    //Cleanse data before inserting

    //Build Insert Query
    $sql_string = "insert into mm_account(first_name, last_name, email_address, ssn, create_dt, account_status, mobile_phone_nbr, home_phone_nbr, pref_mail_acct_ind, pref_phone_mktg_ind, pref_email_mktg_ind, pref_text_ind, pref_aff_sharing_ind, pref_aff_creditworthiness_ind, pref_arbitration_opt_out_ind) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    
    //Connect to the database
    $conn = mm_get_db_connection();

    if (!$stmt = $conn->prepare($sql_string)) {
        mm_log_error("mm_create_account", "$conn->error", "$conn->errno");
        $return_value = $conn->errno;
        $return_message = $conn->error;

    } else {
        if ($agree_to_communications) {
            $stmt->bind_param('sssssssssssssss', $first_name, $last_name, $email_address, $ssn, $create_dt, $account_status, $mobile_phone_nbr, $home_phone_nbr, $Y, $Y, $Y, $Y, $Y, $Y, $Y);
        } else {
            $stmt->bind_param('sssssssssssssss', $first_name, $last_name, $email_address, $ssn, $create_dt, $account_status, $mobile_phone_nbr, $home_phone_nbr, $N, $N, $N, $N, $Y, $Y, $Y);
        }
        if (!$stmt->execute()) {
            $return_value = $conn->errno;
            $return_message = $conn->error;
        } else {
            //The account was inserted successfully
            $account_nbr = $stmt->insert_id;
            $return_value = 0;
            $return_message = "Account created successfully";
        }
    }

    //Close Connection if Exists
    if (is_resource($conn)) {
        $conn->close();
    }

    //Setup Return Array
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["account_nbr"] = isset($account_nbr) ? $account_nbr : 0;

    //Return values
    return $return_array;
}

function mm_update_database_value_2($table_name, $column_name, $value, $type, $table_key_txt, $table_key_value, $user = 'SYSTEM') {
    error_log("flow: mm_update_database_value_2 ($table_name, $column_name, $value, $type, $table_key_txt, $table_key_value, $user)\n");

    $return_value = 0;
    $return_message = '';

    // the following in needed to generate exceptions
    // mysqli_report(MYSQLI_REPORT_ALL);

    if ($type == 's' || $type == 'S') {
        $sql_ready_value = "'" . $value . "'";
    } else {
        $sql_ready_value = $value;
    }
    $type = "'" . $type . "'";

    $conn = mm_get_db_connection();

    //Get Current Value
    $sql_string = "select $column_name from $table_name where $table_key_txt = ?";
    if (!$stmt = $conn->prepare($sql_string)) {
        mm_log_error("mm_update_database_value_2", "$conn->error", "$conn->errno");
        $return_value = $conn->errno;
        $return_message = $conn->error;
    } else {
        if ($type == 'i') {
            $stmt->bind_param('i', $table_key_value);
        } else if ($type == 's') {
            $stmt->bind_param('s', $table_key_value);
        } else {
            $stmt->bind_param('d', $table_key_value);
        }

        if (!$stmt->execute()) {
            mm_log_error("mm_update_database_value_2", "$conn->error", "$conn->errno");
            $return_value = $conn->errno;
            $return_message = $conn->error;
        } else {
            $rows = $stmt->get_result();
            $num_rows = $rows->num_rows;
            if ($num_rows == 1) {
                $row = $rows->fetch_assoc();
                $old_value = $row["$column_name"];
            }
        }
    }

    //Update With the New Value only if different
    if ($old_value != $value) {
        $sql_string = "update $table_name set $column_name=$sql_ready_value where $table_key_txt = ?";
        if (!$stmt = $conn->prepare($sql_string)) {
            mm_log_error("mm_update_database_value_2", "Error with this query: $sql_string and this is the error $conn->error", "$conn->errno");
            $return_value = $conn->errno;
            $return_message = $conn->error;

        } else {
            if ($type == 'i') {
                $stmt->bind_param('i', $table_key_value);
            } else if ($type == 's') {
                $stmt->bind_param('s', $table_key_value);
            } else {
                $stmt->bind_param('d', $table_key_value);
            }
            if (!$stmt->execute()) {
                $return_value = $conn->errno;
                $return_message = $conn->error;
            } else {
                //The account was inserted successfully
                $account_nbr = $stmt->insert_id;
                $return_value = 0;
                $return_message = "Account created successfully";
            }
        }

        //Log the change
        $create_dt = date("Y-m-d H:i:s");
        $sql_string = "insert into mm_column_history(table_name, column_name, old_value, new_value, change_dt,key_value, user) values (?,?,?,?,?,?,?)";
        $stmt = $conn->prepare($sql_string);
        $stmt->bind_param('sssssis', $table_name, $column_name, $old_value, $value, $create_dt, $table_key_value, $user);
        $stmt->execute();
    }

    if (is_resource($conn)) {
        $conn->close();
    }

    //mysqli_report(MYSQLI_REPORT_OFF);

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;

    return $return_array;
}

function mm_update_database_value_with_retry($table_name, $column_name, $value, $type, $table_key_txt, $table_key_value, $user = 'SYSTEM') {

    //error_log("flow: mm_update_database_value_with_retry ($table_name, $column_name, $value, $type, $table_key_txt, $table_key_value, $user)\n");

    $maxAttempts = 10;
    $result = array();

    try {
        $backoff = new \STS\Backoff\Backoff($maxAttempts, null, null, true);
        $result = $backoff->run(function () use ($table_name, $column_name, $value, $type, $table_key_txt, $table_key_value, $user) {
            return mm_update_database_value_2($table_name, $column_name, $value, $type, $table_key_txt, $table_key_value, $user);
        });
    } catch (Exception $e) {
        mm_log_error("mm_update_database_value_with_retry", "SERIOUS ERROR: update_database_with_value was called unsuccessfully after retrying $maxAttempts attempts", $e->getMessage());
    }

    return $result;
}

/*************************************************************************************
 * Function mm_update_database_value($table_name, $column_name, $value, $type, $table_key_txt, $table_key_value)
 *************************************************************************************/
function mm_update_database_value($table_name, $column_name, $value, $type, $table_key_txt, $table_key_value, $user = 'SYSTEM') {

    return mm_update_database_value_with_retry($table_name, $column_name, $value, $type, $table_key_txt, $table_key_value, $user);

}

/*********************************************************************************************************
 * Function mm_create_application($app_data)
 *********************************************************************************************************/
function mm_create_application($app_data) {
    error_log("flow: mm_create_application\n");
    //Initialize Variables
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    $first_name = $app_data["first_name"];
    $last_name = $app_data["last_name"];
    $state = $app_data["state"];
    $email_address = $app_data["email_address"];
    $street_address = $app_data["street_address"];
    $appt_suite = isset($app_data["appt_suite"]) ? $app_data["appt_suite"] : '';
    $city = $app_data["city"];
    $zip_code = $app_data["zip_code"];
    $own_or_rent = $app_data["own_or_rent"];
    $monthly_rent_amt = isset($app_data["monthly_rent_amt"]) ? $app_data["monthly_rent_amt"] : 0;
    $monthly_mtg_amt = isset($app_data["monthly_mtg_amt"]) ? $app_data["monthly_mtg_amt"] : 0;
    $mobile_phone_nbr = $app_data["mobile_phone_nbr"];
    $home_phone_nbr = isset($app_data["home_phone_nbr"]) ? $app_data["home_phone_nbr"] : '';
    $income_source_1 = $app_data["income_source_1"];
    $wage_type_1 = isset($app_data["wage_type_1"]) ? $app_data["wage_type_1"] : '';
    $hourly_rate_1 = isset($app_data["hourly_rate_1"]) ? $app_data["hourly_rate_1"] : 0;
    $hours_per_week_1 = isset($app_data["hours_per_week_1"]) ? $app_data["hours_per_week_1"] : 0;
    $annual_salary_1 = $app_data["annual_salary_1"];
    $pay_frequency_1 = $app_data["pay_frequency_1"];
    $day_paid_month_1 = isset($app_data["day_paid_month_1"]) ? $app_data["day_paid_month_1"] : '';
    $day_paid_twice_monthly_1 = isset($app_data["day_paid_twice_monthly_1"]) ? $app_data["day_paid_twice_monthly_1"] : '';
    $day_paid_weekly_1 = isset($app_data["day_paid_weekly_1"]) ? $app_data["day_paid_weekly_1"] : '';
    $day_paid_biweekly_1 = isset($app_data["day_paid_biweekly_1"]) ? $app_data["day_paid_biweekly_1"] : '';
    $direct_deposit_ind_1 = $app_data["direct_deposit_ind_1"];
    $next_pay_dt_1 = $app_data["next_pay_dt_1"];
    $monthly_income_amt_1 = isset($app_data["monthly_income_amt_1"]) ? $app_data["monthly_income_amt_1"] : 0;
    $last_paycheck_amt_1 = isset($app_data["last_paycheck_amt_1"]) ? $app_data["last_paycheck_amt_1"] : 0;
    $company_name_1 = isset($app_data["company_name_1"]) ? $app_data["company_name_1"] : '';
    $secondary_income_ind = isset($app_data["secondary_income_ind"]) ? $app_data["secondary_income_ind"] : "No";
    $income_source_2 = isset($app_data["income_source_2"]) ? $app_data["income_source_2"] : '';
    $wage_type_2 = $app_data["wage_type_2"];
    $hourly_rate_2 = $app_data["hourly_rate_2"];
    $hours_per_week_2 = $app_data["hours_per_week_2"];
    $annual_salary_2 = $app_data["annual_salary_2"];
    $pay_frequency_2 = $app_data["pay_frequency_2"];
    $day_paid_month_2 = $app_data["day_paid_month_2"];
    $day_paid_twice_monthly_2 = $app_data["day_paid_twice_monthly_2"];
    $day_paid_weekly_2 = $app_data["day_paid_weekly_2"];
    $day_paid_biweekly_2 = $app_data["day_paid_biweekly_2"];
    $direct_deposit_ind_2 = $app_data["direct_deposit_ind_2"];
    $next_pay_dt_2 = $app_data["next_pay_dt_2"] == '' ? "1900-01-01" : $app_data["next_pay_dt_2"];
    $monthly_income_amt_2 = $app_data["monthly_income_amt_2"];
    $last_paycheck_amt_2 = isset($app_data["last_paycheck_amt_2"]) ? $app_data["last_paycheck_amt_2"] : 0;
    $company_name_2 = $app_data["company_name_2"];
    $routing_nbr = $app_data["routing_nbr"];
    $bank_acct_nbr = $app_data["bank_acct_nbr"];
    $bank_acct_age_years = $app_data["bank_acct_age_years"];
    $bank_acct_age_months = $app_data["bank_acct_age_months"];
    $ssn = $app_data["ssn"];
    $dob = $app_data["dob"];
    $contact_agreement_agreement =     isset($app_data["contact_agreement_agreement"])     ? $app_data["contact_agreement_agreement"]     : '';
    $electronic_disclosure_agreement = isset($app_data["electronic_disclosure_agreement"]) ? $app_data["electronic_disclosure_agreement"] : '';
    $credit_bureau_inquiry_agreement = isset($app_data["credit_bureau_inquiry_agreement"]) ? $app_data["credit_bureau_inquiry_agreement"] : '';
    $delaware_schedule_agreement =     isset($app_data["delaware_schedule_agreement"])     ? $app_data["delaware_schedule_agreement"]     : '';
    $california_schedule_acknowledgement = isset($app_data["california_schedule_acknowledgement"]) ? $app_data["california_schedule_acknowledgement"] : '';
    $illinois_agreement = isset($app_data["illinois_agreement"]) ? $app_data["illinois_agreement"] : '';
    $gf_lead_id = $app_data["gf_lead_id"];
    $account_nbr = $app_data["account_nbr"];
    $create_dt = date("Y-m-d H:i:s");
    $bank_acct_type = $app_data["bank_acct_type"];
    $application_status = 1; //Default the application status to draft
    $last_4_ssn = $app_data["last_4_ssn"];

    //Build SQL Statement

    $sql_string = "insert into mm_application (first_name, last_name, state, email_address, street_address, appt_suite, city, zip_code, own_or_rent, monthly_rent_amt, monthly_mtg_amt, mobile_phone_nbr, home_phone_nbr, income_source_1, wage_type_1, hourly_rate_1, hours_per_week_1, annual_salary_1, pay_frequency_1, day_paid_month_1, day_paid_twice_monthly_1, day_paid_weekly_1, day_paid_biweekly_1, direct_deposit_ind_1, next_pay_dt_1, monthly_income_amt_1, company_name_1, income_source_2, wage_type_2, hourly_rate_2, hours_per_week_2, annual_salary_2, pay_frequency_2, day_paid_month_2, day_paid_twice_monthly_2, day_paid_weekly_2, day_paid_biweekly_2, direct_deposit_ind_2, next_pay_dt_2, monthly_income_amt_2, company_name_2, routing_nbr, bank_acct_nbr, bank_acct_age_years, bank_acct_age_months, ssn, dob, contact_agreement_agreement, electronic_disclosure_agreement, last_4_ssn, credit_bureau_inquiry_agreement, create_dt, application_status, gf_lead_id, account_nbr, last_paycheck_amt_1, last_paycheck_amt_2, bank_acct_type, secondary_income_ind, delaware_schedule_agreement, california_schedule_acknowledgement) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    //Get a database connection
    $conn = mm_get_db_connection();
    //Attempt to prepare the SQL statement
    if (!$stmt = $conn->prepare($sql_string)) {
        mm_log_error("mm_create_application", "$conn->error", $conn->errno);
        $return_value = $conn->errno;
        $return_message = $conn->error;

    } else {
        $stmt->bind_param('sssssssssddssssdddsisssssdsssdddsisssssdsssiisssssssiiiddssss', $first_name, $last_name, $state, $email_address, $street_address, $appt_suite, $city, $zip_code, $own_or_rent, $monthly_rent_amt, $monthly_mtg_amt, $mobile_phone_nbr, $home_phone_nbr, $income_source_1, $wage_type_1, $hourly_rate_1, $hours_per_week_1, $annual_salary_1, $pay_frequency_1, $day_paid_month_1, $day_paid_twice_monthly_1, $day_paid_weekly_1, $day_paid_biweekly_1, $direct_deposit_ind_1, $next_pay_dt_1, $monthly_income_amt_1, $company_name_1, $income_source_2, $wage_type_2, $hourly_rate_2, $hours_per_week_2, $annual_salary_2, $pay_frequency_2, $day_paid_month_2, $day_paid_twice_monthly_2, $day_paid_weekly_2, $day_paid_biweekly_2, $direct_deposit_ind_2, $next_pay_dt_2, $monthly_income_amt_2, $company_name_2, $routing_nbr, $bank_acct_nbr, $bank_acct_age_years, $bank_acct_age_months, $ssn, $dob, $contact_agreement_agreement, $electronic_disclosure_agreement, $last_4_ssn, $credit_bureau_inquiry_agreement, $create_dt, $application_status, $gf_lead_id, $account_nbr, $last_paycheck_amt_1, $last_paycheck_amt_2, $bank_acct_type, $secondary_income_ind,$delaware_schedule_agreement,$california_schedule_acknowledgement);
        if (!$stmt->execute()) {
            $return_value = $conn->errno;
            $return_message = $conn->error;
        } else {
            $application_nbr = $stmt->insert_id;
            if ($state == 'IL') {
                $gross_income_amt_1 = isset($app_data["gross_income_1"]) ? $app_data["gross_income_1"] : 0;
                $gross_income_amt_2 = isset($app_data["gross_income_2"]) ? $app_data["gross_income_2"] : 0;
                $il_data = array('gross_income_1'=>$gross_income_amt_1,'gross_income_2'=>$gross_income_amt_2,'illinois_agreement'=>$illinois_agreement);
                mm_insert_state_specific_WI($application_nbr, 'Illinois_Gross_Income', 'json', json_encode($il_data));
            }
            $return_value = 0;
            $return_message = "Application Added Successfully";
        }
    }

    //Close Connection if open
    if (is_resource($conn)) {
        $conn->close();
    }

    //Build Return Array
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["application_nbr"] = isset($application_nbr) ? $application_nbr : 0;

    //Return function results
    return $return_array;

}

/********************************************************************************************************************************************************
 * Function mm_get_lp_loans($ssn, $email_address) - This function will get the loan history needed for application data from the loan management system
 ********************************************************************************************************************************************************/
function mm_get_lp_loans($ssn, $email_address) {
    error_log("flow: mm_get_lp_loans\n");
    $return_array = array();

    $return_array["return_value"] = 0;
    $return_array["return_message"] = "";
    $return_array["date_last_payoff"] = '';
    $return_array["active_loan_ind"] = 0;
    $return_array["charge_off_ind"] = 0;
    $return_array["former_ind"] = 0;

    return $return_array;
}

/***************************************************************************************************************************************************************
 * Function mm_is_bank_holiday($date) - Returns 1 if the date is a bank holiday 0 otherwise
 ***************************************************************************************************************************************************************/
function mm_is_bank_holiday($date) {
    error_log("flow: mm_is_bank_holiday\n");
    $holiday_dates = array('01/02/2017', '01/16/2017', '02/20/2017', '05/29/2017', '07/04/2017', '09/04/2017', '10/09/2017', '11/23/2017', '12/25/2017',
        '01/01/2018', '01/15/2018', '02/19/2018', '05/28/2018', '07/04/2018', '09/03/2018', '10/08/2018', '11/11/2018', '11/23/2018', '12/25/2018',
        '01/01/2019', '01/21/2019', '02/18/2019', '05/27/2019', '07/04/2017', '09/02/2019', '10/14/2019', '11/11/2019', '11/28/2019', '12/25/2019',
        '01/01/2020', '01/20/2020', '02/17/2020', '05/25/2020', '07/03/2020', '09/07/2020', '10/12/2020', '11/11/2020', '11/26/2020', '12/25/2020',
        '01/01/2021', '01/18/2021', '01/18/2021', '02/15/2021', '05/31/2021', '07/05/2021', '09/06/2021', '10/11/2021', '11/11/2021', '12/24/2021', '12/31/2021',
        '01/17/2022', '02/21/2022', '05/30/2022', '07/04/2022', '09/05/2022', '10/10/2022', '11/11/2022', '11/24/2022', '12/26/2022', '01/02/2023', '02/20/2023',
        '05/29/2023', '07/04/2023', '09/04/2023', '10/09/2023', '11/11/2023', '11/23/2023', '12/25/2023');
    $return_value = 0;
    foreach ($holiday_dates as $holiday) {
        if ($holiday == $date) {
            $return_value = 1;
            break;
        }
    }

    return $return_value;
}

/********************************************************************************************************************************************************************
 * Function update_application_status($application_nbr, $status_id) - Update the application status of the provided application.  Log that the application status was changed also.
 ********************************************************************************************************************************************************************/
function mm_update_application_status($application_nbr, $application_status, $noaa_cd = 0) {
    error_log("flow: mm_update_application_status: ($application_nbr, $application_status, $noaa_cd)\n");
    $return_array = array();
    $continue_processing = 1;
    $return_value = 0;
    $return_message = "";

    //Connect to the database
    $conn = mm_get_db_connection();
    $sql_string = "select application_status from mm_application where application_nbr = ?";
    //Prepare the SQL Statement
    if (!$stmt = $conn->prepare($sql_string)) {
        mm_log_error("mm_update_application_status", "$conn->error", $conn->errno);
        $return_value = $conn->errno;
        $return_message = $conn->error;
        $continue_processing = 0;

    } else {
        $stmt->bind_param('i', $application_nbr);
        if (!$stmt->execute()) {
            $return_value = $conn->errno;
            $return_message = $conn->error;
            mm_log_error("mm_update_application_status", "$conn->error", $conn->errno);
            $continue_processing = 0;
        } else {
            //The query was successful
            $rows = $stmt->get_result();
            $num_rows = $rows->num_rows;
            if ($num_rows == 1) {
                $row = $rows->fetch_assoc();
                $curr_application_status = $row["application_status"];
            } else {
                //Handle that the account number wasn't in the database
                $continue_processing = 0;
                $return_value = 1;
                $return_message = "The application provided doesn't exist.";
                $curr_application_status = 0;
            }
        }
    }
    if ($continue_processing == 1) {
        //Getting the current status was successful.  Update the new status
        $sql_string = "update mm_application set application_status = ? where application_nbr = ?";
        if (!$stmt = $conn->prepare($sql_string)) {
            mm_log_error("mm_update_application_status", "$conn->error", $conn->errno);
            $return_value = $conn->errno;
            $return_message = $conn->error;
            $continue_processing = 0;
        } else {
            $stmt->bind_param('ii', $application_status, $application_nbr);
            if (!$stmt->execute()) {
                $return_value = $conn->errno;
                $return_message = $conn->error;
                mm_log_error("mm_update_application_status", "$conn->error", $conn->errno);
                $continue_processing = 0;
            } else {
                //The query was successful - No action to take right now
            }
        }
    }

    if ($continue_processing == 1) {
        //The status was updated so add a record for debugging purposes to the mm_application_status_history
        $sql_string = "insert into mm_application_status_history (application_nbr, old_application_status, new_application_status, change_dt) values (?,?,?,?)";
        if (!$stmt = $conn->prepare($sql_string)) {
            $return_value = $conn->errno;
            $return_message = $conn->error;
            mm_log_error("mm_update_application_status", "$conn->error", $conn->errno);
            //$continue_processing = 0;
        } else {
            $create_dt = date("Y-m-d H:i:s");
            $stmt->bind_param('iiis', $application_nbr, $curr_application_status, $application_status, $create_dt);
            if (!$stmt->execute()) {
                $return_value = $conn->errno;
                $return_message = $conn->error;
                mm_log_error("mm_update_application_status", "$conn->error", $conn->errno);
                //$continue_processing = 0;
            } else {
                $return_value = 0;
                $return_message = "The application status was successfully updated.";
            }
        }

    }

    if (is_resource($conn)) {
        $conn->close();
    }
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;

    return $return_array;

}

/*********************************************************************************************************************************************************************
 * Function mm_housing_expense_to_code($housing_amt) - Converts the housing amount to the arranges allowed by Klevrlend
 *********************************************************************************************************************************************************************/
function mm_housing_expense_to_code($housing_amt) {
    error_log("flow: mm_housing_expense_to_code\n");
    if ($housing_amt < 400) {
        $return_string = 'Less than $400';
    } else if ($housing_amt < 600) {
        $return_string = '$400 - $599';
    } else if ($housing_amt < 800) {
        $return_string = '$600 - $799';
    } else if ($housing_amt < 1000) {
        $return_string = '$800 - $999';
    } else if ($housing_amt < 1200) {
        $return_string = '$1000 - $1199';
    } else if ($housing_amt < 1400) {
        $return_string = '$1200 - $1399';
    } else if ($housing_amt < 1600) {
        $return_string = '$1400 - $1599';
    } else if ($housing_amt < 1800) {
        $return_string = '$1600 - $1799';
    } else if ($housing_amt < 2000) {
        $return_string = '$1800 - $1999';
    } else {
        $return_string = "$2000+";
    }

    return $return_string;

}

/*********************************************************************************************************************************************************************
 * Function mm_calc_next_paydate($curr_date, $pay_frequency) - Calculates the next paydate from the current date given the provided frequencies
 *********************************************************************************************************************************************************************/
function mm_calc_next_paydate($curr_date, $pay_frequency) {

    return "2017/01/01";
}

/**************************************************************************************************************************************************************************
 * Function mm_build_uw_request($app_data) - Builds a Json Message to be sent to the originations system
 *************************************************************************************************************************************************************************
 * @param $app_data
 * @return array
 */
function mm_build_uw_request($app_data) {
    error_log("flow: mm_build_uw_request\n");
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    $temp_array = array();
    $custom_fields = array(array());
    //$content = array();
    $days_since_decline = 10000;
    $account_nbr = 0;
    $lp_customer_id = '';
    $account_status = '';

    //Get the application history to determine days since last hard decline
    $email_address = $app_data["email_address"];
    $account_details = mm_get_account_details_from_email_address($email_address);
    if ($account_details["return_value"]) {
        //Insert error handling if unable to get the account details
    } else {
        $account_nbr = $account_details["account_nbr"];
        $lp_customer_id = isset($account_details["lp_customer_id"]) ? $account_details["lp_customer_id"] : '';
    }

    $app_history_arr = mm_get_application_history($account_nbr);

    if ($app_history_arr["return_value"]) {
        //Need to insert error handling
    } else {
        $app_history = $app_history_arr["app_history"];
        $num_apps = $app_history_arr["num_apps"];
        if ($num_apps == 0) {
            
        } else {
            $today = new DateTime();
            $today->setTime(0, 0, 0);
            foreach ($app_history as $app) {
                $app_status = $app["application_status"];
                $app_date = $app["create_dt"];
                $app_dt = new DateTime($app_date);
                $app_dt->setTime(0, 0, 0);
                if ($app_status == 8) {
                    // Mark was here: commenting out the following line as I think its trying to compute an integer value
                    //$days_diff = $today->diff($app_dt)->format("%a") + 1;
                    $days_diff = $today->diff($app_dt)->days + 1;
                    if ($days_diff <= $days_since_decline) {
                        //This decline is more recent than any seen before
                        $days_since_decline = $days_diff;
                    }
                }
            }

        }
    }

    $account_status_arr = mm_get_account_status($app_data["email_address"]);
    if ($account_status_arr["return_value"]) {
        //There was an error getting the customer history - Need to add error handling
    } else {
        //The account status return value was zero
        $account_status = $account_status_arr["account_status"];
        //$application_status = $account_status_arr["application_status"];

    }
    $header_data = get_origination_system_connection_details();
    $temp_array["DaysSinceDecline"] = $days_since_decline;
    if ($lp_customer_id == '') {
        $temp_array["IsReturningCustomer"] = 'N';
    } else {
        $temp_array["IsReturningCustomer"] = "Y";
    }
    if ($account_status == 'ACTIVELOAN') {
        $temp_array["HasActiveLoan"] = 'Y';
    } else {
        $temp_array["HasActiveLoan"] = 'N';
    }
    if ($account_status == "CHARGEDOFF") {
        $temp_array["HasChargedOffLoan"] = 'Y';
        $temp_array["CollectionDifficulty"] = 'Y';
    } else {
        $temp_array["HasChargedOffLoan"] = 'N';
        $temp_array["CollectionDifficulty"] = 'N';
    }
    $temp_array["TotalMonthlyIncome"] = 0;

    $temp_array["UserName"] = $header_data["UserName"];
    $temp_array["Password"] = $header_data["Password"];
    $temp_array["ApplicationSource"] = $header_data["ApplicationSource"];
    $temp_array["AuthToken"] = $header_data["AuthToken"];
    $temp_array["PortfolioID"] = $header_data["PortfolioID"];
    $temp_array["LoanReason"] = 'Personal Loan';
    $temp_array["FirstName"] = utf8_encode($app_data["first_name"]);
    $temp_array["MiddleName"] = '';
    $temp_array["LastName"] = utf8_encode($app_data["last_name"]);
    $temp_array["CellPhone"] = $app_data["mobile_phone_nbr"];
    $temp_array["Email"] = utf8_encode($app_data["email_address"]);
    $temp_array["DOB"] = substr($app_data["dob"], 5, 2) . substr($app_data["dob"], 8, 2) . substr($app_data["dob"], 0, 4);
    $temp_array["SSN"] = $app_data["ssn"];
    $temp_array["HomeAddress1"] = utf8_encode($app_data["street_address"]);
    $temp_array["HomeAddress2"] = isset($app_data["appt_suite"]) ? utf8_encode($app_data["appt_suite"]) : '';
    $temp_array["HomeCity"] = utf8_encode($app_data["city"]);
    $temp_array["HomeState"] = utf8_encode($app_data["state"]);
    $temp_array["HomeZip"] = $app_data["zip_code"];
    $temp_array["HomePhone"] = $app_data["home_phone_nbr"] == '' ? '0000000000' : $app_data["home_phone_nbr"];
    $temp_array["HomeType"] = $app_data["own_or_rent"];
    if ($app_data["income_source_1"] == "I'm self-employed") {
        $temp_array["EmploymentStatus"] = "Self Employed";
    } else if ($app_data["income_source_1"] == "I work for a company") {
        $temp_array["EmploymentStatus"] = "Employed";
    } else if ($app_data["income_source_1"] == "I receive Social Security / disability benefits") {
        $temp_array["EmploymentStatus"] = "Social Security";
    } else if ($app_data["income_source_1"] == "I receive pension / retirement benefits") {
        $temp_array["EmploymentStatus"] = "Other";
    } else if ($app_data["income_source_1"] == "Other / not listed") {
        $temp_array["EmploymentStatus"] = "Other";
    } else {
        $unexpected_emp_type = $app_data["income_source_1"];
        mm_log_error('mm_build_uw_request', "Unexpected Income Type: $unexpected_emp_type");
        $temp_array["EmploymentStatus"] = "Other";
    }
    $temp_array["EmployerName"] = isset($app_data["company_name_1"]) ? utf8_encode($app_data["company_name_1"]) : "N/A";
    if ($app_data["pay_frequency_1"] == "Paid Monthly") {
        $temp_array["PayFrequency"] = "MNTH";
    } else if ($app_data["pay_frequency_1"] == "Paid Twice Monthly") {
        $temp_array["PayFrequency"] = "SMMT";
    } else if ($app_data["pay_frequency_1"] == "Bi-Weekly") {
        $temp_array["PayFrequency"] = "BIWK";
    } else if ($app_data["pay_frequency_1"] == "Paid Weekly") {
        $temp_array["PayFrequency"] = "WKLY";
    } else {
        $unexpected_pay_freq = $app_data["pay_frequency_1"];
        $temp_array["PayFrequency"] = "BIWK";
        mm_log_error("mm_build_uw_request", "Unexpected Pay Frequency 1: $unexpected_pay_freq");
    }
    
    $temp_array["OtherIncomeSource"] = "OTHR"; //Hardcoded because not needed for now
    if ($app_data["own_or_rent"] == "Own") {
        $temp_array["MonthlyHousePayment"] = mm_housing_expense_to_code($app_data["monthly_mtg_amt"]);
    } else if ($app_data["own_or_rent"] == "Rent") {
        $temp_array["MonthlyHousePayment"] = mm_housing_expense_to_code($app_data["monthly_rent_amt"]);
    } else {
        //Unexpected value for own/rent flag so handle the error and set housing expense to zero
        $own_rent_flag = $app_data["own_or_rent"];
        $temp_array["MonthlyHousePayment"] = 0;
        mm_log_error("mm_build_uw_request", "Unexpected Own/Rent Flag: $own_rent_flag.  Housing Payment set to zero");
    }
    $temp_array["NextPayDate1"] = substr($app_data["next_pay_dt_1"], 5, 2) . substr($app_data["next_pay_dt_1"], 8, 2) . substr($app_data["next_pay_dt_1"], 0, 4);
    $temp_array["NextPayDate2"] = mm_calc_next_paydate($app_data["next_pay_dt_1"], 'M');
    $temp_array["NextPayDate2"] = substr($temp_array["NextPayDate2"], 5, 2) . substr($temp_array["NextPayDate2"], 8, 2) . substr($temp_array["NextPayDate2"], 0, 4);
    if ($app_data["direct_deposit_ind_1"] == "Y") {
        $temp_array["DirectDeposit"] = "true";
    } else {
        $temp_array["DirectDeposit"] = "false";
    }
    $temp_array["ReferenceID"] = $app_data["application_nbr"];
    $temp_array["PerformDataValidation"] = "true";
    // todo: mark, fix this with result from data input
    $temp_array["BankAccountType"] = "Checking";
    $temp_array["BankAccountNumber"] = $app_data["bank_acct_nbr"];
    $temp_array["BankABA"] = $app_data["routing_nbr"];
    $income_array = mm_calculate_monthly_income_amt($app_data["application_nbr"]);
    //Need Error Handling - Jason
    $temp_array["MonthlyIncome"] = $income_array["monthly_income"];
    $temp_array["OtherIncome"] = $income_array["other_income"];
    //$temp_array["OtherIncome"] = 0;
    $temp_array["TotalMonthlyIncome"] = ceil($temp_array["MonthlyIncome"] + $temp_array["OtherIncome"]);

    
    //Create tracker of num custom fields added
    $num_custom = 0;


    // mark was here:  We are now just sending a very high amount t0 Klevrlend at this point
    $affordable_loan_amt = "10000";
    //$affordable_loan_amt = "2500";
    $temp1 = array();
    $temp1["Name"] = "AffordabilityLineAmount";
    $temp1["Value"] = $affordable_loan_amt;
    $temp1["Type"] = "int";
    $custom_fields[0] = $temp1;
    $num_custom = 1;
    
    //Populate the last approved amount for former customers
    $account_nbr = $app_data["account_nbr"];
    $former_approved_amt_arr = mm_get_last_approved_amt($account_nbr);
    $former_approved_amt_return_value = $former_approved_amt_arr["return_value"];
    $former_approved_amt_return_message = $former_approved_amt_arr["return_message"];
    if ($former_approved_amt_return_value == 0) {
        $last_approved_amt = $former_approved_amt_arr["last_approved_amt"];
        mm_update_database_value('mm_application', 'last_approved_amt', $last_approved_amt, 'd', 'application_nbr', $app_data["application_nbr"]);
        $temp1["Name"] = "FormerAmount";
        $temp1["Value"] = $last_approved_amt;
        //Hardcoded for now will revisit at a later time when our former line assignment strategy becomes more advanced.
        if ($temp_array["IsReturningCustomer"] == 'N') {
            $temp1["Value"] = "0";
        } else {
            $temp1["Value"] = "2500";
        }
        $temp1["Type"] = "int";
        $custom_fields[$num_custom] = $temp1;
        //$num_custom += 1;
    } else {
        $return_value = 1;
        $return_message = $former_approved_amt_return_message;
    }
    //Set Customer Fields Element
    if (sizeof($custom_fields) > 0) {
        $temp_array["CustomFields"] = $custom_fields;
    }
    
    error_log_dump_associative_array("before json_encode", $temp_array);
    $json_message = json_encode($temp_array);
    $tmp = gettype($json_message);
    error_log("After json_encode, message(type: $tmp): $json_message");
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["uw_request_json"] = $json_message;

    return $return_array;
}

/***************************************************
 *
 ***************************************************/
function mm_calculate_monthly_income_amt($application_nbr) {
    error_log("flow: mm_calculate_monthly_income_amt ($application_nbr)\n");
    //Needs error handling on missing applications
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    $MonthlyIncome = 0;
    $OtherIncome = 0;
    $TotalIncome = 0;
    $application_data = mm_get_application_details($application_nbr);
    //Get Application specific state details like gross income for Illinois
    if($application_data["app_data"]['state'] == 'IL'){
        $application_state_specific_data_obj = mm_get_application_state_specific_details($application_nbr);
                    $application_state_specific_data = $application_state_specific_data_obj["app_data"];

    }

    if ($application_data["return_value"] != 0) {
        //Insert Error Handling
        $return_value = 1;
        $return_message = "Unable to get the application details for application $application_nbr";
    } else {
        $app_data = $application_data["app_data"];
        $state_code = $app_data['state'];

        //$app_data["secondary_income_ind"] = "No";
        if ($app_data["income_source_1"] == "I'm self-employed") {
            if($state_code == 'IL'){
                $MonthlyIncome = floor($application_state_specific_data->gross_income_1);
            }else{
                $MonthlyIncome = floor($app_data["monthly_income_amt_1"]);
            }
        } else if ($app_data["income_source_1"] == "I work for a company") {
            if ($app_data["wage_type_1"] == "Hourly") {
                $MonthlyIncome = floor($app_data["hourly_rate_1"] * min($app_data["hours_per_week_1"], 40) * 52 / 12);
            } else if ($app_data["wage_type_1"] == "Salary") {
                $MonthlyIncome = floor($app_data["annual_salary_1"] / 12);
            } else {
                //Expecting a wage type and didn't get it so add some error handling and default the income to a failing amount
                mm_log_error('mm_calculate_monthly_income_amt', "Unexpected Wage Type For Company Employee so setting income to 0");
                $return_value = 1;
                $MonthlyIncome = 0;
            }
        } else if ($app_data["income_source_1"] == "I receive Social Security / disability benefits" || $app_data["income_source_1"] == "I receive pension / retirement benefits" || $app_data["income_source_1"] == "Other / not listed") {
            $monthly_frequency = 0;
            if ($app_data["pay_frequency_1"] == "Paid Monthly") {
                $monthly_frequency = 1;
            } else if ($app_data["pay_frequency_1"] == "Paid Twice Monthly") {
                $monthly_frequency = 2;
            } else if ($app_data["pay_frequency_1"] == "Bi-Weekly") {
                $monthly_frequency = 2.16667;
            } else if ($app_data["pay_frequency_1"] == "Paid Weekly") {
                $monthly_frequency = 4.33333;
            } else {
                //Unexpected value for pay frequency so log an error message
                $unexpected_pay_frequency = $app_data["pay_frequency_1"];
                mm_log_error('mm_calculate_monthly_income_amt', "Unexpected Pay Frequency 1 Trying to calculate monthly income: $unexpected_pay_frequency");
                $return_value = 1;
                $MonthlyIncome = 0;
            }
            if($state_code == 'IL'){
                $MonthlyIncome = $monthly_frequency * $application_state_specific_data->gross_income_1;
            }else{
                $MonthlyIncome = $monthly_frequency * $app_data["last_paycheck_amt_1"];  
            }
            
            error_log("flow: mm_calculate_monthly_income_amt, MonthlyIncome calculated: $MonthlyIncome\n");
        } else {
            $unexpected_emp_type = $app_data["income_source_1"];
            mm_log_error('mm_calculate_monthly_income_amt', " Unable to calculate Income Due to Unexpected Income Type: $unexpected_emp_type");
            $return_value = 1;
            $MonthlyIncome = 0;
        }
        // mark: temporary until review with Jason
        if ($app_data["secondary_income_ind"] == "Yes") {
            error_log("flow: mm_calculate_monthly_income_amt, also has Secondary income\n");
            if ($app_data["income_source_2"] == "I'm self-employed") {
                if($state_code == 'IL'){
                    $OtherIncome = floor($application_state_specific_data->gross_income_2);
                }else{
                    $OtherIncome = floor($app_data["monthly_income_amt_2"]);
                }
            } else if ($app_data["income_source_2"] == "I work for a company") {
                if ($app_data["wage_type_2"] == "Hourly") {
                    $OtherIncome = floor($app_data["hourly_rate_2"] * min($app_data["hours_per_week_2"], 40) * 52 / 12);
                } else if ($app_data["wage_type_2"] == "Salary") {
                    $OtherIncome = floor($app_data["annual_salary_2"] / 12);
                } else {
                    //Expecting a wage type and didn't get it so add some error handling and default the income to a failing amount
                    mm_log_error('mm_calculate_monthly_income_amt', "Unexpected Wage Type For Other Income and COmpany Employee so setting income to 0");
                    $return_value = 1;
                    $OtherIncome = 0;
                }
            } else if ($app_data["income_source_2"] == "I receive social security / disability benefits" || $app_data["income_source_2"] == "I receive pension / retirement benefits" || $app_data["income_source_2"] == "Other / not listed") {
                $monthly_frequency = 0;
                if ($app_data["pay_frequency_2"] == "Paid Monthly") {
                    $monthly_frequency = 1;
                } else if ($app_data["pay_frequency_2"] == "Paid Twice Monthly") {
                    $monthly_frequency = 2;
                } else if ($app_data["pay_frequency_2"] == "Bi-Weekly") {
                    $monthly_frequency = 2.16667;
                } else if ($app_data["pay_frequency_2"] == "Paid Weekly") {
                    $monthly_frequency = 4.33333;
                } else {
                    //Unexpected value for pay frequency so log an error message
                    $unexpected_pay_frequency = $app_data["pay_frequency_2"];
                    mm_log_error('mm_calculate_monthly_income_amt', "Unexpected Pay Frequency 1 Trying to calculate monthly income: $unexpected_pay_frequency");
                    $return_value = 1;
                    $OtherIncome = 0;
                }
                if($state_code == 'IL'){
                    $OtherIncome = $monthly_frequency * $application_state_specific_data->gross_income_2;
                }else{
                    $OtherIncome = $monthly_frequency * $app_data["last_paycheck_amt_2"];
                }
                error_log("flow: mm_calculate_monthly_income_amt, OtherIncome calculated: $OtherIncome\n");
            } else {
                $unexpected_emp_type = $app_data["income_source_2"];
                mm_log_error('mm_calculate_monthly_income_amt', " Unable to calculate Income Due to Unexpected Income Type: $unexpected_emp_type");
                $OtherIncome = 0;
                $return_value = 1;
            }
        } else if ($app_data["secondary_income_ind"] == "No") {
            $OtherIncome = 0;
        } else {
            //Unexpected Value for Other Incomes.  Set value to zero and log an error message
            $secondary_income_ind = $app_data["secondary_income_ind"];
            $OtherIncome = 0;
            mm_log_error("mm_calculate_monthly_income_amt", "Unexpected Secondary Income Indicator: $secondary_income_ind.  Other Income set to 0");
            $return_value = 1;
        }

    }

    $TotalIncome = $OtherIncome + $MonthlyIncome;
    error_log("flow: mm_calculate_monthly_income_amt, TotalIncome returned: $TotalIncome\n");

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["total_income"] = $TotalIncome;
    $return_array["other_income"] = $OtherIncome;
    $return_array["monthly_income"] = $MonthlyIncome;

    return $return_array;
}

/*****************************************************************************************************************************************************************
 * Function mm_send_uw_request($app_data) - The purpose of this function is to 1) build an UW Request, set the
 *****************************************************************************************************************************************************************/
function mm_send_uw_request($app_data) {
    $application_nbr = isset($app_data["application_nbr"]) ? $app_data["application_nbr"] : -1;
    error_log("flow: mm_send_uw_request (application_number: $application_nbr)\n");
    $return_array = array();
    $uw_response = array();
    $uw_decision_nbr = 0;
    $application_nbr = $app_data["application_nbr"];
    $server_details = get_origination_system_connection_details();
    $uw_request_array = mm_build_uw_request($app_data);
    $uw_request_value = $uw_request_array["return_value"];
    $uw_request_json = $uw_request_array["uw_request_json"];
    $url = $server_details["url"];
    $url = $url . "UWAdd";
    $curl = curl_init($url);
    $return_value = 0;
    $return_message = '';

    if ($uw_request_value == 1) {
        //There was a problem generating the UW Request Add Error Handling
    } else {
        //Save the message being sent
        mm_log_uw_request($uw_request_json, $application_nbr);

        // todo: refactor
        $conn = mm_get_db_connection();
        $sql_string = "insert into mm_uw_reqeust_queue (application_nbr, state, request_json, attempts) values (?,?,?,?)";
        $stmt = $conn->prepare($sql_string);
        $one = 1;
        $state = 'SEND';
        $stmt->bind_param('sssi', $application_nbr, $state, $uw_request_json, $one);
        $stmt->execute();
        $new_row_id = $conn->insert_id;

        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $server_details["header_array"]);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $uw_request_json);

        $json_response = curl_exec($curl);
        mm_log_uw_response($json_response, $application_nbr);

        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($status != 201 && $status != 200) {
            $reason = strpos($json_response, 'timeout') !== false ? 'TIMEOUT' : 'OTHER';
            $state = 'ERROR';
            $sql_string = "update mm_uw_reqeust_queue set state = ?, reason = ?, last_response = ? where request_nbr = ?";
            $stmt = $conn->prepare($sql_string);
            $stmt->bind_param('ssss', $state, $reason, $json_response, $new_row_id);
            $stmt->execute();
            //There was an error in the message sending  Add error handling
            $return_value = 1;
            $return_message = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);
        } else {
            $state = 'OK';
            $sql_string = "update mm_uw_reqeust_queue set state = ?, last_response = ? where request_nbr = ?";
            $stmt = $conn->prepare($sql_string);
            $stmt->bind_param('sss', $state, $json_response, $new_row_id);
            $stmt->execute();

            //The request worked.  Process the request, update the database application status  For now just echo the response
            $uw_response = json_decode($json_response, true);
            $return_value = 0;
            $return_message = '';
            $insert_uw_decision = mm_create_uw_decision($json_response);
            $uw_decision_nbr = $insert_uw_decision["decision_nbr"];
        }
    }
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["uw_decision_nbr"] = $uw_decision_nbr;
    $return_array["uw_response"] = $uw_response;

    curl_close($curl);

    return $return_array;

}

/*****************************************************************************************************************
 * Function mm_create_preverif_decision($json_response)
 *****************************************************************************************************************/
function mm_create_preverif_decision($response_data) {
    error_log("flow: mm_create_preverif_decision\n");
    error_log("$response_data".$response_data["RequiredDocs"]);
    //NEED TO ADD ERROR HANDLING
    $return_array = array();
        $required_docs = '';

    //Default the variables
    $application_id = $response_data["ApplicationID"];
    $application_nbr = $response_data["application_nbr"];
    $app_status_cd = $response_data["AppStatusCode"];
    $app_status_desc = $response_data["AppStatusDesc"];
    $required_docs = $response_data["RequiredDocs"];
    
    $required_docs_names_arr = array();
    
    if(count($required_docs) > 0){
            error_log("$response_data ----".$required_docs);

        for($k=0;$k<count($required_docs);$k++){
            $requiredDocsArr[$k] = $required_docs[$k]['Name'];
        }
        $requiredDocs = implode(',',$requiredDocsArr);
    }else{
        $requiredDocs = '';
    }
    
    /*Code Commented by Swapna because the flags response is not handled correctly - changed on June 29th 2018
    $ofac_flag = array_key_exists("OFACFlag", $response_data) ? $response_data["OFACFlag"] : null;
    $facta_flag = array_key_exists("FACTAFlag", $response_data) ? $response_data["FACTAFlag"] : null;
    $bankruptcy_flag = array_key_exists("BankruptcyFlag", $response_data) ? $response_data["BankruptcyFlag"] : null;
    $manual_ach_flag = array_key_exists("ManualACHFlag", $response_data) ? $response_data["ManualACHFlag"] : null;
    $mm_outsort_flag = array_key_exists("MMOutsortFlag", $response_data) ? $response_data["MMOutsortFlag"] : null;
    $income_type_flag = array_key_exists("IncomeTypeFlag", $response_data) ? $response_data["IncomeTypeFlag"] : null;
    $ilpaystub_flag = array_key_exists("ILPaystubFlag", $response_data) ? $response_data["ILPaystubFlag"] : null;
    */
    $create_dt = date("Y-m-d H:i:s");
    
    //code added by swapna to handle the flags and insert
    $rules_list = $response_data["RuleList"];

    $rules_list_arr = array();
    
    if(count($rules_list) > 0){
            error_log("$response_data rules list----".$rules_list);

        for($k=0;$k<count($rules_list);$k++){
            $rules_list_arr[$k] = $rules_list[$k]['RuleName'];
            error_log($rules_list[$k]['RuleName']);
            error_log($rules_list[$k]['Result']);
            if($rules_list[$k]['RuleName'] == "OFACFlag"){
                $ofac_flag = $rules_list[$k]['Result'];
            }else if($rules_list[$k]['RuleName'] == "FACTAFlag"){
                $facta_flag = $rules_list[$k]['Result'];
            }else if($rules_list[$k]['RuleName'] == "BankruptcyFlag"){
                $bankruptcy_flag = $rules_list[$k]['Result'];
            }else if($rules_list[$k]['RuleName'] == "ManualACHFlag"){
                $manual_ach_flag = $rules_list[$k]['Result'];
            }else if($rules_list[$k]['RuleName'] == "MMOutsortFlag"){
                $mm_outsort_flag = $rules_list[$k]['Result'];
            }else if($rules_list[$k]['RuleName'] == "IncomeTypeFlag"){
                $income_type_flag = $rules_list[$k]['Result'];
            }else if($rules_list[$k]['RuleName'] == "ILPaystubFlag"){
                $ilpaystub_flag = $rules_list[$k]['Result'];
            }
        }
    }else{
        $ofac_flag = null;
        $facta_flag = null;
        $bankruptcy_flag = null;
        $manual_ach_flag = null;
        $mm_outsort_flag = null;
        $income_type_flag = null;
        $ilpaystub_flag = null;
    }

    //Build the query string
    $sql_string = "insert into mm_preverif_decision ( orig_appid, application_nbr, app_status_cd, app_status_desc,
                      required_docs, create_dt, ofac_flag, facta_flag, bankruptcy_flag, manual_ach_flag, mm_outsort_flag, income_type_flag, ilpaystub_flag)
                      values (?,?,?,?,?,?,?,?,?,?,?,?,?)";

    //Get connection, prepare statement, bind parameters, and execute query
    $conn = mm_get_db_connection();
    $stmt = $conn->prepare($sql_string);
    $stmt->bind_param('iisssssssssss', $application_id, $application_nbr, $app_status_cd, $app_status_desc, $requiredDocs, $create_dt,
        $ofac_flag, $facta_flag, $bankruptcy_flag, $manual_ach_flag, $mm_outsort_flag, $income_type_flag, $ilpaystub_flag);
    $stmt->execute();
    $insert_nbr = $stmt->insert_id;
    mm_update_database_value('mm_application', 'preverif_decision_nbr', $insert_nbr, 'i', 'application_nbr', $application_nbr);

    //close database connections
    if (is_resource($conn)) {
        $conn->close();
    }

    //return values;
    return $return_array;

}

/*****************************************************************************************************************
 * Function mm_create_lead_detail($unique_id, $ip_address, $http_user_agent, $page_desc)
 *****************************************************************************************************************/
function mm_create_lead_detail($unique_id, $ip_address, $http_user_agent, $page_desc) {
    error_log("flow: mm_create_lead_detail\n");
    //NEED TO ADD ERROR HANDLING
    $return_array = array();
    //Default the variables
    $create_dt = date("Y-m-d H:i:s");

    //Build the query string
    $sql_string = "insert into mm_lead_detail (unique_id, ip_address, http_user_agent, page_desc, create_dt) values (?,?,?,?,?)";

    //Get connection, prepare statement, bind parameters, and execute query
    $conn = mm_get_db_connection();
    $stmt = $conn->prepare($sql_string);
    $stmt->bind_param('sssss', $unique_id, $ip_address, $http_user_agent, $page_desc, $create_dt);
    $stmt->execute();

    //$return_value = 0;

    //close database connections
    if (is_resource($conn)) {
        $conn->close();
    }

    //return values;
    return $return_array;

}

/*****************************************************************************************************************
 * Function mm_create_field_validation_error($unique_id, $ip_address, $form_id, $field_id, $value, $message)
 *****************************************************************************************************************/
function mm_create_field_validation_error($unique_id, $ip_address, $form_id, $field_id, $value, $message) {
    error_log("flow: mm_create_field_validation_error\n");
    //NEED TO ADD ERROR HANDLING
    $return_value = 0;
    $validation_error_id = 0;
    $return_message = '';
    $return_array = array();
    //Default the variables
    $create_dt = date("Y-m-d H:i:s");

    //Build the query string
    $sql_string = "insert into mm_field_validation_error (unique_id, ip_address, form_id, field_id, value, message, create_dt)  values (?,?,?,?,?,?,?)";
    try {
        $conn = mm_get_pdo_connection();
        $bind_params = [$unique_id, $ip_address, $form_id, $field_id, $value, $message, $create_dt];
        $stmt = $conn->prepare($sql_string);
        $stmt->execute($bind_params);
        $validation_error_id = $conn->lastInsertId();

    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "$e";
        return $return_array;
    }

    $conn = null;

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["validation_error_id"] = $validation_error_id;

    //return values;
    return $return_array;

}

/************************************************************************************************************************
 * Function mm_create_uw_decision($json_response)
 ************************************************************************************************************************/
function mm_create_uw_decision($json_response) {
    error_log("flow: mm_create_uw_decision\n");
    //Initialize the variables
    $return_array = array();
    $decision_data = json_decode($json_response, true);
    //error_log_dump_associative_array("mark: dumping values passed into mm_create_uw_decision", $decision_data);
    $orig_appid = $decision_data["AppId"];
    $app_status_cd = $decision_data["AppStatusCode"];
    $app_status_desc = $decision_data["AppStatusDesc"];
    $approved_amt = $decision_data["ApprovedAmount"];
    $is_approved = $decision_data["IsApproved"];
    $product = $decision_data["Product"];
    $reference_id = $decision_data["ReferenceId"];
    $reject_reason_1 = $decision_data["RejectReason1"];
    $reject_reason_2 = $decision_data["RejectReason2"];
    $reject_reason_3 = $decision_data["RejectReason3"];
    $reject_reason_4 = $decision_data["RejectReason4"];
    $reject_reason_cd = $decision_data["RejectReasonCode"];
    $segment = $decision_data["Segment"];
    $ssn = $decision_data["SSN"];
    $strategy = $decision_data["Strategy"];
    
    //Code added by swapna on Friday July 7th 2018 as parsing was not done properly from the UW response
    $responseXml = $decision_data["ResponseXml"];
    $p = xml_parser_create();
    xml_parse_into_struct($p, $responseXml, $vals, $index);
    xml_parser_free($p);
    //echo "Index array\n";
    //error_log((array) $index);
    //echo "\nVals array\n";
    //error_log((array) $vals);
    $fico = null;
   foreach ($vals as $xml_elem) {
        $x_tag=$xml_elem['tag'];
        $x_level=$xml_elem['level'];
        $x_type=$xml_elem['type'];
        error_log("xml data".$x_tag."--".$x_level."--".$x_type."--".$xml_elem['value']);
        if (strtolower($x_tag) == 'clrficoscore') {
            $fico = $xml_elem['value'];
        }
    }
    //$fico = array_key_exists('CLRFICOScore', $decision_data) ? $decision_data["CLRFICOScore"] : null;
    //end

    $approved_apr = array_key_exists('APR', $decision_data) ? $decision_data["APR"] : null;
    $approved_term = array_key_exists('Term', $decision_data) ? $decision_data["Term"] : null;

    $clr_action = array_key_exists("CLRAction", $decision_data) ? $decision_data["CLRAction"] : null;
    $clr_exp_auto_most_neg_last_12 = array_key_exists("CLREXPAutoMostNegLast12", $decision_data) ? $decision_data["CLREXPAutoMostNegLast12"] : null;
    $clr_exp_bankcard_total_bal_last_6 = array_key_exists("CLREXPBankcardTotalBalLast6", $decision_data) ? $decision_data["CLREXPBankcardTotalBalLast6"] : null;
    $clr_exp_bankcard_most_neg_last_6 = array_key_exists("CLREXPBankcardMostNegLast6", $decision_data) ? $decision_data["CLREXPBankcardMostNegLast6"] : null;
    $clr_exp_mtg_most_neg_last_6 = array_key_exists("CLREXPMtgMostNegLast6", $decision_data) ? $decision_data["CLREXPMtgMostNegLast6"] : null;
    $clr_ccb_num_ssns = array_key_exists("CLRCBBNumSSNs", $decision_data) ? $decision_data["CLRCBBNumSSNs"] : null;
    $clr_cca_days_since_last_inq = array_key_exists("CLRCCADaysSinceLastInq", $decision_data) ? $decision_data["CLRCCADaysSinceLastInq"] : null;
    $clr_cca_num_total_loans = array_key_exists("CLRCCANumTotalLoans", $decision_data) ? $decision_data["CLRCCANumTotalLoans"] : null;
    $clr_cca_days_since_last_loan_open = array_key_exists("CLRCCADaysSinceLastLoanOpen", $decision_data) ? $decision_data["CLRCCADaysSinceLastLoanOpen"] : null;
    $clr_cca_num_loans_current = array_key_exists("CLRCCANumLoansCurrent", $decision_data) ? $decision_data["CLRCCANumLoansCurrent"] : null;
    $clr_cca_amt_loans_current = array_key_exists("CLRCCAAmtLoansCurrent", $decision_data) ? $decision_data["CLRCCAAmtLoansCurrent"] : null;
    $clr_cca_amt_loans_past_due = array_key_exists("CLRCCAAmtLoansPastDue", $decision_data) ? $decision_data["CLRCCAAmtLoansPastDue"] : null;
    $clr_cca_num_loans_past_due = array_key_exists("CLRCCANumLoansPastDue", $decision_data) ? $decision_data["CLRCCANumLoansPastDue"] : null;
    $clr_cca_num_loans_paid_off = array_key_exists("CLRCCANumLoansPaidOff", $decision_data) ? $decision_data["CLRCCANumLoansPaidOff"] : null;
    $clr_cca_days_since_last_loan_paid = array_key_exists("CLRCCADaysSinceLastLoanPaid", $decision_data) ? $decision_data["CLRCCADaysSinceLastLoanPaid"] : null;
    $clr_cca_days_since_last_loan_co = array_key_exists("CLRCCADaysSinceLastLoanCO", $decision_data) ? $decision_data["CLRCCADaysSinceLastLoanCO"] : null;
    $clr_cca_worst_payment_rating = array_key_exists("CLRCCAWorstPaymentRating", $decision_data) ? $decision_data["CLRCCAWorstPaymentRating"] : null;
    $clr_cca_active_duty_status = array_key_exists("CLRCCAActiveDutyStatus", $decision_data) ? $decision_data["CLRCCAActiveDutyStatus"] : null;

    $decision_nbr = 0;

    $transaction_dt = $decision_data["TransactionDate"];
    $transaction_dt = date_create($transaction_dt);
    $transaction_dt = date_format($transaction_dt, "Y-m-d H:i:s");

    $create_dt = date("Y-m-d H:i:s");

    //Build Insert Query
    $sql_string = "insert into mm_uw_decision(orig_appid, app_status_cd, app_status_desc, approved_amt, is_approved, product,
                    application_nbr, reject_reason_1, reject_reason_2, reject_reason_3, reject_reason_4, reject_reason_cd,
                    segment, ssn, strategy, transaction_dt, create_dt, transaction_id, approved_apr, approved_term, clr_action,
                    clr_exp_auto_most_neg_last_12, clr_exp_bankcard_total_bal_last_6, clr_exp_bankcard_most_neg_last_6, clr_exp_mtg_most_neg_last_6,
                    clr_exp_cbb_num_ssns, clr_cca_days_since_last_inq, clr_cca_num_total_loans, clr_cca_days_since_last_loan_open,
                    clr_cca_num_loans_current, clr_cca_amt_loans_current, clr_cca_amt_loan_past_due, clr_cca_num_loans_past_due,
                    clr_cca_num_loans_paid_off, clr_cca_days_since_last_loan_paid, clr_cca_days_since_last_loan_co,
                    clr_cca_worst_payment_rating, clr_cca_active_duty_status,fico)
                    values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    //Connect to the database
    $conn = mm_get_db_connection();

    if (!$stmt = $conn->prepare($sql_string)) {
        mm_log_error("mm_create_uw_decision", "$conn->error", $conn->errno);
        $return_value = $conn->errno;
        $return_message = $conn->error;

    } else {
        $stmt->bind_param('issdssissssisssssidisidiiiiiiiddiiiiss', $orig_appid, $app_status_cd, $app_status_desc, $approved_amt, $is_approved, $product,
            $reference_id, $reject_reason_1, $reject_reason_2, $reject_reason_3, $reject_reason_4, $reject_reason_cd,
            $segment, $ssn, $strategy, $transaction_dt, $create_dt, $transaction_id, $approved_apr, $approved_term,
            $clr_action, $clr_exp_auto_most_neg_last_12, $clr_exp_bankcard_total_bal_last_6, $clr_exp_bankcard_most_neg_last_6,
            $clr_exp_mtg_most_neg_last_6, $clr_ccb_num_ssns, $clr_cca_days_since_last_inq, $clr_cca_num_total_loans,
            $clr_cca_days_since_last_loan_open, $clr_cca_num_loans_current, $clr_cca_amt_loans_current, $clr_cca_amt_loans_past_due,
            $clr_cca_num_loans_past_due, $clr_cca_num_loans_paid_off, $clr_cca_days_since_last_loan_paid, $clr_cca_days_since_last_loan_co,
            $clr_cca_worst_payment_rating, $clr_cca_active_duty_status,$fico);

        if (!$stmt->execute()) {
            $return_value = $conn->errno;
            $return_message = $conn->error;
            mm_log_error("mm_create_uw_decision", "$conn->error", $conn->errno);
        } else {

            $decision_nbr = $stmt->insert_id;
            $return_value = 0;
            $return_message = "Decision Added Successfully";
        }
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["decision_nbr"] = $decision_nbr;

    return $return_array;
}

/****************************************************************
 * Function mm_state_cd_to_state($state)
 ***************************************************************
 * @param string $state_cd
 * @return string
 */
function mm_state_cd_to_display(string $state_cd): string {
    error_log("flow: mm_state_cd_to_display\n");
    switch (strtoupper($state_cd)) {
        case "AL":
            $state = "Alabama";
            break;
        case "AK":
            $state = "Alaska";
            break;
        case "AZ":
            $state = "Arizona";
            break;
        case "AR":
            $state = "Arkansas";
            break;
        case "CA":
            $state = "California";
            break;
        case "CO":
            $state = "Colorado";
            break;
        case "CT":
            $state = "Connecticut";
            break;
        case "DE":
            $state = "Delaware";
            break;
        case "FL":
            $state = "Florida";
            break;
        case "GA":
            $state = "Georgia";
            break;
        case "HI":
            $state = "Hawaii";
            break;
        case "ID":
            $state = "Idaho";
            break;
        case "IL":
            $state = "Illinois";
            break;
        case "IN":
            $state = "Indiana";
            break;
        case "IA":
            $state = "Iowa";
            break;
        case "KS":
            $state = "Kansas";
            break;
        case "KY":
            $state = "Kentucky";
            break;
        case "LA":
            $state = "Louisiana";
            break;
        case "ME":
            $state = "Maine";
            break;
        case "MD":
            $state = "Maryland";
            break;
        case "MA":
            $state = "Massachusetts";
            break;
        case "MI":
            $state = "Michigan";
            break;
        case "MN":
            $state = "Minnesota";
            break;
        case "MS":
            $state = "Mississippi";
            break;
        case "MO":
            $state = "Missouri";
            break;
        case "MT":
            $state = "Montana";
            break;
        case "NE":
            $state = "Nebraska";
            break;
        case "NV":
            $state = "Nevada";
            break;
        case "NH":
            $state = "New Hampshire";
            break;
        case "NJ":
            $state = "New Jersey";
            break;
        case "NM":
            $state = "New Mexico";
            break;
        case "NY":
            $state = "New York";
            break;
        case "NC":
            $state = "North Carolina";
            break;
        case "ND":
            $state = "North Dakota";
            break;
        case "OH":
            $state = "Ohio";
            break;
        case "OK":
            $state = "Oklahoma";
            break;
        case "OR":
            $state = "Oregon";
            break;
        case "PA":
            $state = "Pennsylvania";
            break;
        case "RI":
            $state = "Rhode Island";
            break;
        case "SC":
            $state = "South Carolina";
            break;
        case "SD":
            $state = "South Dakota";
            break;
        case "TN":
            $state = "Tennessee";
            break;
        case "TX":
            $state = "Texas";
            break;
        case "UT":
            $state = "Utah";
            break;
        case "VT":
            $state = "Vermont";
            break;
        case "VA":
            $state = "Virginia";
            break;
        case "WA":
            $state = "Washington";
            break;
        case "WV":
            $state = "West Virginia";
            break;
        case "WI":
            $state = "Wisconsin";
            break;
        case "WY":
            $state = "Wyoming";
            break;
        default:
            $state = '';
    }

    return $state;
}

/******************************************************************************************************************************************************************
 * Function mm_state_to_cd($state) - Converts the state from long name to 2 char code
 *****************************************************************************************************************************************************************
 * @param $state
 * @return string
 */
function mm_state_to_cd(string $state): string {
    error_log("flow: mm_state_to_cd\n");
    switch (strtolower($state)) {
        case "alabama":
            $state_cd = "AL";
            break;
        case "alaska":
            $state_cd = "AK";
            break;
        case "arizona":
            $state_cd = "AZ";
            break;
        case "arkansas":
            $state_cd = "AR";
            break;
        case "california":
            $state_cd = "CA";
            break;
        case "colorado":
            $state_cd = "CO";
            break;
        case "connecticut":
            $state_cd = "CT";
            break;
        case "delaware":
            $state_cd = "DE";
            break;
        case "florida":
            $state_cd = "FL";
            break;
        case "georgia":
            $state_cd = "GA";
            break;
        case "hawaii":
            $state_cd = "HI";
            break;
        case "idaho":
            $state_cd = "ID";
            break;
        case "illinois":
            $state_cd = "IL";
            break;
        case "indiana":
            $state_cd = "IN";
            break;
        case "iowa":
            $state_cd = "IA";
            break;
        case "kansas":
            $state_cd = "KS";
            break;
        case "kentucky":
            $state_cd = "KY";
            break;
        case "louisiana":
            $state_cd = "LA";
            break;
        case "maine":
            $state_cd = "ME";
            break;
        case "maryland":
            $state_cd = "MD";
            break;
        case "massachusetts":
            $state_cd = "MA";
            break;
        case "michigan":
            $state_cd = "MI";
            break;
        case "minnesota":
            $state_cd = "MN";
            break;
        case "mississippi":
            $state_cd = "MS";
            break;
        case "missouri":
            $state_cd = "MO";
            break;
        case "montana":
            $state_cd = "MT";
            break;
        case "nebraska":
            $state_cd = "NE";
            break;
        case "nevada":
            $state_cd = "NV";
            break;
        case "new hampshire":
            $state_cd = "NH";
            break;
        case "new jersey":
            $state_cd = "NJ";
            break;
        case "new mexico":
            $state_cd = "NM";
            break;
        case "new york":
            $state_cd = "NY";
            break;
        case "north carolina":
            $state_cd = "NC";
            break;
        case "north dakota":
            $state_cd = "ND";
            break;
        case "ohio":
            $state_cd = "OH";
            break;
        case "oklahoma":
            $state_cd = "OK";
            break;
        case "oregon":
            $state_cd = "OR";
            break;
        case "pennsylvania":
            $state_cd = "PA";
            break;
        case "rhode island":
            $state_cd = "RI";
            break;
        case "south carolina":
            $state_cd = "SC";
            break;
        case "south dakota":
            $state_cd = "SD";
            break;
        case "tennessee":
            $state_cd = "TN";
            break;
        case "texas":
            $state_cd = "TX";
            break;
        case "utah":
            $state_cd = "UT";
            break;
        case "vermont":
            $state_cd = "VT";
            break;
        case "virginia":
            $state_cd = "VA";
            break;
        case "washington":
            $state_cd = "WA";
            break;
        case "west virginia":
            $state_cd = "WV";
            break;
        case "wisconsin":
            $state_cd = "WI";
            break;
        case "wyoming":
            $state_cd = "WY";
            break;
        default:
            $state_cd = 'XX';
    }

    return $state_cd;
}

/****************************************************************************************************************************************************
 * Function mm_submit_application() - Process the receipt of an application
 ****************************************************************************************************************************************************/
function mm_submit_application($app_data) {
    error_log("flow: mm_submit_application\n");
    //Initialize variables
    $return_array = array();
    $return_message = '';
    $return_value = 0;
    $noaa_cd = 0;
    $uw_decision = '';
    //$approved_amt = 0;
    $credit_approved_amt = 0;
    $mm_application_nbr = 0;
    $mm_account_nbr = 0;
    $mm_uw_decision_nbr = 0;
    $uw_application_nbr = 0;
    //$gf_lead_id = $app_data["gf_lead_id"];

    $email_address = $app_data["email_address"];
    $ssn = $app_data["ssn"];
    $state_cd = $app_data["state"];
    $app_status_cd = '';
    $reject_reason_cd = '';

    //Check to see if the accounts exist
    $email_check_array = mm_account_exists('email_address', $email_address);
    $email_acct_nbr = $email_check_array["account_nbr"];
    $email_exists = $email_check_array["return_value"];
    $ssn_check_array = mm_account_exists('ssn', $ssn);
    $ssn_acct_nbr = $ssn_check_array["account_nbr"];
    $ssn_exists = $ssn_check_array["return_value"];
    if ($ssn_exists == 1 && $email_exists == 1) {
        //Both the SSN and Email are tied to an existing account
        //error_log("mark: Both the SSN and Email are tied to an existing account");
        if ($ssn_acct_nbr != $email_acct_nbr) {
            //The SSN and Email address both exist but are tied to different applications
            $continue_processing = 0;
            $return_value = 1;
            $return_message = "The username and SSN provided don't match our system records";
        } else {
            //Both the SSN and email exist and they are tied to the same record
            $mm_account_nbr = $email_acct_nbr;
            $app_data["account_nbr"] = $mm_account_nbr;
            $continue_processing = 1;
            $_SESSION["mm_account_nbr"] = $mm_account_nbr;
        }

    } else if ($ssn_exists + $email_exists == 1) {
        //error_log("mark: This is an error condition because either the SSN or username exists but doesn't match the other item");
        //This is an error condition because either the SSN or username exists but doesn't match the other item
        $return_value = 1;
        $return_message = "The username or SSN provided don't match our system records.";
        $continue_processing = 0;
    } else {
        //Neither the SSN or Email Exist so create a new account record
        //error_log("mark: calling mm_create_account");
        $return_array = mm_create_account($app_data);
        $return_value = $return_array["return_value"];
        $return_message = $return_array["return_message"];
        if ($return_value > 0) {
            //The account wasn't created successfully
            //error_log("mark: The account wasn't created successfully");
            $return_value = 1;
            //$return_message = "The account record couldn't be created.";
            //$return_message = $return_message;
            $continue_processing = 0;
        } else {
            //The account was created successfully
            $continue_processing = 1;
            $mm_account_nbr = $return_array["account_nbr"];
            $app_data["account_nbr"] = $mm_account_nbr;
            //error_log("mark: The account was created successfully: account_nbr: $mm_account_nbr");
            //UPDATE SESSION VARIABLE HERE
            $_SESSION["mm_account_nbr"] = $mm_account_nbr;
        }
    }

    //The account related information was ok, try to get the LMS data and make sure it's ok.
    if ($continue_processing == 1) {
        //The account related information is ok so move to the next step

        //Determine that the customer 1) doesn't have an open loan, 2) hasn't paid off in the last 3 days and 3) doesn't have unpaid debt from a previous loan
        // todo: I think that mm_get_lp_loans needs to take into consideration the above line.
        $lms_data = mm_get_lp_loans($ssn, $email_address);
        $return_value = $lms_data["return_value"];
        $charge_off_ind = $lms_data["charge_off_ind"];
        $active_loan_ind = $lms_data["active_loan_ind"];
        //$former_ind = $lms_data["former_ind"];
        if ($return_value != 0) {
            //There was an issue getting the customer history
            $return_value = 1;
            $return_message = "There was a technical issue.";
            $continue_processing = 0;
            //ADD ADVANCED ERROR HANDLING and Possible set the application status to stuck?

        } else {
            //The LMS system was able to return customer history
            //NEED TO APPEND THIS INFORMATION TO THE UW REQUEST AND HAVE KLEVRLEND MAKE THE DECISION FOR CONSISTENCY
            $days_since_last_payoff = ''; //NEED TO FIX THIS
            if ($days_since_last_payoff > 3 || $active_loan_ind == 1 || $charge_off_ind == 1) {
                //The customer is not eligible to complete an application.  Need to get the messaging rig
                $continue_processing = 0;
                $return_value = 1;
                $return_message = "The customer is not eligible to fill out an application based on prior loan information.";

            } else {
                //The customer is eligible based on customer history data.  Append to the application array and continue processing.
                $continue_processing = 1;
            }
        }
    }

    //Based on LMS info the app is ok so add the record to the database.
    if ($continue_processing == 1) {
        $add_app_data = mm_create_application($app_data);
        if ($add_app_data["return_value"] != 0) {
            //Add ERROR Handling
            $return_value = $add_app_data["return_value"];
            $return_message = $add_app_data["return_message"];
            mm_log_error("mm_submit_application", "Error Adding Application Record: $return_message");
            $continue_processing = 0;
        } else {
            //Record was successfully added so continue
            $app_data["application_nbr"] = $add_app_data["application_nbr"];
            $mm_application_nbr = $add_app_data["application_nbr"];
            $continue_processing = 1;
            //mm_generate_application_pdf($mm_application_nbr);
            $pdf = new PDF;
            $pdf-> mm_generate_application_pdf($mm_application_nbr);
            //UPDATE LATER - cycle through the app_data array and put all of the values in a session for recall later
            foreach ($app_data as $key => $value) {
                $session_key = "app_$key";
                $_SESSION["$session_key"] = $value;
            }
        }
    }

    if ($continue_processing == 1) {
        //At this point, the application data is ok, and we were able to get customer history.  Now Need to construct the Klevrlend Request
        $uw_results = mm_send_uw_request($app_data);
        $request_return_value = $uw_results["return_value"];
        $request_return_message = $uw_results["return_message"];
        if ($request_return_value != 0) {
            //There was an error sending the message so add error handling
            $return_value = 1;
            $return_message = $request_return_message;
            mm_log_error("mm_submit_application", "Issue Sending Underwriting Request: $request_return_message");
            mm_update_database_value('mm_application', 'application_status', 9, 'i', 'application_nbr', $mm_application_nbr);
            $continue_processing = 0;
        } else {
            $continue_processing = 1;
            $uw_response = $uw_results["uw_response"];
            $app_status_cd = $uw_response["AppStatusCode"];
            $credit_approved_amt = $uw_response["ApprovedAmount"];
            //error_log("mark: approved amount after call to mm_send_uw_request: $approved_amt");
            $reject_reason_cd = $uw_response["RejectReasonCode"];
            $uw_application_nbr = $uw_response["AppId"];
            $uw_decision_nbr = $uw_results["uw_decision_nbr"];
            $approved_apr = $uw_response["APR"];
            $approved_term = $uw_response["Term"];
            $is_approved = $uw_response["IsApproved"];
            mm_update_database_value('mm_application', 'orig_appid', $uw_application_nbr, 'i', 'application_nbr', $mm_application_nbr);
            mm_update_database_value('mm_application', 'uw_decision_nbr', $uw_decision_nbr, 'i', 'application_nbr', $mm_application_nbr);
            mm_update_database_value('mm_application', 'approved_apr', $approved_apr, 'd', 'application_nbr', $mm_application_nbr);
            mm_update_database_value('mm_application', 'approved_term', $approved_term, 'i', 'application_nbr', $mm_application_nbr);

            if ($is_approved && (empty($approved_apr) || empty($approved_term))) {
                $return_value = 1;
                $return_message = "Values for APR or TERM from U/W is null, or zero";
                $continue_processing = 0;
                mm_log_error("mm_submit_application", "APR or TERM from U/W is null, or zero: apr($approved_apr), term($approved_term)");
                mm_update_database_value('mm_application', 'application_status', 9, 'i', 'application_nbr', $mm_application_nbr);
            }
        }

    }

    if ($continue_processing == 1) {
        //The UW request must have been successful so process the response.
        if ($app_status_cd == "Duplicate" || $app_status_cd == "Error" || $app_status_cd == "Pending") {
            $return_value = 1;
            $return_message = "There was an issue processing the application with the originations system. $app_status_cd";
            $uw_decision = "Stuck";
            
            $conn = mm_get_db_connection();
            $sql_string = "update mm_uw_reqeust_queue set state = 'ERROR', reason = ? where application_nbr = ?";
            $stmt = $conn->prepare($sql_string);
            $stmt->bind_param('si', $app_status_cd, $mm_application_nbr);
            $stmt->execute();
            
            $stmt->close();
            $conn->close();
            
            //Need to update the application status to stuck
            mm_update_database_value('mm_application', 'application_status', 9, 'i', 'application_nbr', $mm_application_nbr);
            //Need to add an error message
            mm_log_error("mm_submit_application", "Bad Application status: $app_status_cd for app: $mm_application_nbr");
        } else if ($app_status_cd == "Declined") {
            $return_value = 0;
            $return_message = "The application has been declined.";
            $uw_decision = "Declined";
            $noaa_cd = $reject_reason_cd;
            //Need to update the status of the application to declined and add the NOAA
            mm_update_database_value('mm_application', 'application_status', 8, 'i', 'application_nbr', $mm_application_nbr);
            mm_update_database_value('mm_application', 'noaa_cd', $noaa_cd, 'i', 'application_nbr', $mm_application_nbr);
            mm_update_database_value('mm_application', 'approved_amt', 0, 'd', 'application_nbr', $mm_application_nbr);
            //Need to generate a PDF NOAA
            $noaa_details = mm_generate_noaa_pdf($mm_application_nbr, $noaa_cd);
            $noaa_location = $noaa_details["document_path"];

            //Schedule the NOAA email to be sent
            $email_data = array();
            $email_data["to"] = $email_address;
            $email_data["from"] = SUPPORT_EMAIL;
            $email_data["from_name"] = SITE_NAME . " Support Team";
            $email_data["subject"] = SITE_NAME . " Application Decision";
            $email_data["txt_body"] = "We are writing to inform you of our decision regarding your recent application with " . SITE_NAME . ".  At this time, we are unable to grant your credit request.  Attached you will find the notice of adverse action which contains additional information as well as your rights for additional information regarding this decision.\n\nThanks for considering " . SITE_NAME . ",\n\n" . SITE_NAME . " Support Team\nEmail: " . SUPPORT_EMAIL . "\nPhone: " . SUPPORT_PHONE . "\n";
            $email_data["html_body"] = "<html><head></head><body><p>We are writing to inform you of our decision regarding your recent application with " . SITE_NAME . ".  At this time, we are unable to grant your credit request.  Attached you will find the notice of adverse action which contains additional information as well as your rights to request additional information regarding this decision.</p><br><p>Thanks for considering " . SITE_NAME . ",</p><br><p>" . SITE_NAME . " Support Team</p><p>Email: " . SUPPORT_EMAIL . "</p><p>Phone: " . SUPPORT_PHONE . "</p></body></html>";
            $email_data["attachment"] = $noaa_location;
            $tmp_dt = new DateTime();
            $tmp_dt = $tmp_dt->modify("+1 Day");
            $scheduled_dt = $tmp_dt->format("Y-m-d H:i:s");
            $email_data["scheduled_dt"] = $scheduled_dt;
            $email_message = json_encode($email_data);
            $email_results = mm_schedule_email_message($email_message);
            if ($email_results["return_value"] != 0 || $email_results["email_message_nbr"] == 0) {
                //The email scheduling failed so log an error
                mm_log_error('mm_submit_application', "Failed to schedule the NOAA email to be sent to $email_address");
            }

        } else if ($app_status_cd == "Approved") {
            //Update the application record with the approved status and the approval amount
            //Set the return variables
            $uw_decision = "Approved";
            //$approved_amt = $approved_amt;
            //Need to get the details to get the minimum between affordable_loan_amt and the KL Decision amt
            $temp_app_data = mm_get_application_details($mm_application_nbr);
            $temp_arr = $temp_app_data["app_data"];
            if ($temp_arr["pay_frequency_1"] == "Paid Monthly") {
                $affordable_pay_freq = "M";
            } else if ($temp_arr["pay_frequency_1"] == "Paid Twice Monthly") {
                $affordable_pay_freq = "S";
            } else if ($temp_arr["pay_frequency_1"] == "Bi-Weekly") {
                $affordable_pay_freq = "B";
            } else if ($temp_arr["pay_frequency_1"] == "Paid Weekly") {
                $affordable_pay_freq = "B";
            } else {
                //Unexpected pay frequency however, error should already be logged above
                //$unexpected_pay_freq = $app_data["pay_frequency_1"];
                $affordable_pay_freq = "B";
            }

            $cust_state = $temp_arr["state"];
            $income_data = mm_calculate_monthly_income_amt($mm_application_nbr);
            $monthly_income_amt = $income_data["total_income"];
            mm_update_database_value('mm_application', 'total_monthly_income_amt', $monthly_income_amt, 'd', 'application_nbr', $mm_application_nbr);
            $affordable_amt_array = mm_get_affordable_loan_amt($cust_state, $affordable_pay_freq, $monthly_income_amt, $approved_apr, $uw_response["ApprovedAmount"]);
            $affordable_loan_amt = $affordable_amt_array["affordable_loan_amt"];
            error_log("mark: affordable_loan_amt: $affordable_loan_amt\n");
            // if the return value is zero, then need to decline and send NOAA
            if ($affordable_loan_amt <= 0) {
                $temp_array = array();
                $temp_array["AppId"] = $uw_application_nbr;
                $temp_array["AppStatusCode"] = "Declined";
                $temp_array["AppStatusDesc"] = "Manually Declined";
                $temp_array["ApprovedAmount"] = 0;
                $temp_array["IsApproved"] = 0;
                $temp_array["Product"] = "";
                $temp_array["ReferenceId"] = $mm_application_nbr;
                $temp_array["RejectReason1"] = '';
                $temp_array["RejectReason2"] = '';
                $temp_array["RejectReason3"] = '';
                $temp_array["RejectReason4"] = '';
                $temp_array["RejectReasonCode"] = '';
                $temp_array["Segment"] = 'Manual';
                $temp_array["SSN"] = '';
                $temp_array["Strategy"] = 'Manual Strategy';
                $temp_date = new DateTime();
                $temp_date_string = $temp_date->format('Y-m-d H:i:s');
                $temp_array["TransactionDate"] = $temp_date_string;
                $json_response = json_encode($temp_array);
                $insert_uw_decision = mm_create_uw_decision($json_response);
                $uw_return_value = $insert_uw_decision["return_value"];
                if ($uw_return_value != 0) {
                    //There was an error inserting the Uw Record
                    mm_log_error('mm_update_application_status', "Failed adding a new U/W record to the database for application $mm_application_nbr");
                } else {
                    $uw_decision_nbr = $insert_uw_decision["decision_nbr"];
                    //Link the new underwriting record to the application record
                    mm_update_database_value('mm_application', 'uw_decision_nbr', $uw_decision_nbr, 'i', 'application_nbr', $mm_application_nbr);
                }

                $decline_reason = "61";
                $noaa_cd = 61;
                $uw_decision = 'Declined';
                $approved_apr = 0;
                $final_approved_amt = 0;

                //Update the application_status
                mm_update_database_value('mm_application', 'application_status', 8, 'i', 'application_nbr', $mm_application_nbr);
                mm_update_database_value('mm_application', 'application_sub_status', 0, 'i', 'application_nbr', $mm_application_nbr);
                mm_update_database_value('mm_application', 'noaa_cd', $decline_reason, 'i', 'application_nbr', $mm_application_nbr);
                mm_update_database_value('mm_application', 'approved_amt', $final_approved_amt, 'i', 'application_nbr', $mm_application_nbr);
                mm_update_database_value('mm_application', 'affordable_loan_amt', 0, 'i', 'application_nbr', $mm_application_nbr);

                //Generate a NOAA
                $noaa_results = mm_generate_noaa_pdf($mm_application_nbr, $decline_reason);
                if ($noaa_results["return_value"] != 0) {
                    //Failed to generate the NOAA PDF
                    $noaa_message = $noaa_results["return_message"];
                    mm_log_error('mm_update_application_status', "Failed creating a NOAA PDF for application $mm_application_nbr");
                    $return_value = 1;
                    $return_message = "There was an issue $noaa_message";
                } else {
                    //Schedule an email to be sent
                    $noaa_location = $noaa_results["document_path"];

                    //Schedule the NOAA email to be sent
                    $email_data = array();
                    $email_data["to"] = $email_address;
                    $email_data["from"] = SUPPORT_EMAIL;
                    $email_data["from_name"] = "Aspen Financial Direct Support Team";
                    $email_data["subject"] = "Aspen Financial Direct Application Decision";
                    $email_data["txt_body"] = "We are writing to inform you of our decision regarding your recent application with " . SITE_NAME . ".  At this time, we are unable to grant your credit request.  Attached you will find the notice of adverse action which contains additional information as well as your rights for additional information regarding this decision.\n\nThanks for considering " . SITE_NAME . ",\n\n" . SITE_NAME . " Support Team\nEmail:" . SUPPORT_EMAIL . "\nPhone :" . SUPPORT_PHONE . "\n";
                    $email_data["html_body"] = "<html><head></head><body>We are writing to inform you of our decision regarding your recent application with " . SITE_NAME . ".  At this time, we are unable to grant your credit request.  Attached you will find the notice of adverse action which contains additional information as well as your rights to request additional information regarding this decision.<br><br>Thanks for considering " . SITE_NAME . ",<br><br>" . SITE_NAME . " Support Team<br>Email: " . SUPPORT_EMAIL . "<br>Phone: " . SUPPORT_PHONE . "</body></html>";
                    $email_data["attachment"] = $noaa_location;
                    $tmp_dt = new DateTime();
                    //$tmp_dt = $tmp_dt->modify("+1");
                    $scheduled_dt = $tmp_dt->format("Y-m-d H:i:s");
                    $email_data["scheduled_dt"] = $scheduled_dt;
                    $email_message = json_encode($email_data);
                    $email_results = mm_schedule_email_message($email_message);
                    if ($email_results["return_value"] != 0 || $email_results["email_message_nbr"] == 0) {
                        //The email scheduling failed so log an error
                        mm_log_error('mm_update_application_status', "Failed to schedule the NOAA email to be sent to $email_address");
                        $return_value = 1;
                        $return_message = "Failed to send the NOAA email for $email_address";
                    }

                }
            } else {
                //error_log_dump_associative_array("In mm_submit_application before affordable_loan_amt calc, dumping", $temp_arr);
                //$affordable_loan_amt = $temp_arr["affordable_loan_amt"];
                $final_approved_amt = $affordable_loan_amt < $credit_approved_amt ? $affordable_loan_amt : $credit_approved_amt;
                mm_update_database_value('mm_application', 'application_status', 2, 'i', 'application_nbr', $mm_application_nbr);
                //mm_update_database_value('mm_application', 'approved_amt', $final_approved_amt, 'i', 'application_nbr', $mm_application_nbr);
                mm_update_database_value('mm_application', 'approved_amt', $final_approved_amt, 'i', 'application_nbr', $mm_application_nbr);
                
                mm_update_database_value('mm_application', 'affordable_loan_amt', $affordable_loan_amt, 'i', 'application_nbr', $mm_application_nbr);
                mm_update_database_value('mm_application', 'noaa_cd', 0, 'i', 'application_nbr', $mm_application_nbr);
            }
            
            mm_update_database_value('mm_application', 'credit_approved_amt', $credit_approved_amt, 'i', 'application_nbr', $mm_application_nbr);

        } else {
            //Received an application status that we weren't expecting to log an error.
            mm_log_error("mm_submit_application", "Were not expecting app_status_cd: $app_status_cd");
        }

    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["mm_account_nbr"] = $mm_account_nbr;
    $return_array["mm_application_nbr"] = $mm_application_nbr;
    $return_array["mm_uw_decision_nbr"] = $mm_uw_decision_nbr;
    $return_array["uw_application_nbr"] = $uw_application_nbr;
    $return_array["noaa_cd"] = isset($noaa_cd) ? $noaa_cd : 0;
    $return_array["noaa_location"] = isset($noaa_location) ? $noaa_location : '';
    $return_array["approved_amt"] = isset($final_approved_amt) ? $final_approved_amt : 0;
    //$return_array["approved_amt"] = $approved_amt;
    //$return_array["approved_amt"] = $final_approved_amt;
    $return_array["uw_decision"] = isset($uw_decision) ? $uw_decision : '';
    $return_array["state_cd"] = isset($state_cd) ? $state_cd : '';
    $return_array["contract_apr"] = $approved_apr;

    return $return_array;
}

/**************************************************************************************************************************
 * Function mm_expire_applications($num_days=10) - Function will pull all applications that are currently in an "active" state with a create date more than $num_days ago and will set the application status on the to Expired.
 **************************************************************************************************************************/

function mm_expire_applications($num_days = 10) {
    error_log("flow: mm_expire_applications\n");
    $return_array = array();
    $timestamp = new DateTime();
    $temp = $timestamp->format("Y-m-d H:i:s");
    $expiration_dt = $timestamp->modify("- $num_days days")->setTime(0, 0, 0);
    $exp_dt = $expiration_dt->format('Y-m-d H:i:s');
    $num_successfully_expired = 0;
    $num_errors = 0;

    //1.  Select all application where the create date is more than 10 days ago
    try {
        $conn = mm_get_pdo_connection();
        $sql_string = "select application_nbr, email_address, loan_agreement_nbr from mm_application where create_dt <= ? and application_status in (1,2,3,4,5,6,9) and application_nbr not in (1089,1090,1093,1094,1095,1096,1097,900020,901041,901042,901043)";
        //$sql_string = "select application_nbr, email_address from mm_application where create_dt <= ? and application_status in (2) and application_nbr not in (1089,1090,1093,1094,1095,1096,1097,900020,901041,901042,901043)";
        $bind_params = [$exp_dt];
        $stmt = $conn->prepare($sql_string);
        $stmt->execute($bind_params);
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $row_count = $stmt->rowCount();
        if ($row_count > 0) {
            //There are applications that need to be expired.
            foreach ($results as $row) {
                //NEED TO HANDLE ERRORS
                $application_nbr = $row["application_nbr"];
                $email_address = $row["email_address"];
                $loan_agreement_nbr = $row["loan_agreement_nbr"];
                mm_update_database_value("mm_application", 'application_status', 10, 'i', 'application_nbr', $application_nbr);
                //Generate a NOAA
                $noaa_results = mm_generate_noaa_pdf($application_nbr, 99);
                $noaa_return_value = $noaa_results["return_value"];
                if ($noaa_return_value != 0) {
                    $noaa_return_message = $noaa_results["return_message"];
                    $num_errors += 1;
                    mm_log_error('mm_expire_applications', "Failed Generating NOAA for application $application_nbr");
                } else {
                    //Schedule a NOAA to be sent
                    $email_data = array();
                    $noaa_location = $noaa_results["document_path"];
                    $email_data["to"] = $email_address;
                    $email_data["from"] = SUPPORT_EMAIL;
                    $email_data["from_name"] = SITE_NAME . " Support Team";
                    $email_data["subject"] = SITE_NAME . " Application Decision";
                    $email_data["txt_body"] = 'We are writing to inform you of our decision regarding your recent application with ".SITE_NAME.".  At this time, we are unable to grant your credit request.  Attached you will find the notice of adverse action which contains additional information as well as your rights for additional information regarding this decision.\n\nThanks for considering ".SITE_NAME.",\n\n".SITE_NAME." Support Team\nEmail: ".SUPPORT_EMAIL."\nPhone: ".SUPPORT_PHONE."\n';
                    $email_data["html_body"] = '<html><head></head><body><p>We are writing to inform you of our decision regarding your recent application with ".SITE_NAME.".  At this time, we are unable to grant your credit request.  Attached you will find the notice of adverse action which contains additional information as well as your rights to request additional information regarding this decision.</p><br><p>Thanks for considering ".SITE_NAME.",</p><br><p>".SITE_NAME." Support Team</p><p>Email: ".SUPPORT_EMAIL."</p><p>Phone: ".SUPPORT_PHONE."</p></body></html>';
                    $email_data["attachment"] = $noaa_location;
                    $tmp_dt = new DateTime();
                    $tmp_dt = $tmp_dt->modify("+1 Day");
                    $scheduled_dt = $tmp_dt->format("Y-m-d H:i:s");
                    $email_data["scheduled_dt"] = $scheduled_dt;
                    $email_message = json_encode($email_data);
                    $email_results = mm_schedule_email_message($email_message);
                    if ($email_results["return_value"] != 0 || $email_results["email_message_nbr"] == 0) {
                        //The email scheduling failed so log an error
                        mm_log_error('mm_submit_application', "Failed to schedule the NOAA email to be sent to $email_address");
                        $num_errors += 1;
                    } else {
                        //Add a customer note
                        $note_array = array();
                        $note_array["application_nbr"] = $application_nbr;
                        $note_array["category"] = "Application Decisioning";
                        $note_array["sub_category"] = "Application Expiration";
                        $note_array["username"] = "System";
                        $note_array["create_dt"] = $temp;
                        $note_array["txt_body"] = "Application has been expired.";
                        $note_json = json_encode($note_array);
                        $note_results = mm_add_application_note($note_json);
                        $note_return_value = $note_results["return_value"];
                        if ($note_return_value != 0) {
                            $note_return_message = $note_results["return_message"];
                            mm_log_error('mm_expire_applications', "Failed to add a note to application $application_nbr with error message $note_return_message");
                            $num_errors += 1;
                        } else {
                            //Attempt to delete the loan if it's on loan pro
                            if ($loan_agreement_nbr > 0) {
                                //There is a loan agreement in place so delete the loan from loan pro
                                $la_details = mm_get_loan_agreement_details($loan_agreement_nbr);
                                $display_id = $la_details["loan_nbr"];
                                //Get the loan id from loan pro from a display id
                                $lp_loan_details = mm_get_lp_loan_details($display_id);
                                $loan_id = $lp_loan_details["loan_id"];
                                $delete_results = mm_lp_delete_loan($loan_id);
                                $delete_return_value = $delete_results["return_value"];
                                $delete_return_message = $delete_results["return_message"];
                                if ($delete_return_value == 0) {
                                    $num_successfully_expired += 1;
                                } else {
                                    mm_log_error('mm_expire_applications', "Failed to delete the loan $display_id from loan pro.  The return message is: $delete_return_message");
                                }

                            }

                        }
                    }
                }
            }
        }

    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "$e";
        return $return_array;
    }

    $conn = null;

    $return_array["return_value"] = 0;
    $return_array["return_message"] = '';
    //Incorrectly assumes that all records were updated needs to handle errors from above
    $return_array["num_records_expired"] = $row_count;

    return $return_array;

}

/******************************************************************************************
 * Function mm_get_lp_customer_loans($customer_id)
 ******************************************************************************************/
function mm_get_lp_customer_loans($customer_id) {
    error_log("flow: mm_get_lp_customer_loans ($customer_id)\n");
    $loans_arr = array(array());
    $return_array = array();

    //Get all loans tied to a customer - iterate through them and grab the necessary details to be returned.
    try {
        $sdk = LPSDK::GetInstance();
        $loans = $sdk->GetLoansForCustomer($customer_id, [LOAN::LOAN_SETUP, LOAN::LOAN_SETTINGS, LOAN::STATUS_ARCHIVE]);
        $num_loans = 0;
        foreach ($loans as $loan) {
            $loan_setup = $loan->get(LOAN::LOAN_SETUP);
            $loan_status_archive = $loan->get(LOAN::STATUS_ARCHIVE);
            $current_archive = $loan_status_archive[0];
            $days_past_due = $current_archive->get("daysPastDue");
            $loan_settings = $loan->get(LOAN::LOAN_SETTINGS);
            $loan_status_id = $loan_settings->get(LOAN_SETTINGS::LOAN_STATUS_ID);
            $admin_stats = $loan->GetAdminStats();
            $current_balance = $admin_stats["active"]['principalBalance'];

            $next_payment = $loan->GetNextScheduledPayment();

            //Get Next Payment Amount
            $temp = $next_payment["scheduledPayment"]["date"];
            $epoch = filter_var($temp, FILTER_SANITIZE_NUMBER_INT);
            if ($epoch != "") {
                $next_payment_dt = DateTime::CreateFromFormat('U', $epoch)->format('m/d/Y');
            } else {
                $next_payment_dt = "";
            }

            //Get Next Payment Amount
            $next_payment_amt = $next_payment["scheduledPayment"]["chargeAmount"];

            $display_id = $loan->get(LOAN::DISP_ID);
            $loan_sub_status_id = $loan_settings->get(LOAN_SETTINGS::LOAN_SUB_STATUS_ID);
            //if ACTIVE Need to get days past due, next due dates, amount due next payment remaining principal balance
            $apr = $loan_setup->get(LOAN_SETUP::APR);
            //$principal_balance = $loan_status_archive->get(LOAN_STATUS_ARCHIVE::PRINCIPAL_BALANCE);
            //$days_past_due = $loan_status_archive->get(LOAN_STATUS_ARCHIVE::DAYS_PAST_DUE);

            $maturity_dt = DateTime::createFromFormat('U', $loan_setup->get(LOAN_SETUP::ORIG_FINAL_PAY_DATE))->format('m/d/Y');
            $contract_dt = DateTime::createFromFormat('U', $loan_setup->get(LOAN_SETUP::CONTRACT_DATE))->format('m/d/Y');
            $loans_arr[$num_loans]["apr"] = $apr;
            $loans_arr[$num_loans]["loan_sub_status_id"] = $loan_sub_status_id;
            $loans_arr[$num_loans]["loan_status_id"] = $loan_status_id;
            $loans_arr[$num_loans]["maturity_dt"] = $maturity_dt;
            $loans_arr[$num_loans]["display_id"] = $display_id;
            $loans_arr[$num_loans]["next_payment_dt"] = $next_payment_dt;
            $loans_arr[$num_loans]["next_payment_amt"] = $next_payment_amt;
            $loans_arr[$num_loans]["contract_dt"] = $contract_dt;
            $loans_arr[$num_loans]["principal_balance"] = $current_balance;
            $loans_arr[$num_loans]["days_past_due"] = $days_past_due;

            $num_loans += 1;

        }

    } catch (Exception $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "Error: $e";
        return $return_array;
    }

    $return_array["return_value"] = 0;
    $return_array["num_loans"] = $num_loans;
    $return_array["return_message"] = "Success";
    $return_array["loans_arr"] = $loans_arr;
    return $return_array;
}

/**********************************************************************************************************************************************
 * Function mm_get_account_status ($email_address)
 **********************************************************************************************************************************************/
function mm_get_account_status($email_address) {
    error_log("flow: mm_get_account_status ($email_address)\n");
    //Initialize any variables needed
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    //$account_status = '';
    $account_status = '';
    //$account_nbr = 0;
    $application_status = 0;
    $active_loan_ind = 0;
    $active_app_ind = 0;
    $charge_off_loan_ind = 0;
    $maturity_dt = '';
    $contract_dt = '';
    $loan_nbr = '';
    $first_name = '';
    $last_name = '';
    $next_payment_dt = '';
    $next_payment_amt = '';
    $loan_status_string = '';
    $loan_substatus_string = '';
    $days_past_due = 0;
    $principal_balance = 0;
    $lp_error = 0;

    $account_details = mm_get_account_details_from_email_address($email_address);
    $account_nbr = $account_details["account_nbr"];
    if ($account_nbr == 0) {
        error_log("mark: account_nbr is zero\n");
        //$lp_customer_id = 0;
        $charge_off_loan_ind = 0;
        $active_loan_ind = 0;
        $active_app_ind = 0;
    } else {
        error_log("mark: in else clause\n");
        $lp_customer_id = $account_details["lp_customer_id"];
        $account_nbr = $account_details["account_nbr"];
        $first_name = ucwords(strtolower($account_details["first_name"]));
        $last_name = ucwords(strtolower($account_details["last_name"]));
        if (!is_null($lp_customer_id)) {
            error_log("mark: lp_customer_id is not null\n");
            //Look up in Loan Pro and get the Loan History for the customer.  Loop through looking at the loan statuses looking for Active or Closed loans
            $results = mm_get_lp_customer_loans($lp_customer_id);
            if ($results["return_value"] != 0) {
                error_log("mark: no results condition\n");
                //LOG AN ERROR AND ADD ERROR HANDLING
                $lp_error = 1;
            } else {
                error_log("mark: satisfied else clause\n");
                //The function worked so loop through the loans and check the status of each one.
                $loans = $results["loans_arr"];
                foreach ($loans as $loan) {
                    $loan_status = $loan["loan_status_id"];
                    $loan_sub_status = $loan["loan_sub_status_id"];
                    if (($loan_status == 1 && $loan_sub_status == 2) || $loan_status == 2) {
                        //The customer has a loan that is Pending-Ready to Fund or they have a loan with an Active Status
                        $active_loan_ind = 1;
                        $maturity_dt = $loan["maturity_dt"];
                        $contract_dt = $loan["contract_dt"];
                        $loan_nbr = $loan["display_id"];
                        $next_payment_dt = $loan["next_payment_dt"];
                        $next_payment_amt = $loan["next_payment_amt"];
                        $days_past_due = $loan["days_past_due"];
                        $principal_balance = $loan["principal_balance"];

                        if ($loan_status == 1 && $loan_sub_status == 2) {
                            $loan_status_string = "PENDING FUND";
                        } else {
                            //The loan is active so provide a description based on the status
                            if ($loan_status == 2) {
                                $loan_status_string = "ACTIVE";
                            }
                        }
                    } else if ($loan_status == 4) {
                        $charge_off_loan_ind = 1;
                    }
                    //Need to add other conditions for closed
                    error_log("mark: loan_sub_status: $loan_sub_status\n");
                    //Adding loan sub status
                    if ($loan_sub_status == 1) {
                        $loan_substatus_string = "Signature";
                    } else if ($loan_sub_status == 2) {
                        $loan_substatus_string = "Ready To Fund";

                    } else if ($loan_sub_status == 9) {
                        $loan_substatus_string = "Active";

                    } else if ($loan_sub_status == 10) {
                        $loan_substatus_string = "Rescind Requested";

                    } else if ($loan_sub_status == 11) {
                        $loan_substatus_string = "Bankruptcy";

                    } else if ($loan_sub_status == 12) {
                        $loan_substatus_string = "Debt Management";

                    } else if ($loan_sub_status == 13) {
                        $loan_substatus_string = "Deceased";

                    } else if ($loan_sub_status == 14) {
                        $loan_substatus_string = "Paid Off";

                    } else if ($loan_sub_status == 15) {
                        $loan_substatus_string = "Settled";

                    } else if ($loan_sub_status == 16) {
                        $loan_substatus_string = "Rescinded";

                    } else if ($loan_sub_status == 17) {
                        $loan_substatus_string = "Funding Returned";

                    } else if ($loan_sub_status == 20) {
                        $loan_substatus_string = "Bankruptcy";

                    } else if ($loan_sub_status == 21) {
                        $loan_substatus_string = "Debt Management";

                    } else if ($loan_sub_status == 22) {
                        $loan_substatus_string = "Deceased";

                    } else if ($loan_sub_status == 23) {
                        $loan_substatus_string = "Charged Off";

                    } else if ($loan_sub_status == 24) {
                        $loan_substatus_string = "Sold";
                    } else if ($loan_sub_status == 31) {
                        $loan_substatus_string = "Bad Bank";
                    } else {
                        //Handle an unexpected substatus with an error
                    }

                }

            }
        }
        error_log("mark: prior to checking indicators ($charge_off_loan_ind,$active_loan_ind)\n");
        $application_sub_status = 0;
        //If active loan and active indicator are both still zero then the customer doesn't have an active or charged off loan.  Check to see if the customer has an active application
        if ($charge_off_loan_ind == 0 && $active_loan_ind == 0) {
            error_log("mark: indicators both zero\n");
            $result = mm_get_most_recent_application_details($account_nbr);
            $num_apps = $result["num_apps"];
            $app_data = $result["app_data"];
            if ($num_apps == 0) {
                //The customer has no applications so no need to do anything
            } else {
                $application_status = $app_data["application_status"];
                $application_state = $app_data["state"];
                $application_sub_status = $app_data["application_sub_status"];
            }
            if ($application_status == 1 || $application_status == 2 || $application_status == 3 || $application_status == 4 || $application_status == 5 || $application_status == 6 || $application_status == 9) {
                $active_app_ind = 1;
            } else if ($application_status == 7 && $application_sub_status == 1) {
                //The customer has completed the application process and has a loan that is pending loan pro setup
                $active_loan_ind = 1;
                $loan_status_string = "PENDING FUND";
                $loan_substatus_string = "Ready To Fund";
            }
        }
    }
    //Set the account_status and application_status fields
    error_log("mark: lp_error: $lp_error\n");
    if ($lp_error == 1) {
        $account_status = "ERROR";
        $return_message = "There was an error with Getting Information from Loan Pro";
        $return_value = 1;
    } else if ($charge_off_loan_ind == 1) {
        $account_status = "CHARGEDOFF";
    } else if ($active_loan_ind == 1) {
        //NEED to populate more fields given the loan is active
        $account_status = "ACTIVELOAN";
    } else if ($active_app_ind == 1) {
        $account_status = "ACTIVEAPP";
    } else {
        $account_status = "IDLE";
    }

    error_log("mark: returning account_status of: $account_status\n");
    $return_array["account_status"] = $account_status;
    $return_array["account_nbr"] = $account_nbr;
    $return_array["first_name"] = $first_name;
    $return_array["last_name"] = $last_name;
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["application_status"] = $application_status;
    $return_array["loan_status"] = $loan_status_string;
    $return_array["loan_substatus"] = $loan_substatus_string;
    $return_array["maturity_dt"] = $maturity_dt;
    $return_array["contract_dt"] = $contract_dt;
    $return_array["loan_nbr"] = $loan_nbr;
    $return_array["days_past_due"] = $days_past_due;
    $return_array["principal_balance"] = $principal_balance;
    $return_array["next_payment_dt"] = $next_payment_dt;
    $return_array["next_payment_amt"] = $next_payment_amt;
    $return_array["application_state"] = $application_state;
    return $return_array;

}

/**********************************************************************************************************
 * Function mm_send_ftp_document($source_file, $target_file, $target_path, $transmission_type)
 **********************************************************************************************************/
function mm_send_ftp_document($source_file, $target_file, $target_path = '', $transmission_type = 'A') {
    error_log("flow: mm_send_ftp_document: $source_file,$target_file,$target_path\n");
    error_log("mark: starting mm_send_ftp_document:  $source_file,$target_file,$target_path\n");
    //Need to add error handling
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    //$ftp_conn_details = array();
    $ftp_conn_details = mm_get_ftp_details();
    $ftp_server = $ftp_conn_details["server"];
    $ftp_username = $ftp_conn_details["username"];
    $ftp_password = $ftp_conn_details["password"];

    if ($transmission_type == "A") {
        $ftp_mode = FTP_ASCII;
    } else {
        $ftp_mode = FTP_BINARY;
    }

    // set up basic connection
    $conn_id = ftp_connect($ftp_server);

    // login with username and password
    $login_result = ftp_login($conn_id, $ftp_username, $ftp_password);

    //Try and cd to the directory.  If not successful then may need to create a the directory

    if ($target_path != '') {
        $directories = explode('/', $target_path);
        //$nav_path = '';
        $ftp_path = "ftp://$ftp_username:$ftp_password@$ftp_server/";
        foreach ($directories as $directory) {
            if ($directory != '') {
                $ftp_path = $ftp_path . "$directory/";
                $tmp = !file_exists($ftp_path);
                error_log("mark: checking file_exists:  $ftp_path, not file exists: $tmp\n");
                if (!file_exists($ftp_path)) {
                    //Need to add error handling
                    //echo "Path does not exist: $$ftp_path";
                    error_log("mark: FTP path does not exist: $ftp_path\n");
                    //error_log("mark: before ftp_mkdir:  $conn_id,$directory\n");
                    $tmp = ftp_mkdir($conn_id, $directory);
                    error_log("mark: after ftp_mkdir:  $tmp\n");
                } else {
                    //The directory already exists so no need to create it
                }
                error_log("mark: FTP changing to directory: $directory\n");
                ftp_chdir($conn_id, $directory);
            }
        }
    }

    // upload a file
    if (ftp_put($conn_id, $target_file, $source_file, $ftp_mode)) {
        $return_value = 0;
        $return_message = '';
    } else {
        $return_message = "There was a problem while uploading $source_file\n";
        $return_value = 1;
    }
    // close the connection
    ftp_close($conn_id);

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;

    return $return_array;

}

/**********************************************************************************************************
 * Function mm_send_ftp_document($source_file, $target_file, $target_path, $transmission_type)
 * Added by Swapna on June6th2018 To copy files to fa_ftp folder for first associates 
 **********************************************************************************************************/
function mm_send_fa_ftp_document($source_file, $target_file, $target_path = '', $transmission_type = 'A') {
    error_log("flow: mm_send_fa_ftp_document: $source_file,$target_file,$target_path\n");
    error_log("Swapna test fa ftp: starting mm_send_fa_ftp_document:  $source_file,$target_file,$target_path\n");
    //Need to add error handling
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    //$ftp_conn_details = array();
    $ftp_conn_details = mm_get_fa_ftp_details();
    $ftp_server = $ftp_conn_details["server"];
    $ftp_username = $ftp_conn_details["username"];
    $ftp_password = $ftp_conn_details["password"];

    if ($transmission_type == "A") {
        $ftp_mode = FTP_ASCII;
    } else {
        $ftp_mode = FTP_BINARY;
    }

    // set up basic connection
    $conn_id = ftp_connect($ftp_server);

    // login with username and password
    $login_result = ftp_login($conn_id, $ftp_username, $ftp_password);

    //Try and cd to the directory.  If not successful then may need to create a the directory

    if ($target_path != '') {
        //$directories = explode('/', $target_path);
        //$nav_path = '';
        $ftp_path = "ftp://$ftp_username:$ftp_password@$ftp_server/";
        //foreach ($directories as $directory) {
            //if ($directory != '') {
                $loan_date = date("Ymd",strtotime(date("Y-m-d").'+1 day'));
                $ftp_path = $ftp_path.FADIR."/";
                $tmp = !file_exists($ftp_path);
                error_log("Swapna test fa ftp: checking file_exists:  $ftp_path, not file exists: $tmp\n");
                if (!file_exists($ftp_path)) {
                    //Need to add error handling
                    //echo "Path does not exist: $$ftp_path";
                    error_log("Swapna test fa ftp: FTP path does not exist: $ftp_path\n");
                    //error_log("mark: before ftp_mkdir:  $conn_id,$directory\n");
                    $tmp = ftp_mkdir($conn_id, $loan_date);
                    error_log("Swapna test fa ftp: after ftp_mkdir:  $tmp\n");
                } else {
                    //The directory already exists so no need to create it
                }
                error_log("Swapna test fa ftp: FTP changing to directory: $directory\n");
                ftp_chdir($conn_id, $loan_date);
            //}
        //}
    }

    // upload a file
    if (ftp_put($conn_id, $target_file, $source_file, $ftp_mode)) {
        $return_value = 0;
        $return_message = '';
    } else {
        $return_message = "There was a problem while uploading $source_file\n";
        $return_value = 1;
    }
    // close the connection
    ftp_close($conn_id);

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;

    return $return_array;

}

/************************************************************************************************
 * Function mm_get_ftp_document($source_file, $target_file, $transmission_type)
 ******************************************************************************    ******************/
function mm_get_ftp_document($source_file, $target_file, $transmission_type) {
    error_log("flow: mm_get_ftp_document: $source_file, $target_file, $transmission_type\n");
    //Need to add error handling
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    //$ftp_conn_details = array();
    $ftp_conn_details = mm_get_ftp_details();
    $ftp_server = $ftp_conn_details["server"];
    $ftp_username = $ftp_conn_details["username"];
    $ftp_password = $ftp_conn_details["password"];

    if ($transmission_type == "A") {
        $ftp_mode = FTP_ASCII;
    } else {
        $ftp_mode = FTP_BINARY;
    }

    // set up basic connection
    //error_log("mark: attempting connection to: $ftp_server\n");
    $conn_id = ftp_connect($ftp_server);
    if ($conn_id) {
        //error_log("mark: ftp connection successful.\n");
        // login with username and password
        if (ftp_login($conn_id, $ftp_username, $ftp_password)) {
            error_log("mark: ftp login successful.\n");
            error_log("mark: attempting ftp_get with localfile: $target_file, remotefile: $source_file.\n");
            if (ftp_get($conn_id, $target_file, $source_file, $ftp_mode)) {
                $return_value = 0;
                $return_message = '';
            } else {
                $return_message = "There was a problem while downloading $source_file\n";
                $return_value = 1;
            }
        } else {
            // todo: change t0 mm_log_error
            error_log("mark: ftp login NOT successful.\n");
        }

    } else {
        // todo: change t0 mm_log_error
        error_log("mark: ftp connection failed.\n");
    }

    //}
    // close the connection
    ftp_close($conn_id);

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;

    return $return_array;

}

/*******************************************************
 * Function RandomToken - Get a Random string for using as part of a file name
 *******************************************************/
function RandomToken($length = 32) {
    error_log("flow: RandomToken\n");
    if (!isset($length) || intval($length) <= 8) {
        $length = 32;
    }
    if (function_exists('random_bytes')) {
        return bin2hex(random_bytes($length));
    }
    if (function_exists('mcrypt_create_iv')) {
        return bin2hex(mcrypt_create_iv($length, MCRYPT_DEV_URANDOM));
    }
    if (function_exists('openssl_random_pseudo_bytes')) {
        return bin2hex(openssl_random_pseudo_bytes($length));
    }
}

/*********************************************************************************
 * Function mm_send_email($to, $from, $subject, $txt_body, $html_body, $attachment_arr='')
 *********************************************************************************/
function mm_send_email($to, $from, $subject, $txt_body, $html_body = '', $attachment_arr = null, $log_email_ind = 0) {
    error_log("flow: mm_send_email ($to, $from, $subject, $txt_body, $html_body)\n");
    $return_array = array();
    $return_value = 0;
    $return_message = '';

    ini_set('SMTP', SITE_URI);
    ini_set('smtp_port', 25);
    ini_set("include_path", '/home/mmdtcusr/php:' . ini_get("include_path"));
    require_once 'Mail.php';
    require_once 'Mail/mime.php';
    $message = new Mail_mime();
    $message->setTXTBody($txt_body);
    $message->setHTMLBody($html_body);
    if (!is_null($attachment_arr)) {
        foreach ($attachment_arr as $attachment) {
            $message->addAttachment($attachment);
        }
    }

    $recipients = $to;
    $headers['From'] = $from;
    $headers['To'] = $to;
    $headers['Subject'] = $subject;

    $smtp = Mail::factory('smtp');

    $mail = $smtp->send($recipients, $message->headers($headers), $message->get());

    if (PEAR::isError($mail)) {
        $error_message = $mail->getMessage();
        mm_log_error('mm_send_email', "Failed Sending Email to $recipients with message $error_message");
        $return_value = 1;
        $return_message = "$error_message";
    } else {
        $return_value = 0;
        $return_message = "Email Sent Successfully";
    }

    //Potentially look at logging the email in the database at a later time.
    //$mail_log_arr = mm_log_email_message($to, $from, $subject,$txt_body, $html_body, $return_value);

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;

    return $return_value;

}

/**************************************************************************************
 * FUNCTION mm_pci_authenticate() - Get the authorization and secret values from PCI wallet
 **************************************************************************************/

function mm_pci_authenticate() {
    error_log("flow: mm_pci_authenticate\n");
    $return_array = array();
    $pci_credentials = mm_get_pci_credentials();
    $username = $pci_credentials["username"];
    $password = $pci_credentials["password"];

    //Prepare an array for building the json request
    $data_array = array();
    $data_array["username"] = $username;
    $data_array["password"] = $password;

    $json_message = json_encode($data_array);
    $url = "https://easypay.simnang.com/api/authenticate";
    $header_array = array("Content-Type: application/json", 'Accept: application/json');
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header_array);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $json_message);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $json_response = curl_exec($curl);

    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    if ($status != 201 && $status != 200) {
        //There was an error in the message sending  Add error handling
        $return_value = 1;
        $return_message = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);
        $token = '';
        $secret = '';
    } else {
        $response_array = json_decode($json_response, true);
        //$tokens = sizeof($response_array);

        $token = $response_array["token"];
        $secret = $response_array["secret"];
        $return_value = 0;
        $return_message = '';
    }

    //Set return array values
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["token"] = $token;
    $return_array["secret"] = $secret;
    return $return_array;
}

/**********************************************************************
 * Function mm_get_lp_pci_obo_token($secret, $authorization) - Get an OBO token for adding account information
 ************************************************************************/
function mm_get_lp_pci_obo_token($secret, $authorization) {
    error_log("flow: mm_pci_authenticate\n");
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    $obo_token = '';
    //$data_array = array();

    $url = "https://easypay.simnang.com/api/users/obo-token";
    $header_array = array("Content-Type: application/json", 'Accept: application/json', "authorization: $authorization", "secret: $secret");
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header_array);
    curl_setopt($curl, CURLOPT_POST, true);
    //curl_setopt($curl, CURLOPT_POSTFIELDS, $json_message);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $json_response = curl_exec($curl);

    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    if ($status != 201 && $status != 200) {
        //There was an error in the message sending  Add error handling
        $return_value = 1;
        $return_message = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);
        $obo_token = '';
    } else {
        $response_array = json_decode($json_response, true);
        $obo_token = $response_array["token"];
        $return_value = 0;
        $return_message = '';
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["obo_token"] = $obo_token;
    return $return_array;
}

/*******************************************************************************************
 * Function mm_create_lp_pci_customer()
 *******************************************************************************************/
function mm_create_lp_pci_customer($customer_data, $authorization, $secret) {
    error_log("flow: mm_create_lp_pci_customer\n");
    $return_array = array();
    $data_array = array();
    $customer_array = array();
    $primary_address_array = array();
    //$mail_address_array = array();

    $customer_array["birthdate"] = $customer_data["dob"];
    $customer_array["email"] = $customer_data["email_address"];
    $customer_array["first_name"] = $customer_data["first_name"];
    //$customer_array["generation_code"] = '';
    $customer_array["ssn"] = $customer_data["ssn"];
    //$customer_array["middle_name"] = '';
    //$customer_array["company_name"] = '';
    $customer_array["contact_name"] = $customer_data["first_name"] . " " . $customer_data["last_name"];
    $customer_array["last_name"] = $customer_data["last_name"];

    $primary_address_array["address1"] = $customer_data["street_address"];
    $primary_address_array["address2"] = $customer_data["appt_suite"];
    $primary_address_array["city"] = $customer_data["city"];
    $primary_address_array["state"] = $customer_data["state"];
    $primary_address_array["zipcode"] = $customer_data["zip_code"];
    $primary_address_array["country"] = "USA";

    $mailing_address_array["address1"] = $customer_data["street_address"];
    $mailing_address_array["address2"] = $customer_data["appt_suite"];
    $mailing_address_array["city"] = $customer_data["city"];
    $mailing_address_array["state"] = $customer_data["state"];
    $mailing_address_array["zipcode"] = $customer_data["zip_code"];
    $mailing_address_array["country"] = "USA";

    $customer_array["primary-address"] = $primary_address_array;
    $customer_array["mail-address"] = $mailing_address_array;

    $data_array["customer"] = $customer_array;

    $json_message = json_encode($data_array);

    $url = "https://easypay.simnang.com/api/customers";
    $header_array = array("Content-Type: application/json", 'Accept: application/json', "authorization: $authorization", "secret: $secret");
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header_array);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $json_message);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $json_response = curl_exec($curl);

    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    if ($status != 201 && $status != 200) {
        //There was an error in the message sending  Add error handling
        $return_value = 1;
        $return_message = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);
        $customer_id = '';
    } else {
        $response_array = json_decode($json_response, true);
        $customer_id = $response_array["id"];
        //$response_array = json_decode($json_response, true);
        $return_value = 0;
        $return_message = '';
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["customer_id"] = $customer_id;

    return $return_array;

}

/**********************************************************************************************
 * Function mm_add_pci_payment_profile($lp_customer_nbr, $pci_wallet_token) - Add the payment profile to the loan pro customer
 **********************************************************************************************/
function mm_add_pci_payment_profile($lp_customer_nbr, $pci_wallet_token) {
    error_log("flow: mm_add_pci_payment_profile\n");
    $return_array = array();

    //$type = \Simnang\LoanPro\Constants\PAYMENT_ACCOUNT\PAYMENT_ACCOUNT_TYPE__C::CHECKING;
    $type = "paymentAccount.type.checking";
    $savings_acct = false;
    $sdk = LPSDK::GetInstance();
    $pmt_profile = $sdk->CreateCustomerPaymentAccount("Checking", $type, $pci_wallet_token, $savings_acct);

    try {
        //grabs the customer and sets the payment account and then saves
        $customer = LPSDK::GetInstance()->GetCustomer($lp_customer_nbr)->Set(\Simnang\LoanPro\Constants\CUSTOMERS::PAYMENT_ACCOUNTS, $pmt_profile);
        $customer->Save();
        $return_value = 0;
        $return_message = '';
    } catch (\Simnang\LoanPro\Exceptions\ApiException $e) {
        $return_value = 1;
        $return_message = "There was an error adding the payment profile for loan pro customer $lp_customer_nbr.  The error message is $e.  The token trying to be added is $pci_wallet_token.";
        $return_array["return_value"] = $return_value;
        $return_array["return_message"] = $return_message;

        return $return_array;
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;

    return $return_array;

}

/***************************************************************************************
 * Function mm_create_lp_pci_customer_acct_link()
 ***************************************************************************************/
function mm_create_lp_pci_customer_acct_link($pci_customer_nbr, $acct_token, $authorization, $secret) {
    error_log("flow: mm_create_lp_pci_customer_acct_link\n");
    $return_array = array();

    $data_array = array();
    $checking_account = array();
    $checking_account["token"] = $acct_token;
    $data_array["checking-account"] = $checking_account;
    $json_message = json_encode($data_array);

    $url = "https://easypay.simnang.com/api/customers/{$pci_customer_nbr}/add-checking-account";
    $header_array = array("Content-Type: application/json", 'Accept: application/json', "authorization: $authorization", "secret: $secret");
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header_array);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($curl, CURLOPT_POSTFIELDS, $json_message);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $json_response = curl_exec($curl);

    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    if ($status != 201 && $status != 200) {
        //There was an error in the message sending  Add error handling
        $return_value = 1;
        $result = false;
        $return_message = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);
        $customer_id = '';
    } else {
        $response_array = json_decode($json_response, true);
        $result = $response_array["result"];
        $response_array = json_decode($json_response, true);
        $return_value = 0;
        $return_message = '';
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["result"] = $result;

    return $return_array;

}

/************************************************************************************
 * Function mm_create_lp_pci_acct()
 ************************************************************************************/
function mm_create_lp_pci_acct($app_data, $obo_token) {
    error_log("flow: mm_create_lp_pci_acct\n");
    $return_array = array();
    $data_array = array();
    $bank_account = array();

    $bank_account["address"] = $app_data["street_address"];
    $bank_account["zipcode"] = $app_data["zip_code"];
    $bank_account["state"] = $app_data["state"];
    $bank_account["city"] = $app_data["city"];
    $bank_account["country"] = 'USA';
    $bank_account["bank_name"] = 'Default Bank';
    $bank_account["accountholder_name"] = $app_data["first_name"] . " " . $app_data["last_name"];
    $bank_account["routing_number"] = $app_data["routing_nbr"];
    $bank_account["account_number"] = $app_data["bank_acct_nbr"];
    // mark was here:  fixed for bank account type that was entered on the front-end
    //$bank_account["account_type"] = "checking";
    $bank_account["account_type"] = strtolower($app_data["bank_acct_type"]);
    $data_array["checking-account"] = $bank_account;

    $json_message = json_encode($data_array);

    $url = "https://easypay.simnang.com/api/checking-account";
    $header_array = array("Content-Type: application/json", 'Accept: application/json', "authorization: $obo_token");
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header_array);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $json_message);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $json_response = curl_exec($curl);

    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    if ($status != 201 && $status != 200) {
        //There was an error in the message sending  Add error handling
        $return_value = 1;
        $return_message = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);
        $acct_token = '';
    } else {
        $response_array = json_decode($json_response, true);
        $acct_token = $response_array["token"];
        $response_array = json_decode($json_response, true);
        $return_value = 0;
        $return_message = '';
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["acct_token"] = $acct_token;

    return $return_array;

}

/********************************************************************************************
 * Function mm_create_lp_payment_profile($lp_loan_number) - Function take a loan pro loan ID, pulls the necessary information from the MM database, creates(future updates) a payment profile and links it to the LP customer
 ********************************************************************************************/
function mm_create_lp_payment_profile($lp_loan_nbr) {
    error_log("flow: mm_create_lp_payment_profile: $lp_loan_nbr\n");
    $return_array = array();
    $return_message = "";
    $application_nbr = substr($lp_loan_nbr, 1) * 1;
    $app_array = mm_get_application_details($application_nbr);
    $return_value = $app_array["return_value"];
    if ($return_value != 0) {
        $return_value = 1;
        $return_message = "Unable to get the application details related to $lp_loan_nbr";
        $return_array["return_value"] = $return_value;
        $return_array["return_message"] = $return_message;
        return $return_array;
    }
    $app_data = $app_array["app_data"];
    $account_nbr = $app_data["account_nbr"];
    $account_array = mm_get_account_details($account_nbr);
    $return_value = $account_array["return_value"];
    if ($return_value != 0) {
        $return_value = 1;
        $return_message = "Unable to get the account details related to $lp_loan_nbr";
        $return_array["return_value"] = $return_value;
        $return_array["return_message"] = $return_message;
        return $return_array;
    }

    $lp_customer_id = $account_array["lp_customer_id"];

    //$temp_array = array();
    $temp_array = mm_pci_authenticate();
    $return_value = $temp_array["return_value"];
    if ($return_value != 0) {
        //Insert Error Handling
        $return_value = 1;
        $temp_message = $temp_array["return_message"];
        $return_message = "Failed getting OBO Token with error: $temp_message";
    } else {
        //Authenticate worked successfully so get an OBO token for account setup
        $secret = $temp_array["secret"];
        $token = $temp_array["token"];
        $temp_array = mm_get_lp_pci_obo_token($secret, $token);
        $return_value = $temp_array["return_value"];
        if ($return_value != 0) {
            //Failed Getting OBO Token so set return value and message
            $return_value = 1;
            $temp_message = $temp_array["return_message"];
            $return_message = "Failed getting OBO Token with error: $temp_message";
        } else {
            $obo_token = $temp_array["obo_token"];
            $temp_array = mm_create_lp_pci_acct($app_data, $obo_token);
            $return_value = $temp_array["return_value"];
            if ($return_value != 0) {
                //Failed creating the PCI account so return the message
                $return_value = 1;
                $temp_message = $temp_array["return_message"];
                $return_message = "Failed Creating PCI Account with error: $temp_message";
            } else {
                $acct_token = $temp_array["acct_token"];
                $temp_array = mm_create_lp_pci_customer($app_data, $token, $secret);
                $return_value = $temp_array["return_value"];
                if ($return_value != 0) {
                    //Failed Adding the customer to PCI so set the return message appropriately
                    $return_value = 1;
                    $temp_message = $temp_array["return_message"];
                    $return_message = "Failed Creating PCI Customer with error: $temp_message";
                } else {
                    $customer_id = $temp_array["customer_id"];
                    $temp_array = mm_create_lp_pci_customer_acct_link($customer_id, $acct_token, $token, $secret);
                    $return_value = $temp_array["return_value"];
                    if ($return_value != 0) {
                        //There was an error linking the acct and customer in PCI Wallet
                        $return_value = 1;
                        $temp_message = $temp_array["return_message"];
                        $return_message = "Failed Linking PCI Customer to PCI Account with error: $temp_message";
                    } else {
                        $result = $temp_array["result"];
                        if ($result != true) {
                            //The link didn't return an error but was false so log a message
                        } else {
                            $temp_array = mm_add_pci_payment_profile($lp_customer_id, $acct_token);
                            $return_value = $temp_array["return_value"];
                            if ($return_value != 0) {
                                //There was an error adding the payment profile so return an error
                                $return_value = 1;
                                $temp_message = $temp_array["return_message"];
                                $return_message = "Failed adding Payment Profile to Loan Pro Customer $lp_customer_id: $temp_message";
                            } else {
                                //The payment profile was added successfully
                                $return_value = 0;
                                $return_message = '';
                            }
                        }
                    }
                }
            }
        }
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;

    return $return_array;
}

/********************************************************
 * Function mm_get_applications_by_status($app_status=0)
 ********************************************************/
function mm_get_application_details_by_status($app_status = 0) {
    error_log("flow: mm_get_application_details_by_status\n");
    $return_value = 0;
    $return_message = '';
    $num_rows = 0;
    $app_data = array(array());
    $dt = new DateTime();
    $dt = $dt->modify("-30 days");
    $dt_string = $dt->format('Y-m-d');
    if ($app_status == 0) {
        $sql_string = "select * from mm_application where create_dt >= '$dt_string' order by create_dt desc";
    } else {
        $sql_string = "select * from mm_application where application_status = ? and create_dt >= '$dt_string' order by create_dt desc";
    }
    $conn = mm_get_db_connection();
    $stmt = $conn->prepare($sql_string);
    if ($app_status != 0) {
        $stmt->bind_param('i', $app_status);
    }
    $stmt->execute();
    $rows = $stmt->get_result();
    $num_rows = $rows->num_rows;
    if ($num_rows == 0) {
        //The application id didn't exist so default all of the return values
        $return_value = 0;
        $return_message = "There were no applications associated with this account";
    } else {
        $i = 0;
        while ($row = $rows->fetch_assoc()) {
            foreach ($row as $key => $value) {
                $app_data[$i]["$key"] = $value;
            }
            $i += 1;
        }
        $return_value = 0;
        $return_message = '';
    }

    if (is_resource($conn)) {
        $conn->close();
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["app_data"] = $app_data;
    $return_array["num_apps"] = $num_rows;
    return $return_array;

}

/********************************************************************************************************************
 * Function mm_get_affordable_loan_amt($state_cd, $pay_freq, $monthly_income_amt)
 ********************************************************************************************************************/
function mm_get_affordable_loan_amt($state_cd, $pay_freq, $monthly_income_amt, float $apr, $approved_amt) {
    error_log("flow: mm_get_affordable_loan_amt ($state_cd, $pay_freq, $monthly_income_amt, $apr, $approved_amt)\n");

    $return_array = array();
    $return_value = 0;
    $return_message = '';
    $affordable_loan_amt = '';

    if (!in_array($apr, APPROVED_APRS)) {
        // sanity check to ensure we are calculating with the correct APR's
        $return_value = 1;
        $return_message = "APR not found in Approved APR's array";
    } else {
        error_log("swapna checking state".$state_cd);
        
        //$sql_string = "select max(loan_amt) as affordable_loan_amt
        //            from mm_pricing_detail
        //            where state = :state_cd and payment_freq = :pay_freq and monthly_income_amt <= :monthly_income_amt and apr = :apr";
        $sql_string = "select max(sq.loan_amt) as affordable_loan_amt
                       from (   select loan_amt
                                from mm_pricing_detail
                                where state = :state_cd and payment_freq = :pay_freq and monthly_income_amt <= :monthly_income_amt and apr = :apr
                            ) sq
                       where sq.loan_amt <= :approved_amt";
        try {
            $conn = mm_get_pdo_connection();
            //$results = $conn->query($sql_string);
            //$rows = $results->fetchAll();

            $stmt = $conn->prepare($sql_string);
            $stmt->execute([':state_cd' => $state_cd,
                ':pay_freq' => $pay_freq,
                ':monthly_income_amt' => $monthly_income_amt,
                ':apr' => $apr,
                ':approved_amt' => $approved_amt]);
            $rows = $stmt->fetchAll();

            $n = count($rows);
            $num_rows = count($rows);
            error_log("mark: In get_affordable_loan_amt, num_rows returned: $num_rows\n");
            if ($num_rows == 0) {
                $affordable_loan_amt = 0;
                $return_value = 0;
                $return_message = "Successful Query but insufficient income.";
            } else {
                //There should only be one result
                foreach ($rows as $row) {
                    error_log("mark: in loop:  {$row['affordable_loan_amt']}\n");
                    //If the return value is no then there isn't sufficient income for any product
                    if ($row["affordable_loan_amt"] == '') {
                        error_log("mark: setting affordable_loan_amt to zero.\n");
                        $affordable_loan_amt = 0;
                    } else {
                        error_log("mark: setting affordable_loan_amt to: {$row['affordable_loan_amt']}\n");
                        $affordable_loan_amt = $row["affordable_loan_amt"];
                    }
                }
                $return_value = 0;
                $return_message = "Success";
            }

        } catch (PDOException $e) {
            $return_array["return_value"] = 1;
            $return_array["return_message"] = "$e";
            $return_array["sql_string"] = $sql_string;
            mm_log_error('mm_affordable_loan_amt', "$e");
            return $return_array;
        }

        if (is_resource($conn)) {
            $conn = null;
        }
    }

    error_log("mark: returning return_value:  $return_value\n");
    error_log("mark: returning return_message:  $return_message\n");
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["affordable_loan_amt"] = $affordable_loan_amt;

    return $return_array;

}

/******************************************************************************************************
 * Function mm_get_approval_amt_display_values($state_cd, $approved_amt)
 *****************************************************************************************************
 * @param $state_cd
 * @param $approved_amt
 * @param $application_nbr
 * @return array
 */
function mm_get_approval_amt_display_values($state_cd, $approved_amt, $application_nbr) {
    error_log("flow: mm_get_approval_amt_display_values: ($state_cd, $approved_amt)\n");
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    $display_values = array();
    $i = 0;
    $sql_string = "select display_amt from mm_approved_amt_display where state = '$state_cd' and approved_amt = $approved_amt order by display_order asc";
    try {
        $conn = mm_get_pdo_connection();
        $results = $conn->query($sql_string);
        $rows = $results->fetchAll();
        //$n = count($rows);
        $num_rows = count($rows);
        if ($num_rows == 0) {
            $return_value = 1;
            $return_message = "No matching records for state $state_cd and approved amount $approved_amt.";
            mm_update_database_value("mm_application", 'application_status', 9, 'i', 'application_nbr', $application_nbr);
            mm_log_error('mm_get_approval_amt_display_values', $return_message);
        } else {
            //There should only be one result
            foreach ($rows as $row) {
                //If the return value is no then there isn't sufficient income for any product
                if ($row["display_amt"] == '') {
                    $return_value = 1;
                    $return_message = "There are no matching records for state $state_cd and approved amount $approved_amt.";
                    mm_update_database_value("mm_application", 'application_status', 9, 'i', 'application_nbr', $application_nbr);
                    mm_log_error('mm_get_approval_amt_display_values', $return_message);
                } else {
                    $display_value = $row["display_amt"];
                    $display_values[$i] = money_format('%0n', $display_value);
                    $i = $i + 1;
                }
            }
            $return_value = 0;
            $return_message = "Success";
        }

    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "$e";
        $return_array["sql_string"] = $sql_string;
        mm_update_database_value("mm_application", 'application_status', 9, 'i', 'application_nbr', $application_nbr);
        mm_log_error('mm_get_approval_amt_display_values', "$e");
        return $return_array;
    }

    if (is_resource($conn)) {
        $conn = null;
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["display_values"] = $display_values;

    return $return_array;
}

/******************************************************************************************************
 * Function mm_get_pricing_values($state_cd, $approved_amt)
 *****************************************************************************************************
 * @param $state_cd
 * @param $approved_amt
 * @param $pay_freq
 * @param $contract_apr
 * @param $application_nbr
 * @return array
 */
function mm_get_pricing_values($state_cd, $approved_amt, $pay_freq, $contract_apr, $application_nbr) {
    error_log("flow: mm_get_approval_amt_display_values: ($state_cd, $approved_amt, $pay_freq, $contract_apr, $application_nbr)\n");
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    $pricing_values = array();
    $sql_string = " select t1.display_amt, mpd.payment_amt, mpd.number_of_payments
                    from (select * from mm_approved_amt_display maad where maad.state = :state_cd and maad.approved_amt = :approved_amt) t1
                    join mm_pricing_detail mpd on mpd.loan_amt = t1.display_amt
                    where mpd.state = :state_cd and mpd.payment_freq = :pay_freq and mpd.apr = :contract_apr
                    order by t1.display_order";
    try {
        $conn = mm_get_pdo_connection();
        $stmt = $conn->prepare($sql_string);
        $stmt->execute([':state_cd' => $state_cd,
            ':pay_freq' => $pay_freq,
            ':approved_amt' => $approved_amt,
            ':contract_apr' => $contract_apr]);
        $rows = $stmt->fetchAll();
        $num_rows = count($rows);
        if ($num_rows == 0) {
            $return_value = 1;
            $return_message = "No matching records in mm_pricing_detail for state: $state_cd, approved_amt: $approved_amt, pay_freq: $pay_freq, contract_apr: $contract_apr.";
            mm_update_database_value("mm_application", 'application_status', 9, 'i', 'application_nbr', $application_nbr);
            mm_log_error('mm_get_pricing_values', $return_message);
        } else {
            //There should only be one result
            foreach ($rows as $row) {
                //$pricing_values[money_format('%0n',$row["display_amt"])]=$row["payment_amt"];
                $pricing_values[money_format('%0n', $row["display_amt"])] = array($row["payment_amt"], $row["number_of_payments"]);
            }
            $return_value = 0;
            $return_message = "Success";
        }

    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "$e";
        $return_array["sql_string"] = $sql_string;
        mm_update_database_value("mm_application", 'application_status', 9, 'i', 'application_nbr', $application_nbr);
        mm_log_error('mm_get_approval_amt_display_values', "$e");
        return $return_array;
    } finally {
        $conn = null;
    }

    //error_log_dump_associative_array("mark: at mm_get_pricing_values: ", $pricing_values);
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["pricing_values"] = $pricing_values;

    return $return_array;
}

/**************************************************************************************
 * Function mm_get_last_approved_amt($account_nbr)
 **************************************************************************************/
function mm_get_last_approved_amt($account_nbr) {
    error_log("flow: mm_get_last_approved_amt\n");
    $return_value = 0;
    $return_message = '';
    $last_approved_amt = 0;

    $sql_string = "select credit_approved_amt from mm_application as a inner join mm_account as b on a.account_nbr = b.account_nbr where a.account_nbr = $account_nbr and a.application_status = 7 order by application_nbr desc";

    try {
        $conn = mm_get_pdo_connection();
        $results = $conn->query($sql_string);
        $row = $results->fetch(PDO::FETCH_ASSOC);
        //If the return value is no then there isn't sufficient income for any product
        if ($row["credit_approved_amt"] == '') {
            $return_message = "No matching records for account_nbr $account_nbr";
        } else {
            $last_approved_amt = $row["credit_approved_amt"];
        }
        $return_message = "Success";
    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "$e";
        $return_array["sql_string"] = $sql_string;
        mm_log_error('mm_get_last_approved_amt', "$e");
        return $return_array;
    }

    if (is_resource($conn)) {
        $conn = null;
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["last_approved_amt"] = $last_approved_amt;

    return $return_array;
}

/*****************************************************************************
 * Function mm_ach_process_returns()
 *****************************************************************************/
function mm_ach_process_returns($run_dt = '') {
    error_log("flow: mm_ach_process_returns\n");
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    $ach_server_details = mm_get_ach_server_details();
    $ftp_server = $ach_server_details["server"];
    $ftp_username = $ach_server_details["username"];
    $ftp_password = $ach_server_details["password"];
    $ftp_port = $ach_server_details["port"];
    $log_path = mm_get_log_path();
    if ($run_dt == '') {
        $temp_dt = new DateTime();
        $run_dt = $temp_dt->format('Ymd');
    }
    //Using FTP download the return files to a temporary location
    if (!$sftp = new Net_SFTP($ftp_server, $ftp_port)) {
        $return_value = 1;
        $return_message = "Unable to connect to $ftp_server on port $ftp_port";
    } else if (!$sftp->login($ftp_username, $ftp_password)) {
        $return_value = 1;
        $return_message = "Failed logging in to server $ftp_server with username $ftp_username and password $ftp_password";
    } else {
        $sftp->get('Returns/RTNMMARTCR.txt', "$log_path" . "RTNMMARTCR.txt");
        $sftp->get('Returns/RTNMMARTDR.txt', "$log_path" . "RTNMMARTDR.txt");
        $sftp->get('NOC/NOCMMARTCR.txt', "$log_path" . "NOCMMARTCR.txt");
        $sftp->get('NOC/NOCMMARTDR.txt', "$log_path" . "NOCMMARTDR.txt");
        $rtn_dep_size = $sftp->size('Returns/RTNMMARTCR.txt');
        $rtn_pay_size = $sftp->size('Returns/RTNMMARTDR.txt');
        $noc_dep_size = $sftp->size('NOC/NOCMMARTCR.txt');
        $noc_pay_size = $sftp->size('NOC/NOCMMARTDR.txt');
        echo "Pay Size: $rtn_pay_size\n";
        echo "Pay Size: $rtn_dep_size\n";
        echo "Pay Size: $noc_dep_size\n";
        echo "Pay Size: $noc_pay_size\n";
    }

    //Loop through the files and see if there are any records
    $attachment_arr = array();
    if ($return_value != 0) {
        $subject = "ERROR: ACH Returns Process";
    } else if ($rtn_pay_size == 96 && $rtn_dep_size == 96 && $noc_dep_size ==
        96 && $noc_pay_size == 96) {
        //There are no returns
        $subject = "ACH Returns - No Returns Today";
    } else {
        $subject = "ATTN: ACH Returns - There are returns needing processed";
        //Construct the loanpro import file for the payment return file if there are payment returns to process
        if ($rtn_pay_size != 96) {
            $return_file = fopen("$log_path" . "RTNMMARTDR.txt", "r") or die("Unable to open input file!");
            $import_file = fopen("$log_path" . "lp_import_file.csv", "w+") or die("Unable to open input file!");
            $header_record =
                "action,loanid,id,amount,date,type,method,status,early,extraTowards,cashDrawer,info,chargeoff,reverseReason,nachaReturnCode,customFees,customPayoffFees,customInterest,customDiscount,customPrincipal,customEscrow1\n";
            //output the header record
            fwrite($import_file, $header_record);
            while ($line = fgets($return_file)) {
                //Loop through the kl file and build an EDW load record from each row.
                $line = str_replace("\r", "", $line);
                $line = str_replace("\n", "", $line);

                $record_type = substr($line, 0, 1);
                if ($record_type == 6) {
                    $tran_cd = substr($line, 1, 2);
                    $routing_nbr = substr($line, 3, 8);
                    $acct_nbr = substr($line, 12, 17);
                    $amt = substr($line, 29, 10);
                    $id = substr($line, 39, 15);
                    $loan_nbr = substr($id, 0, 9);
                    $payment_nbr = substr($id, 11);
                    $name = substr($line, 54, 22);

                } else if ($record_type == 7) {
                    $addenda_type = substr($line, 1, 2);
                    $return_cd = substr($line, 3, 3);

                    //Construct the return record
                    $output_line =
                        "update,$loan_nbr,$payment_nbr,,,,,reversed,,,,,,nachaErrorCode,$return_cd,,,,,,,\n";
                    fwrite($import_file, $output_line);

                } else {
                    //Not an expected record type so reset all of the variables
                    $tran_cd = '';
                    $routing_nbr = '';
                    $acct_nbr = '';
                    $amt = '';
                    $id = '';
                    $loan_nbr = '';
                    $payment_nbr = '';
                    $name = '';
                    $addenda_type = '';
                    $return_cd = '';
                    $output_line = '';
                }
            }
            fclose($return_file);
            fclose($import_file);
        }
    }
    //Construct an email with the necessary details and send it
    $to = 'jason.bumgarner@moneymart.com';
    $from = 'support@moneymartdirect.com';
    $txt_body = $return_message;
    $html_body = "<html><body><p>$return_message</p></body></html>";
    if ($return_value == 0) {
        $attachment_arr[0] = $log_path . "NOCMMARTDR.txt";
        $attachment_arr[1] = $log_path . "NOCMMARTCR.txt";
        $attachment_arr[2] = $log_path . "RTNMMARTCR.txt";
        $attachment_arr[3] = $log_path . "RTNMMARTDR.txt";
    }
    if ($rtn_pay_size != 96) {
        $attachment_arr[4] = $log_path . "lp_import_file.csv";
    }

    //Archive the documents
    $temp = mm_send_ftp_document($log_path . "RTNMMARTDR.txt",
        "RTNMMARTDR_${run_dt}.txt", "ach_documents/return_files/", "B");
    $temp = mm_send_ftp_document($log_path . "RTNMMARTCR.txt",
        "RTNMMARTCR_${run_dt}.txt", "ach_documents/return_files/", "B");
    $temp = mm_send_ftp_document($log_path . "NOCMMARTCR.txt",
        "NOCMMARTCR_${run_dt}.txt", "ach_documents/return_files/", "B");
    $temp = mm_send_ftp_document($log_path . "NOCMMARTDR.txt",
        "NOCMMARTDR_${run_dt}.txt", "ach_documents/return_files/", "B");
    if ($rtn_pay_size != 96) {
        $temp = mm_send_ftp_document($log_path . "lp_import_file.csv",
            "lp_import_file_${run_dt}.csv", "ach_documents/return_files/", "B");
    }

    //Send email - Need to add error handling
    mm_send_email($to, $from, $subject, $txt_body, $html_body, $attachment_arr, 1);

    //Delete the documents after sending them
    unlink($log_path . "RTNMMARTCR.txt");
    unlink($log_path . "RTNMMARTDR.txt");
    unlink($log_path . "NOCMMARTCR.txt");
    unlink($log_path . "NOCMMARTDR.txt");
    if ($rtn_pay_size != 96) {
        unlink($log_path . "lp_import_file.csv");
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;

    return $return_array;
}

/***********************************************************************************************************************
 * Function mm_log_page_view($data_array)
 ***********************************************************************************************************************/
function mm_log_page_view($data_array) {
    error_log("flow: mm_log_page_view\n");
    $return_value = 0;
    $return_message = '';
    $return_array = array();
    $page_view_nbr = 0;
    $create_datetime = new DateTime();
    $create_dt = $create_datetime->format('Y-m-d H:i:s');

    //Format the variables provided
    $ip_address = isset($data_array["ip_address"]) ? $data_array["ip_address"] : '';
    $request_time = isset($data_array["request_time"]) ? $data_array["request_time"] : '';
    $http_cookie = isset($data_array["http_cookie"]) ? $data_array["http_cookie"] : '';
    $page_name = isset($data_array["page_name"]) ? $data_array["page_name"] : '';

    $sql_string = "insert into mm_page_views (ip_address, http_cookie, page_name, request_time, create_dt) values (?,?,?,?,?)";

    try {
        $conn = mm_get_pdo_connection();
        $stmt = $conn->prepare($sql_string);
        $parms = [$ip_address, $http_cookie, $page_name, $request_time, $create_dt];
        $stmt->execute($parms);
        $page_view_nbr = $conn->lastInsertId();

    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "$e";
        $return_array["sql_string"] = $sql_string;
        return $return_array;
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["page_view_nbr"] = $page_view_nbr;

    return $return_array;

}

/*****************************************************************
 * Function mm_get_household_matches($json_data)-
 * Takes street_address, appt_sute, and first_name and determines if there are any applications
 * in an approved or funded status that are from the same address.  This is used to determine if verifications should be required.
 *****************************************************************/
function mm_get_household_matches($json_data) {
    error_log("flow: mm_get_household_matches\n");
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    $input_data = json_decode($json_data, true);
    $first_name = $input_data["first_name"];
    $street_address = $input_data["street_address"];
    $appt_suite = $input_data["appt_suite"];
    $application_nbr = $input_data["application_nbr"];
    $bind_params = [$street_address, $appt_suite, $first_name, $application_nbr];

    //select records where there is already another approved application in the household.
    $sql_string = "Select application_nbr from mm_application where street_address = ? and appt_suite = ? and first_name <> ? and application_status in (2,3,4,5,6,7) and application_nbr <> ?";
    try {
        $conn = mm_get_pdo_connection();
        $stmt = $conn->prepare($sql_string);
        $stmt->execute($bind_params);
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $row_count = $stmt->rowCount();
    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        //$row_count = 0;
        $return_array["return_message"] = "There was an issue executing the query $sql_string.  Error: $e";
        return $return_array;
    }

    $return_array["row_count"] = $row_count;
    $return_array["results"] = $results;
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;

    return $return_array;
}

/*******************************************************************************************************
 * Function mm_add_application_note($note_json)
 *******************************************************************************************************/

function mm_add_application_note($note_json) {
    error_log("flow: mm_add_application_note\n");
    $note_data = json_decode($note_json, true);
    $application_nbr = $note_data["application_nbr"];
    $category = $note_data["category"];
    $sub_category = $note_data["sub_category"];
    $txt_body = $note_data["txt_body"];
    $create_dt = $note_data["create_dt"];
    $username = $note_data["username"];
    $sql_string = "insert into mm_application_notes (application_nbr, category, sub_category, txt_body, create_dt, username) values (?,?,?,?,?,?)";
    try {
        $conn = mm_get_pdo_connection();
        $stmt = $conn->prepare($sql_string);
        $parms = [$application_nbr, $category, $sub_category, $txt_body, $create_dt, $username];
        $stmt->execute($parms);
        $note_nbr = $conn->lastInsertId();
        $return_array["return_value"] = 0;
        $return_array["return_message"] = '';
        $return_array["note_nbr"] = $note_nbr;

    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "$e";
        $return_array["sql_string"] = $sql_string;
        $return_array["note_nbr"] = 0;
    }

    if (is_resource($conn)) {
        $conn = null;
    }

    return $return_array;

}

/****************************************************************************************************************
 * Function mm_schedule_loan_funding($application_nbr)
 ****************************************************************************************************************/
function mm_schedule_loan_funding($application_nbr) {
    error_log("flow: mm_schedule_loan_funding\n");
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    $status = "READY TO BOOK";
    $create_dt = new DateTime();
    $create_dt_string = $create_dt->format('Y-m-d H:i:s');

    $sql_string = "insert into mm_loan_funding_queue (application_nbr, status, create_dt) values(?,?,?)";
    try {
        $conn = mm_get_pdo_connection();
        $stmt = $conn->prepare($sql_string);
        $parms = [$application_nbr, $status, $create_dt_string];
        $stmt->execute($parms);
        //$loan_funding_nbr = $conn->lastInsertId();

    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "$e";
        $return_array["sql_string"] = $sql_string;
        $loan_funding_nbr = 0;
        return $return_array;
    }

    if (is_resource($conn)) {
        $conn = null;
    }
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    return $return_array;

}
/*******************************************************************************
 * Function mm_lp_activate_loan
 * ****************************************************************************/
function mm_lp_activate_loan($lp_loan_id, $lp_loan_settings_id){
	$return_array = array();
        $lp_credentials = mm_get_lp_credentials();
        $account_token = $lp_credentials["token"];
        $auto_pal_id = $lp_credentials["tenant_id"];
        $header_array = array("Content-Type: application/json", 'Accept: application/json', "Authorization: $account_token", "Autopal-Instance-ID: $auto_pal_id");

	 $url = "https://loanpro.simnang.com/api/public/api/1/odata.svc/Loans(id=$lp_loan_id)";
	  error_log("Activate URL 1: $url\n");

	$request_array = array();
	$lp_settings_array = array();
	$lp_settings_array["loanStatusId"] = 1;
        $lp_settings_array["loanSubStatusId"] = 2;
	$lp_settings_array["__update"] = true;
	$lp_settings_array["__id"] = $lp_loan_settings_id;
	$request_array["LoanSettings"] = $lp_settings_array;
        

	$json_request = json_encode($request_array);
	error_log("Activate Request: $json_request\n");


	 $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER,$header_array );
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $json_request);
        curl_setopt($curl, CURLOPT_HTTPGET, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl,CURLOPT_CUSTOMREQUEST, "PUT");

        $json_response = curl_exec($curl);

        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        //echo "status: $status\n";
        error_log("Activate Response: $json_response\n");

        if ( $status != 201 && $status != 200 ) {
                //There was an error in the message sending  Add error handling
                $return_value = 1;
                $return_message = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);
		$display_id = 0;
                //echo "$return_message\n";
        }else{
                $response_array = json_decode($json_response, true);
                $d = $response_array["d"];
                $display_id = $d["displayId"];

		//Activate the loan
		$url = "https://loanpro.simnang.com/api/public/api/1/Loans($lp_loan_id)/AutoPal.Activate()";
		$curl = curl_init($url);
		$request_array = array();
		$json_request = json_encode($request_array);
	 	curl_setopt($curl, CURLOPT_HTTPHEADER,$header_array );
        	curl_setopt($curl, CURLOPT_POST, true);
        	curl_setopt($curl, CURLOPT_POSTFIELDS,$json_request );
        	//curl_setopt($curl, CURLOPT_POSTFIELDS, $json_request);
        	curl_setopt($curl, CURLOPT_HTTPGET, false);
        	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		error_log("Activate URL: $url\n");
		
		$json_response = curl_exec($curl);
		error_log("Activate Loan Response: $json_response\n");

		//Add error handling inc ase the activate command doesn't work
                $return_value = 0;
                $return_message = '';
        }
	
	
	$return_array["return_value"] = $return_value;
	$return_array["return_message"] = $return_message;
	$return_array["display_id"] = $display_id;
	return $return_array;

}


/**************************************************************
 *Function mm_lp_delete_loan($loan_id)
 **************************************************************/
function mm_lp_delete_loan($loan_id) {
    error_log("flow: mm_lp_delete_loan\n");
    $return_value = 0;
    $return_message = '';
    $return_array = array();

    if ($loan_id == '') {
        $return_value = 1;
        $return_message = "Invalid Loan Id: $loan_id";
    } else {
        $lp_credentials = mm_get_lp_credentials();
        $account_token = $lp_credentials["token"];
        $auto_pal_id = $lp_credentials["tenant_id"];
        $header_array = array("Content-Type: application/json", 'Accept: application/json', "Authorization: $account_token", "Autopal-Instance-ID: $auto_pal_id");
        $url = "https://loanpro.simnang.com/api/public/api/1/odata.svc/Loans($loan_id)";
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header_array);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $json_response = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($status != 201 && $status != 200) {
            //There was an error in the message sending  Add error handling
            $return_value = 1;
            $return_message = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);
        } else {
            //The delete command was successful
            $return_value = 0;
            $return_message = '';
        }

    }
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    return $return_array;

}
