<?php
/*
 * http://dev.moneymartdirect.com/QA_Not_For_Prod/generate_loan_agreement/generate_loan_agreement_test.php
 */
session_start();
require_once '../../wp-content/themes/moneymartdirect/include/mm_middleware.php';


class MyPDF extends PDF
{
    public function mm_generate_loan_agreement_pdf(int $application_nbr, $app_data_arr, $la_data, $payment_schedule_arr, $lender_data, $doc_path='PDFs/', $signature_data = 0) {
        //Get Data Needed to Complete the Document
        $app_data = $app_data_arr["app_data"];
        $payment_schedule = $payment_schedule_arr["payment_schedule"];

        //Initialize All of the variables needed
        $loan_number = $la_data["loan_nbr"];
        $fa_uploaded = $la_data["fa_uploaded"];
        $account_nbr = $app_data["account_nbr"];
        $temp_dt = new DateTime($la_data["origination_dt"]);
        $orig_dt = $temp_dt->format('m/d/Y');
        $orig_dt_yyyymmdd = $temp_dt->format('Ymd');
        $temp_dt = new DateTime($la_data["effective_dt"]);
        $contract_dt = $temp_dt->format('m/d/Y');  // Loan Pro Contract date
        $contract_dt_yyyymmdd = $temp_dt->format('Ymd');  // Loan Pro Contract date

        // This APR is from LoanPro and is exact
        $tila_apr = sprintf("%.2f", floor($la_data["apr"] * 100.0) / 100.0) . '%';
        $contract_apr = sprintf("%.2f", floor($app_data["approved_apr"] * 100.0) / 100.0);
        $lender_name = $lender_data["legal_name"];

        $lender_mailing_street_address = $lender_data["mailing_street_address"];
        $lender_physical_street_address = $lender_data["physical_street_address"];

        $lender_mailing_city = $lender_data["mailing_city"];
        $lender_physical_city = $lender_data["physical_city"];

        $lender_mailing_state = $lender_data["mailing_state_cd"];
        $lender_physical_state = $lender_data["physical_state_cd"];

        $lender_mailing_zip_code = $lender_data["mailing_zip_code"];
        $lender_physical_zip_code = $lender_data["physical_zip_code"];

        $lender_phone_nbr = $lender_data["phone_nbr"];
        $lender_dba_name = $lender_data["dba_name"];
        $customer_name = $app_data["first_name"] . " " . $app_data["last_name"];
        $customer_street_address = $app_data["street_address"] . " " . $app_data["appt_suite"];
        $customer_zip_code = $app_data["zip_code"];
        $customer_state = $app_data["state"];
        $customer_city = $app_data["city"];
        $customer_phone_nbr = "(" . substr($app_data["mobile_phone_nbr"], 0, 3) . ") " . substr($app_data["mobile_phone_nbr"], 3, 3) . "-" . substr($app_data["mobile_phone_nbr"], 6);
        $amount_financed = "\$" . $la_data["principal_amt"];
        $total_finance_charge = "\$" . $la_data["total_finance_charge"];
        $total_of_all_payments = "\$" . $la_data["total_amt_due"];
        $payment_freq = $la_data['payment_freq'];

        //Build the PDF document
        $pdf = new PDF('P', 'mm', 'Letter');
        $pdf->SetMargins(4, 4, 4);
        $pdf->AddPage();

        // top 2 lines
        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(0, 10, 'THIS AGREEMENT SHALL NOT CONSTITUTE A "NEGOTIABLE INSTRUMENT"', 0, 0, 'C');
        $pdf->Ln(5);
        $pdf->setFont('Arial', '', 12);
        if ($customer_state == 'MS') {
            $pdf->Cell(0, 10, 'Credit Availability Act Loan Agreement and Federal Truth-in-Lending Disclosures', 0, 0, 'C');
        } else {
            $pdf->Cell(0, 10, 'Consumer Installment Loan Agreement and Federal Truth-in-Lending Disclosures', 0, 0, 'C');
        }
        $pdf->Ln(10);

        // third line
        $tmp_y = $pdf->GetY();
        $pdf->Cell(0, 10, "Loan Number: $loan_number", 0, 0, 'C');
        $pdf->Ln(10);

        // two columns:  get the y value for the next column and set the left margin
        $tmp_y = $pdf->GetY();
        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(0, 10, 'BORROWER:', 0, 0, 'L');
        $pdf->Ln(6);
        $pdf->setFont('Arial', '', 12);
        $pdf->Cell(0, 10, "$customer_name", 0, 0, 'L');
        $pdf->Ln(5);
        $pdf->Cell(0, 10, "$customer_street_address", 0, 0, 'L');
        $pdf->Ln(5);
        $pdf->Cell(0, 10, "$customer_city, $customer_state $customer_zip_code", 0, 0, 'L');
        $pdf->Ln(5);
        $pdf->Cell(0, 10, "$customer_phone_nbr", 0, 0, 'L');

        $pdf->SetLeftMargin(100);
        $pdf->SetY($tmp_y);
        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(0, 10, 'LENDER NAME & LOCATION:', 0, 0, 'L');
        $pdf->Ln(6);
        $pdf->setFont('Arial', '', 12);

        $pdf->Cell(0, 10, "$lender_name" . " d/b/a $lender_dba_name", 0, 0, 'L');
        $pdf->Ln(5);
        $pdf->Cell(0, 10, "$lender_physical_street_address", 0, 0, 'L');
        $pdf->Ln(5);
        $pdf->Cell(0, 10, "$lender_physical_city, $lender_physical_state $lender_physical_zip_code", 0, 0, 'L');
        $pdf->Ln(5);
        $pdf->Cell(0, 10, "$lender_phone_nbr", 0, 0, 'L');
        if ($customer_state == 'CA') {
            // Lender License Number
            $pdf->Ln(5);
            $pdf->Cell(0, 10, '60DBO 74712', 0, 0, 'L');
        }
        $pdf->Ln(12);

        // dates
        $y_temp = $pdf->GetY();
        $pdf->Setx(0);
        $pdf->SetLeftMargin(4);
        $pdf->Cell(0, 4, "Origination Date: $orig_dt", 0, 2, 'L');
        $pdf->SetY($y_temp, false);
        $pdf->SetLeftMargin(100);
        $pdf->Cell(0, 4, "Disbursement Date: $contract_dt", 0, 2, 'L');
        $pdf->Setx(0);
        $pdf->SetLeftMargin(4);
        $pdf->Cell(0, 4, "Effective Date: $contract_dt", 0, 2, 'L');
        $pdf->Ln(2);

        // Terms and Conditions paragraph
        $text = $this->Generate_Terms_And_Conditions_Block($customer_state, $lender_name, $lender_dba_name);
        if ($customer_state == 'CA') {
            $pdf->setFont('Arial', '', 10);
        }
        $pdf->MultiCell(0, 4, $text, 0, 'L');
        if ($customer_state != 'CA') {
            $pdf->Ln(4);
        }
        $pdf->setFont('Arial', 'BU', 12);

        // Federal Truth-In-Lending Disclosures paragraph
        if ($customer_state == 'CA') {
            $pdf->MultiCell(0, 5, 'FOR INFORMATION CONTACT THE DEPARTMENT OF BUSINESS OVERSIGHT, STATE OF CALIFORNIA.  FEDERAL TRUTH-IN-LENDING DISCLOSURES', 0, 'C');
        } else {
            $pdf->Cell(0, 10, 'FEDERAL TRUTH-IN-LENDING DISCLOSURES', 0, 0, 'C');
            $pdf->Ln(15);
        }

        $page_height = $pdf->getPageHeight();
        $tila_x = $pdf->GetX();
        $tila_y = $pdf->GetY();

        // large/bold line width
        $pdf->SetLineWidth(1);
        $pdf->Rect($tila_x, $tila_y, 208, $page_height - $tila_y - 4);
        $tila_box_y = $pdf->GetY();
        $pdf->SetFillColor(240);
        $pdf->Rect(4, $tila_box_y, 50, 46, 'DF');
        $pdf->Rect(54, $tila_box_y, 55, 46, 'DF');
        $tila_box_x = $pdf->GetX() + 30;
        $pdf->SetX($tila_box_x);
        $pdf->setFont('Arial', 'B', 14);
        $y_temp = $pdf->GetY() + 4;
        $pdf->SetY($y_temp);
        $pdf->MultiCell(45, 4, 'ANNUAL PERCENTAGE RATE', 0, 'C');
        $pdf->SetY($y_temp);
        $pdf->SetX(60); // orig: 55
        $pdf->MultiCell(45, 4, 'FINANCE CHARGE', 0, 'C');
        $pdf->SetY($y_temp);
        $pdf->SetX(110); // orig 100
        $pdf->setFont('Arial', 'B', 12);
        $pdf->MultiCell(45, 4, 'Amount Financed', 0, 'C');
        $pdf->SetY($y_temp);
        $pdf->SetX(160); // orig 150
        $pdf->MultiCell(45, 4, 'Total of Payments', 0, 'C');

        $y_temp += 4;
        $pdf->SetY($y_temp);
        $pdf->SetX(150);
        $pdf->setFont('Arial', '', 12);
        $pdf->Ln(10);
        $y_temp = $pdf->GetY();
        $pdf->MultiCell(45, 4, 'The cost of your credit as a yearly rate.', 0, 'C');
        $pdf->SetY($y_temp);
        $pdf->SetX(60);
        $pdf->MultiCell(45, 4, 'The dollar amount the credit will cost you.', 0, 'C');
        $pdf->SetY($y_temp);
        $pdf->SetX(110);
        $pdf->setFont('Arial', '', 12);
        $pdf->MultiCell(45, 4, 'The amount of credit provided to you or on your behalf.', 0, 'C');
        $pdf->SetY($y_temp);
        $pdf->SetX(160);
        $pdf->MultiCell(45, 4, 'The amount you will have paid after making all payments as scheduled.', 0, 'C');

        $pdf->Ln(2);
        $y_temp = $pdf->GetY();
        $pdf->setFont('Arial', 'B', 12);
        $pdf->MultiCell(45, 4, "$tila_apr", 0, 'C');

        $pdf->SetXY(60, $y_temp);
        $pdf->MultiCell(45, 4, "$total_finance_charge", 0, 'C');

        $pdf->SetXY(110, $y_temp);
        $pdf->setFont('Arial', '', 12);
        $pdf->MultiCell(45, 4, "$amount_financed", 0, 'C');
        $pdf->SetXY(160, $y_temp);
        $pdf->MultiCell(45, 4, "$total_of_all_payments", 0, 'C');
        $pdf->Ln(10);

        $tila_box_y2 = $pdf->getY();
        $pdf->SetY($tila_box_y);
        $pdf->SetLineWidth(.2);
        $pdf->SetFillColor(0, 0, 0);
        $pdf->Rect(109, $tila_box_y, 48, $tila_box_y2 - $tila_box_y - 4, 'D');
        $pdf->Rect(157, $tila_box_y, 55, $tila_box_y2 - $tila_box_y - 4, 'D');

        //End Tila Box Creation
        $pdf->SetY($tila_box_y2 - 2);
        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(40, 4, 'Payment Schedule:', 0, 0, 'L');
        $pdf->setFont('Arial', '', 12);
        $pdf->Cell(50, 4, 'Your Payment Schedule will be:', 0, 0, 'L');
        $pdf->Ln(6);

        $pdf->SetX(6);
        // print table headers
        $pdf->setFont('Arial', 'B', 10);
        $pdf->Cell(40, 4, 'Number of Payments', 1, 0, 'C');
        $pdf->Cell(50, 4, 'Amount of Payments', 1, 0, 'C');
        $pdf->Cell(114, 4, 'When Payments are Due', 1, 0, 'C');
        $pdf->Ln();

        $grouped_by_payment_amt = array_group_by($payment_schedule, "payment_amt");
        $num_payments_arr = array();
        $payment_amounts = array();
        $dates_arr = array();
        $dates_arr_string = array();
        $dates_string = '';

        foreach ($grouped_by_payment_amt as $key => $item) {
            array_push($num_payments_arr, $key);
            if (is_array($item)) {
                $tmp_arr = array();
                foreach ($item as $key1 => $value1) {
                    $tmp1 = $value1['payment_dt'];
                    $tmp2 = $value1['payment_amt'];
                    array_push($tmp_arr, $tmp1);
                    $dates_string .= $tmp1 . ', ';
                }
                $dates_string = rtrim(trim($dates_string), ',');
                array_push($dates_arr_string, $dates_string);
                $dates_string = '';
                array_push($dates_arr, $tmp_arr);
                array_push($payment_amounts, $tmp2);
            }
        }

        $left_margin_NumPayments = 6;
        $pdf->SetX($left_margin_NumPayments);

        // first line
        $arr = $dates_arr[0];
        $pdf->Cell(40, 30, count($arr), 0, 0, 'C');
        $pdf->Cell(50, 30, "$" . $payment_amounts[0], 0, 0, 'C');

        switch ($app_data['pay_frequency_1']) {
            case 'Paid Weekly':
            case 'Bi-Weekly':
                $tmp_dt = date('m/d/Y', strtotime($payment_schedule[0]['payment_dt']));
                $str = 'Every 14 days beginning ' . $tmp_dt;
                break;
            case 'Paid Monthly':
                $day_pd = $app_data['day_paid_month_1'];
                $now = strtotime('now');
                $now_month = date('m', $now);
                $now_year = date('Y', $now);
                $day_w_suffix = date('jS', strtotime("$now_month/$day_pd/$now_year"));
                $str = 'On the ' . $day_w_suffix . ' of each month beginning ' . date('m/d/Y', strtotime($payment_schedule[0]['payment_dt']));
                break;
            case 'Paid Twice Monthly':
                $days_pd = $app_data['day_paid_twice_monthly_1'];
                if ($days_pd == '1st and 15th') {
                    $str = 'On 1st and 15th of each month beginning on ' . date('m/d/Y', strtotime($payment_schedule[0]['payment_dt']));
                } else if ($days_pd == '15th and 30th') {
                    $str = '15th and last day of each month beginning on ' . date('m/d/Y', strtotime($payment_schedule[0]['payment_dt']));
                }
                break;
        }
        $pdf->Cell(114, 30, $str, 0, 0, 'C');

        $pdf->rect(6, 181, 40, 30, "D");
        $pdf->rect(46, 181, 50, 30, "D");
        $pdf->rect(96, 181, 114, 30, "D");

        if (count($grouped_by_payment_amt) > 1) {
            $pdf->setXY(6, 222);
            $arr = $dates_arr[1];
            $pdf->Cell(40, 5, count($arr), 0, 0, 'C');
            $pdf->Cell(50, 5, "$" . $payment_amounts[1], 0, 0, 'C');
            $pdf->Cell(114, 5, date('m/d/Y', strtotime($dates_arr_string[1])), 0, 0, 'C');
        }

        // rectangles for the bottom row
        $pdf->rect(6, 211, 40, 29, "D");
        $pdf->rect(46, 211, 50, 29, "D");
        $pdf->rect(96, 211, 114, 29, "D");

        $pdf->SetAutoPageBreak(false);
        $pdf->SetXY(4, 241);
        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(20, 4, 'Security:', 0, 0, 'L');
        $pdf->setFont('Arial', '', 10);
        $pdf->MultiCell(0, 4, 'If selected as your repayment method, then you are giving a security interest in the Payment Authorization.', 0, 'L');

        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(26, 4, 'Prepayment:', 0, 0, 'L');
        $pdf->setFont('Arial', '', 11);
        $pdf->MultiCell(0, 4, 'If you prepay in full or in part, you will not have to pay a penalty.', 0, 'L');

        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(26, 4, 'Late Charge:', 0, 0, 'L');
        $pdf->setFont('Arial', '', 11);

        $text_late_charge = $this->Generate_Late_Charge_Block($customer_state);
        $pdf->MultiCell(0, 4, $text_late_charge, 0, 'L');
        $pdf->MultiCell(0, 4, 'See the terms of this Agreement for any additional information about nonpayment, default, any required repayment in full before the scheduled date, and prepayment refunds and penalties.', 0, 'L');


        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true);

        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(0, 10, 'Itemization of Amount Financed', 0, 0, 'L');
        $pdf->Ln(6);
        $pdf->setFont('Arial', '', 12);
        $pdf->Cell(120, 10, 'Amount given to you directly:', 0, 0, 'L');
        $pdf->Cell(0, 10, "$amount_financed", 0, 0, 'L');
        $pdf->Ln(6);
        $pdf->Cell(120, 10, '(Plus) Amount paid on your Account:', 0, 0, 'L');
        $pdf->Cell(0, 10, "\$0.00 (Plus)", 0, 0, 'L');
        $pdf->Ln(6);
        $pdf->Cell(120, 10, 'Amount paid to other on your behalf to ______________:', 0, 0, 'L');
        $pdf->Cell(0, 10, "\$0.00", 0, 0, 'L');
        $pdf->Ln(6);
        $pdf->Cell(120, 10, '(Equals) Principal amount of your loan:', 0, 0, 'L');
        $pdf->Cell(0, 10, "$amount_financed", 0, 0, 'L');
        $pdf->Ln(6);
        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(120, 10, '(Equals) Amount Financed:', 0, 0, 'L');
        $pdf->Cell(0, 10, "$amount_financed", 0, 0, 'L');
        $pdf->Ln(6);
        $pdf->Cell(0, 10, '_______________________________________________');
        $pdf->Ln(10);
        $pdf->MultiCell(0, 4, 'HIGH COST CREDIT DISCLOSURE: AN ' . strtoupper($lender_dba_name) . ' LOAN IS AN EXPENSIVE FORM OF CREDIT. IT IS DESIGNED TO HELP CUSTOMERS MEET THEIR SHORT-TERM BORROWING NEEDS. THIS SERVICE IS NOT INTENDED TO PROVIDE A SOLUTION FOR LONGER-TERM CREDIT OR OTHER FINANCIAL NEEDS. ALTERNATIVE FORMS OF CREDIT MAY BE LESS EXPENSIVE AND MORE SUITABLE FOR YOUR FINANCIAL NEEDS.', 0, 'L');
        $pdf->Ln(10);

        $pdf->write_bold(4, 'PROMISE TO PAY:  ');
        if ($customer_state == 'MS') {
            $pdf->Write(4, "You promise to pay to the order of Lender the principal sum of $amount_financed plus a handling fee calculated at a rate of $contract_apr% per year (\"Contract Rate\"). You agree to make payments on the dates and in the amounts shown in the Payment Schedule above, or as may be later modified (\"Payment Due Dates\"). You also promise to pay the Lender all other charges provided for under this Agreement.");
        } else {
            $pdf->Write(4, "You promise to pay to the order of Lender the principal sum of $amount_financed plus interest at a rate of $contract_apr% per year (\"Contract Rate\"). You agree to make payments on the dates and in the amounts shown in the Payment Schedule above, or as may be later modified (\"Payment Due Dates\"). You also promise to pay the Lender all other charges provided for under this Agreement.");
        }
        $pdf->Ln(8);

        if ($customer_state == 'MS') {
            $pdf->write_bold(4, 'HANDLING FEE:  ');
            $pdf->Write(4, 'The handling fee will accrue daily on the unpaid principal balance of this Loan, beginning on the Effective Date. The handling fee will accrue until you pay the unpaid principal balance in full. However, after maturity or acceleration, any unpaid principal shall continue to accrue handling fees until repaid in full at the Contract Rate OR at the maximum rate allowed by law, whichever is less. The handling fee is not interest for the purposes of Mississippi law. If a Loan is refinanced with a new loan from us, you agree that we may continue to charge the handling fee on any refinanced amount of a Loan (shown as the "Amount paid on your Account" in the Itemization of Amount Financed above) until the effective date of the new loan. We calculate the handling fee based on a 365-day year (unless otherwise required by state law).  In calculating your payments, we have assumed you will make each payment on the day and in the amount due set forth in the Payment Schedule. If any payment is received after the Payment Due Date, you must pay any additional handling fee that accrues after the Payment Due Date.  If any payment is made before a Payment Due Date, the handling fee due on the scheduled payment will be reduced and you will owe a lesser handling fee. If any Payment Due Date falls on a non-banking business day, then you agree to pay on the next banking business day, and we will credit such payment as if it was received on the Payment Due Date.  The amount of any decrease or increase in the handling fee due will affect the amount of your final payment.  If the amount of any payment is not enough to pay the handling fee due, the unpaid handling fee will be paid from your next payment(s), if any, and will not be added to the principal balance. Time is of the essence. The handling fee will continue to accrue on past due amounts in accordance with this Agreement and as permitted by applicable state law. The Contract Rate and other charges under this Agreement will never exceed the highest rate or charge allowed by applicable state law for this Loan. If the amount collected is found to have exceeded the highest rate or charge allowed, Lender will refund an amount necessary to comply with the law.');
        } else {
            $pdf->write_bold(4, 'INTEREST:  ');
            $pdf->Write(4, 'Interest will accrue daily on the unpaid principal balance of this Loan, beginning on the Effective Date. Interest will accrue until you pay the unpaid principal balance in full. However, after maturity or acceleration, any unpaid principal shall continue to accrue interest until repaid in full at the Contract Rate OR at the maximum rate allowed by law, whichever is less.  If a Loan is refinanced with a new loan from us, you agree that we may continue to charge interest on any refinanced amount of a Loan (shown as the "Amount paid on your Account" in the Itemization of Amount Financed above) until the effective date of the new loan.  We calculate interest based on a 365-day year (unless otherwise required by state law).  In calculating your payments, we have assumed you will make each payment on the day and in the amount due set forth in the Payment Schedule. If any payment is received after the Payment Due Date, you must pay any additional interest that accrues after the Payment Due Date.  If any payment is made before a Payment Due Date, the interest due on the scheduled payment will be reduced and you will owe less interest. If any Payment Due Date falls on a non-banking business day, then you agree to pay on the next banking business day, and we will credit such payment as if it was received on the Payment Due Date.  The amount of any decrease or increase in interest due will affect the amount of your final payment.  If the amount of any payment is not enough to pay the interest due, the unpaid interest will be paid from your next payment(s), if any, and will not be added to the principal balance. Time is of the essence. Interest will continue to accrue on past due amounts in accordance with this Agreement and as permitted by applicable state law. The interest rate and other charges under this Agreement will never exceed the highest rate or charge allowed by applicable state law for this Loan. If the amount collected is found to have exceeded the highest rate or charge allowed, Lender will refund an amount necessary to comply with law.');
        }
        $pdf->Ln(8);

        if ($customer_state == 'NM') {
            $pdf->write_bold(4, 'COMPUTATION OF INTEREST CHARGES UNDER THE SMALL LOAN ACT:  ');
            $pdf->Write(4, 'The simple interest method shall be used for loans made under the New Mexico Small Loan Act of 1955.  Interest charges shall not be paid, deducted, or received in advance.  Interest charges shall not be compounded.  However, if part or all of the consideration for a loan contract is the unpaid principal balance of a prior loan, then the principal amount payable under the loan contract may include any unpaid charges that have accrued within sixty days on the prior loan.  Such charges shall be computed on the basis of the number of days actually elapsed.');
            $pdf->Ln(8);
        }

        $pdf->write_bold(4, 'PAYMENTS:  ');
        if ($customer_state == 'MS') {
            $pdf->Write(4, "Lender will apply your payments in the following order: (1) any accrued but unpaid handling fee, (2) principal amounts outstanding, then (3) unpaid fees. If you have chosen the Payment Authorization option, each payment (including any accrued but unpaid handling fee and any fees, if applicable) will be debited from your Bank Account on each Payment Due Date and in accordance with the Payment Authorization. Any late payments may extend the term of your loan. See the Payment Authorization below for further information. If you choose to receive your Loan proceeds via check and to repay all amounts due pursuant to this Agreement via check, please mail each payment payable to $lender_dba_name ATTN: Payment Processing $lender_mailing_street_address, $lender_mailing_city, $lender_mailing_state $lender_mailing_zip_code, in time for Lender to receive the payment by 4:00PM Central Time on the applicable Payment Due Date.");
        } else if ($customer_state == 'WI') {
            $pdf->Write(4, "Lender will apply your payments in the following order: (1) accrued but unpaid interest, (2) principal amounts outstanding, then (3) unpaid fees. For purposes of assessing late charges set forth below, payments are applied first to current installments and then to delinquent installments. If you have chosen the Payment Authorization option, each payment (including any accrued but unpaid interest and any fees, if applicable) will be debited from your Bank Account on each Payment Due Date and in accordance with the Payment Authorization. Any late payments may extend the term of your loan. See the Payment Authorization below for further information. If you choose to receive your Loan proceeds via check and to repay all amounts due pursuant to this Agreement via check, please mail each payment payable to $lender_dba_name ATTN: Payment Processing $lender_mailing_street_address, $lender_mailing_city, $lender_mailing_state $lender_mailing_zip_code, in time for Lender to receive the payment by 4:00PM Central Time on the applicable Payment Due Date.");
        } else {
            $pdf->Write(4, "Lender will apply your payments in the following order: (1) accrued but unpaid interest, (2) principal amounts outstanding, then (3) unpaid fees. If you have chosen the Payment Authorization option, each payment (including any accrued but unpaid interest and any fees, if applicable) will be debited from your Bank Account on each Payment Due Date and in accordance with the Payment Authorization. Any late payments may extend the term of your loan. See the Payment Authorization below for further information. If you choose to receive your Loan proceeds via check and to repay all amounts due pursuant to this Agreement via check, please mail each payment payable to $lender_dba_name ATTN: Payment Processing $lender_mailing_street_address, $lender_mailing_city, $lender_mailing_state $lender_mailing_zip_code, in time for Lender to receive the payment by 4:00PM Central Time on the applicable Payment Due Date.");
        }
        $pdf->Ln(8);

        $pdf->write_bold(4, 'PREPAYMENT:  ');
        if ($customer_state == 'MS') {
            $pdf->Write(4, 'You may prepay in whole or in part at any time without penalty. If you prepay in part, you must still make each later payment according to the Payment Schedule until this Loan is paid in full.  Any principal amounts you prepay will not continue to accrue the handling fee.');
        } else {
            $pdf->Write(4, 'You may prepay in whole or in part at any time without penalty. If you prepay in part, you must still make each later payment according to the Payment Schedule until this Loan is paid in full.  Any principal amounts you prepay will not continue to accrue interest.');
        }
        $pdf->Ln(8);

        $pdf->write_bold(4, 'RIGHT OF RESCISSION:  ');
        $pdf->Write(4, "You may rescind or cancel this Loan if you do so on or before 5:00 p.m., Central Time on the third (3rd) business day after the Contract Date (the \"Rescission Deadline\"). To cancel, call Lender at $lender_phone_nbr to tell us you want to rescind or cancel this Loan and provide us with written notice of rescission as directed by our customer service representative.  The Jury Trial Waiver and Arbitration Clause set forth below survives any rescission.");
        $pdf->Ln(8);

        $pdf->SetLeftMargin(10);
        $pdf->Write(4, '(a)	 ');
        $pdf->write_bold(4, 'If you have provided a Payment Authorization:  ');
        if ($customer_state == 'MS') {
            $pdf->Write(4, 'If we timely receive your written notice of rescission on or before the Rescission Deadline but before the Loan proceeds have been credited to your Bank Account, we will not deposit your Loan proceeds to your Bank Account and both ours and your obligations under this agreement will be rescinded. If we timely receive your written notice of rescission on or before the Rescission Deadline but after the Loan proceeds have been credited to your Bank Account, we will debit your Bank Account for the principal amount owed under this Agreement, in accordance with the Payment Authorization. If we receive payment of the principal amount via the debit, ours and your obligations under this Agreement will be rescinded. If we do not receive payment of the principal amount via the debit, then the Agreement will remain in full force and effect until all amounts under this Agreement are repaid in full, including any handling fee.');
        } else {
            $pdf->Write(4, 'If we timely receive your written notice of rescission on or before the Rescission Deadline but before the Loan proceeds have been credited to your Bank Account, we will not deposit your Loan proceeds to your Bank Account and both ours and your obligations under this agreement will be rescinded. If we timely receive your written notice of rescission on or before the Rescission Deadline but after the Loan proceeds have been credited to your Bank Account, we will debit your Bank Account for the principal amount owed under this Agreement, in accordance with the Payment Authorization. If we receive payment of the principal amount via the debit, ours and your obligations under this Agreement will be rescinded. If we do not receive payment of the principal amount via the debit, then the Agreement will remain in full force and effect until all amounts under this Agreement are repaid in full, including any interest.');
        }
        $pdf->Ln(8);
        $pdf->Write(4, '(b)	 ');
        $pdf->write_bold(4, 'If you have elected to receive your Loan proceeds via check delivered by mail:  ');
        if ($customer_state == 'MS') {
            $pdf->Write(4, 'If we timely receive your written notice of rescission on or before the Rescission Deadline, and (a) if we have not mailed the check representing the Loan proceeds to you or (b) if you have not cashed the check representing the Loan proceeds, then we will cancel the check and both ours and your obligations under this Agreement will be rescinded. If you have cashed the check representing the Loan proceeds, you must return the full amount of cash you received to us by the Rescission Deadline. If we do not receive the full amount by the Rescission Deadline, then the Agreement will remain in full force and effect until all amounts owed under this Agreement are repaid in full, including any handling fee.');
        } else {
            $pdf->Write(4, 'If we timely receive your written notice of rescission on or before the Rescission Deadline, and (a) if we have not mailed the check representing the Loan proceeds to you or (b) if you have not cashed the check representing the Loan proceeds, then we will cancel the check and both ours and your obligations under this Agreement will be rescinded. If you have cashed the check representing the Loan proceeds, you must return the full amount of cash you received to us by the Rescission Deadline. If we do not receive the full amount by the Rescission Deadline, then the Agreement will remain in full force and effect until all amounts owed under this Agreement are repaid in full, including any interest.');
        }
        $pdf->Ln(8);

        $pdf->SetX(0);
        $pdf->SetLeftMargin(4);
        $pdf->write_bold(4, 'LATE CHARGE:  ');
        $pdf->Write(4, $text_late_charge);
        $pdf->Ln(8);

        $pdf->write_bold(4, 'ELECTRONIC CHECK RE-PRESENTMENT POLICY:  ');
        $pdf->Write(4, 'In the event a check is returned unpaid for insufficient or uncollected funds, we may re-present the check electronically.  In the ordinary course of business, the check will not be provided to you with your bank statement, but a copy can be retrieved by contacting your financial institution.');
        $pdf->Ln(8);

        $pdf->write_bold(4, 'CHECK CONVERSION NOTIFICATION:  ');
        $pdf->Write(4, "When you provide a check as payment, you agree we can either use the information from your check to make a one-time electronic withdrawal from your Bank Account or to process the payment as a check transaction. When we use information from your check to make a withdrawal from your Bank Account, funds may be withdrawn from your Bank Account as soon as the same day we receive your payment and you will not receive your check back from your financial institution.  For questions please call our customer service number, $lender_phone_nbr.");
        $pdf->Ln(8);

        $pdf->write_bold(4, 'BORROWER BANK CHARGES:  ');
        $pdf->Write(4, 'Your financial institution may charge a fee if your Bank Account becomes overdrawn or if a payment is attempted from your Bank Account that would cause it to become overdrawn. You will not hold us or our agents, representatives, successors or assigns responsible for any such fee you must pay.');
        $pdf->Ln(8);

        $pdf->write_bold(4, 'SECURITY:  ');
        if ($customer_state == 'WI' || $customer_state == 'IL') {
            $pdf->Write(4, 'You grant a security interest in the Payment Authorization, if you provided one.');
        } else {
            $pdf->Write(4, 'We have disclosed to you that our interest in the payment method indicated in the Federal Truth-in-Lending Disclosures above is a security interest for Truth-in-Lending purposes only because federal and applicable state law do not clearly address the issue. However, the federal Truth-in-Lending disclosures are not intended to create a security interest under applicable state law.');
        }
        $pdf->Ln(8);

        $pdf->write_bold(4, 'REFINANCE POLICY:  ');
        $pdf->Write(4, 'Subject to applicable state law and our credit policies, we will determine, in our sole discretion, whether your Loan may be refinanced.');
        $pdf->Ln(8);

        $pdf->write_bold(4, 'CREDIT REPORTING:  ');
        if ($customer_state == 'UT') {
            $pdf->Write(4, 'You agree that Lender may make inquiries concerning your credit history and standing. As required by Utah law, you are hereby notified that a negative credit report reflecting on your credit record may be submitted to a credit reporting agency if you fail to fulfill the terms of your credit obligations.');
        } else {
            $pdf->Write(4, 'You agree that Lender may make inquiries concerning your credit history and standing, and may report information concerning your performance under this Agreement to credit reporting agencies. Late payments, missed payments or other defaults on your Loan may be reflected in your credit report.');
        }
        $pdf->Ln(8);

        $pdf->write_bold(4, 'CHANGE OF PRIMARY RESIDENCE:  ');
        $pdf->Write(4, 'You agree to notify Lender of any change in your primary residence as soon as possible, but no later than five (5) days after any change. You agree that the address provided on this Agreement will govern this Agreement until you have met all obligation under this Agreement and that any subsequent change in your address will not affect the terms or enforceability of this Agreement.');
        $pdf->Ln(8);

        $pdf->write_bold(4, 'CORRESPONDENCE WITH LENDER:  ');
        $pdf->Write(4, "General correspondence with Lender concerning this Loan, this Agreement or your relationship with Lender must be sent to Lender at the following address: $lender_name, $lender_mailing_street_address, $lender_mailing_city, $lender_mailing_state $lender_mailing_zip_code. Communications related to the bankruptcy of the Borrower must be sent to Lender at the following address: $lender_name ATTN: Bankruptcy Handling, $lender_mailing_street_address, $lender_mailing_city, $lender_mailing_state $lender_mailing_zip_code.");
        $pdf->Ln(8);

        $pdf->write_bold(4, 'FORCE MAJEURE:  ');
        $pdf->Write(4, 'Unavoidable delays because of inadvertent processing errors and/or "acts of God" may extend the time for the deposit of Loan proceeds and the processing of your payments.');
        $pdf->Ln(8);

        $pdf->write_bold(4, 'TRANSFER OF RIGHTS/HYPOTHECATION:  ');
        if ($customer_state == 'MS') {
            $pdf->Write(4, 'This instrument is non-negotiable in form but may be pledged as collateral security. If so pledged, any payment made to the Lender, either of principal or of any handling fee, upon the debt evidenced by this obligation, will be considered and construed as a payment on this instrument, the same as though it were still in the possession and under the control of the Lender named herein; and the pledgee holding this instrument as collateral security hereby makes said Lender its agent to accept and receive payments hereon, either of principal or of any handling fee.');
        } else {
            $pdf->Write(4, 'This instrument is non-negotiable in form but may be pledged as collateral security. If so pledged, any payment made to the Lender, either of principal or of interest, upon the debt evidenced by this obligation, will be considered and construed as a payment on this instrument, the same as though it were still in the possession and under the control of the Lender named herein; and the pledgee holding this instrument as collateral security hereby makes said Lender its agent to accept and receive payments hereon, either of principal or of interest.');
        }
        $pdf->Ln(8);

        $pdf->write_bold(4, 'SUCCESSORS AND ASSIGNS:  ');
        $pdf->Write(4, 'This Agreement is binding upon your heirs and personal representatives in probate upon anyone to whom you assign your assets or who succeeds you in any other way. You agree that Lender may assign or transfer this Agreement and any of lender\'s rights hereunder at any time without prior notice to you, except as required by applicable law. You may not assign this Agreement without the prior, written consent of Lender.');
        $pdf->Ln(8);

        $pdf->write_bold(4, 'SERVICING COMMUNICATIONS AND COMMUNICATIONS AFTER DEFAULT:  ');
        $pdf->Write(4, 'You authorize Lender and its authorized representatives to contact you according to your consent provided in your application or according to your account preferences, as modified by you after submitting your application. This may include (i) calling you during reasonable hours at any of the phone numbers listed on your most recent application (ii) contacting you by text message or other wireless communication method on the mobile phone number listed on your application, (iii) leaving a message with a person or a voice mail service, and (iv) contacting you using autodialers or pre-recorded message, including calls to your mobile phone.');
        $pdf->Ln(8);

        $pdf->setFont('Arial', 'B', 12);
        $pdf->MultiCell(0, 4, 'AUTOMATIC PAYMENT AUTHORIZATION:', 0, 'L');
        $pdf->Ln(4);
        $pdf->MultiCell(0, 4, '(Applies only if you select the electronic funding option or authorize recurring debit card payments)', 0, 'L');
        $pdf->Ln(4);
        $pdf->setFont('Arial', '', 12);

        // paragraph 1
        $pdf->MultiCell(0, 4, 'This Automatic Payment Authorization ("Payment Authorization") is a part of and related to this Agreement. You voluntarily authorize us, and our successors, affiliates, agents, representatives, employees, and assigns, to initiate automatic credit and debit entries to your Bank Account in accordance with this Agreement. For purposes of this Payment Authorization, "Bank Account" also includes any other bank account that you provide to Lender, from which you intend to permit recurring payments.  You agree that we will initiate a credit entry to your Bank Account for the amount that we are to give to you directly, as indicated in the Itemization of Amount Financed, on or about the Disbursement Date. You also authorize us to credit or debit your Bank Account to correct any errors related to this Loan.', 0, 'L');
        $pdf->Ln(4);

        // paragraph 2
        if ($customer_state == 'MS') {
            $pdf->MultiCell(0, 4, 'You agree that we will initiate a debit entry to debit your Bank Account on or after each Payment Due Date in the payment amount described in the Payment Schedule (including as modified) plus any late fees and/or accrued but unpaid handling fees, up to and including the full amount you owe. You acknowledge that you will receive a notice at least 10 days before a payment is debited from your Bank Account if the payment we are going to debit from your Bank Account falls outside this range. You have the right to receive notice of all varying amounts but have agreed to only receive notice if a debit will fall outside this range.', 0, 'L');
        } else if ($customer_state == 'WI') {
            $pdf->MultiCell(0, 4, 'You agree that we will initiate a debit entry to debit your Bank Account on or after each Payment Due Date in the payment amount described in the Payment Schedule (including as modified), plus any late fees and/or accrued but unpaid interest up to a maximum of $25 more than the payment amount described in the Payment Schedule. You acknowledge that you will receive a notice at least 10 days before a payment is debited from your Bank Account if the payment we are going to debit from your Bank Account falls outside this range. You have the right to receive notice of all varying amounts but have agreed to only receive notice if a debit will fall outside this range.', 0, 'L');
        } else {
            $pdf->MultiCell(0, 4, 'You agree that we will initiate a debit entry to debit your Bank Account on or after each Payment Due Date in the payment amount described in the Payment Schedule (including as modified) plus any late fees and/or accrued but unpaid interest, up to and including the full amount you owe. You acknowledge that you will receive a notice at least 10 days before a payment is debited from your Bank Account if the payment we are going to debit from your Bank Account falls outside this range. You have the right to receive notice of all varying amounts but have agreed to only receive notice if a debit will fall outside this range.', 0, 'L');
        }
        $pdf->Ln(4);

        // paragraph 3
        $pdf->MultiCell(0, 4, 'For each scheduled payment, whenever a debit entry to your Bank Account is returned to us for any reason, we may initiate a debit entry to your Bank Account up to two additional times after our first attempt for each scheduled payment amount, to the extent permitted by law.  If your payment is due on a non-business day, it will be processed on the next business day and credited as if made on the Payment Date.  If you still have an outstanding balance after the final Payment Date, then you authorize us to issue debits in the same (or lesser) amount as the final Payment Date debit on the same recurring intervals as set forth in the Payment Schedule (including as modified) until the balance is paid in full.', 0, 'L');
        $pdf->Ln(4);

        // paragraph 4
        if ($customer_state == 'MS') {
            $pdf->MultiCell(0, 4, "You agree that this Payment Authorization is for repayment of a Credit Availability Act loan and that payments shall recur at substantially regular intervals as set forth in this Agreement. This Payment Authorization is to remain in full force and effect for the transaction until you pay your Loan, including any handling fee, in full. You may only revoke this Payment Authorization by contacting us directly at $lender_phone_nbr or via mail at $lender_mailing_street_address $lender_mailing_city, $lender_mailing_state $lender_mailing_zip_code.  If you revoke your Payment Authorization, you agree to make payments to us as set forth in the \"Payments\" section above by another acceptable payment method. In no event will any revocation of this Payment Authorization be effective with respect to entries processed by us prior to us receiving such revocation.", 0, 'L');
        } else {
            $pdf->MultiCell(0, 4, "You agree that this Payment Authorization is for repayment of a consumer installment loan and that payments shall recur at substantially regular intervals as set forth in this Agreement. This Payment Authorization is to remain in full force and effect for the transaction until you pay your Loan, including any interest, in full. You may only revoke this Payment Authorization by contacting us directly at $lender_phone_nbr or via mail at $lender_mailing_street_address $lender_mailing_city, $lender_mailing_state $lender_mailing_zip_code.  If you revoke your Payment Authorization, you agree to make payments to us as set forth in the \"Payments\" section above by another acceptable payment method. In no event will any revocation of this Payment Authorization be effective with respect to entries processed by us prior to us receiving such revocation.", 0, 'L');
        }
        $pdf->Ln(4);

        // paragraph 5
        $pdf->MultiCell(0, 4, 'Your bank may charge you a fee in connection with our credit and/or debit entries. Contact your financial institution for more information specific to your Bank Account.', 0, 'L');
        $pdf->Ln(4);

        // paragraph 6
        $pdf->MultiCell(0, 4, 'Missing or Incorrect Information. If there is any missing or incorrect information in or with your loan application regarding your bank, bank routing number, or account number, then you authorize us to verify and correct such information.', 0, 'L');
        $pdf->Ln(4);

        // paragraph 7
        $pdf->MultiCell(0, 4, 'This Payment Authorization is a payment mechanism only and does not give us collection rights greater than those otherwise contained in this Agreement.', 0, 'L');
        $pdf->Ln(4);

        // Events of Default
        $pdf->write_bold(4, 'EVENTS OF DEFAULT:  ');
        if ($customer_state == 'AL' || $customer_state == 'DE' || $customer_state == 'MS' ||
            $customer_state == 'NM' || $customer_state == 'UT' || $customer_state == 'CA' ||
            $customer_state == 'ID' || $customer_state == 'IL') {
            $pdf->Write(4, 'The following constitute events of default under this Agreement: (a) failure to pay as scheduled; and (b) our prospect of payment or performance is significantly impaired.');
        } else if ($customer_state == 'MO') {
            $pdf->Write(4, 'The following constitute events of default under this Agreement: (a) failure to pay as scheduled; and (b) our prospect of payment or performance is significantly impaired.  If you default, then you have a right to cure such default in accordance with Missouri law.');
        } else if ($customer_state == 'SC') {
            $pdf->Write(4, 'The following constitute events of default under this Agreement: (a) failure to pay as scheduled; and (b) our prospect of payment or performance is significantly impaired.  If you default, then you have a right to cure such default in accordance with South Carolina law.');
        } else if ($customer_state == 'WI') {
            $pdf->Write(4, 'The following constitute events of default under this Agreement: to have outstanding an amount of one full payment or more which has remained unpaid for more than 10 days after the scheduled or deferred due date. If you default, then you have a right to cure such default in accordance with Wisconsin law.');
        }
        $pdf->Ln(8);


        $pdf->write_bold(4, 'LENDER\'S RIGHTS IN THE EVENT OF DEFAULT:  ');
        if ($customer_state == 'AL') {
            $pdf->Write(4, 'Upon the occurrence of any event of default, Lender may at its option, do any one or more of the following: (a) exercise all rights, powers and remedies given by law; (b) subject to any limitations imposed by applicable state law, declare the entire unpaid outstanding balance due, but any failure of Lender to exercise such option will not constitute a waiver of Lender\'s option in the event of any subsequent default; (c) recover from you reasonable attorney fees incurred by Lender as a result of the suit or activity and to which the Lender becomes entitled by law, not exceeding 15% of the unpaid debt after default; and/or (d) assign any and all of Lender\'s interest in and to this Agreement to a third party, thereby vesting in such a third party all rights, powers and privileges of Lender hereunder. Any delay by Lender in exercising any or all of these rights shall not constitute a waiver of such rights.');
        } else if ($customer_state == 'CA') {
            $pdf->Write(4, 'Upon the occurrence of any event of default, Lender may at its option, do any one or more of the following: (a) exercise all rights, powers and remedies given by law; (b) subject to any limitations imposed by applicable state law, declare the entire unpaid outstanding balance due, but any failure of Lender to exercise such option will not constitute a waiver of Lender\'s option in the event of any subsequent default; (c) recover from you all costs and disbursements in connection with any suit to collect a loan to which the Lender becomes entitled by law; and/or (d) assign any and all of Lender\'s interest in and to this Agreement to a third party, thereby vesting in such a third party all rights, powers and privileges of Lender hereunder. Any delay by Lender in exercising any or all of these rights shall not constitute a waiver of such rights.');
        } else if ($customer_state == 'DE') {
            $pdf->Write(4, 'Upon the occurrence of any event of default, Lender may at its option, do any one or more of the following: (a) exercise all rights, powers and remedies given by law; (b) subject to any limitations imposed by applicable state law, declare the entire unpaid outstanding balance due, but any failure of Lender to exercise such option will not constitute a waiver of Lender\'s option in the event of any subsequent default; (c) recover from you all costs and disbursements in connection with any suit to collect a loan, including reasonable attorney fees from an attorney not a regularly salaried employee of the licensee incurred by Lender as a result of the suit or activity and to which the Lender becomes entitled by law, not to exceed 20% of the unpaid principal and interest; and/or (d) assign any and all of Lender\'s interest in and to this Agreement to a third party, thereby vesting in such a third party all rights, powers and privileges of Lender hereunder. Any delay by Lender in exercising any or all of these rights shall not constitute a waiver of such rights.');
        } else if ($customer_state == 'MS') {
            $pdf->Write(4, 'Upon the occurrence of any event of default, Lender may at its option, do any one or more of the following: (a) exercise all rights, powers and remedies given by law; (b) subject to any limitations imposed by applicable state law, declare the entire unpaid outstanding balance due, but any failure of Lender to exercise such option will not constitute a waiver of Lender\'s option in the event of any subsequent default; (c) recover from you all costs and disbursements in connection with any suit to collect a loan, including reasonable attorney fees incurred by Lender as a result of the suit or activity and to which the Lender becomes entitled by law; and/or (d) assign any and all of Lender\'s interest in and to this Agreement to a third party, thereby vesting in such a third party all rights, powers and privileges of Lender hereunder. Any delay by Lender in exercising any or all of these rights shall not constitute a waiver of such rights.');
        } else if ($customer_state == 'NM') {
            $pdf->Write(4, 'Upon the occurrence of any event of default, Lender may at its option, do any one or more of the following: (a) exercise all rights, powers and remedies given by law; (b) subject to any limitations imposed by applicable state law, declare the entire unpaid outstanding balance due, but any failure of Lender to exercise such option will not constitute a waiver of Lender\'s option in the event of any subsequent default; (c) recover from you all costs and disbursements in connection with any suit to collect a loan, including reasonable attorney fees incurred by Lender as a result of the suit or activity and to which the Lender becomes entitled by law; and/or (d) assign any and all of Lender\'s interest in and to this Agreement to a third party, thereby vesting in such a third party all rights, powers and privileges of Lender hereunder. Any delay by Lender in exercising any or all of these rights shall not constitute a waiver of such rights.');
        } else if ($customer_state == 'UT') {
            $pdf->Write(4, 'Upon the occurrence of any event of default, Lender may at its option, do any one or more of the following: (a) exercise all rights, powers and remedies given by law; (b) subject to any limitations imposed by applicable state law, declare the entire unpaid outstanding balance due, but any failure of Lender to exercise such option will not constitute a waiver of Lender\'s option in the event of any subsequent default; (c) recover from you all costs and disbursements in connection with any suit to collect a loan, including reasonable attorney fees incurred by Lender as a result of the suit or activity and to which the Lender becomes entitled by law; and/or (d) assign any and all of Lender\'s interest in and to this Agreement to a third party, thereby vesting in such a third party all rights, powers and privileges of Lender hereunder. Any delay by Lender in exercising any or all of these rights shall not constitute a waiver of such rights. If we hire an attorney or a third party collection agency to collect what you owe, you will also pay the lesser of (a) the actual amount we are required to pay the third party collection agency or attorney, regardless of whether that amount is a specific dollar amount or a percentage of the amount owed to us; or (b) 40% of the principal amount owed to us.');
        } else if ($customer_state == 'SC') {
            $pdf->Write(4, 'Upon the occurrence of any event of default, Lender may at its option, do any one or more of the following: (a) exercise all rights, powers and remedies given by law; (b) subject to any limitations imposed by applicable state law, declare the entire unpaid outstanding balance due, but any failure of Lender to exercise such option will not constitute a waiver of Lender\'s option in the event of any subsequent default; (c) recover from reasonable attorney fees, limited to 15% of the unpaid debt after default and referral to outside counsel, incurred by Lender as a result of the suit or activity and to which the Lender becomes entitled by law; and/or (d) assign any and all of Lender\'s interest in and to this Agreement to a third party, thereby vesting in such a third party all rights, powers and privileges of Lender hereunder. Where the principal amount of the loan is $3,700 or less, you will not be responsible for attorney\'s fees. Any delay by Lender in exercising any or all of these rights shall not constitute a waiver of such rights.');
        } else if ($customer_state == 'WI') {
            $pdf->Write(4, 'Upon the occurrence of any event of default, Lender may at its option and subject to your right to cure, do any one or more of the following: (a) exercise all rights, powers and remedies given by law; (b) subject to any limitations imposed by applicable state law, declare the entire unpaid outstanding balance due, but any failure of Lender to exercise such option will not constitute a waiver of Lender\'s option in the event of any subsequent default; and/or (c) assign any and all of Lender\'s interest in and to this Agreement to a third party, thereby vesting in such a third party all rights, powers and privileges of Lender hereunder. Any delay by Lender in exercising any or all of these rights shall not constitute a waiver of such rights.');
        } else if ($customer_state == 'ID') {
            $pdf->Write(4, 'Upon the occurrence of any event of default, Lender may at its option and subject to your right to cure, do any one or more of the following: (a) exercise all rights, powers and remedies given by law; (b) subject to any limitations imposed by applicable state law, declare the entire unpaid outstanding balance due, but any failure of Lender to exercise such option will not constitute a waiver of Lender\'s option in the event of any subsequent default; (c) recover from you all costs and disbursements in connection with any suit to collect a loan, including reasonable attorney fees incurred by Lender as a result of the suit or activity and to which the Lender becomes entitled by law; and/or (d) assign any and all of Lender\'s interest in and to this Agreement to a third party, thereby vesting in such a third party all rights, powers and privileges of Lender hereunder. If the principal balance is $1,000 or less, you will not be responsible for payment of attorney\'s fees. Any delay by Lender in exercising any or all of these rights shall not constitute a waiver of such rights.');
        } else {
            $pdf->Write(4, 'Upon the occurrence of any event of default, Lender may at its option and subject to your right to cure, do any one or more of the following: (a) exercise all rights, powers and remedies given by law; (b) subject to any limitations imposed by applicable state law, declare the entire unpaid outstanding balance due, but any failure of Lender to exercise such option will not constitute a waiver of Lender\'s option in the event of any subsequent default; (c) recover from you all costs and disbursements in connection with any suit to collect a loan, including reasonable attorney fees incurred by Lender as a result of the suit or activity and to which the Lender becomes entitled by law; and/or (d) assign any and all of Lender\'s interest in and to this Agreement to a third party, thereby vesting in such a third party all rights, powers and privileges of Lender hereunder. Any delay by Lender in exercising any or all of these rights shall not constitute a waiver of such rights.');
        }
        $pdf->Ln(8);

        if ($customer_state == 'WI') {
            $pdf->write_bold(4, 'NOTICE TO MARRIED BORROWERS:  ');
            $pdf->Write(4, 'If you are married: (1) You confirm that the Loan is being incurred in the interest of your marriage or family. (2) No provision of any marital property agreement, unilateral agreement, or court decree under Wisconsin\'s Marital Property Act will adversely affect a creditor\'s interest unless prior to the time credit is granted, the creditor is furnished a copy of that agreement or decree or is given complete information about the agreement or decree.   (3) You understand and agree that we are required by state law to notify your spouse regarding this transaction.');
        }
        $pdf->Ln(8);

        $tmp_state = mm_state_cd_to_display($customer_state);
        $pdf->write_bold(4, 'GOVERNING LAW; SEVERABILITY; INTERSTATE COMMERCE:  ');
        if ($customer_state == 'WI') {
            $pdf->Write(4, 'This Agreement is governed by the laws of the State of Wisconsin, except that the Jury Trial Waiver and Arbitration Clause is governed by the Federal Arbitration Act (“FAA”), 9 U.S.C. §§ 1-9.  To the extent not prohibited by the Wisconsin Consumer Act, if any provision of this Agreement is held unenforceable, including any provision of the Jury Trial Waiver and Arbitration Clause, the remainder of this Agreement will remain in full force and effect.  You and we agree that the transaction represented by this Agreement involves interstate commerce for all purposes.');
        } else {
            $pdf->Write(4, "This Agreement is governed by the laws of the State of $tmp_state, except that the Jury Trial Waiver and Arbitration Clause is governed by the Federal Arbitration Act (\"FAA\"), 9 U.S.C. §§ 1-9.  If any provision of this Agreement is held unenforceable, including any provision of the Jury Trial Waiver and Arbitration Clause, the remainder of this Agreement will remain in full force and effect.  You and we agree that the transaction represented by this Agreement involves interstate commerce for all purposes.");
        }
        $pdf->Ln(8);

        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(0, 4, 'JURY TRIAL WAIVER AND ARBITRATION CLAUSE.', 0, 1, 'C');
        $pdf->setFont('Arial', '', 12);
        $pdf->MultiCell(0, 4, 'By signing this agreement, you agree to the Jury Trial Waiver and Arbitration Clause ("Clause"). You also understand that this Clause supersedes the arbitration provision included in your application.  In addition, this Clause applies to any modification of this Agreement.', 0, 'L');
        $pdf->Ln(4);

        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(0, 6, 'Background and Scope.', 0, 1, 'C');
        $pdf->setFont('Arial', '', 12);

        $pdf->SetWidths(array(30, 30, 140));
        $pdf->Row(array('What is arbitration?', 'An alternative to court.', 'In arbitration, a third party ("Arbiter") resolves Disputes in a hearing ("hearing"). You, related third parties, and we, waive the right to go to court. Such "parties" waive jury trials.'));
        $pdf->Row(array('Is it different from court and jury trials?', 'Yes.', 'The hearing is private and less formal than court. Arbiters may limit pre-hearing fact finding, called "discovery." The decision of the Arbiter is final. Courts rarely overturn Arbiters\' decisions.'));
        $pdf->Row(array('Who does the Clause cover?', 'You, Us, and Others.', 'This Clause governs the parties, their heirs, successors, assigns, and third parties related to any Dispute.'));
        $pdf->Row(array('Which Disputes are covered?', 'All Disputes.', 'In this Clause, the word "Disputes" has the broadest possible meaning. This Clause governs all "Disputes" involving the parties to this Agreement and/or our servicers and agents, including but not limited to consultants, banks, payment processors, software providers, data providers and credit bureaus. This includes all claims even indirectly related to your application and agreements with us. This includes claims related to information you previously gave us. It includes all past agreements. It includes extensions, renewals, refinancings, or payment plans. It includes claims related to collections, privacy, and customer information.  It includes claims related to setting aside this Clause. It includes claims about the Clause\'s validity and scope. It includes claims about whether to arbitrate.'));
        $pdf->Row(array('Are you waiving rights?', 'Yes.', 'You waive your rights to:
    1.	Have juries resolve Disputes.
    2.	Have courts, other than small-claims courts, resolve Disputes.
    3.	Serve as a private attorney general or in a representative capacity.
    4.	Be in a class action.
    '));
        $pdf->Row(array('Are you waiving class action rights?', 'Yes.', 'COURTS AND ARBITERS WON\'T ALLOW CLASS ACTIONS. You waive your rights to participate in a class action as a representative and a member. Only individual arbitration or small-claims courts will resolve Disputes. You waive your right to have representative claims. Unless reversed on appeal, if a court invalidates this waiver, the Clause will be void.'));
        $pdf->Row(array('What law applies?', 'The Federal Arbitration Act ("FAA").', 'This transaction involves interstate commerce, so the FAA governs. If a court finds the FAA doesn\'t apply, and the finding can\'t be appealed, then your state\'s law governs. The Arbiter must apply substantive law consistent with the FAA. The Arbiter must follow statutes of limitation and privilege claims.'));
        $pdf->Row(array('Can the parties try to resolve Disputes first?', 'Yes.', "We can try to resolve Disputes if you call us at $lender_phone_nbr. If we are unable to resolve the Dispute by phone, mail us notice within 30 days of the date you first brought the Dispute to our attention.  In your notice, tell us the details and how you want to resolve it. We will try to resolve the Dispute. If we make a written offer (\"Settlement Offer\"), you can reject it and arbitrate. If we don't resolve the Dispute, either party may start arbitration. To start arbitration, contact an Arbiter or arbitration group listed below. No party may disclose settlement proposals to the Arbiter during arbitration."));
        $pdf->Row(array('How should you contact us?', 'By mail.', "Send mail to: $lender_name $lender_mailing_street_address $lender_mailing_city, $lender_mailing_state $lender_mailing_zip_code. You can call us or use certified mail to confirm receipt."));
        $pdf->Row(array('Can small-claims court resolve some Disputes?', 'Yes.', 'Each party has the right to arbitrate, or to go to small-claims court if the small-claims court has the power to hear the Dispute.  Arbitration will resolve all Disputes that the small-claims court does not have the power to hear.  If there is an appeal from small-claims court, or if a Dispute changes so that the small-claims court loses the power to hear it, then the Dispute will be heard only by an Arbiter.'));
        $pdf->Row(array('Do other options exist?', 'Yes.', 'Both parties may seek remedies which don\'t claim money damages. This includes pre-judgment seizure, injunctions, or equitable relief.'));
        $pdf->Row(array('Will this Clause continue to govern?', 'Yes, unless otherwise agreed.', 'The Clause will remain effective unless the parties agree otherwise in writing. The Clause governs if you rescind the transaction. It governs if you default, renew, prepay, or pay. It governs if your contract is discharged through bankruptcy. The Clause will remain effective despite a transaction\'s termination, amendment, expiration, or performance.'));
        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(0, 6, 'Process.', 0, 1, 'C');
        $pdf->setFont('Arial', '', 12);
        $pdf->Row(array('How does arbitration start?', 'Mailing a notice.', 'Either party may mail the other a request to arbitrate, even if a lawsuit has been filed. The notice should describe the Dispute and relief sought. The receiving party must mail a response within 20 days. If you mail the demand, you may choose the arbitration group. Or your demand may state that you want the parties to choose a local Arbiter. If we or related third parties mail the demand, you must respond within 20 days. Your response must choose an arbitration group or propose a local Arbiter. If it doesn\'t, we may choose the group.'));
        $pdf->Row(array('Who arbitrates?', 'AAA, JAMS, or an agreed Arbiter.', 'You may select the American Arbitration Association ("AAA") (1-800-778-7879) http://www.adr.org or JAMS (1-800-352-5267) http://www.jamsadr.com. The parties to a Dispute may also agree in writing to a local attorney, retired judge, or Arbiter in good standing with an arbitration group. The Arbiter must arbitrate under AAA or JAMS consumer rules. You may get a copy of these rules from such group. Any rules that conflict with any of our agreements with you don\'t apply. If these options aren\'t available and the parties can\'t agree on another, a court may choose the Arbiter. Such Arbiter must enforce your agreements with us as they are written.'));
        $pdf->Row(array('Will the hearing be held nearby?', 'Yes.', 'The Arbiter will order the hearing within 30 miles of your home or where the transaction occurred unless otherwise required by law.'));
        $pdf->Row(array('What about appeals?', 'Appeals are limited.', 'The Arbiter\'s decision will be final. A party may file the Arbiter\'s award with the proper court. Arbitration will resolve appeals of a small-claims court judgment. A party may appeal under the FAA. If the amount in controversy exceeds $10,000.00, a party may appeal the Arbiter\'s finding. Such appeal will be to a three-Arbiter panel from the same arbitration group. The appeal will be de novo, and resolved by majority vote. The appealing party bears appeal costs, despite the outcome.'));
        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(0, 6, 'Arbitration Fees and Awards.', 0, 1, 'C');
        $pdf->setFont('Arial', '', 12);
        $pdf->Row(array('Will we advance Arbitration Fees?', 'Yes, but you pay your costs.', 'We will advance your arbitration fees if you ask us to in writing.  This includes filing, administrative, hearing, and Arbiter\'s fees. You are responsible for paying your attorney fees and other expenses.'));
        $pdf->Row(array('Are damages and attorney fees possible?', 'Yes, if allowed by law.', 'The Arbiter may award the same damages as a court.  Arbiters may award reasonable attorney fees and expenses, if allowed by law.'));
        $pdf->Row(array('Will you pay Arbitration Fees if you win?', 'No.', 'If the Arbiter awards you funds you don\'t reimburse us the arbitration fees.'));
        $pdf->Row(array('Will you ever pay Arbitration Fees?', 'Yes.', 'If the Arbiter doesn\'t award you funds, then you must repay the arbitration fees. If you must pay arbitration fees, the amount won\'t exceed state court costs.'));
        $pdf->Row(array('What happens if you win?', 'You could receive an award from Arbiter.', 'If you are successful in arbitration you may receive an Arbiter\'s award. If so ordered by the Arbiter, we will pay your attorney the attorney fees conferred, as well as reasonable expert witness costs and other costs you incurred.'));
        $pdf->Row(array('Can an award be explained?', 'Yes.', 'A party may request details from the Arbiter within 14 days of the ruling.  Upon such request, the Arbiter will explain the ruling in writing.'));
        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(0, 6, 'Other Options.', 0, 1, 'C');
        $pdf->setFont('Arial', '', 12);
        $pdf->Row(array('If you don\'t want to arbitrate, can you still contract for our services?', 'Yes. You can contract for our services and decide not to arbitrate.', 'Consider these choices:
    1.	Informal Dispute Resolution. Contact us and attempt to settle any Disputes.
    2.	Small-claims Court. Seek to resolve Disputes in small-claims court, within state law limits.
    3.	Opt-Out of Arbitration. Sign the Agreement and then timely opt-out of Arbitration.
    '));
        $pdf->Row(array('Can you opt-out of the Clause?', 'Yes. Within 60 days.', 'Send us written notice of your intent to opt-out of the Arbitration Clause of this Agreement within sixty (60) calendar days of signing your Agreement. List your name, address, account number and date. List that you "opt out." If you opt out, it will only apply to this Agreement.'));
        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(0, 6, 'Additional Acknowledgements and Electronic Signature.', 0, 1, 'C');
        $pdf->setFont('Arial', '', 12);
        $str = 'YOU ACKNOWLEDGE AND REPRESENT THAT:
    (A)	YOU HAVE READ, UNDERSTAND, AND AGREE TO ALL OF THE TERMS AND CONDITIONS OF THIS AGREEMENT INCLUDING THE WAIVER OF JURY TRIAL AND ARBITRATION CLAUSE AND RIGHT TO PARTICIPATE IN A CLASS ACTION AS WELL AS THE PRIVACY NOTICE;
    (B)	THE INFORMATION GIVEN IN CONNECTION WITH THIS AGREEMENT IS TRUE AND CORRECT;
    (C)	YOU HAVE THE FINANCIAL ABILITY TO REPAY IN ACCORDANCE WITH THIS AGREEMENT;
    (D)	YOU AUTHORIZE LENDER TO VERIFY THE INFORMATION GIVEN IN CONNECTION WITH THIS AGREEMENT AND GIVE LENDER CONSENT TO OBTAIN INFORMATION ON YOU FROM CONSUMER REPORTING AGENCIES OR OTHER SERVICES;
    (E)	THIS AGREEMENT WAS FILLED IN BEFORE YOU SIGNED IT;
    (F)	YOU HAD AN OPPORTUNITY TO PRINT A COMPLETED COPY OF THIS AGREEMENT FOR YOUR RECORDS;
    (G)	THIS AGREEMENT IS SUBJECT TO APPROVAL BY LENDER;';

        if ($customer_state == 'CA') {
            $str = $str . '
    (H)	YOU ARE 18 YEARS OF AGE OR OLDER AND NOT AN EMPLOYEE OF MONEY MART; AND
    (I)	YOU ARE NOT A DEBTOR UNDER ANY PROCEEDING IN BANKRUPTCY AND HAVE NO INTENTION TO FILE A PETITION FOR RELIEF UNDER THE U.S. BANKRUPTCY CODE.
    (J)	NO PERSON HAS PERFORMED ANY ACT AS A BROKER IN CONNECTION WITH THE MAKING OF THIS LOAN.';
        } else if ($customer_state == 'AL') {
            $str = $str . '
    (H)	YOU ARE 19 YEARS OF AGE OR OLDER AND NOT AN EMPLOYEE OF MONEY MART; AND
    (I)	YOU ARE NOT A DEBTOR UNDER ANY PROCEEDING IN BANKRUPTCY AND HAVE NO INTENTION TO FILE A PETITION FOR RELIEF UNDER THE U.S. BANKRUPTCY CODE.';
        } else if ($customer_state == 'WI') {
            $str = $str . '
    (H)	YOU ARE 18 YEARS OF AGE OR OLDER AND NOT AN EMPLOYEE OF MONEY MART; AND
    (I)	YOU ARE NOT A DEBTOR UNDER ANY PROCEEDING IN BANKRUPTCY AND HAVE NO INTENTION TO FILE A PETITION FOR RELIEF UNDER THE U.S. BANKRUPTCY CODE.';
        } else if ($customer_state == 'IL') {
            $str = $str . '
    (H)	YOU ARE 18 YEARS OF AGE OR OLDER AND NOT AN EMPLOYEE OF MONEY MART; AND
    (I)	YOU ARE NOT A DEBTOR UNDER ANY PROCEEDING IN BANKRUPTCY AND HAVE NO INTENTION TO FILE A PETITION FOR RELIEF UNDER THE U.S. BANKRUPTCY CODE.
    (J)	YOU MAY CONTACT THE DIVISION AT (888) 298-8089 FOR THE PURPOSE OF RECEIVING INFORMATION REGARDING CREDIT OR ASSISTANCE WITH CREDIT PROBLEMS.';
        } else {
            $str = $str . '
    (H)	YOU ARE 18 YEARS OF AGE OR OLDER AND NOT AN EMPLOYEE OF MONEY MART; AND
    (I)	YOU ARE NOT A DEBTOR UNDER ANY PROCEEDING IN BANKRUPTCY AND HAVE NO INTENTION TO FILE A PETITION FOR RELIEF UNDER THE U.S. BANKRUPTCY CODE.';
        }

        $pdf->MultiCell(0, 5, $str, 0);

        $pdf->setFont('Arial', 'B', 12);
        $pdf->MultiCell(0, 4, 'BY CLICKING THE ACKNOWLEDGMENT BUTTON BELOW, YOU UNDERSTAND THAT YOU ARE ELECTRONICALLY SIGNING THIS AGREEMENT. YOU AGREE THAT THIS ELECTRONIC SIGNATURE HAS THE FULL FORCE AND EFFECT OF YOUR PHYSICAL SIGNATURE AND THAT IT BINDS YOU TO THIS AGREEMENT IN THE SAME MANNER A PHYSICAL SIGNATURE WOULD.', 0, 'C');
        $pdf->Ln(10);

        if ($customer_state == 'WI') {
            $pdf->MultiCell(0, 5, 'NOTICE TO CUSTOMER

    DO NOT SIGN THIS IF IT CONTAINS ANY BLANK SPACES.

    YOU ARE ENTITLED TO AN EXACT COPY OF ANY AGREEMENT YOU SIGN.

    YOU HAVE THE RIGHT AT ANY TIME TO PAY IN ADVANCE THE UNPAID BALANCE DUE UNDER THIS AGREEMENT AND YOU MAY BE ENTITLED TO A PARTIAL REFUND OF THE FINANCE CHARGE.
    ', 0);
        } else if ($customer_state == 'IL') {
            $pdf->MultiCell(0, 5, 'In addition to agreeing to the terms of this agreement, I acknowledge, by my signature below, receipt from Aspen Financial Solutions, Inc. a pamphlet regarding small consumer loans.', 0);
        }

        $pdf->Cell(0, 5, 'Customer Electronic Signature:', 0, 1, 'L');
        $pdf->setFont('Arial', '', 12);
        $pdf->Ln(5);
        if ($customer_state == 'AL') {
            $pdf->Cell(0, 5, 'CAUTION -- IT IS IMPORTANT THAT YOU THOROUGHLY READ THE CONTRACT BEFORE YOU SIGN IT.', 0, 1, 'L');
            $pdf->Ln(5);
        }

        $pdf->setFont('Arial', '', 12);
        $pdf->Cell(0, 5, "$lender_name D/B/A $lender_dba_name", 0, 1, "L");
        $pdf->Ln(2);
        $pdf->setFont('Arial', 'U', 12);
        $pdf->Cell(70, 5, "/s/ Rex Northen", 0, 0, "L");
        $pdf->Cell(70, 5, "/s/ $customer_name", 0, 0, "L");
        $pdf->Cell(70, 5, $orig_dt, 0, 1, "L");

        $pdf->setFont('Arial', '', 12);
        $pdf->Cell(70, 5, "Senior Vice President", 0, 0, "L");
        $pdf->Cell(70, 5, $customer_name, 0, 0, "L");
        $pdf->Cell(70, 5, "Date", 0, 1, "L");

        $pdf->ln(10);
        $pdf->Cell(0, 5, "AFD-$customer_state-Ver. 2 " . date('m/y'));

        //Save the PDF document

        $today = getdate();
        $year = $today['year'];
        $month = strlen($today['mon']) < 2 ? "0" . $today['mon'] : $today['mon'];
        $day = strlen($today['mday']) < 2 ? "0" . $today['mday'] : $today['mday'];
        $hours = $today['hours'];
        $minutes = $today['minutes'];
        $seconds = $today['seconds'];
        $timestamp = "{$year}{$month}{$day}_{$hours}{$minutes}{$seconds}";

        if ($signature_data == 1) {
            $file = "loan_agreement_$application_nbr - signed.pdf";
        } else {
            $file = "loan_agreement_{$application_nbr}_{$timestamp}.pdf";
        }

        $pdf->Output('F', 'PDFs/'.$file);


        //Hard coded for now
        $return_array["document_path"] = $file;
        $return_array["return_value"] = 0;
        return $return_array;
    }

}





$application_nbr =  isset($_POST['app_num'])        && $_POST['app_num']       != ""    ? $_POST['app_num']          : "";
$orig_date =        isset($_POST['new_orig_dt'])    && $_POST['new_orig_dt']   != ""    ? $_POST['new_orig_dt']      : "";
$orig_time =        isset($_POST['new_orig_time'])  && $_POST['new_orig_time'] != ""    ? $_POST['new_orig_time']    : "";




$application_nbr = preg_replace('/[^0-9]/', '', $application_nbr);
$error_msg = "";

if ($application_nbr != "" && $orig_date != "" && $orig_time != "")
{
    $return_data = mm_get_application_details($application_nbr);
    if (isset($return_data['return_value']) && $return_data['return_value'] == 1)
    {
        $error_msg = $return_data['return_message'];
    }
    else
    {
        $app_data = $return_data["app_data"];
        $app_state = $app_data["state"];
        $loan_agreement_nbr = $app_data['loan_agreement_nbr'];
        $requested_deposit_type = $app_data["requested_deposit_type"];

        $loan_agreement_details = mm_get_loan_agreement_details($loan_agreement_nbr);
        $payment_schedule = mm_get_payment_schedule($loan_agreement_nbr);
        $lender_details = mm_get_lender_details($app_state);

        $loan_agreement_details['origination_dt'] = $orig_date;
        $loan_agreement_details['effective_dt'] = mm_calculate_contract_date($orig_date);

//$new_orig_dt_timestamp = new DateTime($new_origination_date);
//$day_of_week = $new_orig_dt_timestamp->format('l');
//
//$new_orig_dt_timestamp_string = $new_orig_dt_timestamp->Format('m/d/Y H:i:s');
//$effective_dt = mm_calculate_contract_date($new_orig_dt_timestamp_string, $requested_deposit_type);
//$effective_dt_timestamp = new DateTime($effective_dt);
//$eff_day_of_week = $effective_dt_timestamp->format('l');

/*
 * mm_loan_agreement structure:
 * 
 * Array
(
    [loan_agreement_nbr] => 383
    [origination_dt] => 2018-08-01
    [effective_dt] => 2018-08-02
    [apr] => 131.9893
    [first_due_dt] => 2018-09-06
    [maturity_dt] => 2020-02-06
    [payment_freq] => P
    [number_of_payments] => 18
    [total_finance_charge] => 3428.28
    [principal_amt] => 2500.00
    [total_amt_due] => 5928.28
    [late_fee_amt] => 10.00
    [late_fee_pct] => 5.00
    [late_fee_calc] => GT
    [late_fee_grace_period] => 10
    [returned_item_fee_amt] => 0.00
    [application_nbr] => 8834
    [lender_nbr] => 8
    [create_dt] => 2018-08-01 15:58:22
    [loan_nbr] => L00008834
    [ach_authorization] => I Agree
    [ach_authorization_ack] => I Agree
    [ach_agree_to_pay] => 
    [signature] => swapnaalatest9999 standard
    [signature_i_agree] => I Agree
    [signature_timestamp] => 2018-08-01 15:58:51
    [signature_ip] => 47.187.3.249
    [lp_loan_id] => 640
    [lp_loan_settings_id] => 577
    [lp_loan_setup_id] => 606
    [is_verified] => 0
    [fa_uploaded] => 0
)
 */

//$conn = mm_get_db_connection();
//
//$sql = "update mm_loan_agreement set origination_dt=? where loan_agreement_nbr=?";
//if (! $statement = $conn->prepare($sql))
//{
//	echo "DB error (prepare): error number: " . $conn->errno . ", error message: " . $conn->error . "<br><br>";
//}
//else
//{
//	$statement->bind_param('si', $new_origination_date, $loan_agreement_nbr);
//
//	if (! $statement->execute())
//	{
//		echo "DB error (execute): error number: " . $conn->errno . ", error message: " . $conn->error . "<br><br>";
//	}
//	else
//	{
//		echo "update statement executed";
//
//	}
//}




        $pdf = new MyPDF;

        $pdf_results = $pdf->mm_generate_loan_agreement_pdf($application_nbr,
                                                            $return_data,
                                                            $loan_agreement_details,
                                                            //$la_details,
                                                            $payment_schedule,
                                                            //$payment_sched_result,
                                                            $lender_details,
                                                            'PDFs/');
        $return_value = $pdf_results["return_value"];
        if ($return_value != 0) {
            //$return_message = "Failed creating the loan agreement pdf";
            echo "Failed Creating the Loan Agreement PDF";
        } else {
            $document_path = $pdf_results["document_path"];
        }
//echo "<pre>";
//
//echo mm_get_document_path() . "<br>";
//
//echo "<br><br>";
//echo "application_nbr: " . $application_nbr . "<br>";
//echo "return_data: <br>";
//print_r($return_data);
//echo "<br>app_data: <br>";
//print_r($app_data);
//echo "<br>loan_agreement_details:<br>";
//print_r($loan_agreement_details);
//echo "<br>payment_schedule:<br>";
//print_r($payment_schedule);
//echo "<br>lender_details<br>";
//print_r($lender_details);
//print_r($pdf_results);

        $_SESSION['loan_agreement_pdf'] = 'PDFs/'.$document_path;

        $link = "<a href=\"view_loan_agreement_pdf.php\" target=\"_blank\">View agreement</a> for loan number {$application_nbr}, {$orig_date} {$orig_time}";
    }
}
else
{
    $link = "";
}




?>
<!DOCTYPE html lang="en-us">
    <head>
        <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
        <script>
        
        function do_validate()
        {
            if ($('#app_num').val() == "")
            {
                alert('No application number');
                return false;
            }
            else if ($('#new_orig_dt').val() == "")
            {
                alert('No origination date');
                return false;
            }
            else if ($('#new_orig_time').val() == "")
            {
                alert('No origination time');
                return false;
            }

            if (! isValidDate($('#new_orig_dt').val()))
            {
                alert('Invalid origination date format');
                return false;
            }
            else if (! isValidTime($('#new_orig_time').val()))
            {
                alert('Invalid origination time format');
                return false;
            }

            return true;
        }
        
        function isValidDate(dateString)
        {
            // First check for the pattern
            if(!/^\d{4}\-\d{1,2}\-\d{1,2}$/.test(dateString))
            {
                return false;
            }

            // Parse the date parts to integers
            var parts = dateString.split("-");
            var day = parseInt(parts[2], 10);
            var month = parseInt(parts[1], 10);
            var year = parseInt(parts[0], 10);


            // Check the ranges of month and year
            if (year < 1000 || year > 3000 || month == 0 || month > 12)
            {
                return false;
            }

            var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

            // Adjust for leap years
            if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
            {
                monthLength[1] = 29;
            }

            // Check the range of the day
            return day > 0 && day <= monthLength[month - 1];
        }
        
        function isValidTime(timeString)
        {
            // First check for the pattern
            if(!/^\d{1,2}\:\d{1,2}$/.test(timeString))
            {
                return false;
            }

            // Parse the date parts to integers
            var parts = timeString.split(":");
            var hour = parseInt(parts[0], 10);
            var minute = parseInt(parts[1], 10);
            
            if (hour > 24)
            {
                return false;
            }
            else if (minute > 59)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        </script>
    </head>
    <body>
        <div style="width: 30%; margin-bottom: 60px; font-weight: bold;">
            <h2>Loan Agreement Effective Date Test</h2>
            This tool is for testing that the Effective Date correctly adjusts to reflect the Wells Fargo daily processing schedule, including accounting for holidays.
        </div>
        
        <div style="color: red;"><?php echo $error_msg; ?></div>
        <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" onsubmit="return do_validate();">
            <table border="1" cellpadding="4">
                <tr>
                    <td>Loan Agreement Number</td>
                    <td><input type="text" name="app_num" id="app_num" value="<?php echo $application_nbr; ?>"></td>
                     <td>&nbsp;</td>
               </tr>
                <tr>
                    <td>Origination Date (MM/DD/YYYY)</td>
                    <td><input type="date" name="new_orig_dt" id="new_orig_dt" value="<?php echo $orig_date; ?>"></td>
                    <td>
                        <script>
                        $(document).ready(function() {
                            show_day_of_week($("#new_orig_dt").val());
                            
                            $("input").on("change", function() {
                                $("#link_div").hide();
                            });
                            
                            $("#new_orig_dt").on("change", function() {
                                show_day_of_week($(this).val());
                            });
                        });
                        
                        function show_day_of_week(date)
                        {
                            var day_names = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                            var orig_date = new Date(date);
                            var day_of_week = orig_date.getDay();
                            $("#weekday_name").text(day_names[day_of_week+1]);
                        }
                        </script>
                        <div id="weekday_name" style="width: 100px;"></div>
                    </td>
               </tr>
                <tr>
                    <td>Origination Time (HH:MM)</td>
                    <td><input type="time" name="new_orig_time" id="new_orig_time" value="<?php echo $orig_time; ?>"></td>
                    <td></td>
               </tr>
                <tr>
                    <td colspan="3" align="middle"><input type="submit" value="Submit"></td>
                </tr>
            </table>
        </form>
        <br>
        <br>
        <div id="link_div"><?php echo $link; ?></div>
    </body>
</html>
