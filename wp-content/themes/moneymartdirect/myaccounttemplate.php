<?php
/*
Template Name: MY ACCOUNT TEMPLATE
*/

get_header();
error_log("mark: in myaccounttemplate.php\n");
	
// Check if user is logged in 
if ( !is_user_logged_in() ){
	$post_id = 646;
	$queried_post = get_post($post_id);
	//$title = $queried_post->post_title;
	//echo $title;
	$content = $queried_post->post_content;
	$content = apply_filters('the_content', $content);
	$content = str_replace(']]>', ']]&gt;', $content);
	echo $content;
}else{
        //The customer is logged in.  Display a Welcome header message "Welcome First_name Last_Name".  Almost like a banner
        	echo "Looks like the customer is logged in.<br>";
        
        //Check to see if there ia an action passed as part of the query string i.e. firstfederalcredit.com/my-account?action=APPHISTORY
        if(isset($_GET["action"])){
            //If the URL passed a query parameter of action then it will be used to determine what content to display
            
            //Display a button/link that navigates back to the home my account page
            
            //Get the query string value for action.  NEED TO ADD CLEANSING TO PREVENT CODE INJECTION
            $action = $_GET["action"];
            if($action=="APPHISTORY"){
                //Insert code here to get the application history and display the content.  Debbie to define content, Jason to provide data routine, Bent to output it.
            }else if($action == "LOANHISTORY"){
                //Insert code here to get the loan history and display the content.  Debbie to define content, Jason to provide data routine, Ben to output it.
            
                
            }else if($action =="CUSTSETTINGS"){
                //Insert code here to display the form for the customer to have the ability to update their personal settings.
                
            }else if($action=="MAKEPMT"){
                    //The customer has selected to make a payment so we need to display the payment options portal
            }else{
                //Received an action that we didn't expect.  Should only happen if the user manually changed the value.
                //We can either disply a message stating the action doesn't exists or just display the base my account page content
            }
        }else{
                //No action is currently selected so therefore get the account status and determine what available actions to show
            $account_status_arr = mm_get_account_status();
            if($account_status_arr["return_value"] >0){
            //There was an issue getting the account status.  Log a message and display an error message
                $return_message = $account_status_arr["return_message"];
                mm_log_error('my account page', "Unable to get account status.  Error is: $return_message");
          
             }else{
            $account_status = $account_status_arr["account_status"];
            echo "This customer's account status is $account_status<br>";
             $mm_account_nbr = $account_status_arr["account_nbr"];
            if($account_status == "IDLE"){
              //The account is idle so therefore show a link/button to display the apply now feature.  The url to the form page should prepopulate as much of the data as possible.  
            }else if($account_status == "ACTIVEAPP"){
                  //The account has an active account so depending on the status of the application display a resume action link/button
                     
                     //Get the application status of the active application
                     $app_status = $account_status_arr["application_status"];
                 
                  if($app_status == 1){
                        //The application is considered in draft - Should never happen, but if it does then display a link/button to take the customer to the application-processing page   
                  }else if($app_status == 2){
                      //The customer is approved pending acceptance so display a link to take the customer to the application-processing page
                  }else if($app_status == 3){
                        //The customer has accepted the requested amount and deposit method.  Display a resume app link/button that takes the customer to the verification-processing page    
                           
                  }else if($app_status == 4){
                      //The customer has been approved pending customer documents.  Display the message for them to send in their documents
                  }else if($app_status == 5){   
                      //The customer has been approved pending agent review.  Display the message that we have everything we need and are working on verifying their information.
                  }else if($app_status == 6){
                    //The customer is approved pending Customer Loan Documents. The resume application button/link should redirect them to the verification-processing page
                      
                  }else if($app_status == 9){
                          //The customer has an application that is in stuck status.  Display the error message that we use through the application process.
                  }else{
                        //There shouldn't be any other application statuses that appear when the customer status is ACTIVEAPP.  Capture them as errors here and display the error messaage
                        mm_log_error('my account page', "customer status is ACTIVEAPP and received unexpected application status $app_status for account $mm_account_nbr");
                      
                  }
            }else if($account_status == "ACTIVELOAN"){
                  //The account has an active loan so depending on the status display information about the loan as well the ability to make a payment
              
            }else if($account_status == "CHARGEDOFF"){
              
              
            }
          //Display links/buttons for the functionality that is always available regardless of account and application status.  I believe we landed on application history, loan history and customer settings
          
             } 
      }
	
}
?>

<?php
get_footer(); 
?>
	
	
	
