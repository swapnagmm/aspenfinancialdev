<?php
// include MM middleware code
require_once 'include/mm_middleware.php';

add_filter('gform_submit_button_15', '__return_false'); /* Hides the submit button on form 15 - How it works/what it costs */
add_filter('gform_submit_button_20', '__return_false'); /* Hides the submit button on form 20 - How it works/what it costs */
add_filter('gform_enable_password_field', '__return_true'); /* enables the password fields */

/**
 * Code to start and handle sessions
 */
function myStartSession() {
    $tmp = session_id();
    DEBUG ? error_log("functions.php flow: myStartSession, session_id: $tmp\n") : null;

    if (!$tmp) {
        session_start();
    }

    $current_user = wp_get_current_user();
    if ($current_user->ID > 0) {
        mm_restore_session($current_user->user_email);
    }
    else {
        error_log("mark: No current email address found in myStartSession\n");
    }
}
add_action('init', 'myStartSession', 1);

/**
 *
 */
function myEndSession() {
    DEBUG ? error_log("functions.php flow: myEndSession\n") : null;
    session_start();
    error_log("Swapna sessions clear before data",print_r($_SESSION));
    session_destroy();
    error_log("Swapna sessions clear after data",print_r($_SESSION));

}
add_action('wp_logout', 'myEndSession', 11);
//add_action('wp_login', 'myEndSession');
//add_action('end_session_action', 'myEndSession',11);

/**
 * @param $login
 */
function restoreSession($login) {
    DEBUG ? error_log("functions.php flow: restoreSession ($login)\n") : null;
    $user_info = get_user_by('login', $login);
    $email_address = $user_info->user_login;
    mm_restore_session($email_address);
}
add_action('wp_login', 'restoreSession', 11);

/**
 *
 */
function call_mm_log_page_view() {
    DEBUG ? error_log("functions.php flow: call_mm_log_page_view\n") : null;

    $data_array["page_name"] = isset($_SERVER["REQUEST_URI"]) ? $_SERVER["REQUEST_URI"] : '';
    $data_array["ip_address"] = isset($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : '';
    $data_array["request_time"] = isset($_SERVER["REQUEST_TIME"]) ? $_SERVER["REQUEST_TIME"] : '';
    $data_array["http_cookie"] = isset($_SERVER["HTTP_COOKIE"]) ? $_SERVER["HTTP_COOKIE"] : '';
    mm_log_page_view($data_array);
}
add_action('init', 'call_mm_log_page_view', 11);


/**
 * Enqueue the scrips - tie the child theme to the parent (Divi)
 */
function my_theme_enqueue_styles() {
    DEBUG ? error_log("functions.php flow: my_theme_enqueue_styles\n") : null;
    $parent_style = 'divi-style';

    wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css');
    wp_enqueue_style('child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array($parent_style),
        wp_get_theme()->get('Version')
    );
    wp_register_style('google-fonts', '//fonts.googleapis.com/css?family=Open+Sans|Open+Sans+Condensed:300');
    wp_register_style('fontawesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
}
add_action('wp_enqueue_scripts', 'my_theme_enqueue_styles');

wp_enqueue_script('mmdjs', get_stylesheet_directory_uri() . '/js/mmd.js', array('jquery'), null, true);

/**
 * @return int
 */
function gf_get_current_page()
{
    return rgpost('gform_source_page_number_' . $_POST['gform_submit']) ? rgpost('gform_target_page_number_' . $_POST['gform_submit']) : 1;
}

/**
 * If a customer enters a lead, check to see if they already have an account before continuing.
 * @param $result
 * @param $value
 * @param $form
 * @param $field
 * @return mixed
 */
function gf_validate_lead_email($result, $value, $form, $field) {
    DEBUG ? error_log("functions.php flow: gf_validate_lead_email value:$value\n") : null;
    //Add code here to validate that the email address entered is ok.
    $temp_email = rgpost('input_2');
    $_SESSION["TEST EMAIL"] = $temp_email;
    $current_user = wp_get_current_user();
    $user_login = $current_user->user_login;
    $_SESSION["SESSION USER LOGIN"] = $user_login;
    if ($user_login == '') {
        //There isn't a user logged in so
        $temp_results = mm_get_account_details_from_email_address($temp_email);
        $_SESSION["Temp email"] = $temp_results["account_nbr"];
        if ($temp_results["account_nbr"] > 0) {
            //The username exists
            $result["is_valid"] = false;
            $result["message"] = "<font color=\"white\"><BR>This email address is already in use.  Please <a href=\"my-account\"><font color=\"white\"><u>log in</U></font></a> to continue.</font>";
            return $result;
        }
    }

    return $result;
}
add_filter("gform_field_validation_6_2", 'gf_validate_lead_email', 11, 4);


/**
 * After the application is submitted, store the lead id of the associated application
 * @param $entry
 * @param $form
 */
function after_lead_completion($entry, $form) {
    DEBUG ? error_log("functions.php flow: after_lead_completion\n") : null;
    $first_name = $entry["1.3"];
    $last_name = $entry["1.6"];
    $email_address = $entry["2"];
    $body = "There was a new lead entered on the home page.\n\nFirst Name: $first_name\nLast Name: $last_name\nEmail Address: $email_address\n";
    $html = "<html><head></head><body><H1>There was a new lead entered on the home page.</H1><p>First Name: $first_name</p><p>Last Name: $last_name</p><p>Email Address: $email_address</p></body></html>";
    mm_send_email(ADMIN_EMAIL_ADDRESSES,
        'MM Direct Monitoring <support@moneymartdirect.com>',
        EMAIL_SUBJECT . 'New Lead Completed',
        $body,
        $html);

    //Add code to log info about the customer after submission
    //Add a capture of some lead detail
    $unique_id = isset($_SERVER["UNIQUE_ID"]) ? $_SERVER["UNIQUE_ID"] : 'NOT SET';
    $ip_address = isset($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : "NOT SET";
    $http_user_agent = isset($_SERVER["HTTP_USER_AGENT"]) ? $_SERVER["HTTP_USER_AGENT"] : "NOT SET";
    $page_desc = "TEASER";
    mm_create_lead_detail($unique_id, $ip_address, $http_user_agent, $page_desc);

}
add_action('gform_after_submission_6', 'after_lead_completion', 11, 2);

/**
 * @param $entry
 * @param $form
 */
function after_submission_test($entry, $form) {
    //error_log_vardump("entry: ",$entry);
    DEBUG ? error_log("functions.php flow: after_submission_test\n") : null;
    $first_name = $entry["53.3"];
    $last_name = $entry["53.6"];
    $last_4_ssn = $entry["76"];
    $mobile_phone_nbr = $entry["59"];
    $email_address = $entry["54"];
    $ssn = "xxxxxx" . $last_4_ssn;
    $body = "There was a new application created on the website.\n\nFirst Name: $first_name\nLast Name: $last_name\nEmail Address: $email_address\nPhone:$mobile_phone_nbr\nSSN:$ssn\n";
    $html = "<html><head></head><body></body><h1>There was a new lead entered on the home page.</h1><p>First Name: $first_name</p><p>Last Name: $last_name</p><p>Email Address: $email_address</p>";
    $html .= "<p>Phone Number: $mobile_phone_nbr</p><p>SSN: $ssn</p></body></html>";

    mm_send_email(AFTER_SUBMISSION_TEST_LIST, 'MM Direct Monitoring <support@moneymartdirect.com>',
                  EMAIL_SUBJECT . 'New Application Completed', $body, $html);

    //Add code to log info about the customer after submission
    //Add a capture of some lead detail
    $unique_id = isset($_SERVER["UNIQUE_ID"]) ? $_SERVER["UNIQUE_ID"] : 'NOT SET';
    $ip_address = isset($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : "NOT SET";
    $http_user_agent = isset($_SERVER["HTTP_USER_AGENT"]) ? $_SERVER["HTTP_USER_AGENT"] : "NOT SET";
    $page_desc = "APP_SUBMIT";
    mm_create_lead_detail($unique_id, $ip_address, $http_user_agent, $page_desc);

    $_SESSION["app_state"] = mm_state_to_cd($entry["52"]);
    $_SESSION["app_pay_frequency_1"] = $entry["9"];
    $_SESSION["app_gf_lead_id"] = $entry["id"];

    // the following are needed for the WI logic in form 17
    $_SESSION['app_street_address'] = $entry['51.1'];
    $_SESSION['app_appt_suite'] = $entry['51.2'];
    $_SESSION['app_city'] = $entry['51.3'];
    $_SESSION['app_zip_code'] = $entry['51.5'];
}
add_action('gform_after_submission_14', 'after_submission_test', 11, 2);


/**
 * after the loan terms form is submitted capture the lead id on this form as well and save it in a session
 * @param $entry
 * @param $form
 */
function form_17_after_submission($entry, $form) {
    //error_log_vardump("entry: ",$entry);
    $app_status = $_SESSION["app_application_status"];
    DEBUG ? error_log("functions.php flow: form_17_after_submission, status: $app_status\n") : null;

    // Mark put this code here.  We should never ever get this far without the current status being 2
    if ($app_status != 2) {
        $app_nbr = $_SESSION["app_application_nbr"];
        mm_update_database_value('mm_application', 'application_status', 9, 'i', "application_nbr", $app_nbr);
        error_log("Killing any further processing due to inconsistent state.  Application number: $app_nbr\n");
        die();
    }

    global $current_user;
    get_currentuserinfo();
    $email_address = $current_user->user_email;
    mm_restore_session($email_address);

    if (session_id()) {
        $preverif_lead_id = $entry["id"];
        $requested_amt = $entry["1"];
        $requested_deposit_type = $entry["4"];
        $mm_application_nbr = $_SESSION["app_application_nbr"];
        $_SESSION["app_requested_deposit_type"] = $requested_deposit_type;
        $_SESSION["TESTING_APPLICATION_NBR"] = $_SESSION["app_application_nbr"];
        mm_update_database_value('mm_application', 'requested_amt', $requested_amt, 'd', "application_nbr", $mm_application_nbr);
        mm_update_database_value('mm_application', 'requested_deposit_type', $requested_deposit_type, 's', "application_nbr", $mm_application_nbr);
        mm_update_database_value('mm_application', 'application_status', 3, 'i', "application_nbr", $mm_application_nbr);
        mm_update_database_value('mm_application', 'preverif_lead_id', $preverif_lead_id, 'i', "application_nbr", $mm_application_nbr);

        if ($_SESSION["app_state"] == 'WI') {
            $wi_married_flag = $entry['12'] == 'Married' ? true : false;
            $wi_spouse_first_name = isset($entry['14.3']) ? $entry['14.3'] : '';
            $wi_spouse_last_name = isset($entry['14.6']) ? $entry['14.6'] : '';
            $wi_spouse_same_address_flag = $entry['15.1'] == '' ? false : true;
            $wi_spouse_street_address = isset($entry['13.1']) ? $entry['13.1'] : '';
            $wi_spouse_apt_suite = isset($entry['13.2']) ? $entry['13.2'] : '';
            $wi_spouse_city = isset($entry['13.3']) ? $entry['13.3'] : '';
            $wi_spouse_zipcode = isset($entry['13.5']) ? $entry['13.5'] : '';
            $wi_spouse_state = isset($entry['13.4']) ? $entry['13.4'] : '';
            $wi_address = array('street_address' => $wi_spouse_street_address, 'apt_suite' => $wi_spouse_apt_suite, 'city' => $wi_spouse_city, 'state' => $wi_spouse_state, 'zipcode' => $wi_spouse_zipcode,);
            $wi_spouse_info = array('first_name' => $wi_spouse_first_name, 'last_name' => $wi_spouse_last_name, 'same_address_flag' => $wi_spouse_same_address_flag, 'address' => $wi_address);
            $wi_data = array('married_flag' => $wi_married_flag, 'spouse' => $wi_spouse_info);
            mm_insert_state_specific_WI($mm_application_nbr, 'Marital_Notice_Info', 'json', json_encode($wi_data));
        }

    }

    //Add code to log info about the customer after submission
    //Add a capture of some lead detail
    $unique_id = isset($_SERVER["UNIQUE_ID"]) ? $_SERVER["UNIQUE_ID"] : 'NOT SET';
    $ip_address = isset($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : "NOT SET";
    $http_user_agent = isset($_SERVER["HTTP_USER_AGENT"]) ? $_SERVER["HTTP_USER_AGENT"] : "NOT SET";
    $page_desc = "APP_LOAN_AMT";
    mm_create_lead_detail($unique_id, $ip_address, $http_user_agent, $page_desc);
}
add_action('gform_after_submission_17', 'form_17_after_submission', 10, 2);


function populate_cust_address_1( $value, $field, $name ) {
    DEBUG ? error_log("functions.php flow: populate_cust_address_1: value: $value\n") : null;
    return $_SESSION['app_street_address'];
}
add_filter( 'gform_field_value_cust_address_1', 'populate_cust_address_1', 12, 4 );

function populate_cust_address_2( $value ) {
    DEBUG ? error_log("functions.php flow: populate_cust_address_2: value: $value\n") : null;
    return $_SESSION['app_appt_suite'];
}
add_filter( 'gform_field_value_cust_address_2', 'populate_cust_address_2', 12, 4 );

function populate_cust_city( $value ) {
    DEBUG ? error_log("functions.php flow: populate_cust_city: value: $value\n") : null;
    return $_SESSION['app_city'];
}
add_filter( 'gform_field_value_cust_city', 'populate_cust_city', 12, 4 );

function populate_cust_state( $value ) {
    DEBUG ? error_log("functions.php flow: populate_cust_state: value: $value\n") : null;
    return $_SESSION['app_state'];
}
add_filter( 'gform_field_value_cust_state', 'populate_cust_state', 12, 4 );

function populate_cust_zip( $value ) {
    DEBUG ? error_log("functions.php flow: populate_cust_zip: value: $value\n") : null;
    return $_SESSION['app_zip_code'];
}
add_filter( 'gform_field_value_cust_zip', 'populate_cust_zip', 12, 4 );

/**
 * remove admin bar for all users except admin level
 */
function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}
add_action('after_setup_theme', 'remove_admin_bar');

/**
 * creates unique UUID value
 * This is not likely to be necessary anymore, but we'll leave it just in case it becomes useful
 * @param string $prefix
 * @return string
 */
function uuid($prefix = '') {
    $chars = md5(uniqid(mt_rand(), true));
    $uuid = substr($chars, 0, 8) . '-';
    $uuid .= substr($chars, 8, 4) . '-';
    $uuid .= substr($chars, 12, 4) . '-';
    $uuid .= substr($chars, 16, 4) . '-';
    $uuid .= substr($chars, 20, 12);
    return $prefix . $uuid;
}
add_filter("gform_field_value_uuid", "uuid");

/**
 * Custom GF validation function used to ensure customer signature of last 4 matches the SSN on the application
 * @param $result
 * @param $value
 * @param $form
 * @param $field
 * @return string
 */
function gf_validate_last_4($result, $value, $form, $field) {
    DEBUG ? error_log("functions.php flow: gf_validate_last_4, value: $value\n") : null;
    $ssn = rgpost('input_78');
    $last_4 = substr($ssn, -4);
    //If the values match then set the is_valid to true
    if ($result['is_valid'] && $value == $last_4) {
        $result['is_valid'] = true;
        $result['message'] = '';
    } else {
        $result['is_valid'] = false;
        $result['message'] = "The last 4 digits don't match your application";
    }
    return $result;
}
add_filter("gform_field_validation_14_76", 'gf_validate_last_4', 10, 4);

/**
 * validate the zip code on application
 * @param $result
 * @param $value
 * @param $form
 * @param $field
 * @return mixed
 */
function gf_validate_zip_code($result, $value, $form, $field) {
    DEBUG ? error_log("functions.php flow: gf_validate_zip_code\n") : null;

     if (!$result["is_valid"]) {
        return $result;
    } else {
        $zip_code = rgpost('input_51_5');
        if (strlen($zip_code) == 5) {
            if (is_numeric($zip_code)) {
                $result["is_valid"] = true;
                $result["message"] = '';
            } else {
                $result["is_valid"] = false;
                $result["message"] = "Invalid Zip Code";
            }
        } else if (strlen($zip_code) == 10) {

            $zip5 = substr($zip_code, 0, 5);
            $zip4 = substr($zip_code, -4);
            $zip_dash = substr($zip_code, 5, 1);

            if (is_numeric($zip5) && is_numeric($zip4) && $zip_dash == '-') {
                $result["is_valid"] = true;
                $result["message"] = '';
            } else {
                $result["is_valid"] = false;
                $result["message"] = 'Invalid Zip Code';

            }
        } else {
            $result["is_valid"] = false;
            $result["message"] = 'Invalid Zip Code';

        }
    }

    //Add code to log info about the customer after submission
    //Add a capture of some lead detail
    $unique_id = isset($_SERVER["UNIQUE_ID"]) ? $_SERVER["UNIQUE_ID"] : 'NOT SET';
    $ip_address = isset($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : "NOT SET";
    $http_user_agent = isset($_SERVER["HTTP_USER_AGENT"]) ? $_SERVER["HTTP_USER_AGENT"] : "NOT SET";
    $page_desc = "APP_PAGE2";
    mm_create_lead_detail($unique_id, $ip_address, $http_user_agent, $page_desc);

    return $result;
}
add_filter("gform_field_validation_14_51", 'gf_validate_zip_code', 11, 4);

/**
 * @param $result
 * @param $value
 * @param $form
 * @param $field
 * @return mixed
 */
function gf_validate_bank_acct_type($result, $value, $form, $field) {
    DEBUG ? error_log("functions.php flow: gf_validate_bank_acct_type, value: $value\n") : null;

    if (!$result["is_valid"]) {
        return $result;
    } else {
        $acct_type = rgpost('input_113');
        if ('Savings' == $acct_type || 'Prepaid' == $acct_type) {
            $result["is_valid"] = false;
            $result["message"] = 'At this time, only checking accounts may be used.  Please enter a valid checking account.';
        }
    }

    return $result;
}
add_filter("gform_field_validation_14_113", 'gf_validate_bank_acct_type', 11, 4);


/**
 * validate the hourly rate 1
 * @param $result
 * @param $value
 * @param $form
 * @param $field
 * @return mixed
 */
function gf_validate_hourly_rate($result, $value, $form, $field) {
    DEBUG ? error_log("functions.php flow: gf_validate_hourly_rate, value: $value\n") : null;

    if (!$result["is_valid"]) {
        return $result;
    } else {
        $hourly_rate = rgpost('input_7');
        $hourly_rate = str_replace(",", "", str_replace("\$", '', $hourly_rate));
        $hourly_or_salary = rgpost('input_6');
        if ($hourly_or_salary != 'Hourly') {
            return $result;
        } else if ($hourly_rate > 0 && $hourly_rate < 75) {
            $result["is_valid"] = true;
            $result["message"] = '';
        } else {
            $result["is_valid"] = false;
            $result["message"] = "Please enter a valid hourly rate";
        }

        return $result;
    }
}
add_filter("gform_field_validation_14_7", 'gf_validate_hourly_rate', 11, 4);

/**
 * validate the hourly rate 2
 * @param $result
 * @param $value
 * @param $form
 * @param $field
 * @return mixed
 */
function gf_validate_hourly_rate_2($result, $value, $form, $field) {
    DEBUG ? error_log("functions.php flow: gf_validate_hourly_rate_2, value: $value\n") : null;
    if (!$result["is_valid"]) {
        return $result;
    } else {
        $hourly_rate = rgpost('input_81');
        $hourly_rate = str_replace(",", "", str_replace("\$", '', $hourly_rate));
        $hourly_or_salary = rgpost('input_80');
        if ($hourly_or_salary != 'Hourly') {
            return $result;
        } else if ($hourly_rate > 0 && $hourly_rate < 75) {
            $result["in_valid"] = true;
            $result["message"] = '';

        } else {
            $result["is_valid"] = false;
            $result["message"] = "Please enter a valid hourly rate";
        }

        return $result;
    }
}
add_filter("gform_field_validation_14_81", 'gf_validate_hourly_rate_2', 11, 4);

/**
 * validate the state entered
 * @param $result
 * @param $value
 * @param $form
 * @param $field
 * @return mixed
 */
function gf_validate_state($result, $value, $form, $field) {
    DEBUG ? error_log("functions.php flow: gf_validate_state, value: $value\n") : null;

    if (!$result["is_valid"]) {
        return $result;
    } else {
        $state = rgpost('input_52');

        if ($state != 'Missouri' &&
            $state != 'Utah' &&
            $state != 'California' &&
            $state != 'New Mexico' &&
            $state != 'Mississippi' &&
            $state != 'Delaware' &&
            $state != 'Alabama' &&
            $state != 'Idaho' &&
            $state != 'South Carolina' &&
            $state != 'Wisconsin' &&
            $state != 'Illinois') {
            $result["is_valid"] = false;
            $result["message"] = "We are currently not offering loans in your state.";
        } else {
            $_SESSION["app_state"] = mm_state_to_cd($state);
            $result["is_valid"] = true;
            $result["message"] = '';
        }

        return $result;
    }
}
add_filter("gform_field_validation_14_52", 'gf_validate_state', 11, 4);

/**
 * validate the salary
 * @param $result
 * @param $value
 * @param $form
 * @param $field
 * @return mixed
 */
function gf_validate_annual_salary($result, $value, $form, $field) {
    DEBUG ? error_log("functions.php flow: gf_validate_annual_salary\n") : null;

    if (!$result["is_valid"]) {
        return $result;
    } else {
        $annual_salary = rgpost('input_33');
        $annual_salary = str_replace(',', '', str_replace("\$", '', $annual_salary));
        $_SESSION["annual salary"] = $annual_salary;
        $hourly_or_salary = rgpost('input_6');
        if ($hourly_or_salary != 'Salary') {
            return $result;
        } else if ($annual_salary > 0 && $annual_salary < 150000) {
            $result["in_valid"] = true;
            $result["message"] = '';

        } else {
            $result["is_valid"] = false;
            $result["message"] = "Please enter a valid annual salary";
        }

        return $result;
    }
}
add_filter("gform_field_validation_14_33", 'gf_validate_annual_salary', 11, 4);

/**
 * validate the salary 2
 * @param $result
 * @param $value
 * @param $form
 * @param $field
 * @return mixed
 */
function gf_validate_annual_salary_2($result, $value, $form, $field) {
    DEBUG ? error_log("functions.php flow: gf_validate_annual_salary_2\n") : null;

    if (!$result["is_valid"]) {
        return $result;
    } else {
        $annual_salary = rgpost('input_84');
        $annual_salary = str_replace(',', '', str_replace("\$", '', $annual_salary));
        $_SESSION["annual salary 2"] = $annual_salary;
        $hourly_or_salary = rgpost('input_80');
        if ($hourly_or_salary != 'Salary') {
            return $result;
        } else if ($annual_salary > 0 && $annual_salary < 150000) {
            $result["in_valid"] = true;
            $result["message"] = '';
        } else {
            $result["is_valid"] = false;
            $result["message"] = "Please enter a valid annual salary";
        }

        return $result;
    }
}
add_filter("gform_field_validation_14_84", 'gf_validate_annual_salary_2', 11, 4);

/**
 * validate the number of hours
 * @param $result
 * @param $value
 * @param $form
 * @param $field
 * @return mixed
 */
function gf_validate_hours_worked($result, $value, $form, $field) {
    DEBUG ? error_log("functions.php flow: gf_validate_hours_worked\n") : null;

    if (!$result["is_valid"]) {
        return $result;
    } else {
        $hours_worked = rgpost('input_8');
        $_SESSION["hours_worked"] = $hours_worked;
        $hourly_or_salary = rgpost('input_6');
        if ($hourly_or_salary != 'Hourly') {
            return $result;
        } else if ($hours_worked > 0 && $hours_worked < 81) {
            $result["in_valid"] = true;
            $result["message"] = '';
        } else {
            $result["is_valid"] = false;
            $result["message"] = "Please enter a valid number of hours worked";
        }

        return $result;
    }
}
add_filter("gform_field_validation_14_8", 'gf_validate_hours_worked', 11, 4);

/**
 * validate the number of hours 2
 * @param $result
 * @param $value
 * @param $form
 * @param $field
 * @return mixed
 */
function gf_validate_hours_worked_2($result, $value, $form, $field) {
    DEBUG ? error_log("functions.php flow: gf_validate_hours_worked_2\n") : null;

    if (!$result["is_valid"]) {
        return $result;
    } else {
        $hours_worked = rgpost('input_82');
        $_SESSION["hours_worked"] = $hours_worked;
        $hourly_or_salary = rgpost('input_80');
        if ($hourly_or_salary != 'Hourly') {
            return $result;
        } else if ($hours_worked > 0 && $hours_worked < 81) {
            $result["in_valid"] = true;
            $result["message"] = '';

        } else {
            $result["is_valid"] = false;
            $result["message"] = "Please enter a valid number of hours worked";
        }

        return $result;
    }
}
add_filter("gform_field_validation_14_82", 'gf_validate_hours_worked_2', 11, 4);

/**
 * validate the next pay date
 * @param $result
 * @param $value
 * @param $form
 * @param $field
 * @return mixed
 */
function gf_validate_next_pay_date($result, $value, $form, $field) {
    DEBUG ? error_log("functions.php flow: gf_validate_next_pay_date\n") : null;

    if (!$result["is_valid"]) {
        return $result;
    } else {
        $pay_date = new DateTime(rgpost('input_14'), new DateTimeZone('America/Chicago'));
        $pay_date->setTime(0, 0, 0);
        $pay_date_dom = $pay_date->format('d') * 1;
        $pay_frequency = rgpost('input_9');

        $dom_paid = rgpost('input_11') * 1;
        $semi_dom_paid = rgpost('input_17');

        $_SESSION["PAY_DATE_DOM"] = $pay_date_dom * 1;
        $_SESSION["PAY_FREQUENCY"] = $pay_frequency;
        $_SESSION["DOM_PAID"] = "$dom_paid";
        $_SESSION["SEMI_PAY"] = $semi_dom_paid;

        $today = new DateTime('now', new DateTimeZone('America/Chicago'));
        $today->setTime(0, 0, 0);
        $today_dom = $today->format('d');
        $today2 = clone $today;
        $today_2_last_dom = $today2->modify("last day of");
        $today_is_last_dom = $today_dom == $today_2_last_dom->format('d');
        $temp_dt = new DateTime($today->format('m/d/Y'));
        $last_dom = $temp_dt->format('m/t/Y');

        if ($today_dom < 15) {
            $temp_dt = new DateTime($today->format('m/d/Y'));
            $semi_next_pay_date = $temp_dt->format('m/15/Y');
        } else {
            if ($semi_dom_paid == "1st and 15th") {
                $temp_dt = new DateTime($today->format('m/d/Y'));
                $semi_next_pay_date = $temp_dt->modify("first day of next month")->format('m/d/Y');
            } else {
                // 15th and Last
                if ($today_is_last_dom) {
                    $tmp_dt = $temp_dt->modify("first day of next month")->modify("+14 day");
                    $semi_next_pay_date = $tmp_dt->format('m/d/Y');
                } else {
                    $semi_next_pay_date = $last_dom;
                }
            }
        }

        //Make sure the paydate is in the future
        $date_diff = $today->diff($pay_date, false)->format("%r%a");
        if ($date_diff < 1) {
            $result["is_valid"] = false;
            $result["message"] = "Please enter a future date";
        } else if ($pay_frequency == "Bi-Weekly" && $date_diff > 14) {
            $result["is_valid"] = false;
            $result["message"] = "Please select your NEXT pay date";
        } else if ($pay_frequency == "Paid Weekly" && $date_diff > 7) {
            $result["is_valid"] = false;
            $result["message"] = "Please select your NEXT pay date";
        } else if ($pay_frequency == "Paid Monthly" && $date_diff > 31) {
            $result["is_valid"] = false;
            $result["message"] = "Please select your NEXT pay date";
        } else if ($pay_frequency == "Paid Twice Monthly" && $date_diff > 16) {
            $result["is_valid"] = false;
            $result["message"] = "Please select your NEXT pay date";
        } else if ($pay_frequency == "Paid Monthly" && $dom_paid != $pay_date_dom) {
            $result["is_valid"] = false;
            $result["message"] = "Please select a date that matches your pay schedule.";
        } else if ($pay_frequency == "Paid Twice Monthly" && $semi_next_pay_date != $pay_date->format('m/d/Y')) {
            $result["is_valid"] = false;
            $result["message"] = "Please select a date that matches your pay schedule - $semi_next_pay_date";
        } else {
            $result["is_valid"] = true;
            $result["message"] = "";
        }

        //Add code to log info about the customer after submission
        //Add a capture of some lead detail
        $unique_id = isset($_SERVER["UNIQUE_ID"]) ? $_SERVER["UNIQUE_ID"] : 'NOT SET';
        $ip_address = isset($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : "NOT SET";
        $http_user_agent = isset($_SERVER["HTTP_USER_AGENT"]) ? $_SERVER["HTTP_USER_AGENT"] : "NOT SET";
        $page_desc = "APP_PAGE3";
        mm_create_lead_detail($unique_id, $ip_address, $http_user_agent, $page_desc);

        return $result;
    }
}
add_filter("gform_field_validation_14_14", 'gf_validate_next_pay_date', 11, 4);

/**
 * validate the next pay date 2
 * @param $result
 * @param $value
 * @param $form
 * @param $field
 * @return mixed
 */
function gf_validate_next_pay_date_2($result, $value, $form, $field) {
    DEBUG ? error_log("functions.php flow: gf_validate_next_pay_date_2\n") : null;

    if (!$result["is_valid"]) {
        return $result;
    } else {
        $pay_date = new DateTime(rgpost('input_102'));
        $pay_date->setTime(0, 0, 0);
        $today = new DateTime();
        $today->setTime(0, 0, 0);
        $date_diff = $today->diff($pay_date, false)->format("%r%a");
        if ($date_diff < 1) {
            $result["is_valid"] = false;
            $result["message"] = "Please enter a future date";
        } else {
            $result["is_valid"] = true;
            $result["message"] = "";
        }
        return $result;
    }
}
add_filter("gform_field_validation_14_102", 'gf_validate_next_pay_date_2', 11, 4);

function charAt($str, $pos) {
    return (substr($str, $pos, 1) !== false) ? substr($str, $pos, 1) : -1;
}

/**
 * validate the routing number
 * @param $result
 * @param $value
 * @param $form
 * @param $field
 * @return mixed
 */
function gf_validate_routing_nbr($result, $value, $form, $field) {
    DEBUG ? error_log("functions.php flow: gf_validate_routing_nbr\n") : null;

    if (!$result["is_valid"]) {
        return $result;
    } else {
        $routing_nbr = rgpost('input_63');
        $confirm_routing_nbr = rgpost('input_109');
        if ($routing_nbr !== $confirm_routing_nbr) {
            $result["is_valid"] = false;
            $result["message"] = "Routing number and Confirm Routing number are not equal";
            return $result;
        }

        if (strlen($routing_nbr) != 9 || !is_numeric($routing_nbr)) {
            $result["is_valid"] = false;
            $result["message"] = "Please enter a valid 9 digit routing number";
            return $result;
        }

        $n = 0;
        for ($i = 0; $i < strlen($routing_nbr); $i += 3) {
            $n += (((int)charAt($routing_nbr, $i) * 3)
                + ((int)charAt($routing_nbr, $i + 1) * 7)
                + ((int)charAt($routing_nbr, $i + 2)));
        }

        if (!($n !== 0 && $n % 10 === 0)) {
            $result["is_valid"] = false;
            $result["message"] = "Invalid routing number";
            return $result;
        } else {
            $result["is_valid"] = true;
            $result["message"] = "";
        }

        //Add code to log info about the customer after submission
        //Add a capture of some lead detail
        $unique_id = isset($_SERVER["UNIQUE_ID"]) ? $_SERVER["UNIQUE_ID"] : 'NOT SET';
        $ip_address = isset($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : "NOT SET";
        $http_user_agent = isset($_SERVER["HTTP_USER_AGENT"]) ? $_SERVER["HTTP_USER_AGENT"] : "NOT SET";
        $page_desc = "APP_PAGE4";
        mm_create_lead_detail($unique_id, $ip_address, $http_user_agent, $page_desc);
        return $result;
    }
}
add_filter("gform_field_validation_14_63", 'gf_validate_routing_nbr', 11, 4);

// validate the confirm routing number
add_filter("gform_field_validation_14_109", 'gf_validate_confirm_routing_nbr', 11, 4);

function gf_validate_confirm_routing_nbr($result, $value, $form, $field) {
    DEBUG ? error_log("functions.php flow: gf_validate_confirm_routing_nbr\n") : null;
    if (!$result["is_valid"]) {
        return $result;
    } else {
        $routing_nbr = rgpost('input_63');
        $confirm_routing_nbr = rgpost('input_109');
        if ($routing_nbr !== $confirm_routing_nbr) {
            $result["is_valid"] = false;
            $result["message"] = "Routing number and Confirm Routing number are not equal";
            return $result;
        }

        return $result;
    }
}

// validate the bank number
add_filter("gform_field_validation_14_64", 'gf_validate_bank_acct_nbr', 11, 4);

function gf_validate_bank_acct_nbr($result, $value, $form, $field) {
    DEBUG ? error_log("functions.php flow: gf_validate_bank_acct_nbr\n") : null;
    if (!$result["is_valid"]) {
        return $result;
    } else {
        $bank_acct_nbr = trim(rgpost('input_64'));
        $confirm_bank_acct_nbr = trim(rgpost('input_110'));

        if ($bank_acct_nbr !== $confirm_bank_acct_nbr) {
            $result["is_valid"] = false;
            $result["message"] = "Account number and Confirm Account Number are not equal";
            return $result;
        }

        if (strlen($bank_acct_nbr) > 17 || !is_numeric($bank_acct_nbr)) {
            $result["is_valid"] = false;
            $result["message"] = "Please enter a valid bank account number";
        } else {
            $result["is_valid"] = true;
            $result["message"] = "";
        }
        return $result;
    }
}

// validate the bank number
add_filter("gform_field_validation_14_110", 'gf_validate_confirm_bank_acct_nbr', 11, 4);

function gf_validate_confirm_bank_acct_nbr($result, $value, $form, $field) {
    DEBUG ? error_log("functions.php flow: gf_validate_confirm_bank_acct_nbr\n") : null;
    if (!$result["is_valid"]) {
        return $result;
    } else {
        $bank_acct_nbr = trim(rgpost('input_64'));
        $confirm_bank_acct_nbr = trim(rgpost('input_110'));

        if ($bank_acct_nbr !== $confirm_bank_acct_nbr) {
            $result["is_valid"] = false;
            $result["message"] = "Account number and Confirm Account Number are not equal";
            return $result;
        }

        return $result;
    }
}

// validate the bank acct years
add_filter("gform_field_validation_115", 'gf_validate_bank_years', 11, 4);

function gf_validate_bank_years($result, $value, $form, $field) {

    if (!$result["is_valid"]) {
        return $result;
    } else {
        $bank_acct_age = trim(rgpost('input_115'));

        if (!is_int((int)$bank_acct_age)) {
            $result["is_valid"] = false;
            $result["message"] = "Please enter numbers only for bank age.";
            return $result;
        }

        if ($bank_acct_age < 0 || $bank_acct_age > 99) {
            $result["is_valid"] = false;
            $result["message"] = "Please enter a valid bank age";
        } else {
            $result["is_valid"] = true;
            $result["message"] = "";
        }
        return $result;
    }
}

// validate the bank acct months
add_filter("gform_field_validation_14_116", 'gf_validate_bank_months', 11, 4);

function gf_validate_bank_months($result, $value, $form, $field) {

    if (!$result["is_valid"]) {
        return $result;
    } else {
        $bank_acct_age = trim(rgpost('input_116'));

        if (!is_int((int)$bank_acct_age)) {
            $result["is_valid"] = false;
            $result["message"] = "Please enter numbers only for bank age.";
            return $result;
        }

        if ($bank_acct_age < 0 || $bank_acct_age > 11) {
            $result["is_valid"] = false;
            $result["message"] = "Please enter a valid value between 0 and 11";
        } else {
            $result["is_valid"] = true;
            $result["message"] = "";
        }
        return $result;
    }
}

// validate I Agree Text on Loan Signature form
add_filter("gform_field_validation_19_6", 'gf_validate_i_agree', 11, 4);

function gf_validate_i_agree($result, $value, $form, $field) {

    if (!$result["is_valid"]) {
        return $result;
    } else {
        $i_agree = trim(rgpost('input_6'));
        if (strtolower($i_agree) != 'i agree') {
            $result["is_valid"] = false;
            $result["message"] = "Please type I Agree in this box";
        } else {
            $result["is_valid"] = true;
            $result["message"] = "";
        }
        return $result;
    }
}

// validate ACH Authorization Is Provided if ACH deposit method is selected
add_filter("gform_field_validation_19_11", 'gf_validate_loan_signature', 11, 4);

function gf_validate_loan_signature($result, $value, $form, $field) {

    if (!$result["is_valid"]) {
        return $result;
    } else {
        $customer_signature_firstname = trim(rgpost('input_11_3'));
        $customer_signature_lastname = trim(rgpost('input_11_6'));
        $customer_firstname = trim($_SESSION['app_first_name']);
        $customer_lastname = trim($_SESSION['app_last_name']);

        $result["is_valid"] = true;
        $result["message"] = "";

        if (strtolower($customer_signature_firstname) != strtolower($customer_firstname)) {
            $result["is_valid"] = false;
            $result["message"] = "Please type $customer_firstname in the First box";
        } else if (strtolower($customer_signature_lastname) != strtolower($customer_lastname)) {
            $result["is_valid"] = false;
            $result["message"] = "Please type $customer_lastname in the Last box";
        }

        return $result;
    }
}

// *** DO NOT ERASE ** Super powerful for gravity forms debugging
// add_filter( 'gform_field_validation', 'custom_validation', 10, 4 );
// function custom_validation( $result, $value, $form, $field ) {
//     error_log_vardump("IN custom_validation _POST: ",$_POST);
//     error_log_vardump("IN custom_validation result: ",$result);
//     error_log_vardump("IN custom_validation value: ",$value);
//     error_log_vardump("IN custom_validation form: ",$form);
//     error_log_vardump("IN custom_validation field: ",$field);
//     $result['is_valid'] = true;
//     return $result;
// }

add_action('gform_after_submission_19', 'form_19_after_submission', 10, 2);
function form_19_after_submission($entry, $form) {
    DEBUG ? error_log("functions.php flow: form_19_after_submission\n") : null;
    global $current_user;
    get_currentuserinfo();
    $email_address = $current_user->user_email;
    mm_restore_session($email_address);

    $deposit_method = $_SESSION["app_requested_deposit_type"];
    $i_agree = 'I Agree';
    if ($deposit_method == 'ACH') {
        $ach_auth = $i_agree;
        $ach_auth_ack = $i_agree;
    } else {
        $ach_auth = null;
        $ach_auth_ack = null;
    }

    $customer_signature_firstname = trim($entry['11.3']);
    $customer_signature_lastname = trim($entry['11.6']);

    $customer_signature = $customer_signature_firstname." ".$customer_signature_lastname;

    $customer_ip = $entry["8"];
    $customer_signature_timestamp = date("Y-m-d H:i:s");
    $loan_agreement_nbr = $_SESSION["app_loan_agreement_nbr"];

    //Update the loan agreement with the signature values
    mm_update_database_value("mm_loan_agreement", 'ach_authorization', $ach_auth, 's', 'loan_agreement_nbr', $loan_agreement_nbr);
    mm_update_database_value("mm_loan_agreement", 'ach_authorization_ack', $ach_auth_ack, 's', 'loan_agreement_nbr', $loan_agreement_nbr);
    mm_update_database_value("mm_loan_agreement", 'signature', addslashes($customer_signature), 's', 'loan_agreement_nbr', $loan_agreement_nbr);
    mm_update_database_value("mm_loan_agreement", 'signature_i_agree', $i_agree, 's', 'loan_agreement_nbr', $loan_agreement_nbr);
    mm_update_database_value("mm_loan_agreement", 'signature_ip', $customer_ip, 's', 'loan_agreement_nbr', $loan_agreement_nbr);
    mm_update_database_value("mm_loan_agreement", 'signature_timestamp', $customer_signature_timestamp, 's', 'loan_agreement_nbr', $loan_agreement_nbr);

    //Add code to log info about the customer after submission
    //Add a capture of some lead detail
    $unique_id = isset($_SERVER["UNIQUE_ID"]) ? $_SERVER["UNIQUE_ID"] : 'NOT SET';
    $ip_address = isset($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : "NOT SET";
    $http_user_agent = isset($_SERVER["HTTP_USER_AGENT"]) ? $_SERVER["HTTP_USER_AGENT"] : "NOT SET";
    $page_desc = "APP_LOAN_SIGNATURE";
    mm_create_lead_detail($unique_id, $ip_address, $http_user_agent, $page_desc);
}

add_filter("gform_field_validation_14_54", 'gf_validate_username', 13, 4);

function gf_validate_username($result, $value, $form, $field) {
    $email = rgpost('input_54');
    DEBUG ? error_log("functions.php flow: gf_validate_username for: $email\n") : null;

    if (!$result["is_valid"]) {
        return $result;
    } else if (is_user_logged_in()) {
        DEBUG ? error_log("functions.php flow: gf_validate_username, user is logged in to Wordpress\n") : null;
        $current_user = wp_get_current_user();
        $wp_email = $current_user->user_email;
        if (strtolower($wp_email) != strtolower($email)) {
            DEBUG ? error_log("functions.php flow: gf_validate_username, emails are not equal\n") : null;
            $result["is_valid"] = false;
            $result["message"] = "Please use the email address tied to your account: $wp_email";
            //mm_create_field_validation('',$_SERVER["REMOTE_ADDR"], 14,54, $email, $result["message"]);
            return $result;
        } else {
            $result["is_valid"] = true;
            $result["message"] = '';
            return $result;
        }
    } else {
        DEBUG ? error_log("functions.php flow: gf_validate_username, the user is not logged into Wordpress\n") : null;
        //The user is not logged in so check and see if the email address exists in the database
        $results = mm_get_account_details_from_email_address($email);
        $acct_nbr = $results["account_nbr"];
        $_SESSION["ACCT NUMBER A"] = $acct_nbr;
        if ($acct_nbr != 0) {
            //The email address exists and the customer isn't logged in so throw an error
            DEBUG ? error_log("functions.php flow: gf_validate_username, returning false from method as email address exists, but not logged in.\n") : null;
            $result["is_valid"] = false;
            $result["message"] = "This email address is already in use.  Please log into your account above to continue.";
            return $result;
        } else {
            $result["is_valid"] = true;
            $result["message"] = '';
            return $result;
        }
    }
}

/*************************************************************************
 *
 *
 **************************************************************************/
// validate the username and password - create an account if necessary
add_filter("gform_field_validation_14_55", 'gf_validate_login', 12, 4);

function gf_validate_login($result, $value, $form, $field) {
     //$login_user = 1;
    if (!$result["is_valid"]) {
        return $result;
    }

    //Get Data From POST For Username and Password
    $username = rgpost('input_54');
    $email = rgpost('input_54');
    $password = rgpost('input_55');

    DEBUG ? error_log("functions.php flow: gf_validate_login for: $email\n") : null;

    if (is_user_logged_in()) {
        DEBUG ? error_log("functions.php flow: gf_validate_login, user is logged in to Wordpress\n") : null;
        $user = get_user_by('login', $username);
        if ($user && wp_check_password($password, $user->data->user_pass, $user->ID)) {
            $result["is_valid"] = true;
            $result["message"] = "";
            DEBUG ? error_log("functions.php flow: gf_validate_login, returning true for password match\n") : null;
            return $result;
        } else {
            $result["is_valid"] = false;
            $result["message"] = "Please enter the correct password to continue.";
            DEBUG ? error_log("functions.php flow: gf_validate_login, returning false for password match\n") : null;
            return $result;
        }
    }

    $creds = array(
        'user_login' => $username,
        'user_password' => $password,
    );

    //If the user doesn't already exist then create a new user
    if (!username_exists($username)) {
        DEBUG ? error_log("functions.php flow: gf_validate_login, creating a new user in Wordpress\n") : null;
        // let's create a new user
        $user_id = wp_create_user($username, $password, $email);
    }
    $_SESSION["username"] = $creds["user_login"];
    $_SESSION["password"] = $creds["user_password"];
    //Signin the User
    $user = wp_authenticate($username, $password);
    $user = wp_signon($creds, false);

    if (is_wp_error($user)) {

        $result["is_valid"] = false;
        $result["message"] = "Your username or password doesn't match what we have on file.";
        //mm_create_field_validation('',$_SERVER["REMOTE_ADDR"], 14,55, $email, $result["message"]);
        DEBUG ? error_log("functions.php flow: gf_validate_login, returning false due to Wordpress error\n") : null;
        return $result;
    } else if (!$result['is_valid']) {
        return $result;
    } else {
        $result["is_valid"] = true;
        $result["message"] = '';
        DEBUG ? error_log("functions.php flow: gf_validate_login, returning true for the method.\n") : null;
        return $result;
    }

}

//Add code to compare SSN to the Email address on file 78
add_filter("gform_field_validation_14_78", 'validate_application_ssn', 12, 4);

function validate_application_ssn($result, $value, $form, $field) {
    DEBUG ? error_log("functions.php flow: validate_application_ssn\n") : null;
    if (!$result["is_valid"]) {
        return $result;
    } else {
        $raw_ssn = trim(rgpost('input_78'));
        $raw_confirm_ssn = trim(rgpost('input_114'));

        if ($raw_ssn !== $raw_confirm_ssn) {
            $result["is_valid"] = false;
            $result["message"] = "SSN and Confirm SSN are not equal";
            return $result;
        }

        //Get the ssn and email fields from the form input
        $email_address = rgpost('input_54');

        $ssn = substr($raw_ssn, 0, 3) . substr($raw_ssn, 4, 2) . substr($raw_ssn, -4);

        $email_check_array = mm_account_exists('email_address', $email_address);
        $email_acct_nbr = $email_check_array["account_nbr"];
        $email_exists = $email_check_array["return_value"];

        $_SESSION["Checking Email"] = $email_address;
        $_SESSION["Email_exists"] = $email_exists;
        $_SESSION["Email_acct_nbr"] = $email_acct_nbr;

        $ssn_check_array = mm_account_exists('ssn', $ssn);
        $ssn_acct_nbr = $ssn_check_array["account_nbr"];
        $ssn_exists = $ssn_check_array["return_value"];
        $_SESSION["Checking SSN"] = $ssn;
        $_SESSION["SSN Exists"] = $ssn_exists;
        $_SESSION["ssn_acct_nbr"] = $ssn_acct_nbr;

        if ($ssn_exists == 1 && $email_exists == 1) {
            //Both the SSN and Email are tied to an existing account
            if ($ssn_acct_nbr != $email_acct_nbr) {
                //The SSN and Email address both exist but are tied to different applications
                $result["is_valid"] = false;
                $result["message"] = "Incorrect SSN for this account.";
                //mm_create_field_validation('',$_SERVER["REMOTE_ADDR"], 14,78, $email_address, $result["message"]);
                return $result;
            } else {
                //Both the SSN and email exist and they are tied to the same record
                $result["is_valid"] = true;
                $result["message"] = '';
                return $result;

            }

        } else if ($ssn_exists + $email_exists == 1) {
            //This is an error condition because either the SSN or username exists but doesn't match the other item

            $result["is_valid"] = false;
            $result["message"] = "Incorrect SSN for this account.";
            //mm_create_field_validation('',$_SERVER["REMOTE_ADDR"], 14,78, $email_address, $result["message"]);
            return $result;
        } else {
            //Neither the email address or SSN are in MM Account so continue;
            $result["is_valid"] = true;
            $result["message"] = '';
            return $result;

        }

    }

}

// CREATE ACCOUNT

// create user after submit
//add_action( "gform_after_submission_14", "mm_after_submission_create_account", 10,2 );

function mm_after_submission_create_account($entry, $form) {
    DEBUG ? error_log("functions.php flow: mm_after_submission_create_account\n") : null;
    // get the user created arguments and store them in an array
    // each of the $_POST elements should be read from the form visual editor
    $username = esc_attr(rgpost('input_54'));
    $email = sanitize_email(rgpost('input_54'));
    $password = rgpost('input_55');

    $creds = array(
        'user_login' => $username,
        'user_password' => $password,
    );

    // if we're doing a cron job let's forget about it
    if (defined('DOING_CRON') || isset($_GET['doing_wp_cron'])) {
        return;
    };

    // check if a username exists
    $user_id = username_exists($username);

    // let's validate the email and the user
    if (!$user_id and email_exists($email) == false) {

        // let's create a new user
        $user_id = wp_create_user($username, $password, $email);

        // let's sign in the new user
        $user = wp_signon($creds, false);

        if (is_wp_error($user)) {
            echo $user->get_error_message();
            return;
        }

    } else {
        // validation failed
        echo('user exists');
        return;
    }
}

/*-------------------------------------------------------------------------------------*/
/* Login Hooks and Filters - custom redirect for failed login
/*-------------------------------------------------------------------------------------*/
if (!function_exists('custom_login_fail')) {
    function custom_login_fail($username) {
        $referrer = $_SERVER['HTTP_REFERER']; // where did the post submission come from?
        // if there's a valid referrer, and it's not the default log-in screen
        if (!empty($referrer) && !strstr($referrer, 'wp-login') && !strstr($referrer, 'wp-admin')) {
            if (!strstr($referrer, '?login=failed') && !strstr($referrer, '?login=empty')) { // make sure we don’t append twice
                wp_redirect($referrer . '?login=failed'); // append some information (login=failed) to the URL for the theme to use
            } else {
                wp_redirect($referrer);
            }
            exit;
        }
    }
}
add_action('wp_login_failed', 'custom_login_fail'); // hook failed login

if (!function_exists('custom_login_empty')) {
    function custom_login_empty($username) {
        $user = get_user_by('login', $username);
        $referrer = $_SERVER['HTTP_REFERER'];
        if (strstr($referrer, 'my-account') && $user == null) { // mylogin is the name of the loginpage.
            if (!strstr($referrer, '?login=empty') && !strstr($referrer, '?login=failed')) { // prevent appending twice
                wp_redirect($referrer . '?login=empty');
            } else {
                wp_redirect($referrer);
            }
            exit;
        }
    }
}
add_action('wp_authenticate', 'custom_login_empty');

add_filter('wp_nav_menu_objects', 'restructure_menu_links', 10, 2);

function restructure_menu_links($items, $args) {
    if (is_user_logged_in() && $args->theme_location == 'primary-menu') {

        $new_links = array();

        $link = home_url('log-out');

        $label = 'Log Out'; // add your custom menu item content here

        // Create a nav_menu_item object
        $item = array(
            'title' => $label,
            'menu_item_parent' => 0,
            'ID' => 'logoutlink',
            'db_id' => '',
            'url' => $link,
            'classes' => array('menu-item'),
        );

        $new_links[] = (object)$item; // Add the new menu item to our array

        // insert item
        $location = 3; // insert at 3rd place
        array_splice($items, $location, 0, $new_links);

    } elseif (!is_user_logged_in() && $args->theme_location == 'primary-menu') {

        $new_links = array();

        $link = BASE_DIR . '/member-login';

        $label = 'Log In'; // add your custom menu item content here

        // Create a nav_menu_item object
        $item = array(
            'title' => $label,
            'menu_item_parent' => 0,
            'ID' => 'loginlink',
            'db_id' => '',
            'url' => $link,
            'classes' => array('menu-item'),
        );

        $new_links[] = (object)$item; // Add the new menu item to our array

        // insert item
        $location = 3; // insert at 3rd place
        array_splice($items, $location, 0, $new_links);
    }

    return $items;
}

// Hooking up our functions to WordPress filters
add_filter('wp_mail_from', 'mm_sender_email');

function mm_sender_email($original_email_address) {
    return 'support@aspenfinancialdirect.com';
}

add_filter('wp_mail_from_name', 'mm_sender_name');

function mm_sender_name($original_email_from) {
    return 'Aspen Financial Direct';
}


//
// OVERRIDE PASSWORD HINT TEXT
//
add_filter('password_hint', function ($hint) {
    return __('For greater security, please use upper and lower case letters, numbers, and symbols like ! " ? $ % ^ &amp; ).');
});

/* register a new menu */
function register_my_menu() {
    register_nav_menu('mmd-mini-menu', __('MMD Mini Menu'));
}

add_action('init', 'register_my_menu');

add_filter('wp_nav_menu_objects', 'restructure_menu_links2', 10, 2);

/* add login / logout links to new menu */
function restructure_menu_links2($items, $args) {
    if (is_user_logged_in() && $args->theme_location == 'mmd-mini-menu') {

        $new_links = array();

        $link = home_url('log-out');

        $label = 'Log Out'; // add your custom menu item content here

        // Create a nav_menu_item object
        $item = array(
            'title' => $label,
            'menu_item_parent' => 0,
            'ID' => 'logoutlink',
            'db_id' => '',
            'url' => $link,
            'classes' => array('menu-item'),
        );

        $new_links[] = (object)$item; // Add the new menu item to our array

        // insert item
        $location = 0; // insert at 0rd place from right
        array_splice($items, $location, 0, $new_links);

    } elseif (!is_user_logged_in() && $args->theme_location == 'mmd-mini-menu') {

        $new_links = array();

        $link = BASE_DIR . '/member-login';

        $label = 'Log In'; // add your custom menu item content here

        // Create a nav_menu_item object
        $item = array(
            'title' => $label,
            'menu_item_parent' => 0,
            'ID' => 'loginlink',
            'db_id' => '',
            'url' => $link,
            'classes' => array('menu-item'),
        );

        $new_links[] = (object)$item; // Add the new menu item to our array

        // insert item
        $location = 0; // insert at 0 place from right
        array_splice($items, $location, 0, $new_links);
    }

    return $items;
}


// Added to validate the "Date of birth" date field on Step 4 of 5 - Bank Account & Social Security Number
// Ticket AFDS-90; Sprint 6; 2018-07-31; Tim Poovey
add_filter( 'gform_field_validation_14_70', 'birthday_date_validation', 10, 4 );
/**
 * This function can be called with month "1", day "23", year "190"; this will be false === $result['is_valid'].
 * This happens because the minimum year allowed is 1920.  Likely in GF_Field_Date::get_field_input() in
 * wp-content/plugins/gravityforms/includes/fields/class-gf-field-date.php.
 *
 * @param $result
 * @param $value
 * @param $form
 * @param $field
 *
 * @return mixed
 */
function birthday_date_validation( $result, $value, $form, $field )
{
    $passedCheckdateValidation = false;
    $today            = date('Ymd');

    if ((! $result['is_valid']) && ($field->get_input_type() == 'date')) {
        // Dates considered invalid at this point include "1/23/190"; but, not "1/23/1920"
        $result['is_valid'] = false;
        $result['message']  = '';

        $birthDate = GFCommon::parse_date( $value );

        if ( GFCommon::is_empty_array($birthDate) ) {
            $result['message'] = 'Please enter a valid Date of Birth.';
            return $result;
        }

        if ( ! checkdate( $birthDate['month'], $birthDate['day'], $birthDate['year'] ) ) {
            $fieldResults = isDateFieldValid ($birthDate['year'], $birthDate['month'], $birthDate['day']);

            if (strlen ($birthDate['month']) < 1) {
                $result['message'] .= 'Nothing was entered for month. ';
            }
            elseif (false === ctype_digit ($birthDate['month'])) {
                $result['message'] .= $birthDate['month'] . ' was entered for month; non-numeric character present. ';
            }
            elseif ((1 > $birthDate['month']) || (12 < $birthDate['month'])) {
                $result['message'] .= $birthDate['month'] . ' was entered for month; month must be between 1 and 12. ';
            }
            elseif (false === $fieldResults['month']) {
                $result['message'] .= $birthDate['month'] . ' was entered for month. ';
            }

            if (strlen($birthDate['day']) < 1) {
                $result['message'] .= 'Nothing was entered for day. ';
            } elseif ( false === ctype_digit( $birthDate['day']) ) {
                $result['message'] .= $birthDate['day'] . ' was entered for day; non-numeric character present. ';
            } elseif ((1 > $birthDate['day']) || (31 < $birthDate['day'])) {
                $result['message'] .= $birthDate['day'] . ' was entered for day. ';
            } elseif (false === $fieldResults['day']) {
                $result['message'] .= $birthDate['day'] . ' was entered for day. ';
            }

            if (strlen($birthDate['year']) < 1) {
                $result['message'] .= 'Nothing was entered for year. ';
            } elseif ( false === ctype_digit( $birthDate['year']) ) {
                $result['message'] .= $birthDate['year'] . ' was entered for year; non-numeric character present. ';
            } elseif (1900 > $birthDate['year']) {
                $result['message'] .= 'Birth year must be 1900 or later. ';
            } elseif (substr($today,0,4) < $birthDate['year']) {
                $result['message'] .= 'Birth year must be ' . $birthDate['year'] . ' or earlier. ';
            } elseif (false === $fieldResults['year']) {
                $result['message'] .= $birthDate['year'] . ' was entered for day. ';
            }
            $result['message'] .= 'Please enter a valid Date of Birth.';
            return $result;
        } else {
            $passedCheckdateValidation = true;
        }
    }

    if ( $passedCheckdateValidation || ($result['is_valid'] && ($field->get_input_type() == 'date')) ) {
        // Dates considered valid at this point include "1/23/1920" and "8/9/2020"; but, not "1/23/190"
        $result['message']  = '';

        $birthDate = GFCommon::parse_date( $value );

        $birthDateString = sprintf( "%04d%02d%02d", $birthDate['year'], $birthDate['month'], $birthDate['day']);
        if ( $birthDateString > $today ) {
            $result['is_valid'] = false;
            $result['message'] = 'Please enter a Date of Birth that is not in the future.';
            return $result;
        }

        $stateFieldValue = rgpost('input_52' );
        if ( 'Alabama' == $stateFieldValue ) {
            // If today is 2018-01,           OK:   1999-08-01 > 1932-08-01 birthday
            // If today is 2018-08-01,      Fail:   1999-08-01 < 2001-08-01 birthday
            if (((date('Y') - 19) . date('md')) < $birthDateString) {
                $result['is_valid'] = false;
                $result['message'] = 'Date of Birth entered does not meet the minimum state requirement of 19 years of age.';
                return $result;
            }
        } else {
            if (((date('Y') - 18) . date('md')) < $birthDateString) {
                $result['is_valid'] = false;
                $result['message'] = 'Date of Birth entered does not meet the minimum state requirement of 18 years of age.';
                return $result;
            }
        }

        $yearInteger  = intval( $birthDate['year']);
        if (1900 > $yearInteger) {
            $result['is_valid'] = false;
            $result['message'] .= 'Birth year must be 1900 or later. ';
            return $result;
        }
        $todayYear = substr($today,0,4);
        if ( $todayYear < $yearInteger) {
            $result['is_valid'] = false;
            $result['message'] .= 'Birth year must be ' . $todayYear . ' or earlier. ';
            return $result;
        }
    }

    $result['is_valid'] = true;
    $result['message']  = '';

    return $result;
}


/**
 * @param $year
 * @param $month
 * @param $day
 *
 * @return mixed
 */
function isDateFieldValid( $year, $month, $day )
{
    $errorFound = false;
    $result['month'] = true;
    $result['day']   = true;
    $result['year']  = true;

    // check year
    if ( ( 1900 > $year ) || ( 2200 <= $year ) ) {
        $result['year'] = false;
        $errorFound     = true;
    }

    // check month
    if ( ( 1 > $month ) || ( 12 < $month ) ) {
        $result['month'] = false;
        $errorFound      = true;
    }

    // check day
    if ( ( 1 > $day ) || ( 31 < $day ) ) {
        $result['day'] = false;
        $errorFound    = true;
    }

    if ( true === $errorFound ) {
        return $result;
    }

    // check day for month specific limitations
    switch ($month) {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            if ((1 > $day) || (31 < $day)) {
                $result['day'] = false;
            }
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            if ((1 > $day) || (30 < $day)) {
                $result['day'] = false;
            }
            break;
        case 2:
            if ((1 > $day) || (29 < $day)) {
                $result['day'] = false;
            }
            elseif (29 == $day) {
                if ( ! ( ( $year%400 == 0 ) || ( ( $year%4 == 0 ) && ( $year%100 != 0 ) ) ) ) {
                    $result['day'] = false;
                }
            }
            break;
    }

    return $result;
}