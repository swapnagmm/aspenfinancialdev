<?php
/* file: pdf.php
 * area: FRONTEND
 */
require "fpdf181/fpdf.php";

/**
 * Class PDF
 */
class PDF extends FPDF
{
    /**
     * @var
     */
    public $widths;
    /**
     * @var
     */
    public $aligns;
    /**
     * @var int
     */
    private $ROW_SPACING = 5;

    /****************************************************************************************
     * Function mm_generate_loan_agreement_pdf($application_nbr, $signature_data=0)
     ***************************************************************************************
     * @param int $application_nbr
     * @param $app_data_arr
     * @param $la_data
     * @param $payment_schedule_arr
     * @param $lender_data
     * @param $doc_path
     * @param int $signature_data
     * @return mixed
     */
    public function mm_generate_loan_agreement_pdf(int $application_nbr, $app_data_arr, $la_data, $payment_schedule_arr, $lender_data, $doc_path, $signature_data = 0) {
       error_log("flow: mm_generate_loan_agreement_pdf: ($application_nbr,$signature_data) \n");
        //Get Data Needed to Complete the Document
        //$app_data_arr = mm_get_application_details($application_nbr);
        $app_data = $app_data_arr["app_data"];
        //$loan_agreement_nbr = $app_data["loan_agreement_nbr"];
        //$la_data = mm_get_loan_agreement_details($loan_agreement_nbr);
        //$payment_schedule_arr = mm_get_payment_schedule($loan_agreement_nbr);
        $payment_schedule = $payment_schedule_arr["payment_schedule"];
        //$lender_data = mm_get_lender_details($app_data["state"]);

        //Initialize All of the variables needed
        $loan_number = $la_data["loan_nbr"];
        $fa_uploaded = $la_data["fa_uploaded"];
        $account_nbr = $app_data["account_nbr"];
        $temp_dt = new DateTime($la_data["origination_dt"]);
        $orig_dt = $temp_dt->format('m/d/Y');
	//Added by Jason 7-15-18
        $orig_dt_yyyymmdd = $temp_dt->format('Ymd');
        $temp_dt = new DateTime($la_data["effective_dt"]);
        $contract_dt = $temp_dt->format('m/d/Y');  // Loan Pro Contract date
	//Added by Jason 7-15-18
        $contract_dt_yyyymmdd = $temp_dt->format('Ymd');  // Loan Pro Contract date
        //$bank_acct_nbr = $app_data["bank_acct_nbr"];
        //$bank_acct_length = strlen($bank_acct_nbr);

        // This APR is from LoanPro and is exact
        //$tila_apr = $this->truncate($la_data["apr"], 2) . "%";
        $tila_apr = sprintf("%.2f", floor($la_data["apr"] * 100.0) / 100.0) . '%';
        $contract_apr = sprintf("%.2f", floor($app_data["approved_apr"] * 100.0) / 100.0);
        //$total_finance_charge = "\$" . $la_data["total_amt_due"];
        $lender_name = $lender_data["legal_name"];
        //$lender_display_state = $lender_data["display_state"];

        $lender_mailing_street_address = $lender_data["mailing_street_address"];
        $lender_physical_street_address = $lender_data["physical_street_address"];

        $lender_mailing_city = $lender_data["mailing_city"];
        $lender_physical_city = $lender_data["physical_city"];

        $lender_mailing_state = $lender_data["mailing_state_cd"];
        $lender_physical_state = $lender_data["physical_state_cd"];

        $lender_mailing_zip_code = $lender_data["mailing_zip_code"];
        $lender_physical_zip_code = $lender_data["physical_zip_code"];

        $lender_phone_nbr = $lender_data["phone_nbr"];
        $lender_dba_name = $lender_data["dba_name"];
        $customer_name = $app_data["first_name"] . " " . $app_data["last_name"];
        $customer_street_address = $app_data["street_address"] . " " . $app_data["appt_suite"];
        $customer_zip_code = $app_data["zip_code"];
        $customer_state = $app_data["state"];
        $customer_city = $app_data["city"];
        $customer_phone_nbr = "(" . substr($app_data["mobile_phone_nbr"], 0, 3) . ") " . substr($app_data["mobile_phone_nbr"], 3, 3) . "-" . substr($app_data["mobile_phone_nbr"], 6);
        $amount_financed = "\$" . $la_data["principal_amt"];
        $total_finance_charge = "\$" . $la_data["total_finance_charge"];
        $total_of_all_payments = "\$" . $la_data["total_amt_due"];
        $payment_freq = $la_data['payment_freq'];

        //Build the PDF document
        $pdf = new PDF('P', 'mm', 'Letter');
        $pdf->SetMargins(4, 4, 4);
        $pdf->AddPage();

        // top 2 lines
        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(0, 10, 'THIS AGREEMENT SHALL NOT CONSTITUTE A "NEGOTIABLE INSTRUMENT"', 0, 0, 'C');
        $pdf->Ln(5);
        $pdf->setFont('Arial', '', 12);
        if ($customer_state == 'MS') {
            $pdf->Cell(0, 10, 'Credit Availability Act Loan Agreement and Federal Truth-in-Lending Disclosures', 0, 0, 'C');
        } else {
            $pdf->Cell(0, 10, 'Consumer Installment Loan Agreement and Federal Truth-in-Lending Disclosures', 0, 0, 'C');
        }
        $pdf->Ln(10);

        // third line
        $tmp_y = $pdf->GetY();
        $pdf->Cell(0, 10, "Loan Number: $loan_number", 0, 0, 'C');
        $pdf->Ln(10);

        // two columns:  get the y value for the next column and set the left margin
        $tmp_y = $pdf->GetY();
        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(0, 10, 'BORROWER:', 0, 0, 'L');
        $pdf->Ln(6);
        $pdf->setFont('Arial', '', 12);
        $pdf->Cell(0, 10, "$customer_name", 0, 0, 'L');
        $pdf->Ln(5);
        $pdf->Cell(0, 10, "$customer_street_address", 0, 0, 'L');
        $pdf->Ln(5);
        $pdf->Cell(0, 10, "$customer_city, $customer_state $customer_zip_code", 0, 0, 'L');
        $pdf->Ln(5);
        $pdf->Cell(0, 10, "$customer_phone_nbr", 0, 0, 'L');

        $pdf->SetLeftMargin(100);
        $pdf->SetY($tmp_y);
        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(0, 10, 'LENDER NAME & LOCATION:', 0, 0, 'L');
        $pdf->Ln(6);
        $pdf->setFont('Arial', '', 12);

        $pdf->Cell(0, 10, "$lender_name" . " d/b/a $lender_dba_name", 0, 0, 'L');
        $pdf->Ln(5);
        $pdf->Cell(0, 10, "$lender_physical_street_address", 0, 0, 'L');
        $pdf->Ln(5);
        $pdf->Cell(0, 10, "$lender_physical_city, $lender_physical_state $lender_physical_zip_code", 0, 0, 'L');
        $pdf->Ln(5);
        $pdf->Cell(0, 10, "$lender_phone_nbr", 0, 0, 'L');
        if ($customer_state == 'CA') {
            // Lender License Number
            $pdf->Ln(5);
            $pdf->Cell(0, 10, '60DBO 74712', 0, 0, 'L');
        }
        $pdf->Ln(12);

        // dates
        $y_temp = $pdf->GetY();
        $pdf->Setx(0);
        $pdf->SetLeftMargin(4);
        $pdf->Cell(0, 4, "Origination Date: $orig_dt", 0, 2, 'L');
        $pdf->SetY($y_temp, false);
        $pdf->SetLeftMargin(100);
        $pdf->Cell(0, 4, "Disbursement Date: $contract_dt", 0, 2, 'L');
        $pdf->Setx(0);
        $pdf->SetLeftMargin(4);
        $pdf->Cell(0, 4, "Effective Date: $contract_dt", 0, 2, 'L');
        $pdf->Ln(2);

        // Terms and Conditions paragraph
        $text = $this->Generate_Terms_And_Conditions_Block($customer_state, $lender_name, $lender_dba_name);
        if ($customer_state == 'CA') {
            $pdf->setFont('Arial', '', 10);
        }
        $pdf->MultiCell(0, 4, $text, 0, 'L');
        if ($customer_state != 'CA') {
            $pdf->Ln(4);
        }
        $pdf->setFont('Arial', 'BU', 12);

        // Federal Truth-In-Lending Disclosures paragraph
        if ($customer_state == 'CA') {
            $pdf->MultiCell(0, 5, 'FOR INFORMATION CONTACT THE DEPARTMENT OF BUSINESS OVERSIGHT, STATE OF CALIFORNIA.  FEDERAL TRUTH-IN-LENDING DISCLOSURES', 0, 'C');
        } else {
            $pdf->Cell(0, 10, 'FEDERAL TRUTH-IN-LENDING DISCLOSURES', 0, 0, 'C');
            $pdf->Ln(15);
        }

        $page_height = $pdf->getPageHeight();
        $tila_x = $pdf->GetX();
        $tila_y = $pdf->GetY();

        // large/bold line width
        $pdf->SetLineWidth(1);
        $pdf->Rect($tila_x, $tila_y, 208, $page_height - $tila_y - 4);
        $tila_box_y = $pdf->GetY();
        $pdf->SetFillColor(240);
        $pdf->Rect(4, $tila_box_y, 50, 46, 'DF');
        $pdf->Rect(54, $tila_box_y, 55, 46, 'DF');
        $tila_box_x = $pdf->GetX() + 30;
        $pdf->SetX($tila_box_x);
        $pdf->setFont('Arial', 'B', 14);
        $y_temp = $pdf->GetY() + 4;
        $pdf->SetY($y_temp);
        $pdf->MultiCell(45, 4, 'ANNUAL PERCENTAGE RATE', 0, 'C');
        $pdf->SetY($y_temp);
        $pdf->SetX(60); // orig: 55
        $pdf->MultiCell(45, 4, 'FINANCE CHARGE', 0, 'C');
        $pdf->SetY($y_temp);
        $pdf->SetX(110); // orig 100
        $pdf->setFont('Arial', 'B', 12);
        $pdf->MultiCell(45, 4, 'Amount Financed', 0, 'C');
        $pdf->SetY($y_temp);
        $pdf->SetX(160); // orig 150
        $pdf->MultiCell(45, 4, 'Total of Payments', 0, 'C');

        $y_temp += 4;
        $pdf->SetY($y_temp);
        $pdf->SetX(150);
        $pdf->setFont('Arial', '', 12);
        $pdf->Ln(10);
        $y_temp = $pdf->GetY();
        $pdf->MultiCell(45, 4, 'The cost of your credit as a yearly rate.', 0, 'C');
        $pdf->SetY($y_temp);
        $pdf->SetX(60);
        $pdf->MultiCell(45, 4, 'The dollar amount the credit will cost you.', 0, 'C');
        $pdf->SetY($y_temp);
        $pdf->SetX(110);
        $pdf->setFont('Arial', '', 12);
        $pdf->MultiCell(45, 4, 'The amount of credit provided to you or on your behalf.', 0, 'C');
        $pdf->SetY($y_temp);
        $pdf->SetX(160);
        $pdf->MultiCell(45, 4, 'The amount you will have paid after making all payments as scheduled.', 0, 'C');

        $pdf->Ln(2);
        $y_temp = $pdf->GetY();
        $pdf->setFont('Arial', 'B', 12);
        $pdf->MultiCell(45, 4, "$tila_apr", 0, 'C');

        $pdf->SetXY(60, $y_temp);
        $pdf->MultiCell(45, 4, "$total_finance_charge", 0, 'C');

        $pdf->SetXY(110, $y_temp);
        $pdf->setFont('Arial', '', 12);
        $pdf->MultiCell(45, 4, "$amount_financed", 0, 'C');
        $pdf->SetXY(160, $y_temp);
        $pdf->MultiCell(45, 4, "$total_of_all_payments", 0, 'C');
        $pdf->Ln(10);

        $tila_box_y2 = $pdf->getY();
        $pdf->SetY($tila_box_y);
        $pdf->SetLineWidth(.2);
        $pdf->SetFillColor(0, 0, 0);
        $pdf->Rect(109, $tila_box_y, 48, $tila_box_y2 - $tila_box_y - 4, 'D');
        $pdf->Rect(157, $tila_box_y, 55, $tila_box_y2 - $tila_box_y - 4, 'D');

        //End Tila Box Creation
        $pdf->SetY($tila_box_y2 - 2);
        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(40, 4, 'Payment Schedule:', 0, 0, 'L');
        $pdf->setFont('Arial', '', 12);
        $pdf->Cell(50, 4, 'Your Payment Schedule will be:', 0, 0, 'L');
        $pdf->Ln(6);

        $pdf->SetX(6);
        // print table headers
        $pdf->setFont('Arial', 'B', 10);
        $pdf->Cell(40, 4, 'Number of Payments', 1, 0, 'C');
        $pdf->Cell(50, 4, 'Amount of Payments', 1, 0, 'C');
        $pdf->Cell(114, 4, 'When Payments are Due', 1, 0, 'C');
        $pdf->Ln();

        $grouped_by_payment_amt = array_group_by($payment_schedule, "payment_amt");
        $num_payments_arr = array();
        $payment_amounts = array();
        $dates_arr = array();
        $dates_arr_string = array();
        $dates_string = '';

        foreach ($grouped_by_payment_amt as $key => $item) {
            array_push($num_payments_arr, $key);
            if (is_array($item)) {
                $tmp_arr = array();
                foreach ($item as $key1 => $value1) {
                    $tmp1 = $value1['payment_dt'];
                    $tmp2 = $value1['payment_amt'];
                    array_push($tmp_arr, $tmp1);
                    $dates_string .= $tmp1 . ', ';
                }
                $dates_string = rtrim(trim($dates_string), ',');
                array_push($dates_arr_string, $dates_string);
                $dates_string = '';
                array_push($dates_arr, $tmp_arr);
                array_push($payment_amounts, $tmp2);
            }
        }

        $left_margin_NumPayments = 6;
        $pdf->SetX($left_margin_NumPayments);

        // first line
        $arr = $dates_arr[0];
        // $pdf->Cell(40, 50, count($arr), 0, 0, 'C');
        // $pdf->Cell(50, 50, "$" . $payment_amounts[0], 0, 0, 'C');
        $pdf->Cell(40, 30, count($arr), 0, 0, 'C');
        $pdf->Cell(50, 30, "$" . $payment_amounts[0], 0, 0, 'C');

        //$pdf->MultiCell(114, 5, $dates_arr_string[0]);
        switch ($app_data['pay_frequency_1']) {
            case 'Paid Weekly':
            case 'Bi-Weekly':
                $tmp_dt = date('m/d/Y', strtotime($payment_schedule[0]['payment_dt']));
                $str = 'Every 14 days beginning ' . $tmp_dt;
                break;
            case 'Paid Monthly':
                $day_pd = $app_data['day_paid_month_1'];
                $now = strtotime('now');
                $now_month = date('m', $now);
                $now_year = date('Y', $now);
                $day_w_suffix = date('jS', strtotime("$now_month/$day_pd/$now_year"));
                $str = 'On the ' . $day_w_suffix . ' of each month beginning ' . date('m/d/Y', strtotime($payment_schedule[0]['payment_dt']));
                break;
            case 'Paid Twice Monthly':
                $days_pd = $app_data['day_paid_twice_monthly_1'];
                if ($days_pd == '1st and 15th') {
                    $str = 'On 1st and 15th of each month beginning on ' . date('m/d/Y', strtotime($payment_schedule[0]['payment_dt']));
                } else if ($days_pd == '15th and 30th') {
                    $str = '15th and last day of each month beginning on ' . date('m/d/Y', strtotime($payment_schedule[0]['payment_dt']));
                }
                break;
        }
        $pdf->Cell(114, 30, $str, 0, 0, 'C');

        // rectangles for the middle rows
        // $pdf->rect(6, 181, 40, 55, "D");
        // $pdf->rect(46, 181, 50, 55, "D");
        // $pdf->rect(96, 181, 114, 55, "D");
        $pdf->rect(6, 181, 40, 30, "D");
        $pdf->rect(46, 181, 50, 30, "D");
        $pdf->rect(96, 181, 114, 30, "D");

        if (count($grouped_by_payment_amt) > 1) {
            $pdf->setXY(6, 222);
            $arr = $dates_arr[1];
            // $pdf->Cell(40, 5, count($arr), 0, 0, 'C');
            // $pdf->Cell(50, 5, "$" . $payment_amounts[1], 0, 0, 'C');
            // $pdf->Cell(114, 5, $dates_arr_string[1], 0, 0, 'C');
            $pdf->Cell(40, 5, count($arr), 0, 0, 'C');
            $pdf->Cell(50, 5, "$" . $payment_amounts[1], 0, 0, 'C');
            $pdf->Cell(114, 5, date('m/d/Y', strtotime($dates_arr_string[1])), 0, 0, 'C');
        }

        // rectangles for the bottom row
        // $pdf->rect(6, 236, 40, 4, "D");
        // $pdf->rect(46, 236, 50, 4, "D");
        // $pdf->rect(96, 236, 114, 4, "D");
        $pdf->rect(6, 211, 40, 29, "D");
        $pdf->rect(46, 211, 50, 29, "D");
        $pdf->rect(96, 211, 114, 29, "D");

        $pdf->SetAutoPageBreak(false);
        //$pdf->SetXY(4, 248);  Here is the critical part
        $pdf->SetXY(4, 241);
        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(20, 4, 'Security:', 0, 0, 'L');
        $pdf->setFont('Arial', '', 10);
        $pdf->MultiCell(0, 4, 'If selected as your repayment method, then you are giving a security interest in the Payment Authorization.', 0, 'L');

        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(26, 4, 'Prepayment:', 0, 0, 'L');
        $pdf->setFont('Arial', '', 11);
        $pdf->MultiCell(0, 4, 'If you prepay in full or in part, you will not have to pay a penalty.', 0, 'L');
        
        //Swapna modified and added the condition to exclude for Mississipi story no 435 - August 9th 2018
        if ($customer_state != 'MS') {
        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(26, 4, 'Late Charge:', 0, 0, 'L');
        $pdf->setFont('Arial', '', 11);

        $text_late_charge = $this->Generate_Late_Charge_Block($customer_state);
        $pdf->MultiCell(0, 4, $text_late_charge, 0, 'L');
        }
        
        $pdf->MultiCell(0, 4, 'See the terms of this Agreement for any additional information about nonpayment, default, any required repayment in full before the scheduled date, and prepayment refunds and penalties.', 0, 'L');


        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true);

        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(0, 10, 'Itemization of Amount Financed', 0, 0, 'L');
        $pdf->Ln(6);
        $pdf->setFont('Arial', '', 12);
        $pdf->Cell(120, 10, 'Amount given to you directly:', 0, 0, 'L');
        $pdf->Cell(0, 10, "$amount_financed", 0, 0, 'L');
        $pdf->Ln(6);
        $pdf->Cell(120, 10, '(Plus) Amount paid on your Account:', 0, 0, 'L');
        $pdf->Cell(0, 10, "\$0.00 (Plus)", 0, 0, 'L');
        $pdf->Ln(6);
        $pdf->Cell(120, 10, 'Amount paid to other on your behalf to ______________:', 0, 0, 'L');
        $pdf->Cell(0, 10, "\$0.00", 0, 0, 'L');
        $pdf->Ln(6);
        $pdf->Cell(120, 10, '(Equals) Principal amount of your loan:', 0, 0, 'L');
        $pdf->Cell(0, 10, "$amount_financed", 0, 0, 'L');
        $pdf->Ln(6);
        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(120, 10, '(Equals) Amount Financed:', 0, 0, 'L');
        $pdf->Cell(0, 10, "$amount_financed", 0, 0, 'L');
        $pdf->Ln(6);
        $pdf->Cell(0, 10, '_______________________________________________');
        $pdf->Ln(10);
        $pdf->MultiCell(0, 4, 'HIGH COST CREDIT DISCLOSURE: AN ' . strtoupper($lender_dba_name) . ' LOAN IS AN EXPENSIVE FORM OF CREDIT. IT IS DESIGNED TO HELP CUSTOMERS MEET THEIR SHORT-TERM BORROWING NEEDS. THIS SERVICE IS NOT INTENDED TO PROVIDE A SOLUTION FOR LONGER-TERM CREDIT OR OTHER FINANCIAL NEEDS. ALTERNATIVE FORMS OF CREDIT MAY BE LESS EXPENSIVE AND MORE SUITABLE FOR YOUR FINANCIAL NEEDS.', 0, 'L');
        $pdf->Ln(10);

        //$pdf->setFont('Arial', 'B', 12);
        //$pdf->Write(4, "PROMISE TO PAY: ");
        //$pdf->Ln(4);
        //$pdf->setFont('Arial', '', 12);
        $pdf->write_bold(4, 'PROMISE TO PAY:  ');
//        if ($customer_state == 'MS') {
//            $pdf->MultiCell(0, 4, "PROMISE TO PAY: You promise to pay to the order of Lender the principal sum of $amount_financed plus a handling fee calculated at a rate of $contract_apr% per year (\"Contract Rate\"). You agree to make payments on the dates and in the amounts shown in the Payment Schedule above, or as may be later modified (\"Payment Due Dates\"). You also promise to pay the Lender all other charges provided for under this Agreement.", 0, 'L');
//        } else {
//            $pdf->MultiCell(0, 4, "PROMISE TO PAY: You promise to pay to the order of Lender the principal sum of $amount_financed plus interest at a rate of $contract_apr% per year (\"Contract Rate\"). You agree to make payments on the dates and in the amounts shown in the Payment Schedule above, or as may be later modified (\"Payment Due Dates\"). You also promise to pay the Lender all other charges provided for under this Agreement.", 0, 'L');
//        }
        if ($customer_state == 'MS') {
            $pdf->Write(4, "You promise to pay to the order of Lender the principal sum of $amount_financed plus a handling fee calculated at a rate of $contract_apr% per year (\"Contract Rate\"). You agree to make payments on the dates and in the amounts shown in the Payment Schedule above, or as may be later modified (\"Payment Due Dates\"). You also promise to pay the Lender all other charges provided for under this Agreement.");
        } else {
            $pdf->Write(4, "You promise to pay to the order of Lender the principal sum of $amount_financed plus interest at a rate of $contract_apr% per year (\"Contract Rate\"). You agree to make payments on the dates and in the amounts shown in the Payment Schedule above, or as may be later modified (\"Payment Due Dates\"). You also promise to pay the Lender all other charges provided for under this Agreement.");
        }
        //$pdf->Ln(4);
        $pdf->Ln(8);

        if ($customer_state == 'MS') {
            $pdf->write_bold(4, 'HANDLING FEE:  ');
            //$pdf->MultiCell(0, 4, "The handling fee will accrue daily on the unpaid principal balance of this Loan, beginning on the Effective Date. The handling fee will accrue until you pay the unpaid principal balance in full. However, after maturity or acceleration, any unpaid principal shall continue to accrue handling fees until repaid in full at the Contract Rate OR at the maximum rate allowed by law, whichever is less. The handling fee is not interest for the purposes of Mississippi law. If a Loan is refinanced with a new loan from us, you agree that we may continue to charge the handling fee on any refinanced amount of a Loan (shown as the \"Amount paid on your Account\" in the Itemization of Amount Financed above) until the effective date of the new loan. We calculate the handling fee based on a 365-day year (unless otherwise required by state law).  In calculating your payments, we have assumed you will make each payment on the day and in the amount due set forth in the Payment Schedule. If any payment is received after the Payment Due Date, you must pay any additional handling fee that accrues after the Payment Due Date.  If any payment is made before a Payment Due Date, the handling fee due on the scheduled payment will be reduced and you will owe a lesser handling fee. If any Payment Due Date falls on a non-banking business day, then you agree to pay on the next banking business day, and we will credit such payment as if it was received on the Payment Due Date.  The amount of any decrease or increase in the handling fee due will affect the amount of your final payment.  If the amount of any payment is not enough to pay the handling fee due, the unpaid handling fee will be paid from your next payment(s), if any, and will not be added to the principal balance. Time is of the essence. The handling fee will continue to accrue on past due amounts in accordance with this Agreement and as permitted by applicable state law. The Contract Rate and other charges under this Agreement will never exceed the highest rate or charge allowed by applicable state law for this Loan. If the amount collected is found to have exceeded the highest rate or charge allowed, Lender will refund an amount necessary to comply with the law.", 0, 'L');
            $pdf->Write(4, 'The handling fee will accrue daily on the unpaid principal balance of this Loan, beginning on the Effective Date. The handling fee will accrue until you pay the unpaid principal balance in full. However, after maturity or acceleration, any unpaid principal shall continue to accrue handling fees until repaid in full at the Contract Rate OR at the maximum rate allowed by law, whichever is less. The handling fee is not interest for the purposes of Mississippi law. If a Loan is refinanced with a new loan from us, you agree that we may continue to charge the handling fee on any refinanced amount of a Loan (shown as the "Amount paid on your Account" in the Itemization of Amount Financed above) until the effective date of the new loan. We calculate the handling fee based on a 365-day year (unless otherwise required by state law).  In calculating your payments, we have assumed you will make each payment on the day and in the amount due set forth in the Payment Schedule. If any payment is received after the Payment Due Date, you must pay any additional handling fee that accrues after the Payment Due Date.  If any payment is made before a Payment Due Date, the handling fee due on the scheduled payment will be reduced and you will owe a lesser handling fee. If any Payment Due Date falls on a non-banking business day, then you agree to pay on the next banking business day, and we will credit such payment as if it was received on the Payment Due Date.  The amount of any decrease or increase in the handling fee due will affect the amount of your final payment.  If the amount of any payment is not enough to pay the handling fee due, the unpaid handling fee will be paid from your next payment(s), if any, and will not be added to the principal balance. Time is of the essence. The handling fee will continue to accrue on past due amounts in accordance with this Agreement and as permitted by applicable state law. The Contract Rate and other charges under this Agreement will never exceed the highest rate or charge allowed by applicable state law for this Loan. If the amount collected is found to have exceeded the highest rate or charge allowed, Lender will refund an amount necessary to comply with the law.');
        } else {
            //$pdf->MultiCell(0, 4, "INTEREST: Interest will accrue daily on the unpaid principal balance of this Loan, beginning on the Effective Date. Interest will accrue until you pay the unpaid principal balance in full. However, after maturity or acceleration, any unpaid principal shall continue to accrue interest until repaid in full at the Contract Rate OR at the maximum rate allowed by law, whichever is less.  If a Loan is refinanced with a new loan from us, you agree that we may continue to charge interest on any refinanced amount of a Loan (shown as the \"Amount paid on your Account\" in the Itemization of Amount Financed above) until the effective date of the new loan.  We calculate interest based on a 365-day year (unless otherwise required by state law).  In calculating your payments, we have assumed you will make each payment on the day and in the amount due set forth in the Payment Schedule. If any payment is received after the Payment Due Date, you must pay any additional interest that accrues after the Payment Due Date.  If any payment is made before a Payment Due Date, the interest due on the scheduled payment will be reduced and you will owe less interest. If any Payment Due Date falls on a non-banking business day, then you agree to pay on the next banking business day, and we will credit such payment as if it was received on the Payment Due Date.  The amount of any decrease or increase in interest due will affect the amount of your final payment.  If the amount of any payment is not enough to pay the interest due, the unpaid interest will be paid from your next payment(s), if any, and will not be added to the principal balance. Time is of the essence. Interest will continue to accrue on past due amounts in accordance with this Agreement and as permitted by applicable state law. The interest rate and other charges under this Agreement will never exceed the highest rate or charge allowed by applicable state law for this Loan. If the amount collected is found to have exceeded the highest rate or charge allowed, Lender will refund an amount necessary to comply with law.", 0, 'L');
            $pdf->write_bold(4, 'INTEREST:  ');
            if($customer_state == 'NM')
            {
                $pdf->Write(4, 'Interest will accrue daily on the unpaid principal balance of this Loan, beginning on the Effective Date. Interest will accrue until you pay the unpaid principal balance in full. However, after maturity or acceleration, any unpaid principal shall continue to accrue interest at the Contract Rate until: (1) 12 months after the date of maturity, (2) 90 days after you are adjudicated bankrupt except as otherwise provided by law, or (3) 90 days after you die, whichever occurs first. Thereafter, interest will accrue at a rate of 10% per year.  If a Loan is refinanced with a new loan from us, you agree that we may continue to charge interest on any refinanced amount of a Loan (shown as the "Amount paid on your Account" in the Itemization of Amount Financed above) until the effective date of the new loan.  We calculate interest based on a 365-day year (unless otherwise required by state law).  In calculating your payments, we have assumed you will make each payment on the day and in the amount due set forth in the Payment Schedule. If any payment is received after the Payment Due Date, you must pay any additional interest that accrues after the Payment Due Date.  If any payment is made before a Payment Due Date, the interest due on the scheduled payment will be reduced and you will owe less interest. If any Payment Due Date falls on a non-banking business day, then you agree to pay on the next banking business day, and we will credit such payment as if it was received on the Payment Due Date.  The amount of any decrease or increase in interest due will affect the amount of your final payment.  If the amount of any payment is not enough to pay the interest due, the unpaid interest will be paid from your next payment(s), if any, and will not be added to the principal balance. Time is of the essence. Interest will continue to accrue on past due amounts in accordance with this Agreement and as permitted by applicable state law. The interest rate and other charges under this Agreement will never exceed the highest rate or charge allowed by applicable state law for this Loan. If the amount collected is found to have exceeded the highest rate or charge allowed, Lender will refund an amount necessary to comply with law.');
            }
            else
            {
                $pdf->Write(4, 'Interest will accrue daily on the unpaid principal balance of this Loan, beginning on the Effective Date. Interest will accrue until you pay the unpaid principal balance in full. However, after maturity or acceleration, any unpaid principal shall continue to accrue interest until repaid in full at the Contract Rate OR at the maximum rate allowed by law, whichever is less.  If a Loan is refinanced with a new loan from us, you agree that we may continue to charge interest on any refinanced amount of a Loan (shown as the "Amount paid on your Account" in the Itemization of Amount Financed above) until the effective date of the new loan.  We calculate interest based on a 365-day year (unless otherwise required by state law).  In calculating your payments, we have assumed you will make each payment on the day and in the amount due set forth in the Payment Schedule. If any payment is received after the Payment Due Date, you must pay any additional interest that accrues after the Payment Due Date.  If any payment is made before a Payment Due Date, the interest due on the scheduled payment will be reduced and you will owe less interest. If any Payment Due Date falls on a non-banking business day, then you agree to pay on the next banking business day, and we will credit such payment as if it was received on the Payment Due Date.  The amount of any decrease or increase in interest due will affect the amount of your final payment.  If the amount of any payment is not enough to pay the interest due, the unpaid interest will be paid from your next payment(s), if any, and will not be added to the principal balance. Time is of the essence. Interest will continue to accrue on past due amounts in accordance with this Agreement and as permitted by applicable state law. The interest rate and other charges under this Agreement will never exceed the highest rate or charge allowed by applicable state law for this Loan. If the amount collected is found to have exceeded the highest rate or charge allowed, Lender will refund an amount necessary to comply with law.');
            }
        }
        //$pdf->Ln(4);
        $pdf->Ln(8);

        if ($customer_state == 'NM') {
//            $pdf->MultiCell(0, 4, "COMPUTATION OF INTEREST CHARGES UNDER THE SMALL LOAN ACT:  The simple interest method shall be used for loans made under the New Mexico Small Loan Act of 1955.  Interest charges shall not be paid, deducted, or received in advance.  Interest charges shall not be compounded.  However, if part or all of the consideration for a loan contract is the unpaid principal balance of a prior loan, then the principal amount payable under the loan contract may include any unpaid charges that have accrued within sixty days on the prior loan.  Such charges shall be computed on the basis of the number of days actually elapsed.", 0, 'L');
//            $pdf->Ln(4);
            $pdf->write_bold(4, 'COMPUTATION OF INTEREST CHARGES UNDER THE SMALL LOAN ACT:  ');
            $pdf->Write(4, 'The simple interest method shall be used for loans made under the New Mexico Small Loan Act of 1955.  Interest charges shall not be paid, deducted, or received in advance.  Interest charges shall not be compounded.  However, if part or all of the consideration for a loan contract is the unpaid principal balance of a prior loan, then the principal amount payable under the loan contract may include any unpaid charges that have accrued within sixty days on the prior loan.  Such charges shall be computed on the basis of the number of days actually elapsed.');
            $pdf->Ln(8);
        }

        $pdf->write_bold(4, 'PAYMENTS:  ');
//        if ($customer_state == 'MS') {
//            $pdf->MultiCell(0, 4, "PAYMENTS: Lender will apply your payments in the following order: (1) any accrued but unpaid handling fee, (2) principal amounts outstanding, then (3) unpaid fees. If you have chosen the Payment Authorization option, each payment (including any accrued but unpaid handling fee and any fees, if applicable) will be debited from your Bank Account on each Payment Due Date and in accordance with the Payment Authorization. Any late payments may extend the term of your loan. See the Payment Authorization below for further information. If you choose to receive your Loan proceeds via check and to repay all amounts due pursuant to this Agreement via check, please mail each payment payable to $lender_dba_name ATTN: Payment Processing $lender_mailing_street_address, $lender_mailing_city, $lender_mailing_state $lender_mailing_zip_code, in time for Lender to receive the payment by 4:00PM Central Time on the applicable Payment Due Date.", 0, 'L');
//        } else {
//            $pdf->MultiCell(0, 4, "PAYMENTS: Lender will apply your payments in the following order: (1) accrued but unpaid interest, (2) principal amounts outstanding, then (3) unpaid fees. If you have chosen the Payment Authorization option, each payment (including any accrued but unpaid interest and any fees, if applicable) will be debited from your Bank Account on each Payment Due Date and in accordance with the Payment Authorization. Any late payments may extend the term of your loan. See the Payment Authorization below for further information. If you choose to receive your Loan proceeds via check and to repay all amounts due pursuant to this Agreement via check, please mail each payment payable to $lender_dba_name ATTN: Payment Processing $lender_mailing_street_address, $lender_mailing_city, $lender_mailing_state $lender_mailing_zip_code, in time for Lender to receive the payment by 4:00PM Central Time on the applicable Payment Due Date.", 0, 'L');
//        }
//        $pdf->Ln(4);
        if ($customer_state == 'MS') {
            $pdf->Write(4, "Lender will apply your payments in the following order: (1) any accrued but unpaid handling fee, (2) principal amounts outstanding, then (3) unpaid fees. If you have chosen the Payment Authorization option, each payment (including any accrued but unpaid handling fee and any fees, if applicable) will be debited from your Bank Account on each Payment Due Date and in accordance with the Payment Authorization. Any late payments may extend the term of your loan. See the Payment Authorization below for further information. If you choose to receive your Loan proceeds via check and to repay all amounts due pursuant to this Agreement via check, please mail each payment payable to $lender_dba_name ATTN: Payment Processing $lender_mailing_street_address, $lender_mailing_city, $lender_mailing_state $lender_mailing_zip_code, in time for Lender to receive the payment by 4:00PM Central Time on the applicable Payment Due Date.");
        } else if ($customer_state == 'WI') {
            $pdf->Write(4, "Lender will apply your payments in the following order: (1) accrued but unpaid interest, (2) principal amounts outstanding, then (3) unpaid fees. For purposes of assessing late charges set forth below, payments are applied first to current installments and then to delinquent installments. If you have chosen the Payment Authorization option, each payment (including any accrued but unpaid interest and any fees, if applicable) will be debited from your Bank Account on each Payment Due Date and in accordance with the Payment Authorization. Any late payments may extend the term of your loan. See the Payment Authorization below for further information. If you choose to receive your Loan proceeds via check and to repay all amounts due pursuant to this Agreement via check, please mail each payment payable to $lender_dba_name ATTN: Payment Processing $lender_mailing_street_address, $lender_mailing_city, $lender_mailing_state $lender_mailing_zip_code, in time for Lender to receive the payment by 4:00PM Central Time on the applicable Payment Due Date.");
        } else {
            $pdf->Write(4, "Lender will apply your payments in the following order: (1) accrued but unpaid interest, (2) principal amounts outstanding, then (3) unpaid fees. If you have chosen the Payment Authorization option, each payment (including any accrued but unpaid interest and any fees, if applicable) will be debited from your Bank Account on each Payment Due Date and in accordance with the Payment Authorization. Any late payments may extend the term of your loan. See the Payment Authorization below for further information. If you choose to receive your Loan proceeds via check and to repay all amounts due pursuant to this Agreement via check, please mail each payment payable to $lender_dba_name ATTN: Payment Processing $lender_mailing_street_address, $lender_mailing_city, $lender_mailing_state $lender_mailing_zip_code, in time for Lender to receive the payment by 4:00PM Central Time on the applicable Payment Due Date.");
        }
        $pdf->Ln(8);

        $pdf->write_bold(4, 'PREPAYMENT:  ');
        if ($customer_state == 'MS') {
            $pdf->Write(4, 'You may prepay in whole or in part at any time without penalty. If you prepay in part, you must still make each later payment according to the Payment Schedule until this Loan is paid in full.  Any principal amounts you prepay will not continue to accrue the handling fee.');
        } else {
            $pdf->Write(4, 'You may prepay in whole or in part at any time without penalty. If you prepay in part, you must still make each later payment according to the Payment Schedule until this Loan is paid in full.  Any principal amounts you prepay will not continue to accrue interest.');
        }
        //$pdf->Ln(4);
        $pdf->Ln(8);

        $pdf->write_bold(4, 'RIGHT OF RESCISSION:  ');
//        $pdf->MultiCell(0, 4, "RIGHT OF RESCISSION: You may rescind or cancel this Loan if you do so on or before 5:00 p.m., Central Time on the third (3rd) business day after the Contract Date (the \"Rescission Deadline\"). To cancel, call Lender at $lender_phone_nbr to tell us you want to rescind or cancel this Loan and provide us with written notice of rescission as directed by our customer service representative.  The Jury Trial Waiver and Arbitration Clause set forth below survives any rescission.", 0, 'L');
//        $pdf->Ln(4);
        $pdf->Write(4, "You may rescind or cancel this Loan if you do so on or before 5:00 p.m., Central Time on the third (3rd) business day after the Contract Date (the \"Rescission Deadline\"). To cancel, call Lender at $lender_phone_nbr to tell us you want to rescind or cancel this Loan and provide us with written notice of rescission as directed by our customer service representative.  The Jury Trial Waiver and Arbitration Clause set forth below survives any rescission.");
        $pdf->Ln(8);

        $pdf->SetLeftMargin(10);
//        if ($customer_state == 'MS') {
//            $pdf->MultiCell(0, 4, "(a)	If you have provided a Payment Authorization: If we timely receive your written notice of rescission on or before the Rescission Deadline but before the Loan proceeds have been credited to your Bank Account, we will not deposit your Loan proceeds to your Bank Account and both ours and your obligations under this agreement will be rescinded. If we timely receive your written notice of rescission on or before the Rescission Deadline but after the Loan proceeds have been credited to your Bank Account, we will debit your Bank Account for the principal amount owed under this Agreement, in accordance with the Payment Authorization. If we receive payment of the principal amount via the debit, ours and your obligations under this Agreement will be rescinded. If we do not receive payment of the principal amount via the debit, then the Agreement will remain in full force and effect until all amounts under this Agreement are repaid in full, including any handling fee.", 0, 'L');
//        } else {
//            $pdf->MultiCell(0, 4, "(a)	If you have provided a Payment Authorization: If we timely receive your written notice of rescission on or before the Rescission Deadline but before the Loan proceeds have been credited to your Bank Account, we will not deposit your Loan proceeds to your Bank Account and both ours and your obligations under this agreement will be rescinded. If we timely receive your written notice of rescission on or before the Rescission Deadline but after the Loan proceeds have been credited to your Bank Account, we will debit your Bank Account for the principal amount owed under this Agreement, in accordance with the Payment Authorization. If we receive payment of the principal amount via the debit, ours and your obligations under this Agreement will be rescinded. If we do not receive payment of the principal amount via the debit, then the Agreement will remain in full force and effect until all amounts under this Agreement are repaid in full, including any interest.", 0, 'L');
//        }
//        $pdf->Ln(4);
//        if ($customer_state == 'MS') {
//            $pdf->MultiCell(0, 4, "(b)	If you have elected to receive your Loan proceeds via check delivered by mail: If we timely receive your written notice of rescission on or before the Rescission Deadline, and (a) if we have not mailed the check representing the Loan proceeds to you or (b) if you have not cashed the check representing the Loan proceeds, then we will cancel the check and both ours and your obligations under this Agreement will be rescinded. If you have cashed the check representing the Loan proceeds, you must return the full amount of cash you received to us by the Rescission Deadline. If we do not receive the full amount by the Rescission Deadline, then the Agreement will remain in full force and effect until all amounts owed under this Agreement are repaid in full, including any handling fee.", 0, 'L');
//        } else {
//            $pdf->MultiCell(0, 4, "(b)	If you have elected to receive your Loan proceeds via check delivered by mail: If we timely receive your written notice of rescission on or before the Rescission Deadline, and (a) if we have not mailed the check representing the Loan proceeds to you or (b) if you have not cashed the check representing the Loan proceeds, then we will cancel the check and both ours and your obligations under this Agreement will be rescinded. If you have cashed the check representing the Loan proceeds, you must return the full amount of cash you received to us by the Rescission Deadline. If we do not receive the full amount by the Rescission Deadline, then the Agreement will remain in full force and effect until all amounts owed under this Agreement are repaid in full, including any interest.", 0, 'L');
//        }
//        $pdf->Ln(4);
        $pdf->Write(4, '(a)	 ');
        $pdf->write_bold(4, 'If you have provided a Payment Authorization:  ');
        if ($customer_state == 'MS') {
            $pdf->Write(4, 'If we timely receive your written notice of rescission on or before the Rescission Deadline but before the Loan proceeds have been credited to your Bank Account, we will not deposit your Loan proceeds to your Bank Account and both ours and your obligations under this agreement will be rescinded. If we timely receive your written notice of rescission on or before the Rescission Deadline but after the Loan proceeds have been credited to your Bank Account, we will debit your Bank Account for the principal amount owed under this Agreement, in accordance with the Payment Authorization. If we receive payment of the principal amount via the debit, ours and your obligations under this Agreement will be rescinded. If we do not receive payment of the principal amount via the debit, then the Agreement will remain in full force and effect until all amounts under this Agreement are repaid in full, including any handling fee.');
        } else {
            $pdf->Write(4, 'If we timely receive your written notice of rescission on or before the Rescission Deadline but before the Loan proceeds have been credited to your Bank Account, we will not deposit your Loan proceeds to your Bank Account and both ours and your obligations under this agreement will be rescinded. If we timely receive your written notice of rescission on or before the Rescission Deadline but after the Loan proceeds have been credited to your Bank Account, we will debit your Bank Account for the principal amount owed under this Agreement, in accordance with the Payment Authorization. If we receive payment of the principal amount via the debit, ours and your obligations under this Agreement will be rescinded. If we do not receive payment of the principal amount via the debit, then the Agreement will remain in full force and effect until all amounts under this Agreement are repaid in full, including any interest.');
        }
        $pdf->Ln(8);
        $pdf->Write(4, '(b)	 ');
        $pdf->write_bold(4, 'If you have elected to receive your Loan proceeds via check delivered by mail:  ');
        if ($customer_state == 'MS') {
            $pdf->Write(4, 'If we timely receive your written notice of rescission on or before the Rescission Deadline, and (a) if we have not mailed the check representing the Loan proceeds to you or (b) if you have not cashed the check representing the Loan proceeds, then we will cancel the check and both ours and your obligations under this Agreement will be rescinded. If you have cashed the check representing the Loan proceeds, you must return the full amount of cash you received to us by the Rescission Deadline. If we do not receive the full amount by the Rescission Deadline, then the Agreement will remain in full force and effect until all amounts owed under this Agreement are repaid in full, including any handling fee.');
        } else {
            $pdf->Write(4, 'If we timely receive your written notice of rescission on or before the Rescission Deadline, and (a) if we have not mailed the check representing the Loan proceeds to you or (b) if you have not cashed the check representing the Loan proceeds, then we will cancel the check and both ours and your obligations under this Agreement will be rescinded. If you have cashed the check representing the Loan proceeds, you must return the full amount of cash you received to us by the Rescission Deadline. If we do not receive the full amount by the Rescission Deadline, then the Agreement will remain in full force and effect until all amounts owed under this Agreement are repaid in full, including any interest.');
        }
        $pdf->Ln(8);

        $pdf->SetX(0);
        $pdf->SetLeftMargin(4);
//        if ($customer_state == 'AL') {
//            $pdf->MultiCell(0, 4, "LATE CHARGE: If any scheduled payment is not paid in full within 15 days after its due date, you will be charged a late fee of 5% of the payment or $15, whichever is greater, not to exceed $100. Only one late charge shall be charged on any one late installment, regardless of the period during which the installment remains unpaid.", 0, 'L');
//        } else if ($customer_state == 'CA') {
//            $pdf->MultiCell(0, 4, "LATE CHARGE: If any scheduled payment is not paid in full within 10 after its due date, you will be charged a late fee of $10.", 0, 'L');
//        } else if ($customer_state == 'MS') {
//            $pdf->MultiCell(0, 4, "LATE CHARGE: If a payment is more than 15 calendar days late, you will be charged the lesser of $5.00 or 5% of the installment due.", 0, 'L');
//        } else if ($customer_state == 'NM') {
//            $pdf->MultiCell(0, 4, "LATE CHARGE: If a payment is more than 10 days late, you will be charged 5% of the installment due, up to a maximum of $10.", 0, 'L');
//        } else if ($customer_state == 'UT') {
//            $pdf->MultiCell(0, 4, "LATE CHARGE: If a payment is more than 10 days late, you will be charged a late fee of $30 .", 0, 'L');
//        } else {
//            $pdf->MultiCell(0, 4, "LATE CHARGE: If a payment is more than 15 days late, you will be charged 5% of the installment due.", 0, 'L');
//        }
//        $pdf->Ln(4);

        //Swapna modified and added the condition to exclude for Mississipi story no 435 - August 9th 2018
        if ($customer_state != 'MS') {
            $pdf->write_bold(4, 'LATE CHARGE:  ');
            $pdf->Write(4, $text_late_charge);
            $pdf->Ln(8);
        }

//        $pdf->MultiCell(0, 4, "ELECTRONIC CHECK RE-PRESENTMENT POLICY: In the event a check is returned unpaid for insufficient or uncollected funds, we may re-present the check electronically.  In the ordinary course of business, the check will not be provided to you with your bank statement, but a copy can be retrieved by contacting your financial institution.", 0, 'L');
//        $pdf->Ln(4);
        $pdf->write_bold(4, 'ELECTRONIC CHECK RE-PRESENTMENT POLICY:  ');
        $pdf->Write(4, 'In the event a check is returned unpaid for insufficient or uncollected funds, we may re-present the check electronically.  In the ordinary course of business, the check will not be provided to you with your bank statement, but a copy can be retrieved by contacting your financial institution.');
        $pdf->Ln(8);

//        $pdf->MultiCell(0, 4, "CHECK CONVERSION NOTIFICATION: When you provide a check as payment, you agree we can either use the information from your check to make a one-time electronic withdrawal from your Bank Account or to process the payment as a check transaction. When we use information from your check to make a withdrawal from your Bank Account, funds may be withdrawn from your Bank Account as soon as the same day we receive your payment and you will not receive your check back from your financial institution.  For questions please call our customer service number, $lender_phone_nbr.", 0, 'L');
//        $pdf->Ln(4);
        $pdf->write_bold(4, 'CHECK CONVERSION NOTIFICATION:  ');
        $pdf->Write(4, "When you provide a check as payment, you agree we can either use the information from your check to make a one-time electronic withdrawal from your Bank Account or to process the payment as a check transaction. When we use information from your check to make a withdrawal from your Bank Account, funds may be withdrawn from your Bank Account as soon as the same day we receive your payment and you will not receive your check back from your financial institution.  For questions please call our customer service number, $lender_phone_nbr.");
        $pdf->Ln(8);

//        $pdf->MultiCell(0, 4, "BORROWER BANK CHARGES: Your financial institution may charge a fee if your Bank Account becomes overdrawn or if a payment is attempted from your Bank Account that would cause it to become overdrawn. You will not hold us or our agents, representatives, successors or assigns responsible for any such fee you must pay.", 0, 'L');
//        $pdf->Ln(4);
        $pdf->write_bold(4, 'BORROWER BANK CHARGES:  ');
        $pdf->Write(4, 'Your financial institution may charge a fee if your Bank Account becomes overdrawn or if a payment is attempted from your Bank Account that would cause it to become overdrawn. You will not hold us or our agents, representatives, successors or assigns responsible for any such fee you must pay.');
        $pdf->Ln(8);

//        $pdf->MultiCell(0, 4, "SECURITY: We have disclosed to you that our interest in the payment method indicated in the Federal Truth-in-Lending Disclosures above is a security interest for Truth-in-Lending purposes only because federal and applicable state law do not clearly address the issue. However, the federal Truth-in-Lending disclosures are not intended to create a security interest under applicable state law.", 0, 'L');
//        $pdf->Ln(4);
        $pdf->write_bold(4, 'SECURITY:  ');
        if ($customer_state == 'WI' || $customer_state == 'IL') {
            $pdf->Write(4, 'You grant a security interest in the Payment Authorization, if you provided one.');
        } else {
            $pdf->Write(4, 'We have disclosed to you that our interest in the payment method indicated in the Federal Truth-in-Lending Disclosures above is a security interest for Truth-in-Lending purposes only because federal and applicable state law do not clearly address the issue. However, the federal Truth-in-Lending disclosures are not intended to create a security interest under applicable state law.');
        }
        $pdf->Ln(8);

//        $pdf->MultiCell(0, 4, "REFINANCE POLICY: Subject to applicable state law and our credit policies, we will determine, in our sole discretion, whether your Loan may be refinanced.", 0, 'L');
//        $pdf->Ln(4);
        $pdf->write_bold(4, 'REFINANCE POLICY:  ');
        $pdf->Write(4, 'Subject to applicable state law and our credit policies, we will determine, in our sole discretion, whether your Loan may be refinanced.');
        $pdf->Ln(8);

//        if ($customer_state == 'UT') {
//            $pdf->MultiCell(0, 4, "CREDIT REPORTING: You agree that Lender may make inquiries concerning your credit history and standing. As required by Utah law, you are hereby notified that a negative credit report reflecting on your credit record may be submitted to a credit reporting agency if you fail to fulfill the terms of your credit obligations.", 0, 'L');
//            $pdf->Ln(4);
//        } else {
//            $pdf->MultiCell(0, 4, "CREDIT REPORTING: You agree that Lender may make inquiries concerning your credit history and standing, and may report information concerning your performance under this Agreement to credit reporting agencies. Late payments, missed payments or other defaults on your Loan may be reflected in your credit report.", 0, 'L');
//            $pdf->Ln(4);
//        }
        $pdf->write_bold(4, 'CREDIT REPORTING:  ');
        if ($customer_state == 'UT') {
            $pdf->Write(4, 'You agree that Lender may make inquiries concerning your credit history and standing. As required by Utah law, you are hereby notified that a negative credit report reflecting on your credit record may be submitted to a credit reporting agency if you fail to fulfill the terms of your credit obligations.');
        } else {
            $pdf->Write(4, 'You agree that Lender may make inquiries concerning your credit history and standing, and may report information concerning your performance under this Agreement to credit reporting agencies. Late payments, missed payments or other defaults on your Loan may be reflected in your credit report.');
        }
        $pdf->Ln(8);

//        $pdf->MultiCell(0, 4, "CHANGE OF PRIMARY RESIDENCE: You agree to notify Lender of any change in your primary residence as soon as possible, but no later than five (5) days after any change. You agree that the address provided on this Agreement will govern this Agreement until you have met all obligation under this Agreement and that any subsequent change in your address will not affect the terms or enforceability of this Agreement.", 0, 'L');
//        $pdf->Ln(4);
        $pdf->write_bold(4, 'CHANGE OF PRIMARY RESIDENCE:  ');
        $pdf->Write(4, 'You agree to notify Lender of any change in your primary residence as soon as possible, but no later than five (5) days after any change. You agree that the address provided on this Agreement will govern this Agreement until you have met all obligation under this Agreement and that any subsequent change in your address will not affect the terms or enforceability of this Agreement.');
        $pdf->Ln(8);

//        $pdf->MultiCell(0, 4, "CORRESPONDENCE WITH LENDER: General correspondence with Lender concerning this Loan, this Agreement or your relationship with Lender must be sent to Lender at the following address: $lender_name, $lender_mailing_street_address, $lender_mailing_city, $lender_mailing_state $lender_mailing_zip_code. Communications related to the bankruptcy of the Borrower must be sent to Lender at the following address: $lender_name ATTN: Bankruptcy Handling, $lender_mailing_street_address, $lender_mailing_city, $lender_mailing_state $lender_mailing_zip_code.", 0, 'L');
//        $pdf->Ln(4);
        $pdf->write_bold(4, 'CORRESPONDENCE WITH LENDER:  ');
        $pdf->Write(4, "General correspondence with Lender concerning this Loan, this Agreement or your relationship with Lender must be sent to Lender at the following address: $lender_name, $lender_mailing_street_address, $lender_mailing_city, $lender_mailing_state $lender_mailing_zip_code. Communications related to the bankruptcy of the Borrower must be sent to Lender at the following address: $lender_name ATTN: Bankruptcy Handling, $lender_mailing_street_address, $lender_mailing_city, $lender_mailing_state $lender_mailing_zip_code.");
        $pdf->Ln(8);

//        $pdf->MultiCell(0, 4, "FORCE MAJEURE: Unavoidable delays because of inadvertent processing errors and/or \"acts of God\" may extend the time for the deposit of Loan proceeds and the processing of your payments.", 0, 'L');
//        $pdf->Ln(4);
        $pdf->write_bold(4, 'FORCE MAJEURE:  ');
        $pdf->Write(4, 'Unavoidable delays because of inadvertent processing errors and/or "acts of God" may extend the time for the deposit of Loan proceeds and the processing of your payments.');
        $pdf->Ln(8);

//        if ($customer_state == 'MS') {
//            $pdf->MultiCell(0, 4, "TRANSFER OF RIGHTS/HYPOTHECATION: This instrument is non-negotiable in form but may be pledged as collateral security. If so pledged, any payment made to the Lender, either of principal or of any handling fee, upon the debt evidenced by this obligation, will be considered and construed as a payment on this instrument, the same as though it were still in the possession and under the control of the Lender named herein; and the pledgee holding this instrument as collateral security hereby makes said Lender its agent to accept and receive payments hereon, either of principal or of any handling fee.", 0, 'L');
//        } else {
//            $pdf->MultiCell(0, 4, "TRANSFER OF RIGHTS/HYPOTHECATION: This instrument is non-negotiable in form but may be pledged as collateral security. If so pledged, any payment made to the Lender, either of principal or of interest, upon the debt evidenced by this obligation, will be considered and construed as a payment on this instrument, the same as though it were still in the possession and under the control of the Lender named herein; and the pledgee holding this instrument as collateral security hereby makes said Lender its agent to accept and receive payments hereon, either of principal or of interest.", 0, 'L');
//        }
//        $pdf->Ln(4);
        $pdf->write_bold(4, 'TRANSFER OF RIGHTS/HYPOTHECATION:  ');
        if ($customer_state == 'MS') {
            $pdf->Write(4, 'This instrument is non-negotiable in form but may be pledged as collateral security. If so pledged, any payment made to the Lender, either of principal or of any handling fee, upon the debt evidenced by this obligation, will be considered and construed as a payment on this instrument, the same as though it were still in the possession and under the control of the Lender named herein; and the pledgee holding this instrument as collateral security hereby makes said Lender its agent to accept and receive payments hereon, either of principal or of any handling fee.');
        } else {
            $pdf->Write(4, 'This instrument is non-negotiable in form but may be pledged as collateral security. If so pledged, any payment made to the Lender, either of principal or of interest, upon the debt evidenced by this obligation, will be considered and construed as a payment on this instrument, the same as though it were still in the possession and under the control of the Lender named herein; and the pledgee holding this instrument as collateral security hereby makes said Lender its agent to accept and receive payments hereon, either of principal or of interest.');
        }
        $pdf->Ln(8);

//        $pdf->MultiCell(0, 4, "SUCCESSORS AND ASSIGNS: This Agreement is binding upon your heirs and personal representatives in probate upon anyone to whom you assign your assets or who succeeds you in any other way. You agree that Lender may assign or transfer this Agreement and any of lender's rights hereunder at any time without prior notice to you, except as required by applicable law. You may not assign this Agreement without the prior, written consent of Lender.", 0, 'L');
//        $pdf->Ln(4);
        $pdf->write_bold(4, 'SUCCESSORS AND ASSIGNS:  ');
        $pdf->Write(4, 'This Agreement is binding upon your heirs and personal representatives in probate upon anyone to whom you assign your assets or who succeeds you in any other way. You agree that Lender may assign or transfer this Agreement and any of lender\'s rights hereunder at any time without prior notice to you, except as required by applicable law. You may not assign this Agreement without the prior, written consent of Lender.');
        $pdf->Ln(8);

//        $pdf->MultiCell(0, 4, "SERVICING COMMUNICATIONS AND COMMUNICATIONS AFTER DEFAULT: You authorize Lender and its authorized representatives to contact you according to your consent provided in your application or according to your account preferences, as modified by you after submitting your application. This may include (i) calling you during reasonable hours at any of the phone numbers listed on your most recent application (ii) contacting you by text message or other wireless communication method on the mobile phone number listed on your application, (iii) leaving a message with a person or a voice mail service, and (iv) contacting you using autodialers or pre-recorded message, including calls to your mobile phone.", 0, 'L');
//        $pdf->Ln(4);
        $pdf->write_bold(4, 'SERVICING COMMUNICATIONS AND COMMUNICATIONS AFTER DEFAULT:  ');
        $pdf->Write(4, 'You authorize Lender and its authorized representatives to contact you according to your consent provided in your application or according to your account preferences, as modified by you after submitting your application. This may include (i) calling you during reasonable hours at any of the phone numbers listed on your most recent application (ii) contacting you by text message or other wireless communication method on the mobile phone number listed on your application, (iii) leaving a message with a person or a voice mail service, and (iv) contacting you using autodialers or pre-recorded message, including calls to your mobile phone.');
        $pdf->Ln(8);

        $pdf->setFont('Arial', 'B', 12);
        $pdf->MultiCell(0, 4, 'AUTOMATIC PAYMENT AUTHORIZATION:', 0, 'L');
        $pdf->Ln(4);
        $pdf->MultiCell(0, 4, '(Applies only if you select the electronic funding option or authorize recurring debit card payments)', 0, 'L');
        $pdf->Ln(4);
        $pdf->setFont('Arial', '', 12);

        // paragraph 1
        $pdf->MultiCell(0, 4, 'This Automatic Payment Authorization ("Payment Authorization") is a part of and related to this Agreement. You voluntarily authorize us, and our successors, affiliates, agents, representatives, employees, and assigns, to initiate automatic credit and debit entries to your Bank Account in accordance with this Agreement. For purposes of this Payment Authorization, "Bank Account" also includes any other bank account that you provide to Lender, from which you intend to permit recurring payments.  You agree that we will initiate a credit entry to your Bank Account for the amount that we are to give to you directly, as indicated in the Itemization of Amount Financed, on or about the Disbursement Date. You also authorize us to credit or debit your Bank Account to correct any errors related to this Loan.', 0, 'L');
        $pdf->Ln(4);

        // paragraph 2
        if ($customer_state == 'MS') {
            $pdf->MultiCell(0, 4, 'You agree that we will initiate a debit entry to debit your Bank Account on or after each Payment Due Date in the payment amount described in the Payment Schedule (including as modified) plus any accrued but unpaid handling fees, up to and including the full amount you owe. You acknowledge that you will receive a notice at least 10 days before a payment is debited from your Bank Account if the payment we are going to debit from your Bank Account falls outside this range. You have the right to receive notice of all varying amounts but have agreed to only receive notice if a debit will fall outside this range.', 0, 'L');
        } else if ($customer_state == 'WI') {
            $pdf->MultiCell(0, 4, 'You agree that we will initiate a debit entry to debit your Bank Account on or after each Payment Due Date in the payment amount described in the Payment Schedule (including as modified), plus any late fees and/or accrued but unpaid interest up to a maximum of $25 more than the payment amount described in the Payment Schedule. You acknowledge that you will receive a notice at least 10 days before a payment is debited from your Bank Account if the payment we are going to debit from your Bank Account falls outside this range. You have the right to receive notice of all varying amounts but have agreed to only receive notice if a debit will fall outside this range.', 0, 'L');
        } else {
            $pdf->MultiCell(0, 4, 'You agree that we will initiate a debit entry to debit your Bank Account on or after each Payment Due Date in the payment amount described in the Payment Schedule (including as modified) plus any late fees and/or accrued but unpaid interest, up to and including the full amount you owe. You acknowledge that you will receive a notice at least 10 days before a payment is debited from your Bank Account if the payment we are going to debit from your Bank Account falls outside this range. You have the right to receive notice of all varying amounts but have agreed to only receive notice if a debit will fall outside this range.', 0, 'L');
        }
        $pdf->Ln(4);

        // paragraph 3
        $pdf->MultiCell(0, 4, 'For each scheduled payment, whenever a debit entry to your Bank Account is returned to us for any reason, we may initiate a debit entry to your Bank Account up to two additional times after our first attempt for each scheduled payment amount, to the extent permitted by law.  If your payment is due on a non-business day, it will be processed on the next business day and credited as if made on the Payment Date.  If you still have an outstanding balance after the final Payment Date, then you authorize us to issue debits in the same (or lesser) amount as the final Payment Date debit on the same recurring intervals as set forth in the Payment Schedule (including as modified) until the balance is paid in full.', 0, 'L');
        $pdf->Ln(4);

        // paragraph 4
        if ($customer_state == 'MS') {
            $pdf->MultiCell(0, 4, "You agree that this Payment Authorization is for repayment of a Credit Availability Act loan and that payments shall recur at substantially regular intervals as set forth in this Agreement. This Payment Authorization is to remain in full force and effect for the transaction until you pay your Loan, including any handling fee, in full. You may only revoke this Payment Authorization by contacting us directly at $lender_phone_nbr or via mail at $lender_mailing_street_address $lender_mailing_city, $lender_mailing_state $lender_mailing_zip_code.  If you revoke your Payment Authorization, you agree to make payments to us as set forth in the \"Payments\" section above by another acceptable payment method. In no event will any revocation of this Payment Authorization be effective with respect to entries processed by us prior to us receiving such revocation.", 0, 'L');
        } else {
            $pdf->MultiCell(0, 4, "You agree that this Payment Authorization is for repayment of a consumer installment loan and that payments shall recur at substantially regular intervals as set forth in this Agreement. This Payment Authorization is to remain in full force and effect for the transaction until you pay your Loan, including any interest, in full. You may only revoke this Payment Authorization by contacting us directly at $lender_phone_nbr or via mail at $lender_mailing_street_address $lender_mailing_city, $lender_mailing_state $lender_mailing_zip_code.  If you revoke your Payment Authorization, you agree to make payments to us as set forth in the \"Payments\" section above by another acceptable payment method. In no event will any revocation of this Payment Authorization be effective with respect to entries processed by us prior to us receiving such revocation.", 0, 'L');
        }
        $pdf->Ln(4);

        // paragraph 5
        $pdf->MultiCell(0, 4, 'Your bank may charge you a fee in connection with our credit and/or debit entries. Contact your financial institution for more information specific to your Bank Account.', 0, 'L');
        $pdf->Ln(4);

        // paragraph 6
        $pdf->MultiCell(0, 4, 'Missing or Incorrect Information. If there is any missing or incorrect information in or with your loan application regarding your bank, bank routing number, or account number, then you authorize us to verify and correct such information.', 0, 'L');
        $pdf->Ln(4);

        // paragraph 7
        $pdf->MultiCell(0, 4, 'This Payment Authorization is a payment mechanism only and does not give us collection rights greater than those otherwise contained in this Agreement.', 0, 'L');
        $pdf->Ln(4);

        // Events of Default
//        if ($customer_state == 'AL' || $customer_state == 'DE' || $customer_state == 'MS' ||
//            $customer_state == 'NM' || $customer_state == 'UT' || $customer_state == 'CA') {
//            $pdf->MultiCell(0, 4, "EVENTS OF DEFAULT: The following constitute events of default under this Agreement: (a) failure to pay as scheduled; and (b) our prospect of payment or performance is significantly impaired.", 0, 'L');
//        } else {
//            $pdf->MultiCell(0, 4, "EVENTS OF DEFAULT: The following constitute events of default under this Agreement: (a) failure to pay as scheduled; and (b) our prospect of payment or performance is significantly impaired.  If you default, then you have a right to cure such default in accordance with Missouri law.", 0, 'L');
//        }
//        $pdf->Ln(4);
        $pdf->write_bold(4, 'EVENTS OF DEFAULT:  ');
        if ($customer_state == 'AL' || $customer_state == 'DE' || $customer_state == 'MS' ||
            $customer_state == 'NM' || $customer_state == 'UT' || $customer_state == 'CA' ||
            $customer_state == 'ID' || $customer_state == 'IL') {
            $pdf->Write(4, 'The following constitute events of default under this Agreement: (a) failure to pay as scheduled; and (b) our prospect of payment or performance is significantly impaired.');
        } else if ($customer_state == 'MO') {
            $pdf->Write(4, 'The following constitute events of default under this Agreement: (a) failure to pay as scheduled; and (b) our prospect of payment or performance is significantly impaired.  If you default, then you have a right to cure such default in accordance with Missouri law.');
        } else if ($customer_state == 'SC') {
            $pdf->Write(4, 'The following constitute events of default under this Agreement: (a) failure to pay as scheduled; and (b) our prospect of payment or performance is significantly impaired.  If you default, then you have a right to cure such default in accordance with South Carolina law.');
        } else if ($customer_state == 'WI') {
            $pdf->Write(4, 'The following constitute events of default under this Agreement: to have outstanding an amount of one full payment or more which has remained unpaid for more than 10 days after the scheduled or deferred due date. If you default, then you have a right to cure such default in accordance with Wisconsin law.');
        }
        $pdf->Ln(8);

        
        $pdf->write_bold(4, 'LENDER\'S RIGHTS IN THE EVENT OF DEFAULT:  ');
        if ($customer_state == 'AL') {
            $pdf->Write(4, 'Upon the occurrence of any event of default, Lender may at its option, do any one or more of the following: (a) exercise all rights, powers and remedies given by law; (b) subject to any limitations imposed by applicable state law, declare the entire unpaid outstanding balance due, but any failure of Lender to exercise such option will not constitute a waiver of Lender\'s option in the event of any subsequent default; (c) recover from you reasonable attorney fees incurred by Lender as a result of the suit or activity and to which the Lender becomes entitled by law, not exceeding 15% of the unpaid debt after default; and/or (d) assign any and all of Lender\'s interest in and to this Agreement to a third party, thereby vesting in such a third party all rights, powers and privileges of Lender hereunder. Any delay by Lender in exercising any or all of these rights shall not constitute a waiver of such rights.');
        } else if ($customer_state == 'CA') {
            $pdf->Write(4, 'Upon the occurrence of any event of default, Lender may at its option, do any one or more of the following: (a) exercise all rights, powers and remedies given by law; (b) subject to any limitations imposed by applicable state law, declare the entire unpaid outstanding balance due, but any failure of Lender to exercise such option will not constitute a waiver of Lender\'s option in the event of any subsequent default; (c) recover from you all costs and disbursements in connection with any suit to collect a loan to which the Lender becomes entitled by law; and/or (d) assign any and all of Lender\'s interest in and to this Agreement to a third party, thereby vesting in such a third party all rights, powers and privileges of Lender hereunder. Any delay by Lender in exercising any or all of these rights shall not constitute a waiver of such rights.');
        } else if ($customer_state == 'DE') {
            $pdf->Write(4, 'Upon the occurrence of any event of default, Lender may at its option, do any one or more of the following: (a) exercise all rights, powers and remedies given by law; (b) subject to any limitations imposed by applicable state law, declare the entire unpaid outstanding balance due, but any failure of Lender to exercise such option will not constitute a waiver of Lender\'s option in the event of any subsequent default; (c) recover from you all costs and disbursements in connection with any suit to collect a loan, including reasonable attorney fees from an attorney not a regularly salaried employee of the licensee incurred by Lender as a result of the suit or activity and to which the Lender becomes entitled by law, not to exceed 20% of the unpaid principal and interest; and/or (d) assign any and all of Lender\'s interest in and to this Agreement to a third party, thereby vesting in such a third party all rights, powers and privileges of Lender hereunder. Any delay by Lender in exercising any or all of these rights shall not constitute a waiver of such rights.');
        } else if ($customer_state == 'MS') {
            $pdf->Write(4, 'Upon the occurrence of any event of default, Lender may at its option, do any one or more of the following: (a) exercise all rights, powers and remedies given by law; (b) subject to any limitations imposed by applicable state law, declare the entire unpaid outstanding balance due, but any failure of Lender to exercise such option will not constitute a waiver of Lender\'s option in the event of any subsequent default; (c) recover from you all costs and disbursements in connection with any suit to collect a loan, including reasonable attorney fees incurred by Lender as a result of the suit or activity and to which the Lender becomes entitled by law; and/or (d) assign any and all of Lender\'s interest in and to this Agreement to a third party, thereby vesting in such a third party all rights, powers and privileges of Lender hereunder. Any delay by Lender in exercising any or all of these rights shall not constitute a waiver of such rights.');
        } else if ($customer_state == 'NM') {
            $pdf->Write(4, 'Upon the occurrence of any event of default, Lender may at its option, do any one or more of the following: (a) exercise all rights, powers and remedies given by law; (b) subject to any limitations imposed by applicable state law, declare the entire unpaid outstanding balance due, but any failure of Lender to exercise such option will not constitute a waiver of Lender\'s option in the event of any subsequent default; (c) recover from you all costs and disbursements in connection with any suit to collect a loan, including reasonable attorney fees incurred by Lender as a result of the suit or activity and to which the Lender becomes entitled by law; and/or (d) assign any and all of Lender\'s interest in and to this Agreement to a third party, thereby vesting in such a third party all rights, powers and privileges of Lender hereunder. Any delay by Lender in exercising any or all of these rights shall not constitute a waiver of such rights.');
        } else if ($customer_state == 'UT') {
            $pdf->Write(4, 'Upon the occurrence of any event of default, Lender may at its option, do any one or more of the following: (a) exercise all rights, powers and remedies given by law; (b) subject to any limitations imposed by applicable state law, declare the entire unpaid outstanding balance due, but any failure of Lender to exercise such option will not constitute a waiver of Lender\'s option in the event of any subsequent default; (c) recover from you all costs and disbursements in connection with any suit to collect a loan, including reasonable attorney fees incurred by Lender as a result of the suit or activity and to which the Lender becomes entitled by law; and/or (d) assign any and all of Lender\'s interest in and to this Agreement to a third party, thereby vesting in such a third party all rights, powers and privileges of Lender hereunder. Any delay by Lender in exercising any or all of these rights shall not constitute a waiver of such rights. If we hire an attorney or a third party collection agency to collect what you owe, you will also pay the lesser of (a) the actual amount we are required to pay the third party collection agency or attorney, regardless of whether that amount is a specific dollar amount or a percentage of the amount owed to us; or (b) 40% of the principal amount owed to us.');
        } else if ($customer_state == 'SC') {
            $pdf->Write(4, 'Upon the occurrence of any event of default, Lender may at its option, do any one or more of the following: (a) exercise all rights, powers and remedies given by law; (b) subject to any limitations imposed by applicable state law, declare the entire unpaid outstanding balance due, but any failure of Lender to exercise such option will not constitute a waiver of Lender\'s option in the event of any subsequent default; (c) recover from reasonable attorney fees, limited to 15% of the unpaid debt after default and referral to outside counsel, incurred by Lender as a result of the suit or activity and to which the Lender becomes entitled by law; and/or (d) assign any and all of Lender\'s interest in and to this Agreement to a third party, thereby vesting in such a third party all rights, powers and privileges of Lender hereunder. Where the principal amount of the loan is $3,700 or less, you will not be responsible for attorney\'s fees. Any delay by Lender in exercising any or all of these rights shall not constitute a waiver of such rights.');
        } else if ($customer_state == 'WI') {
            $pdf->Write(4, 'Upon the occurrence of any event of default, Lender may at its option and subject to your right to cure, do any one or more of the following: (a) exercise all rights, powers and remedies given by law; (b) subject to any limitations imposed by applicable state law, declare the entire unpaid outstanding balance due, but any failure of Lender to exercise such option will not constitute a waiver of Lender\'s option in the event of any subsequent default; and/or (c) assign any and all of Lender\'s interest in and to this Agreement to a third party, thereby vesting in such a third party all rights, powers and privileges of Lender hereunder. Any delay by Lender in exercising any or all of these rights shall not constitute a waiver of such rights.');
        } else if ($customer_state == 'ID') {
            $pdf->Write(4, 'Upon the occurrence of any event of default, Lender may at its option and subject to your right to cure, do any one or more of the following: (a) exercise all rights, powers and remedies given by law; (b) subject to any limitations imposed by applicable state law, declare the entire unpaid outstanding balance due, but any failure of Lender to exercise such option will not constitute a waiver of Lender\'s option in the event of any subsequent default; (c) recover from you all costs and disbursements in connection with any suit to collect a loan, including reasonable attorney fees incurred by Lender as a result of the suit or activity and to which the Lender becomes entitled by law; and/or (d) assign any and all of Lender\'s interest in and to this Agreement to a third party, thereby vesting in such a third party all rights, powers and privileges of Lender hereunder. If the principal balance is $1,000 or less, you will not be responsible for payment of attorney\'s fees. Any delay by Lender in exercising any or all of these rights shall not constitute a waiver of such rights.');
        } else {
            $pdf->Write(4, 'Upon the occurrence of any event of default, Lender may at its option and subject to your right to cure, do any one or more of the following: (a) exercise all rights, powers and remedies given by law; (b) subject to any limitations imposed by applicable state law, declare the entire unpaid outstanding balance due, but any failure of Lender to exercise such option will not constitute a waiver of Lender\'s option in the event of any subsequent default; (c) recover from you all costs and disbursements in connection with any suit to collect a loan, including reasonable attorney fees incurred by Lender as a result of the suit or activity and to which the Lender becomes entitled by law; and/or (d) assign any and all of Lender\'s interest in and to this Agreement to a third party, thereby vesting in such a third party all rights, powers and privileges of Lender hereunder. Any delay by Lender in exercising any or all of these rights shall not constitute a waiver of such rights.');
        }
        $pdf->Ln(8);

        if ($customer_state == 'WI') {
            $pdf->write_bold(4, 'NOTICE TO MARRIED BORROWERS:  ');
            $pdf->Write(4, 'If you are married: (1) You confirm that the Loan is being incurred in the interest of your marriage or family. (2) No provision of any marital property agreement, unilateral agreement, or court decree under Wisconsin\'s Marital Property Act will adversely affect a creditor\'s interest unless prior to the time credit is granted, the creditor is furnished a copy of that agreement or decree or is given complete information about the agreement or decree.   (3) You understand and agree that we are required by state law to notify your spouse regarding this transaction.');
        }
        $pdf->Ln(8);

        $tmp_state = mm_state_cd_to_display($customer_state);
//        $pdf->MultiCell(0, 4, "GOVERNING LAW; SEVERABILITY; INTERSTATE COMMERCE:  This Agreement is governed by the laws of the State of $tmp_state, except that the Jury Trial Waiver and Arbitration Clause is governed by the Federal Arbitration Act (\"FAA\"), 9 U.S.C. §§ 1-9.  If any provision of this Agreement is held unenforceable, including any provision of the Jury Trial Waiver and Arbitration Clause, the remainder of this Agreement will remain in full force and effect.  You and we agree that the transaction represented by this Agreement involves interstate commerce for all purposes.", 0, 'L');
//        $pdf->Ln(4);
        $pdf->write_bold(4, 'GOVERNING LAW; SEVERABILITY; INTERSTATE COMMERCE:  ');
        if ($customer_state == 'WI') {
            $pdf->Write(4, 'This Agreement is governed by the laws of the State of Wisconsin, except that the Jury Trial Waiver and Arbitration Clause is governed by the Federal Arbitration Act ("FAA"), 9 U.S.C. §§ 1-9.  To the extent not prohibited by the Wisconsin Consumer Act, if any provision of this Agreement is held unenforceable, including any provision of the Jury Trial Waiver and Arbitration Clause, the remainder of this Agreement will remain in full force and effect.  You and we agree that the transaction represented by this Agreement involves interstate commerce for all purposes.');
        } else {
            $pdf->Write(4, "This Agreement is governed by the laws of the State of $tmp_state, except that the Jury Trial Waiver and Arbitration Clause is governed by the Federal Arbitration Act (\"FAA\"), 9 U.S.C. §§ 1-9.  If any provision of this Agreement is held unenforceable, including any provision of the Jury Trial Waiver and Arbitration Clause, the remainder of this Agreement will remain in full force and effect.  You and we agree that the transaction represented by this Agreement involves interstate commerce for all purposes.");
        }
        $pdf->Ln(8);

        $pdf->setFont('Arial', 'B', 12);
        //$pdf->MultiCell(0, 4, 'JURY TRIAL WAIVER AND ARBITRATION CLAUSE.', 0, 'L');
        $pdf->Cell(0, 4, 'JURY TRIAL WAIVER AND ARBITRATION CLAUSE.', 0, 1, 'C');
        //$pdf->Ln(4);
        $pdf->setFont('Arial', '', 12);
        $pdf->MultiCell(0, 4, 'By signing this agreement, you agree to the Jury Trial Waiver and Arbitration Clause ("Clause"). You also understand that this Clause supersedes the arbitration provision included in your application.  In addition, this Clause applies to any modification of this Agreement.', 0, 'L');
        $pdf->Ln(4);

        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(0, 6, 'Background and Scope.', 0, 1, 'C');
        $pdf->setFont('Arial', '', 12);

        $pdf->SetWidths(array(30, 30, 140));

        $pdf->SetStyles(array('B', 'B', ''));
        $pdf->Row(array('What is arbitration?', 'An alternative to court.', 'In arbitration, a third party ("Arbiter") resolves disputes informally. You, related third parties, and we waive the right to go to court. Such "parties" waive jury trials.'));
        $pdf->Row(array('Is it different from court and jury trials?', 'Yes.', 'Any required hearing is private and less formal than court. Arbiters may limit pre-hearing fact finding, called "discovery." The decision is usually final. Courts rarely overturn Arbiters.'));
        $pdf->Row(array('Who does the Clause cover?', 'You, Us, and Others.', 'This Clause governs you and us; your heirs; our successors, assigns, affiliates and controlling parties; and "related parties" who have provided services in connection with any loan to you.'));
        $pdf->Row(array('Which Claims are covered?', 'Almost All Claims.', 'This Clause governs all "Claims" of one party against another. In this Clause, the word "Claims" has the broadest reasonable meaning consistent with this Clause. It includes all claims even indirectly related to your application, the loan, this Agreement and your agreements with us. It includes claims related to information you previously gave us. It includes all past agreements. It includes extensions, renewals, refinancings or payment plans. It includes claims related to collections, privacy and customer information. However, it DOES NOT include claims related to the validity, enforceability, coverage or scope of this Clause. Those claims shall be determined by a court.'));

        $pdf->SetStyles(array('B', 'B', 'BU'));
        $pdf->Row(array('Are you waiving rights?', 'Yes.', 'You waive your rights to:
1.	Have juries resolve Claims.
2.	Have courts, other than small-claims courts, resolve Claims.
3.	Bring Claims as a private attorney general or representative.
4.	Have Claims decided in a class action.
'));
        $pdf->SetStyles(array('B', 'B', 'U'));
        $pdf->Row(array('Are you waiving class action and similar rights?', 'Yes.', 'THIS CLAUSE DOES NOT allow class actions. You waive your right to be in a class action or a class arbitration, either as a representative or a member. Only individual arbitration, or small-claims courts, will resolve Claims. You waive your right to bring representative Claims. Unless reversed on appeal, if a court invalidates this waiver, the Clause will be void.'));
        $pdf->SetStyles(array('B', 'B', 'B'));
        $pdf->Row(array('Are you waiving your right to seek a public injunction?', 'Yes, if permitted under the FAA.', 'You also waive your right to seek a public injunction if such a waiver is permitted by the FAA. If a court decides that such a waiver is not permitted, and that decision is not reversed on appeal, your Claim for a public injunction will be decided in court and all other Claims will be decided in arbitration under this Clause. In such a case the parties will request that the court stay the Claim for a public injunction until the arbitration award regarding individual relief has been entered in court. In no event will a claim for public injunctive relief be arbitrated. '));
        $pdf->SetStyles(array('B', 'B', ''));
        $pdf->Row(array('What law applies?', 'The FAA.', 'This transaction involves interstate commerce, so the FAA governs. The Arbiter must apply substantive law consistent with the FAA. The Arbiter must follow statutes of limitation and privilege rules.'));
        $pdf->Row(array('How should you contact us?', 'By mail.', 'You must send any mail or notice regarding arbitration to Aspen Financial Solutions, Inc. PO Box 802533 Dallas, TX 75380., Attn. Arbitration Notice. You can call us at (877) 293-2987 or use certified mail to confirm receipt. '));
        $pdf->Row(array('Can a small-claims court resolve any Claims?', 'Yes.', 'We will not require you to arbitrate a Claim you bring on an individual basis in small-claims court. However, if there is an appeal from small-claims court, or if a Claim changes so that the small-claims court loses the power to hear it, then the Claim will only be heard by an Arbiter. '));
        $pdf->Row(array('Will this Clause continue to govern?', 'Yes, unless otherwise agreed.', 'The Clause stays effective unless the parties sign an agreement stating it doesn\'t. The Clause governs if you rescind the transaction. It governs if you default, renew, prepay or pay. It governs if the Agreement is discharged through bankruptcy. The Clause remains effective, despite a transaction\'s termination, amendment, expiration or performance.'));

        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(0, 6, 'Process.', 0, 1, 'C');
        $pdf->setFont('Arial', '', 12);
        $pdf->Row(array('What must a party do before starting a lawsuit or arbitration?', 'Send a written Claim notice and work to resolve the Claim', 'Before starting a lawsuit or arbitration, the complaining party ("Claimant") must give the other party (the "Responding Party") written notice of the Claim. The notice must explain in reasonable detail the nature of the Claim and any supporting facts. You or an attorney you have personally hired must sign the notice and must provide your full name and a phone number where you (or your attorney) can be reached. A collections letter from us to you will serve as our written notice of a Claim. Once a Claim notice is sent, the Claimant must give the Responding Party a reasonable opportunity over the next 30 days to resolve the Claim on an individual basis.'));
        $pdf->Row(array('Who manages arbitrations?','AAA, JAMS, or an agreed Arbiter.', 'The Claimant selects the company to manage the arbitration—either the American Arbitration Association ("AAA") (1-800-778-7879), www.adr.org, or JAMS (1-800-352-5267), www.jamsadr.com. The parties may also agree in writing to a local attorney, retired judge or Arbiter in good standing with another arbitration group. The Arbiter must arbitrate under AAA or JAMS consumer rules. You may get a copy of these rules from such group. Any rules that conflict with this Clause don\'t apply. If these options aren\'t available, a court may choose the Arbiter. Such Arbiter must enforce your agreements with us, as they are written.'));
        $pdf->Row(array('How does a Claimant start an arbitration?','By following the arbitration company\'s rules.', 'To start an arbitration, the Claimant must follow the rules of the arbitration company the Claimant selects.'));
        $pdf->Row(array('How is arbitration demanded?', 'By written notice or motion to compel arbitration.', 'To require arbitration, the Responding Party may mail the Claimant a demand to arbitrate or file a motion to compel arbitration. The Responding Party does not waive the right to require arbitration by bringing a different Claim against the Claimant in court. Once the Respondent demands arbitration, it is up to the Claimant to start an arbitration.'));
        $pdf->Row(array('Will the hearing be held nearby?', 'Yes.', 'The Arbiter will order any hearing near your home.'));
        $pdf->Row(array('What about appeals?', 'Appeals are limited.', 'The Arbiter\'s decision is generally final. However, if the amount in controversy exceeds $ 10,000.00, a party may appeal the Arbiter\'s finding. Such appeal will be to a three-Arbiter panel from the same arbitration group. The appeal will be de novo, and resolved by majority vote. The appealing party bears appeal costs, despite the outcome. Also, a party may appeal under the FAA. A party may file the Arbiter\'s award with the proper court.'));

        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(0, 6, 'Arbitration Fees and Awards.', 0, 1, 'C');
        $pdf->setFont('Arial', '', 12);
        $pdf->Row(array('Will we advance Arbitration Fees?', 'Yes.', 'We will advance the arbitration fees if you ask us to in good faith. This includes filing, administrative, hearing and Arbiter\'s fees.'));
        $pdf->Row(array('When do we cover your attorneys\' fees?', 'If you win.', 'We will cover your reasonable attorneys\' fees and costs if you win.'));
        $pdf->Row(array('Will you ever have to pay Arbitration Fees?', 'Sometimes, but only if you lose.', 'If the Arbiter awards you funds, you don\'t need to repay us the Arbitration Fees. Also, you don\'t need to repay us if the Arbiter decides it would be unfair to make you repay us. Unless you act in bad faith, you never have to repay us more than the amount of state court costs.'));
        $pdf->Row(array('What happens if you win?', 'You could get more than the Arbiter awarded. ', 'If you follow the steps set forth in response to the question " What must a party do before starting a lawsuit or arbitration?" and an Arbiter awards you more than our final settlement offer or we don\'t make a settlement offer, we will pay three amounts. We will pay (1) the award, excluding attorneys\' fees and costs, plus (2) the "bonus payment," plus (3) the "attorney payment." The bonus payment is 10% of the award. The attorney payment is 110% of your reasonable attorneys\' fees and costs.'));
        $pdf->Row(array('Can an award be explained?', 'Yes.', 'A party may request details from the Arbiter, within 14 days of the ruling. Upon such request, the Arbiter will explain the ruling in writing.'));
        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(0, 6, 'Other Options.', 0, 1, 'C');
        $pdf->setFont('Arial', '', 12);
        $pdf->Row(array('If you don\'t want to arbitrate, can you opt-out of the Clause?', 'Yes. Within 60 days.', 'Write us within 60 calendar days of signing this Agreement to opt-out of this Clause. List your name, address, loan number and date. This is the only way you can opt out.'));
        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(0, 6, 'Additional Acknowledgements and Electronic Signature.', 0, 1, 'C');
        $pdf->setFont('Arial', '', 12);
        $str = 'YOU ACKNOWLEDGE AND REPRESENT THAT:
(A)	YOU HAVE READ, UNDERSTAND, AND AGREE TO ALL OF THE TERMS AND CONDITIONS OF THIS AGREEMENT INCLUDING THE WAIVER OF JURY TRIAL AND ARBITRATION CLAUSE AND RIGHT TO PARTICIPATE IN A CLASS ACTION AS WELL AS THE PRIVACY NOTICE;
(B)	THE INFORMATION GIVEN IN CONNECTION WITH THIS AGREEMENT IS TRUE AND CORRECT;
(C)	YOU HAVE THE FINANCIAL ABILITY TO REPAY IN ACCORDANCE WITH THIS AGREEMENT;
(D)	YOU AUTHORIZE LENDER TO VERIFY THE INFORMATION GIVEN IN CONNECTION WITH THIS AGREEMENT AND GIVE LENDER CONSENT TO OBTAIN INFORMATION ON YOU FROM CONSUMER REPORTING AGENCIES OR OTHER SERVICES;
(E)	THIS AGREEMENT WAS FILLED IN BEFORE YOU SIGNED IT;
(F)	YOU HAD AN OPPORTUNITY TO PRINT A COMPLETED COPY OF THIS AGREEMENT FOR YOUR RECORDS;
(G)	THIS AGREEMENT IS SUBJECT TO APPROVAL BY LENDER;';

        if ($customer_state == 'CA') {
            $str = $str . '
(H)	YOU ARE 18 YEARS OF AGE OR OLDER AND NOT AN EMPLOYEE OF MONEY MART; AND
(I)	YOU ARE NOT A DEBTOR UNDER ANY PROCEEDING IN BANKRUPTCY AND HAVE NO INTENTION TO FILE A PETITION FOR RELIEF UNDER THE U.S. BANKRUPTCY CODE.
(J)	NO PERSON HAS PERFORMED ANY ACT AS A BROKER IN CONNECTION WITH THE MAKING OF THIS LOAN.';
//             $pdf->MultiCell(0, 5, 'YOU ACKNOWLEDGE AND REPRESENT THAT:
// (A)	YOU HAVE READ, UNDERSTAND, AND AGREE TO ALL OF THE TERMS AND CONDITIONS OF THIS AGREEMENT INCLUDING THE WAIVER OF JURY TRIAL AND ARBITRATION CLAUSE AND RIGHT TO PARTICIPATE IN A CLASS ACTION AS WELL AS THE PRIVACY NOTICE;
// (B)	THE INFORMATION GIVEN IN CONNECTION WITH THIS AGREEMENT IS TRUE AND CORRECT;
// (C)	YOU HAVE THE FINANCIAL ABILITY TO REPAY IN ACCORDANCE WITH THIS AGREEMENT;
// (D)	YOU AUTHORIZE LENDER TO VERIFY THE INFORMATION GIVEN IN CONNECTION WITH THIS AGREEMENT AND GIVE LENDER CONSENT TO OBTAIN INFORMATION ON YOU FROM CONSUMER REPORTING AGENCIES OR OTHER SERVICES;
// (E)	THIS AGREEMENT WAS FILLED IN BEFORE YOU SIGNED IT;
// (F)	YOU HAD AN OPPORTUNITY TO PRINT A COMPLETED COPY OF THIS AGREEMENT FOR YOUR RECORDS;
// (G)	THIS AGREEMENT IS SUBJECT TO APPROVAL BY LENDER;
// (H)	YOU ARE 18 YEARS OF AGE OR OLDER AND NOT AN EMPLOYEE OF MONEY MART; AND
// (I)	YOU ARE NOT A DEBTOR UNDER ANY PROCEEDING IN BANKRUPTCY AND HAVE NO INTENTION TO FILE A PETITION FOR RELIEF UNDER THE U.S. BANKRUPTCY CODE.
// (J)	NO PERSON HAS PERFORMED ANY ACT AS A BROKER IN CONNECTION WITH THE MAKING OF THIS LOAN.
// ', 0);
        } else if ($customer_state == 'AL') {
            $str = $str . '
(H)	YOU ARE 19 YEARS OF AGE OR OLDER AND NOT AN EMPLOYEE OF MONEY MART; AND
(I)	YOU ARE NOT A DEBTOR UNDER ANY PROCEEDING IN BANKRUPTCY AND HAVE NO INTENTION TO FILE A PETITION FOR RELIEF UNDER THE U.S. BANKRUPTCY CODE.';
//             $pdf->MultiCell(0, 5, 'YOU ACKNOWLEDGE AND REPRESENT THAT:
// (A)	YOU HAVE READ, UNDERSTAND, AND AGREE TO ALL OF THE TERMS AND CONDITIONS OF THIS AGREEMENT INCLUDING THE WAIVER OF JURY TRIAL AND ARBITRATION CLAUSE AND RIGHT TO PARTICIPATE IN A CLASS ACTION AS WELL AS THE PRIVACY NOTICE;
// (B)	THE INFORMATION GIVEN IN CONNECTION WITH THIS AGREEMENT IS TRUE AND CORRECT;
// (C)	YOU HAVE THE FINANCIAL ABILITY TO REPAY IN ACCORDANCE WITH THIS AGREEMENT;
// (D)	YOU AUTHORIZE LENDER TO VERIFY THE INFORMATION GIVEN IN CONNECTION WITH THIS AGREEMENT AND GIVE LENDER CONSENT TO OBTAIN INFORMATION ON YOU FROM CONSUMER REPORTING AGENCIES OR OTHER SERVICES;
// (E)	THIS AGREEMENT WAS FILLED IN BEFORE YOU SIGNED IT;
// (F)	YOU HAD AN OPPORTUNITY TO PRINT A COMPLETED COPY OF THIS AGREEMENT FOR YOUR RECORDS;
// (G)	THIS AGREEMENT IS SUBJECT TO APPROVAL BY LENDER;
// (H)	YOU ARE 19 YEARS OF AGE OR OLDER AND NOT AN EMPLOYEE OF MONEY MART; AND
// (I)	YOU ARE NOT A DEBTOR UNDER ANY PROCEEDING IN BANKRUPTCY AND HAVE NO INTENTION TO FILE A PETITION FOR RELIEF UNDER THE U.S. BANKRUPTCY CODE.
// ', 0);
        } else if ($customer_state == 'WI') {
            $str = $str . '
(H)	YOU ARE 18 YEARS OF AGE OR OLDER AND NOT AN EMPLOYEE OF MONEY MART; AND
(I)	YOU ARE NOT A DEBTOR UNDER ANY PROCEEDING IN BANKRUPTCY AND HAVE NO INTENTION TO FILE A PETITION FOR RELIEF UNDER THE U.S. BANKRUPTCY CODE.';
//             $pdf->MultiCell(0, 5, 'YOU ACKNOWLEDGE AND REPRESENT THAT:
// (A)	YOU HAVE READ, UNDERSTAND, AND AGREE TO ALL OF THE TERMS AND CONDITIONS OF THIS AGREEMENT INCLUDING THE WAIVER OF JURY TRIAL AND ARBITRATION CLAUSE AND RIGHT TO PARTICIPATE IN A CLASS ACTION AS WELL AS THE PRIVACY NOTICE;
// (B)	THE INFORMATION GIVEN IN CONNECTION WITH THIS AGREEMENT IS TRUE AND CORRECT;
// (C)	YOU HAVE THE FINANCIAL ABILITY TO REPAY IN ACCORDANCE WITH THIS AGREEMENT;
// (D)	YOU AUTHORIZE LENDER TO VERIFY THE INFORMATION GIVEN IN CONNECTION WITH THIS AGREEMENT AND GIVE LENDER CONSENT TO OBTAIN INFORMATION ON YOU FROM CONSUMER REPORTING AGENCIES OR OTHER SERVICES;
// (E)	THIS AGREEMENT WAS FILLED IN BEFORE YOU SIGNED IT;
// (F)	YOU HAD AN OPPORTUNITY TO PRINT A COMPLETED COPY OF THIS AGREEMENT FOR YOUR RECORDS;
// (G)	THIS AGREEMENT IS SUBJECT TO APPROVAL BY LENDER;
// (H)	YOU ARE 18 YEARS OF AGE OR OLDER AND NOT AN EMPLOYEE OF MONEY MART; AND
// (I)	YOU ARE NOT A DEBTOR UNDER ANY PROCEEDING IN BANKRUPTCY AND HAVE NO INTENTION TO FILE A PETITION FOR RELIEF UNDER THE U.S. BANKRUPTCY CODE.
// ', 0);
        } else if ($customer_state == 'IL') {
            $str = $str . '
(H)	YOU ARE 18 YEARS OF AGE OR OLDER AND NOT AN EMPLOYEE OF MONEY MART; AND
(I)	YOU ARE NOT A DEBTOR UNDER ANY PROCEEDING IN BANKRUPTCY AND HAVE NO INTENTION TO FILE A PETITION FOR RELIEF UNDER THE U.S. BANKRUPTCY CODE.
(J)	YOU MAY CONTACT THE DIVISION AT (888) 298-8089 FOR THE PURPOSE OF RECEIVING INFORMATION REGARDING CREDIT OR ASSISTANCE WITH CREDIT PROBLEMS.';
        } else {
            $str = $str . '
(H)	YOU ARE 18 YEARS OF AGE OR OLDER AND NOT AN EMPLOYEE OF MONEY MART; AND
(I)	YOU ARE NOT A DEBTOR UNDER ANY PROCEEDING IN BANKRUPTCY AND HAVE NO INTENTION TO FILE A PETITION FOR RELIEF UNDER THE U.S. BANKRUPTCY CODE.';
//             $pdf->MultiCell(0, 5, 'YOU ACKNOWLEDGE AND REPRESENT THAT:
// (A)	YOU HAVE READ, UNDERSTAND, AND AGREE TO ALL OF THE TERMS AND CONDITIONS OF THIS AGREEMENT INCLUDING THE WAIVER OF JURY TRIAL AND ARBITRATION CLAUSE AND RIGHT TO PARTICIPATE IN A CLASS ACTION AS WELL AS THE PRIVACY NOTICE;
// (B)	THE INFORMATION GIVEN IN CONNECTION WITH THIS AGREEMENT IS TRUE AND CORRECT;
// (C)	YOU HAVE THE FINANCIAL ABILITY TO REPAY IN ACCORDANCE WITH THIS AGREEMENT;
// (D)	YOU AUTHORIZE LENDER TO VERIFY THE INFORMATION GIVEN IN CONNECTION WITH THIS AGREEMENT AND GIVE LENDER CONSENT TO OBTAIN INFORMATION ON YOU FROM CONSUMER REPORTING AGENCIES OR OTHER SERVICES;
// (E)	THIS AGREEMENT WAS FILLED IN BEFORE YOU SIGNED IT;
// (F)	YOU HAD AN OPPORTUNITY TO PRINT A COMPLETED COPY OF THIS AGREEMENT FOR YOUR RECORDS;
// (G)	THIS AGREEMENT IS SUBJECT TO APPROVAL BY LENDER;
// (H)	YOU ARE 18 YEARS OF AGE OR OLDER AND NOT AN EMPLOYEE OF MONEY MART; AND
// (I)	YOU ARE NOT A DEBTOR UNDER ANY PROCEEDING IN BANKRUPTCY AND HAVE NO INTENTION TO FILE A PETITION FOR RELIEF UNDER THE U.S. BANKRUPTCY CODE.
// ', 0);
        }
        
        $pdf->MultiCell(0, 5, $str, 0);
        
        $pdf->setFont('Arial', 'B', 12);
        $pdf->MultiCell(0, 4, 'BY CLICKING THE ACKNOWLEDGMENT BUTTON BELOW, YOU UNDERSTAND THAT YOU ARE ELECTRONICALLY SIGNING THIS AGREEMENT. YOU AGREE THAT THIS ELECTRONIC SIGNATURE HAS THE FULL FORCE AND EFFECT OF YOUR PHYSICAL SIGNATURE AND THAT IT BINDS YOU TO THIS AGREEMENT IN THE SAME MANNER A PHYSICAL SIGNATURE WOULD.', 0, 'C');
        $pdf->Ln(10);
        
        if ($customer_state == 'WI') {
            $pdf->MultiCell(0, 5, 'NOTICE TO CUSTOMER

DO NOT SIGN THIS IF IT CONTAINS ANY BLANK SPACES.

YOU ARE ENTITLED TO AN EXACT COPY OF ANY AGREEMENT YOU SIGN.

YOU HAVE THE RIGHT AT ANY TIME TO PAY IN ADVANCE THE UNPAID BALANCE DUE UNDER THIS AGREEMENT AND YOU MAY BE ENTITLED TO A PARTIAL REFUND OF THE FINANCE CHARGE.
', 0);
        } else if ($customer_state == 'IL') {
            $pdf->MultiCell(0, 5, 'In addition to agreeing to the terms of this agreement, I acknowledge, by my signature below, receipt from Aspen Financial Solutions, Inc. a pamphlet regarding small consumer loans.', 0);
        }

        //$pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(0, 5, 'Customer Electronic Signature:', 0, 1, 'L');
        $pdf->setFont('Arial', '', 12);
        $pdf->Ln(5);
        if ($customer_state == 'AL') {
            $pdf->Cell(0, 5, 'CAUTION -- IT IS IMPORTANT THAT YOU THOROUGHLY READ THE CONTRACT BEFORE YOU SIGN IT.', 0, 1, 'L');
            $pdf->Ln(5);
        }

        $pdf->setFont('Arial', '', 12);
        $pdf->Cell(0, 5, "$lender_name D/B/A $lender_dba_name", 0, 1, "L");
        $pdf->Ln(2);
        $pdf->setFont('Arial', 'U', 12);
        $pdf->Cell(70, 5, "/s/ Rex Northen", 0, 0, "L");
        $pdf->Cell(70, 5, "/s/ $customer_name", 0, 0, "L");
        $pdf->Cell(70, 5, $orig_dt, 0, 1, "L");

        $pdf->setFont('Arial', '', 12);
        $pdf->Cell(70, 5, "Senior Vice President", 0, 0, "L");
        $pdf->Cell(70, 5, $customer_name, 0, 0, "L");
        $pdf->Cell(70, 5, "Date", 0, 1, "L");

        $pdf->ln(10);

        /* Swapna: story AFDS-442 versioning of the agreements based on the state and updates made in the clauses */
        $version_nbr = '2';
        $version_data_arr = mm_get_loan_agreement_versions($customer_state);
        $version_data = $version_data_arr['version_arr'];
        $version_nbr = $version_data[0]['version_nbr'];
        $version_month_year = $version_data[0]['version_month_year'];
        $version_name = $version_data[0]['version_name'];
        error_log_dump_associative_array('swapna: version_data: ', $version_data);

        //$pdf->Cell(0, 5, "AFD-$customer_state-Ver. " . date('m/y'));
        $pdf->Cell(0, 5, $version_name."-$customer_state-Ver. ".$version_nbr." ".$version_month_year);
        //$pdf->Cell(0, 5, "AFD-$customer_state-Ver. 2 " . date('m/y'));

        //Save the PDF document
        //$doc_path = mm_get_document_path();
        $doc_path = $doc_path . "$account_nbr/";
        if (!file_exists($doc_path)) {
            //The account directory doesn't exists so create it
            if (!mkdir($doc_path)) {
                //Unable to make the directory.
                mm_log_error('mm_generate_loan_agreement_pdf', "Unable to create the directory $doc_path");
            }
        }
        $doc_path = $doc_path . "$application_nbr/";
        if (!file_exists($doc_path)) {
            //The application directory doesn't exist so create it
            if (!mkdir($doc_path)) {
                mm_log_error('mm_generate_loan_agreement_pdf', "Unable to create the directory $doc_path");
            }
        }
        $doc_path = $doc_path . "loan_agreement.pdf";

        $pdf->Output('F', $doc_path);

        //Move the document to a secure location

        if ($signature_data == 1) // if signed
        {
            error_log("signature data is 1");
            mm_send_ftp_document($doc_path, "loan_agreement_$application_nbr - signed.pdf", "customer_documents/$account_nbr/$application_nbr/", $xmit_type);
	   //Moved by Jason 7-15-18
	 //Code to copy loan aggrement file to first associate folder with timestamps using ftp - Added by Swapna on July 6th 2018
            $date = date("Ymd");
            $loandatetime = date("YmdHisv");
	    //Not necessary to calculate loan date.  Already calculated above as contract date.  Jason 7-15-18
            error_log('$loan_date----',$loan_date);
            error_log($doc_path, $loan_number."_".$date."_".$loandatetime.".pdf", "../../fa_ftp/$loan_date/");
            //mm_send_fa_ftp_document($doc_path, $loan_number."_".$date."_".$loandatetime.".pdf", "../../fa_ftp/$loan_date/", "B",$loan_number,$fa_uploaded);
		//Replaced the line from above by Jason 7-15-18

            
            mm_send_fa_ftp_document($doc_path, $loan_number."_".$orig_dt_yyyymmdd."_".$loandatetime.".pdf", "../../fa_ftp/$contract_dt_yyyymmdd/", "B",$loan_number,$fa_uploaded);
        } // end if signed
        else
        { // if not signed
            // if not signed, it does not go to an fa_ftp directory
            mm_send_ftp_document($doc_path, "loan_agreement_$application_nbr.pdf", "customer_documents/$account_nbr/$application_nbr/", "B");
        } // end if not signed




        //Delete the temporary document

        if (!unlink($doc_path)) {
            mm_log_error('mm_generate_loan_agreement_pdf', "Unable to delete the file $doc_path");
        }

        //Hard coded for now
        $return_array["document_path"] = "customer_documents/$account_nbr/$application_nbr/loan_agreement_$application_nbr.pdf";
        $return_array["return_value"] = 0;
        return $return_array;
    }

    /*****************************************************************************************************************
     * Function mm_generate_application_pdf($application_nbr)
     ****************************************************************************************************************
     * @param $application_nbr
     * @return array
     */
    public function mm_generate_application_pdf($application_nbr) {
        error_log("flow: mm_generate_application_pdf\n");
        //Initialize Variables
        $return_array = array();

        //Get Application Data
        $app_array = mm_get_application_details($application_nbr);
        
        //Get Application specific state details like gross income for Illinois
        if($app_array["app_data"]['state'] == 'IL'){
            $app_state_specific_array = mm_get_application_state_specific_details($application_nbr);
            $app_state_spcific_data = $app_state_specific_array["app_data"];

        }
        
        $app_data = $app_array["app_data"];

        error_log_dump_associative_array('mark: app_details: ', $app_data);
        $account_nbr = $app_data["account_nbr"];
        $ssn1 = $app_data["ssn"];
        $ssn_clean = "XXX-XX-" . substr($ssn1, 5, 4);

        //Generate Application PDF
        $pdf = new PDF('P', 'mm', 'Letter');
        $pdf->AddPage();

        //$pdf->SetWidths(array(30, 30, 140));
        $pdf->SetWidths(array(80, 116));

        //Add Heading For Personal Details Section
        $pdf->setFont('Arial', 'B', 12);
        $pdf->Cell(0, 10, 'Application For Credit', 0, 0, 'C');
        $pdf->Ln(10);
        $pdf->print_header('Section 1:Personal Information');
        //Add output for Personal Details Section
        $pdf->setFont('Arial', 'B', 8);
        $pdf->Row(array('First Name: ', $app_data["first_name"]));
        $pdf->Row(array('Last Name: ', $app_data["last_name"]));
        $pdf->Row(array('DOB: ', $app_data["dob"]));
        $pdf->Row(array('Street Address:  ', $app_data["street_address"]));
        $pdf->Row(array('Apt/Suite:  ', $app_data["appt_suite"]));
        $pdf->Row(array('City:  ', $app_data["city"]));
        $pdf->Row(array('State:  ', $app_data["state"]));
        $pdf->Row(array('Zip Code:  ', $app_data["zip_code"]));
        $pdf->Row(array('Email Address:  ', $app_data["email_address"]));
        $pdf->Row(array('Cell Phone:  ', $app_data["mobile_phone_nbr"]));
        $pdf->Row(array('Home Phone:  ', $app_data["home_phone_nbr"]));
        $pdf->Row(array('Social Security Number: ', $ssn_clean));

        //Output information related to employment
        $pdf->print_header('Section 2: Income and Housing Expense');
        $pdf->setFont('Arial', 'B', 8);
        $pdf->Row(array('Do you rent or own your residence?', $app_data["own_or_rent"]));
        $pdf->Row(array('If you own, how much is your monthly mortgage?', $app_data["monthly_mtg_amt"]));
        $pdf->Row(array('If you rent, how much is your monthly rent amount?', $app_data["monthly_rent_amt"]));
        $pdf->print_document_label('PRIMARY INCOME:', 25, 0, 1);
        $pdf->Row(array('Income Source: ', $app_data["income_source_1"]));
        $pdf->Row(array('Company Name: ', $app_data["company_name_1"]));
        $pdf->Row(array('Are you paid hourly or salary? ', $app_data["wage_type_1"]));
        $pdf->Row(array('If Salary, what is your annual salary? ', $app_data["annual_salary_1"]));
        $pdf->Row(array('If Hourly, what is your hourly rate? ', $app_data["hourly_rate_1"]));
        $pdf->Row(array('Average Hours Per Week? ', $app_data["hours_per_week_1"]));
        $pdf->Row(array('Pay Frequency: ', $app_data["pay_frequency_1"]));
        $pdf->Row(array('If Monthly, what day of the month are you paid? ', $app_data["day_paid_month_1"]));
        //$pdf->Row(array('If Weekly or Bi-Weekly, what day of the week are you paid? ', $app_data["day_paid_weekly_1"]));
        if($app_data["state"] == 'IL'){
            $pdf->Row(array('If you have a gross monthly income, what is the amount? Your gross monthly income is the amount of money you earn before anything is taken out for taxes or other deductions. For more info: http://dev.moneymartdirect.com/2018/06/15/information', $app_state_spcific_data->gross_income_1));
        }else{
            $pdf->Row(array('If you draw a monthly income, what is the amount? ', $app_data["monthly_income_amt_1"]));
        }
        $pdf->Row(array('If Twice Monthly, what days of the month are you paid? ', $app_data["day_paid_twice_monthly_1"]));
        $pdf->Row(array('Do you have direct deposit? ', $app_data["direct_deposit_ind_1"]));
        $pdf->Row(array('Next Pay Date:  ', $app_data["next_pay_dt_1"]));
        if($app_data["state"] != 'IL'){
            $pdf->Row(array('Last Paycheck Amount:  ', $app_data["last_paycheck_amt_1"]));
        }
        $pdf->print_document_label('SECONDARY INCOME:', 25, 0, 1);
        $pdf->Row(array('Income Source: ', $app_data["income_source_2"]));
        $pdf->Row(array('Company Name: ', $app_data["company_name_2"]));
        $pdf->Row(array('Are you paid hourly or salary? ', $app_data["wage_type_2"]));
        $pdf->Row(array('If Salary, what is your annual salary? ', $app_data["annual_salary_2"]));
        $pdf->Row(array('If Hourly, what is your hourly rate? ', $app_data["hourly_rate_2"]));
        $pdf->Row(array('Average Hours Per Week? ', $app_data["hours_per_week_2"]));
        $pdf->Row(array('Pay Frequency: ', $app_data["pay_frequency_2"]));
        $pdf->Row(array('If Monthly, what day of the month are you paid? ', $app_data["day_paid_month_2"]));
        //$pdf->Row(array('If Weekly or Bi-Weekly, what day of the week are you paid? ', $app_data["day_paid_weekly_2"]));
        if($app_data["state"] == 'IL'){
            $pdf->Row(array('If you have a gross monthly income, what is the amount? Your gross monthly income is the amount of money you earn before anything is taken out for taxes or other deductions.For more info: http://dev.moneymartdirect.com/2018/06/15/information', $app_state_spcific_data->gross_income_2));
        }else{
            $pdf->Row(array('If you draw a monthly income, what is the amount? ', $app_data["monthly_income_amt_2"]));
        }
        $pdf->Row(array('If Twice Monthly, what days of the month are you paid? ', $app_data["day_paid_twice_monthly_2"]));
        $pdf->Row(array('Do you have direct deposit? ', $app_data["direct_deposit_ind_2"]));
        $pdf->Row(array('Next Pay Date:  ', $app_data["next_pay_dt_2"]));
        if($app_data["state"] != 'IL'){
        $pdf->Row(array('Last Paycheck Amount:  ', $app_data["last_paycheck_amt_2"]));
        }

        //Output information related to Banking Information
        $pdf->print_header('Section 3: Banking Information');
        $pdf->setFont('Arial', 'B', 8);
        $pdf->Row(array('Bank Routing Number:  ', $app_data["routing_nbr"])); // was 25
        $pdf->Row(array('Bank Account Number:  ', $app_data["bank_acct_nbr"]));
        $pdf->Row(array('Bank Account Type:  ', $app_data["bank_acct_type"]));
        $pdf->print_document_label('How long have you had this account?  ', 60, 0, 1); // was 55
        $pdf->Row(array('Years: ', $app_data["bank_acct_age_years"]));
        $pdf->Row(array('Months: ', $app_data["bank_acct_age_months"]));
        $pdf->Cell(5, 10, "  X  ", 0, 0, 'C');
        $pdf->setFont('Arial', '', 8);
        $pdf->Cell(5, 10, "", 0, 0, 'L');
        $pdf->Cell(0, 10, "Check here if this account is active and in good standing.*", 0, 0, 'L');
        $pdf->Ln(5);


        $pdf->print_header('Section 4: Legal Disclosures');
        $pdf->Ln(2);

        // BEGIN Agree to Electronic Disclosures
        $pdf->print_document_label('Agree to Electronic Signature and Communications; Privacy Policy', 60, 0, 1); // was 55
        $pdf->Ln(2);
        $pdf->setFont('Arial', 'I', 6);
        $pdf->MultiCell(0, 4, "By clicking the box below, you agree to this CONSENT TO ELECTRONIC SIGNATURE, DISCLOSURES AND COMMUNICATIONS. 
    Please read this information carefully and, for future reference, either print a copy of this document or retain this information electronically:      
    1. INTRODUCTION: You are submitting a credit application to Lender. We can give you the benefits of our on-line service only if you consent to use and accept electronic signatures, electronic records, and electronic disclosures in connection with the transaction (your \"Consent\"). By completing and submitting an on-line credit application (your \"Application\"), you acknowledge that you have received this document and have consented to the use of electronic communications, electronic signatures, electronic records, and electronic disclosures in connection with this transaction (Collectively, \"Records\").  
    2. ELECTRONIC COMMUNICATIONS: You may request a paper copy of any Record by emailing Lender at: support@aspenfinancialdirect.com. You may request a paper copy even if you withdraw your Consent. Lender will retain the Records as required by law and will provide you with a paper copy of any Record at no charge. 
    3. CONSENTING TO DO BUSINESS ELECTRONICALLY: Before giving your Consent, you should consider whether you have the required hardware and software capabilities described below.  
    4. SCOPE OF CONSENT: Your consent and our agreement to conduct this transaction electronically only apply to this transaction. If we receive your Consent, then we will conduct this transaction with you electronically. 
    5. HARDWARE AND SOFTWARE REQUIREMENTS: To access and retain the Records electronically, you will need to use the following computer software and hardware: A computer with Internet access and an Internet Browser that meets the following minimum requirements: 1.3GHz or faster processor, Microsoft® Windows® XP (32 bit and 64 bit), Windows 7 (32 bit and 64 bit),Windows 8 or 8.1 (32 bit and 64 bit),256MB of RAM,320MB of available hard-disk space, Internet Explorer 7, 8, 9, 10, or 11; Firefox® Extended Support Release; Chrome®. Also the Internet Browser must support at least 128-bit encrypted browsers encryption. To read some documents, you will need a PDF file reader like Adobe Acrobat Reader®. If at any time during this transaction these requirements change in a way that creates a material risk that you may not be able to receive Records electronically, Lender will notify you of these changes. 
    6. WITHDRAWING CONSENT: You may withdraw this Consent at any time and at no charge to you. If you withdraw your Consent prior to receiving the credit transaction, then your withdrawal will prevent you from obtain an online credit transaction (i.e. a credit transaction obtained over the Internet). To withdraw your Consent, you must e-mail us at support@aspenfinancialdirect.com. Note, however, that the withdrawal of your Consent will not affect the legal effectiveness, validity, or enforceability of the credit transaction or of any Records that you received electronically prior to such withdrawal (including but limited to the arbitration agreement).  
    7. CHANGE TO YOUR CONTACT INFORMATION: You must keep us informed of any change in your e-mail address, your ordinary mail address, or other contact information. To update your address information, access your online account created or call us at (877) 293-2987 or e-mail us at support@aspenfinancialdirect.com.
    AUTHORIZATION, AGREEMENT, AND REPRESENTATIONS, BY ENTERING THE LAST FOUR DIGITS OF YOUR SOCIAL SECURITY NUMBER BELOW, YOU CONSENT TO USE AND ACCEPT ELECTRONIC SIGNATURES, ELECTRONIC RECORDS, AND ELECTRONIC DISCLOSURES IN CONNECTION WITH THIS TRANSACTION. YOU ACKNOWLEDGE THAT YOU CAN ACCESS THE RECORDS IN THE DESIGNATED FORMAT DESCRIBED ABOVE, AND YOU UNDERSTAND THAT YOU MAY REQUEST A PAPER COPY OF THE RECORDS AT ANY TIME AND AT NO CHARGE.
    PRIVACY POLICY – By clicking the box below, you also agree to the receipt and review of our Privacy Policy, including as applicable the Privacy Policy for California and North Dakota Residents.", 0, 'L', 0);
        $pdf->setFont('Arial', 'U', 8);
        $pdf->Cell(5, 10, "  X  ", 0, 0, 'C');
        $pdf->setFont('Arial', '', 8);
        $tmp = $app_data["electronic_disclosure_agreement"];
        $pdf->MultiCell(0, 10, $tmp, 0, 'L', 0);
        $pdf->Ln(5);


        // BEGIN Agree to Communications (Optional)
        $pdf->print_document_label('Agree to Marketing Communications (optional)', 60, 0, 1); // was 55
        $pdf->Ln(2);
        $pdf->setFont('Arial', 'I', 6);
        $pdf->MultiCell(0, 4, "You understand that, by checking the box and submitting the Credit Application, you consent to Aspen Financial Direct delivering telemarketing calls and text messages to the cell phone number provided on the Credit Application using an autodialer or a prerecorded message. You also understand that you are not required to provide this authorization as a condition of doing business with Aspen Financial Direct, and that you can proceed without providing this consent by submitting your information without checking this box. You may revoke this authorization by notifying Aspen Financial Direct by any reasonable method, including by phone at 1-877-293-2987 and in writing by U.S. mail to Aspen Financial Direct, Attention: General Counsel, P.O. Box 802533, Dallas, TX 75380.", 0, 'L', 0);
        $tmp = $app_data["contact_agreement_agreement"];
        $pdf->setFont('Arial', 'U', 8);
        if (!empty($tmp)) {
            $pdf->Cell(5, 10, "  X  ", 0, 0, 'C');
        } else {
            $pdf->Cell(5, 10, "     ", 0, 0, 'C');
            $pdf->MultiCell(0, 10, $tmp, 0, 'L', 0);
        }
        $pdf->setFont('Arial', '', 8);
        $pdf->MultiCell(0, 10, $tmp, 0, 'L', 0);
        $pdf->Ln(5);


        // Agree tp Credit Bureau Inquiry
        $pdf->print_document_label('Agree to Credit Application Terms and Conditions (arbitration, verification and credit report, and account opening)', 60, 0, 1); // was 55
        $pdf->Ln(2);
        $pdf->setFont('Arial', 'I', 6);
        $pdf->MultiCell(0, 4, "By clicking the box and entering the last four digits of your social security number below, you submit to Aspen Financial Solutions, Inc. d/b/a Aspen Financial Direct (\"Lender\") this application for the purpose of inducing Lender to extend or maintain credit to you. You certify that this application presents a true, complete and correct statement of the matters shown as of the date of your application and does not omit any pertinent information. You will notify Lender promptly in writing of any material unfavorable change in this information. In the absence of such notice, Lender may consider this a continuing application and substantially correct. If you apply for further credit, this application shall have the same force and effect as if delivered as an original application at the time you request such further credit. 
    •   ARBITRATION - Each of the parties voluntarily agrees to have all claims or controversies that arise from or relate in any way to our prospective, past, present or future business with each other, and with its or his respective affiliates, agents or employees, including the validity of any related agreements and the scope of this arbitration clause, resolved by BINDING ARBITRATION by a single arbitrator in accordance with the Consumer Due Process Protocol of the American Arbitration Association. This arbitration agreement is made pursuant to a transaction involving interstate commerce and shall be governed by the Federal Arbitration Act, 9 U.S.C §§ 1-16. The parties understand that they have a right or opportunity to litigate disputes through a court, but that they prefer to resolve their disputes through arbitration. IF ARBITRATION IS CHOSEN, I WILL NOT HAVE THE RIGHT TO GO TO COURT, TO HAVE A JURY TRIAL, TO PARTICIPATE AS A REPRESENTATIVE OR MEMBER OF ANY CLASS OF CLAIMANTS, OR TO HAVE MY CLAIMS CONSOLIDATED OR JOINED WITH THOSE OF ANY OTHER CLAIMANT. I WILL HAVE VERY LIMITED RIGHTS TO PRETRIAL DISCOVERY AND APPEAL. I understand that Lender will only consider my application if I agree to the terms of this paragraph. By entering the last four digits of your social security number below, you also agree to the arbitration provision set forth above. Finally, you represent and warrant that all the information contained in the Credit Application is true and correct, that you are not currently a debtor in any bankruptcy proceeding, and that you do not intend to file a bankruptcy petition under any chapter of the U.S. Bankruptcy Code either during the term of a transaction or within the 90-day period following your repayment of a transaction.
    •	VERIFICATION OF INFORMATION AND CREDIT REPORT - You authorize Aspen Financial Direct to verify the accuracy of the information contained in this Credit Application by, among other actions, checking your credit report before entering into any transaction with us and to further check your credit report for any legitimate business need in connection with a transaction between you and Aspen Financial Direct, submitting your application to a consumer reporting agency, calling your employer to confirm employment, calling your residence or cell phone to confirm a working phone number, and obtaining your bank account information. Aspen Financial Direct is authorized to answer any questions about Lender\'s experience with you.
    •	IMPORTANT INFORMATION ABOUT OPENING AN ACCOUNT -  To help the government fight the funding of terrorism and money laundering activities, Federal law requires us to obtain, verify, and record information that identifies each borrower. When you apply for a loan, we will ask for your name, address, date of birth, and other information that will allow the lender to identify you. We may also ask to see your driver's license or other identifying document. NOTICE: WE ARE REQUIRED BY LAW TO ADOPT PROCEDURES TO REQUEST AND RETAIN IN OUR RECORDS INFORMATION NECESSARY TO VERIFY YOUR IDENTITY. 
", 0, 'L', 0);
        $pdf->setFont('Arial', 'U', 8);
        $pdf->Cell(5, 10, "  X  ", 0, 0, 'C');
        $pdf->setFont('Arial', '', 8);
        $pdf->MultiCell(0, 10, $app_data["credit_bureau_inquiry_agreement"], 0, 'L', 0);
        $pdf->Ln(5);

        // Delaware
        if ($app_data['state'] == 'DE') {
            $pdf->print_document_label('Delaware Schedule of Charges', 60, 0, 1); // was 55
            $pdf->Ln(2);
            $pdf->setFont('Arial', 'I', 6);
            $pdf->MultiCell(0, 4, "Loans are made by Aspen Financial Solutions, Inc. doing business as Aspen Financial Direct.
             Loan Type Offered: Closed-end interest bearing personal loans.
            Annual Interest Rate: 112% to 275%
            Fees:
                Late Charge:If any scheduled payment is not paid in full within 15 days after its due date, a late fee of 5% of the payment, or $15, whichever is greater will be charged.
         The most recent change to any of these charges was April 30th, 2018.
        ", 0, 'L', 0);
            $pdf->setFont('Arial', 'U', 8);
            $pdf->Cell(5, 10, "  X  ", 0, 0, 'C');
            $pdf->setFont('Arial', '', 8);
            $pdf->MultiCell(0, 10, $app_data["delaware_schedule_agreement"], 0, 'L', 0);
            $pdf->Ln(5);
        }

        // California
        if ($app_data['state'] == 'CA') {
            $pdf->print_document_label('California Schedule Acknowledgement', 60, 0, 1); // was 55
            $pdf->Ln(2);
            $pdf->setFont('Arial', 'U', 8);
            $pdf->Cell(5, 10, "  X  ", 0, 0, 'C');
            $pdf->setFont('Arial', '', 8);
            $pdf->MultiCell(0, 10, $app_data["california_schedule_acknowledgement"], 0, 'L', 0);
            $pdf->Ln(5);
        }

        // Wisconsin
        if ($app_data['state'] == 'WI') {
            $pdf->print_document_label('Wisconsin Disclosure', 60, 0, 1); // was 55
            $pdf->Ln(2);
            $pdf->setFont('Arial', 'U', 8);
            $pdf->Cell(5, 10, "  X  ", 0, 0, 'C');
            $pdf->setFont('Arial', '', 8);
            $pdf->MultiCell(0, 10, 'A written credit application must include a notice to the consumer that no provision of a marital property agreement, a unilateral statement under the marital property law, or a court decree adversely affects the interest of the creditor unless the creditor, prior to the time the credit is granted, is furnished with a copy of the agreement, statement or decree or has actual knowledge of the adverse provision when the debt to the creditor is incurred. Wis. Stat. Ann. § 766.56(2)(b).', 0, 'L', 0);
            $pdf->Ln(5);
        }
        
        // Illinois
        if ($app_data['state'] == 'IL') {
            $pdf->print_document_label('Illinois Small Dollar Loan Agreement', 60, 0, 1); // was 55
            $pdf->Ln(2);
            $pdf->setFont('Arial', 'U', 8);
            $pdf->Cell(5, 10, "  X  ", 0, 0, 'C');
            $pdf->setFont('Arial', '', 8);
            $pdf->MultiCell(0, 10, $app_state_spcific_data->illinois_agreement, 0, 'L', 0);
            $pdf->Ln(5);
        }


        $pdf->print_header('Section 5: Signature');
        $pdf->Ln(2);
        $pdf->setFont('Arial', 'U', 8);
        $pdf->Cell(5, 10, " " . $app_data["last_4_ssn"] . " ", 0, 0, 'C');
        $pdf->setFont('Arial', '', 8);
        $pdf->Cell(0, 10, "    Sign Here with Last 4 of Your SSN (Time Submitted: " . $app_data["create_dt"] . " CST)", 0, 0, 'L');
        $pdf->Ln(10);

        //Store Application PDF
        $doc_path = mm_get_document_path();
        $doc_path = $doc_path . "$account_nbr/";
        if (!file_exists($doc_path)) {
            //The account directory doesn't exists so create it
            if (!mkdir($doc_path)) {
                //Unable to make the directory.
                mm_log_error('mm_generate_application_pdf', "Unable to create the directory $doc_path");

            }
        }
        $doc_path = $doc_path . "$application_nbr/";
        if (!file_exists($doc_path)) {
            //The application directory doesn't exist so create it
            if (!mkdir($doc_path)) {
                mm_log_error('mm_generate_application_pdf', "Unable to create the directory $doc_path");
            }
        }
        $doc_path = $doc_path . "application_$application_nbr.pdf";

        $tmp = $pdf->Output('F', $doc_path);

        //Move the document to a secure location
        $temp = mm_send_ftp_document($doc_path, "application_$application_nbr.pdf", "customer_documents/$account_nbr/$application_nbr/", "B");
        //Delete the temporary document

        if (!unlink($doc_path)) {
            mm_log_error('mm_generate_application_pdf', "Unable to delete the file $doc_path");
        }

        //Hard coded for now
        $return_array["document_path"] = "customer_documents/$account_nbr/$application_nbr/loan_agreement_$application_nbr.pdf";
        $return_array["return_value"] = 0;
        return $return_array;

        //Return the function information
        $return_array["return_value"] = $return_value;
        $return_array["return_message"] = $return_message;
        $return_array["document_path"] = $doc_path;

        return $return_array;
    }


    /**
     * @param $w
     */
    function SetWidths($w) {
        //Set the array of column widths
        $this->widths = $w;
    }

    /**
     * @param $a
     */
    function SetAligns($a) {
        //Set the array of column alignments
        $this->aligns = $a;
    }

    /**
     * @param $data
     */
    function Row($data) {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++) {
            $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));
        }

        $h = 6 * $nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();
            //Draw the border
            if ($i == 0) {
                // Shade the first column
                $this->SetFillColor(240);
                $this->Rect($x, $y, $w, $h, 'DF');
                $this->SetFillColor(0, 0, 0);
            } else {
                $this->Rect($x, $y, $w, $h);
            }
            //Print the text
            $this->SetFont('', isset($this->styles[$i]) ? $this->styles[$i] : '');
            $this->MultiCell($w, 5, $data[$i], 0, $a);
            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    /**
     * @param $h
     */
    function CheckPageBreak($h) {
        //If the height h would cause an overflow, add a new page immediately
        if ($this->GetY() + $h > $this->PageBreakTrigger) {
            $this->AddPage($this->CurOrientation);
        }

    }

    /**
     * @param $w
     * @param $txt
     * @return int
     */
    function NbLines($w, $txt) {
        //Computes the number of lines a MultiCell of width w will take
        $cw = &$this->CurrentFont['cw'];
        if ($w == 0) {
            $w = $this->w - $this->rMargin - $this->x;
        }

        $wmax = ($w - 2 * $this->cMargin) * 1000 / $this->FontSize;
        $s = str_replace("\r", '', $txt);
        $nb = strlen($s);
        if ($nb > 0 and $s[$nb - 1] == "\n") {
            $nb--;
        }

        $sep = -1;
        $i = 0;
        $j = 0;
        $l = 0;
        $nl = 1;
        while ($i < $nb) {
            $c = $s[$i];
            if ($c == "\n") {
                $i++;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
                continue;
            }
            if ($c == ' ') {
                $sep = $i;
            }

            $l += $cw[$c];
            if ($l > $wmax) {
                if ($sep == -1) {
                    if ($i == $j) {
                        $i++;
                    }

                } else {
                    $i = $sep + 1;
                }

                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
            } else {
                $i++;
            }

        }
        return $nl;
    }

    /**
     * @param $text
     */
    function print_header($text) {
        $this->ln(3);
        $this->setFont('Arial', 'B', 10);
        $this->setFillColor(200, 200, 200);
        $this->setTextColor(0, 0, 0);
        $this->Cell(0, 5, $text, 0, 1, 'L', true);
        $this->ln(3);
    }

    /**
     * @param $text
     * @param $width
     * @param $border
     * @param $new_line_ind
     */
    function print_document_label($text, $width, $border, $new_line_ind) {
        $this->setFont('Arial', 'B', 8);
        $this->setFillColor(255, 255, 255);
        $this->Cell($width, $this->ROW_SPACING, $text, $border, $new_line_ind, 'L');
    }

    /**
     * @param $text
     * @param $min_length
     * @param $border
     * @param $new_line_ind
     */
    function print_underline_text($text, $min_length, $border, $new_line_ind) {
        $this->setFont('Arial', 'U', 8);
        $this->setFillColor(255, 255, 255);
        $output_string = $this->pad_text($text, $min_length);
        $this->Cell($min_length, $this->ROW_SPACING, $output_string, $border, $new_line_ind);
    }

    /**
     * @param $text
     * @param $min_length
     * @return string
     */
    function pad_text($text, $min_length) {
        //$output_string = "  " . $text;
        $str_length = strlen($text);
        $output_string = $text;
        while ($str_length < $min_length) {
            $output_string = $output_string . " ";
            $str_length += 1;
        }
        return $output_string;
    }

    /**
     * @param $height
     * @param $text
     */
    function write_bold($height, $text) {
        $this->setFont('', 'B');
        $this->Write($height, $text);
        $this->setFont('', '');
    }

    /**
     * @param string $customer_state
     * @param string $lender_name
     * @param string $lender_dba_name
     * @return string
     */
    public function Generate_Terms_And_Conditions_Block(string $customer_state, string $lender_name, string $lender_dba_name): string {
        $str = 'THE TERMS AND CONDITIONS OF THIS AGREEMENT ARE IMPORTANT.  PLEASE READ THE ENTIRE AGREEMENT CAREFULLY.' . PHP_EOL . 'In this ';
        if ($customer_state == 'MS') {
            $str = $str . 'Credit Availability Act ';
        } else {
            $str = $str . 'Consumer Installment ';
        }
        $str = $str . 'Loan Agreement (this "Agreement") and in the Federal Truth-in-Lending Disclosures (the "Disclosures"), "you" and "your" refer to the Borrower identified above. "We", "us", "our", and "Lender" refer to ';
        $str = $str . $lender_name . ' d/b/a ' . $lender_dba_name . ' or any assignee or subsequent holder of this Agreement. "Loan" means the ';
        if ($customer_state == 'MS') {
            $str = $str . 'Credit Availability Act ';
        } else {
            $str = $str . 'consumer installment ';
        }
        $str = $str . 'loan made by Lender to Borrower under this Agreement and Disclosures. This Agreement contains an arbitration provision. Unless you act promptly to reject the arbitration provision, it will have a substantial effect on your rights in the event of a dispute.';
        if ($customer_state == 'CA') {
            $str = $str . ' This loan is made pursuant to the California Finance Lenders Law, Division 9 (commencing with Section 22000) of the Financial Code.';
            //$pdf->setFont('Arial', '', 10);
        }

        //$pdf->MultiCell(0, 4, $str, 0, 'L');
        return $str;
    }

    /**
     * @param string $customer_state
     * @return string
     */
    public function Generate_Late_Charge_Block(string $customer_state): string {
        error_log("Flow: In Generate_Late_Charge_Block, customer_state=$customer_state\n");
        switch ($customer_state) {
            case 'AL':
                $str = 'If any scheduled payment is not paid in full within 15 days after its due date, you will be charged a late fee of 5% of the payment or $15, whichever is greater, not to exceed $100.';
                break;
            case 'CA':
                $str = 'If any scheduled payment is not paid in full within 10 days after its due date, you will be charged a late fee of $10.';
                break;
            case 'DE':
                $str = 'If a payment is more than 15 days late, you will be charged 5% of the past due installment due.';
                break;
            case 'ID':
                $str = 'If a payment is more than 10 days late, you will be charged the greater of $15 or 5% of the unpaid installment due.';
                break;
            case 'IL':
                $str = 'If a payment is more than 10 days late, you will be charged 5% of the installment due if the installment is in excess of $200, or $10 on installments of $200 or less.';
                break;
            case 'MO':
                $str = 'If a payment is more than 15 days late, you will be charged 5% of the installment due.';
                break;
            case 'MS':
                //Swapna commented to exclude for Mississipi story no 435 - August 9th 2018
                //$str = 'If a payment is more than 15 calendar days late, you will be charged the lesser of $5.00 or 5% of the installment due.';
                $str = '';
                break;
            case 'NM':
                $str = 'If a payment is more than 10 days late, you will be charged 5% of the installment due, up to a maximum of $10.';
                break;
            case 'SC':
                $str = 'If a payment is more than 10 days late, you will be charged the lesser of $18.00 or 5% of the installment due.';
                break;
            case 'UT':
                $str = 'If a payment is more than 10 days late, you will be charged a late fee of $30.';
                break;
            case 'WI':
                $str = 'If you fail to make all or any part of a scheduled installment on or before the 10th day after its scheduled or deferred due date, you may be charged $10.00 or 5% of the unpaid amount of the installment, whichever is less. If interest is assessed after maturity, no late payment fee will be charged on the final installment.';
                break;
            default:
                $str = 'If a payment is more than 15 days late, you will be charged 5% of the installment due.';
        }

        return $str;
    }
    /**
     * @param string $string_to_display
     * @return string
     */
    /*public function Generate_Pdf_bold_string(string $string_to_display): string
    {
        error_log("Flow: In Generate_Pdf_bold_string, string_to_display=$string_to_display\n");
        return "<strong>".$string_to_display."</strong>";
    }*/

    function SetStyles($styles) {
        $this->styles = $styles;
    }

    /*$this->WriteHTML($w, 5, $data[$i], 0, $a);
    elseif($this->ALIGN=='center')
                    $this->Cell($w,$p,$e,0,1,$a);
                else{
                    //$this->Write($p,$e);
                    $this->Write($w, $p, $e, $pos, $a);
                }*/
    function WriteHTML($w,$p,$html,$pos,$a)
    {
        //HTML parser
        $html=str_replace("\n",' ',$html);
        $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
        foreach($a as $i=>$e)
        {
            if($i%2==0)
            {
                //Text
                if($this->HREF)
                    $this->PutLink($this->HREF,$e);
                elseif($this->ALIGN=='center')
                    $this->Cell($w,5,$e,0,1,'C');
                else
                    $this->Write($w,$e);
            }
            else
            {
                //Tag
                if($e[0]=='/')
                    $this->CloseTag(strtoupper(substr($e,1)));
                else
                {
                    //Extract properties
                    $a2=explode(' ',$e);
                    $tag=strtoupper(array_shift($a2));
                    $prop=array();
                    foreach($a2 as $v)
                    {
                        if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                            $prop[strtoupper($a3[1])]=$a3[2];
                    }
                    $this->OpenTag($tag,$prop);
                }
            }
        }
    }

    function OpenTag($tag,$prop)
    {
        //Opening tag
        if($tag=='B' || $tag=='I' || $tag=='U')
            $this->SetStyle($tag,true);
        if($tag=='A')
            $this->HREF=$prop['HREF'];
        if($tag=='BR')
            $this->Ln(5);
        if($tag=='P')
            $this->ALIGN=$prop['ALIGN'];
        if($tag=='HR')
        {
            if( !empty($prop['WIDTH']) )
                $Width = $prop['WIDTH'];
            else
                $Width = $this->w - $this->lMargin-$this->rMargin;
            $this->Ln(2);
            $x = $this->GetX();
            $y = $this->GetY();
            $this->SetLineWidth(0.4);
            $this->Line($x,$y,$x+$Width,$y);
            $this->SetLineWidth(0.2);
            $this->Ln(2);
        }
    }

    function CloseTag($tag)
    {
        //Closing tag
        if($tag=='B' || $tag=='I' || $tag=='U')
            $this->SetStyle($tag,false);
        if($tag=='A')
            $this->HREF='';
        if($tag=='P')
            $this->ALIGN='';
    }

    function SetStyle($tag,$enable)
    {
        //Modify style and select corresponding font
        $this->$tag+=($enable ? 1 : -1);
        $style='';
        foreach(array('B','I','U') as $s)
            if($this->$s>0)
                $style.=$s;
        $this->SetFont('',$style);
    }

    function PutLink($URL,$txt)
    {
        //Put a hyperlink
        $this->SetTextColor(0,0,255);
        $this->SetStyle('U',true);
        $this->Write(5,$txt,$URL);
        $this->SetStyle('U',false);
        $this->SetTextColor(0);
    }
}
