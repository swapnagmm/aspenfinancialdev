<?php

function mm_get_lp_credentials(){
    $return_array = array();
    $return_array["token"] = "Bearer 9d44ea12d24e145abb78dac37b7f5d82d539c5dd"; // dev
    $return_array["tenant_id"] = "5200816";

    return $return_array;
}

function mm_determine_term_for_state($state, $pay_frequency, $requested_amt, $apr) {
    error_log("flow: mm_determine_term_for_state: $state, $pay_frequency, $requested_amt, $apr\n");
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    
    // $sql_string = "select number_of_payments from mm_pricing_detail where state = ? and payment_freq = ? and loan_amt = ? and apr = ?";
    // try {
    //     $conn = mm_get_pdo_connection();
    //     $stmt = $conn->prepare($sql_string);
    //     $params = [$state, $pay_frequency, $requested_amt, $apr];
    //     $stmt->execute($params);
    //     $row = $stmt->fetch(PDO::FETCH_ASSOC);
    //     $return_array["term"] = $row['number_of_payments'];
    $return_array["term"] = 24; 

    // } catch (PDOException $e) {
    //     //$return_array["return_value"] = 1;
    //     //$return_array["return_message"] = "$e";
    //     $return_value = 1;
    //     $return_message = "$e";
    //     $return_array["sql_string"] = $sql_string;
    //     //return $return_array;
    // }

    // if (is_resource($conn)) {
    //     $conn = null;
    // }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    
    return $return_array;
}

function mm_create_lp_loan($loan_data, $custom_fields = null) {
    //error_log("flow: mm_create_lp_loan\n");

    $lp_credentials = mm_get_lp_credentials();
    $account_token = $lp_credentials["token"];
    $auto_pal_id = $lp_credentials["tenant_id"];
    $url = "https://loanpro.simnang.com/api/public/api/1/odata.svc/Loans";
    $header_array = array("Content-Type: application/json", 'Accept: application/json', "Authorization: $account_token", "Autopal-Instance-ID: $auto_pal_id");
    $request_arr = array();
    //Perform some validation on the data that needs to be set for
    $application_nbr = $loan_data["application_nbr"];
    $state = $loan_data["state"];
    $requested_deposit_type = $loan_data["requested_deposit_type"];
    $pay_frequency = $loan_data["pay_frequency"];
    $temp_dt = new DateTime($loan_data["effective_dt"]);
    $effective_dt = $temp_dt->format('Y-m-d');
    $temp_dt = new DateTime($loan_data["orig_dt"]);
    //$orig_dt = $temp_dt->format('Y-m-d');
    $temp_dt = new DateTime($loan_data["first_due_dt"]);
    $first_due_dt = $temp_dt->format('Y-m-d');
    $last_day_of_month = date("Y-m-t", strtotime($first_due_dt));
    $requested_amt = $loan_data["requested_amt"];
    $apr = $loan_data["apr"];
    //Reformat Some of the data as Needed
    $display_id = str_pad($application_nbr, 8, '0', STR_PAD_LEFT);
    $display_id = "L" . $display_id;
    $loan_setup_arr = array();
    $loan_setup_arr["loanClass"] = "loan.class.consumer";
    $loan_setup_arr["loanType"] = "loan.type.installment";
    $loan_setup_arr["loanAmount"] = $requested_amt;
    $loan_setup_arr["loanRate"] = $apr;
    $loan_setup_arr["loanRateType"] = "loan.rateType.annually";

    if ($pay_frequency == "Bi-Weekly" || $pay_frequency == "Paid Weekly") {
        $loan_setup_arr["paymentFrequency"] = "loan.frequency.biWeekly";
        $tmp_pay_frequency = 'B';
    } else if ($pay_frequency == "Paid Monthly") {
        $loan_setup_arr["paymentFrequency"] = "loan.frequency.monthly";
        //Check to see if the date selected is the last day of the month and if it is set the Last Day of Month Flag.  This will force a payment schedule so that all payments are on the last day of the month
        if ($first_due_dt == $last_day_of_month) {
            $loan_setup_arr["dueDateOnLastDOM"] = "1";
        }
        $tmp_pay_frequency = 'M';
    } else if ($pay_frequency == "Paid Twice Monthly") {
        $loan_setup_arr["paymentFrequency"] = "loan.frequency.semiMonthly";
        $tmp_pay_frequency = 'S';
    }

    $term_result = mm_determine_term_for_state($state, $tmp_pay_frequency, $requested_amt, $apr);
    if ($term_result['return_value'] == 0) {
        $loan_setup_arr["loanTerm"] = $term_result['term'];
    } else {
        // error happened
        $term_return_message = $term_result['return_message'];
        error_log("Failed Getting Loan Term for state: $state, pay frequency: $pay_frequency, requested amount: $requested_amt, apr: $apr with error: $term_return_message");
    }

    $loan_setup_arr["contractDate"] = $effective_dt;
    $loan_setup_arr["firstPaymentDate"] = $first_due_dt;
    $loan_setup_arr["calcType"] = "loan.calcType.simpleInterest";
    $loan_setup_arr["daysInYear"] = "loan.daysInYear.actual";
    $loan_setup_arr["interestApplication"] = "loan.interestApplication.betweenTransactions";
    $loan_setup_arr["begEnd"] = "loan.begend.end";
    $loan_setup_arr["firstPeriodDays"] = "loan.firstPeriodDays.actual";
    $loan_setup_arr["firstDayInterest"] = "loan.firstdayinterest.yes";
    $loan_setup_arr["discountCalc"] = "loan.discountCalc.straightLine";
    $loan_setup_arr["diyAlt"] = "loan.diyAlt.no";
    $loan_setup_arr["roundDecimals"] = "5";
    $loan_setup_arr["lastAsFinal"] = "loan.lastasfinal.yes";
    $loan_setup_arr["curtailPercentBase"] = "loan.curtailpercentbase.loanAmount";
    $loan_setup_arr["nddCalc"] = "loan.nddCalc.standard";
    $loan_setup_arr["endInterest"] = "loan.endInterest.no";
    $loan_setup_arr["feesPaidBy"] = "loan.feesPaidBy.date";
    $loan_setup_arr["graceDays"] = $loan_data["late_fee_grace_period"];
    if ($loan_data["late_fee_calc"] == "GT") {
        $loan_setup_arr["lateFeeType"] = "loan.lateFee.4";
    } else if ($loan_data["late_fee_calc"] == "LT") {
        $loan_setup_arr["lateFeeType"] = "loan.lateFee.5";
    } else if ($loan_data["late_fee_calc"] == "AMT") {
        $loan_setup_arr["lateFeeType"] = "loan.lateFee.2";
    } else if ($loan_data["late_fee_calc"] == "PCT") {
        $loan_setup_arr["lateFeeType"] = "loan.lateFee.3";
    } else if ($loan_data["late_fee_calc"] == "STD") {
        $loan_setup_arr["lateFeeType"] = "loan.lateFee.2";
    }
    $loan_setup_arr["lateFeeAmount"] = $loan_data["late_fee_amt"];
    $loan_setup_arr["lateFeePercent"] = $loan_data["late_fee_pct"];
    $loan_setup_arr["lateFeeCalc"] = "loan.lateFeeCalc.standard";
    $loan_setup_arr["lateFeePercentBase"] = "loan.latefeepercentbase.regular";
    $loan_setup_arr["paymentDateApp"] = "loan.pmtdateapp.actual";

    $portfolio_arr = array();
    $portfolio_results_arr = array();
    $portfolio_metadata_arr = array();

    //Assign the state level portfolio
    if ($state == "MO") {
        $portfolio_value = 27;
    } else if ($state == "UT") {
        $portfolio_value = 28;
    } else if ($state == "NM") {
        $portfolio_value = 30;
    } else if ($state == "MS") {
        $portfolio_value = 31;
    } else if ($state == "CA") {
        $portfolio_value = 29;
    } else if ($state == "WI") {
        $portfolio_value = 34;
    } else if ($state == "SC") {
        $portfolio_value = 32;
    } else if ($state == "AL") {
        $portfolio_value = 33;
    } else if ($state == 'DE') {
        $portfolio_value = 37;
    } else if ($state == 'ID') {
        $portfolio_value = 42;
    } else {
        $portfolio_value = 36;
        mm_log_error('mm_create_lp_loan', "Non-Critical Added a loan for State $state and was assigned to the other portfolio");
    }

    $portfolio_metadata_arr["type"] = "Entity.Portfolio";
    $portfolio_metadata_arr["uri"] = '/api/1/odata.svc/Portfolios(id=25)';
    $portfolio_results_arr[0]["__metadata"] = $portfolio_metadata_arr;


    //Assign a portfolio based on the deposit preference of the customer
    if ($requested_deposit_type == "ACH") {
        //Customer signed up for ACH Authorization
        $portfolio_value = 6;
    } else {
        //Customer signed up for Manual Check
        $portfolio_value = 7;
    }

    $portfolio_metadata_arr["type"] = "Entity.Portfolio";
    $portfolio_metadata_arr["uri"] = '/api/1/odata.svc/Portfolios(id=6)';
    $portfolio_results_arr[1]["__metadata"] = $portfolio_metadata_arr;

    $portfolio_arr["results"] = $portfolio_results_arr;

    //Add sub-portfolios

    $subportfolio_arr = array();
    $subportfolio_metadata_arr = array();
    $subportfolio_results_arr = array();

    //Add stater sub-portfolio type

    //Assign the state level portfolio
    if ($state == "MO") {
        $portfolio_value = 27;
    } else if ($state == "UT") {
        $portfolio_value = 28;
    } else if ($state == "NM") {
        $portfolio_value = 30;
    } else if ($state == "MS") {
        $portfolio_value = 31;
    } else if ($state == "CA") {
        $portfolio_value = 29;
    } else if ($state == "WI") {
        $portfolio_value = 34;
    } else if ($state == "SC") {
        $portfolio_value = 32;
    } else if ($state == "AL") {
        $portfolio_value = 33;
    } else if ($state == 'DE') {
        $portfolio_value = 37;
    } else if ($state == 'ID') {
        $portfolio_value = 42;
    } else {
        $portfolio_value = 36;
        mm_log_error('mm_create_lp_loan', "Non-Critical Added a loan for State $state and was assigned to the other portfolio");
    }

    $subportfolio_metadata_arr["type"] = "Entity.SubPortfolio";
    $subportfolio_metadata_arr["uri"] = '/api/1/odata.svc/SubPortfolios(id=' . $portfolio_value . ')';
    $subportfolio_results_arr[0]["__metadata"] = $subportfolio_metadata_arr;


    if ($requested_deposit_type == "ACH") {
        //Customer signed up for ACH Authorization
        $portfolio_value = 40;
    } else {
        //Customer signed up for Manual Check
        $portfolio_value = 41;
    }

    $subportfolio_metadata_arr["type"] = "Entity.SubPortfolio";
    $subportfolio_metadata_arr["uri"] = '/api/1/odata.svc/SubPortfolios(id=' . $portfolio_value . ')';
    $subportfolio_results_arr[1]["__metadata"] = $subportfolio_metadata_arr;

    $subportfolio_arr["results"] = $subportfolio_results_arr;

    $loan_settings_arr = array();
    //Austin W - Setup for setting custom fields for new values
    $results = array();
    if (isset($custom_fields)) {
        foreach($custom_fields as $key => $value) {
            switch ($key) {
                case 'FICO Score':
                    // $tmp = new StdClass();
                    // $tmp->customFieldId = 18;
                    // $tmp->customFieldValue = $value;
                    $tmp = array("customFieldId" => 18, "customFieldValue" => $value);
                    break;
                case 'Gross Monthly Income':
                    // $tmp = new StdClass();
                    // $tmp->customFieldId = 19;
                    // $tmp->customFieldValue = $value;
                    $tmp = array("customFieldId" => 19, "customFieldValue" => $value);
                    break;
                case 'ACH Authorization Acknowledgement':
                    // $tmp = new StdClass();
                    // $tmp->customFieldId = 20;
                    // $tmp->customFieldValue = $value;
                    $tmp = array("customFieldId" => 20, "customFieldValue" => $value);
                    break;
            }
            array_push($results, $tmp);
        }

    }
    // $loan_settings_customfield_arr = array(
    //     "CustomFieldValues" => array(
    //         "results" => $results
    //     )
    // );
    $loan_settings_customfield_arr = array("results"=>$results);
    //$loan_settings_customfield_arr["results"] = $results;

    //echo var_dump($loan_settings_customfield_arr);
    echo json_encode($loan_settings_customfield_arr);

    //Check to see if the loan already exists
    $loan_agreement_nbr = $loan_data["loan_agreement_nbr"];
    if ($loan_agreement_nbr == 0 || $loan_agreement_nbr == '') {
        //The loan should already exist on LoanPro
        $loan_settings_arr["loanStatusId"] = "1";
        $loan_settings_arr["loanSubStatusId"] = "1";
        $loan_settings_arr["autopayEnabled"] = "1";
        $loan_settings_arr["CustomFieldValues"] = $loan_settings_customfield_arr;
    } else {
        $la_details = mm_get_loan_agreement_details($loan_agreement_nbr);
        $lp_loan_settings_id = $la_details["lp_loan_settings_id"];
        $lp_loan_setup_id = $la_details["lp_loan_setup_id"];
        $lp_loan_id = $la_details["lp_loan_id"];
        $loan_settings_arr["__id"] = $lp_loan_settings_id;
        $loan_settings_arr["__update"] = true;
        $loan_setup_arr["__id"] = $lp_loan_setup_id;
        $loan_setup_arr["__update"] = true;
        $url = $url . "(id=$lp_loan_id)";
    }


    //finalize the request array
    $request_arr["displayId"] = $display_id;
    $request_arr["title"] = 'Personal Loan';
    $request_arr["LoanSettings"] = $loan_settings_arr;
    $request_arr["LoanSetup"] = $loan_setup_arr;
    $request_arr["Portfolios"] = $portfolio_arr;
    $request_arr["SubPortfolios"] = $subportfolio_arr;

    $json_request = json_encode($request_arr);
    error_log("Create Loan: Json Request = $json_request");

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header_array);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $json_request);
    curl_setopt($curl, CURLOPT_HTTPGET, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    if ($loan_agreement_nbr > 0) {
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
    }

    $json_response = curl_exec($curl);
    error_log("Create Loan: Json Response = $json_response");

    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    if ($status != 201 && $status != 200) {
        //There was an error in the message sending  Add error handling
        $return_value = 1;
        $return_message = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);
        echo "$return_message\n";
    } else {
        $response_array = json_decode($json_response, true);
        $d = $response_array["d"];
        $display_id = $d["displayId"];
        $return_value = 0;
        $return_message = '';
    }
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["display_id"] = $display_id;
    return $return_array;

}

$params = array(
'application_nbr' => '7203',
//'application_nbr' => '8552',
'loan_agreement_nbr' => '',
'state' => 'MO',
'requested_deposit_type' => 'ACH',
'pay_frequency' => 'Paid Monthly',
'effective_dt' => '07/09/2018',
'first_due_dt' => '2018-07-30',
'orig_dt' => '07/08/2018',
'requested_amt' => '1500.00',
'apr' => '213.27',
'late_fee_amt' => '15.00',
'late_fee_pct' => '5.00',
'late_fee_calc' => 'AMT',
'returned_item_fee_amt' => '0.00',
'late_fee_grace_period' => '15');
$custom_fields = array(
    'FICO Score' => '540',
    'Gross Monthly Income' => '399.89',
    'ACH Authorization Acknowledgement' => 'I agree to ACH'
);
mm_create_lp_loan($params, $custom_fields);