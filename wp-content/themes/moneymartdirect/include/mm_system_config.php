<?php

require_once('constants.php');

date_default_timezone_set('America/Chicago');

function baseUrl(){
    return sprintf("%s://%s",
        isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
        $_SERVER['SERVER_NAME'] . BASE_DIR
    );
}

function mm_get_db_credentials(){
    $return_array = array();
    $return_array["db_server_name"] = "localhost";
    $return_array["db_user_name"]   = MM_DB_USER_NAME;
    $return_array["db_password"]    = MM_DB_PASSWORD;
    $return_array["db_name"]        = MM_DB_NAME;

    return $return_array;
}

function mm_get_lp_credentials(){
    $return_array = array();
    $return_array["token"] = "Bearer 9d44ea12d24e145abb78dac37b7f5d82d539c5dd";
    $return_array["tenant_id"] = "5200816";

    return $return_array;
}

function mm_get_lp_environment_portfolio(){
    //For test return the value 4 and for production return the value 5
    return "4";
}

function mm_get_pci_credentials(){
    $return_array = array();
    $return_array["username"] = "support@moneymartdirect.com";
    $return_array["password"] = "Direct2Con!19";

    return $return_array;
}

function mm_get_ftp_details(){
    $return_array = array();
    $return_array["server"] = "moneymartdirect.com";
    $return_array["password"] = "gk3EQ3yJWaN7";
    $return_array["username"]  = "mm_web_documents_dev@moneymartdirect.com";
    $return_array["port"] = "21";

    return $return_array;
}

function mm_get_fa_ftp_details(){
    $return_array = array();
    $return_array["server"] = "moneymartdirect.com";
    //$return_array["password"] = "66zStncj#fW8";
    $return_array["password"] = "#R)0d8Xc#X{7";
    $return_array["username"]  = "fa_dev_ftp@aspenfinancialdirect.com";
    //$return_array["username"]  = "fa_ftp@aspenfinancialdirect.com";
    $return_array["port"] = "21";

    return $return_array;
}
//Code Added by Swapna on Aug 28th 2018 for AFDS-522
function mm_get_pending_ftp_details(){
    $return_array = array();
    $return_array["server"] = "moneymartdirect.com";
    $return_array["password"] = "!@#$%^@mm";
    $return_array["username"]  = "dev_pending_fa_ftp@moneymartdirect.com";
    $return_array["port"] = "21";

    return $return_array;
}

function mm_get_pdo_connection(){
    $credentials = mm_get_db_credentials();
    $db_server_name = $credentials["db_server_name"];
    $db_user_name = $credentials["db_user_name"];
    $db_password = $credentials["db_password"];
    $db_name = $credentials["db_name"];        $conn = new PDO("mysql:host=$db_server_name;dbname=$db_name", $db_user_name, $db_password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    return $conn;
}

function mm_get_db_connection(){
    $credentials = mm_get_db_credentials();
    $db_server_name = $credentials["db_server_name"];
    $db_user_name = $credentials["db_user_name"];
    $db_password = $credentials["db_password"];
    $db_name = $credentials["db_name"];
    $conn = new mysqli($db_server_name, $db_user_name, $db_password, $db_name);

    return $conn;
}

function get_lms_connection_details(){
}

function mm_get_log_path(){
    $log_path = MM_LOG_PATH;
    return $log_path;
}

function mm_get_document_path(){
    $log_path = MM_LOG_PATH;
    return $log_path;
}

function get_origination_system_connection_details(){
	$return_array = array();
	$header_array = array();
	//$return_array["url"] = "http://kleverlend-dev-app.azurewebsites.net/api/";
    $return_array["url"] = "http://moneymart-dev-app.azurewebsites.net/api/";
    $header_array = array("Content-type: application/json", "key: Authorization", "Authorization: Basic dGVzdDp0ZXN0", "Description: \"\"");
	$return_array["UserName"] = "vivacetest1";
	$return_array["Password"] = "Password123";
	$return_array["ApplicationSource"] = 'TestSource';
	$return_array["AuthToken"] = '123ABCDEF';
    $return_array["PortfolioID"] = '1';
	$return_array["header_array"] = $header_array;

	return $return_array;
}

function mm_get_ach_server_details(){
    $return_array = array();
    $return_array["server"] = "transfer.greenbank.com";
    $return_array["username"] = "ASPEN_LTD";
    $return_array["password"] = "ZcF77fGr";
    $return_array["port"] = "992";

    return $return_array;
}

function mm_get_ach_path(){
    return MM_ACH_DOCS_PATH;
}

function mm_get_document_storage_path(){
    return MM_CUST_DOCS_PATH;
}

function mm_create_cache() {
    if (!class_exists('Memcached'))
    {
        return false;
    }
    $cache = new Memcached();
    $cache->addServer('127.0.0.1', 11211);
    return $cache;
}

function mm_set_cache_value($key, $value, $expiration=60) {
    error_log("mark: setting cache key: $key");
    if (ENVIRONMENT != "LOCAL") {
        mm_create_cache ()->set ($key, $value, $expiration);
    }
}

function mm_get_cache_value($key) {
    error_log("mark: getting cache key: $key");
    if (ENVIRONMENT != "LOCAL") {
        return mm_create_cache ()->get ($key);
    }
    return null;
}


function error_log_vardump(string $text, $obj) {
    error_log("$text".var_export($obj,true));
}

function error_log_dump_associative_array($pretext, $inarray) {
    error_log($pretext);
    if (is_array($inarray)) {
        foreach ($inarray as $key => $value) {
            if (is_array($value)){
                foreach ($value as $key2 => $value2) {
                    error_log("'$key2' => '$value2'");
                }
            }
            else {
                error_log("'$key' => '$value'");
            }
        }
    }
    else
        error_log_vardump("Not an array passed into error_log_dump_associative_array.", $inarray);
}

function array_group_by(array $array, $key)
	{
		if (!is_string($key) && !is_int($key) && !is_float($key) && !is_callable($key) ) {
			trigger_error('array_group_by(): The key should be a string, an integer, or a callback', E_USER_ERROR);
			return null;
		}
		$func = (!is_string($key) && is_callable($key) ? $key : null);
		$_key = $key;
		// Load the new array, splitting by the target key
		$grouped = [];
		foreach ($array as $value) {
			$key = null;
			if (is_callable($func)) {
				$key = call_user_func($func, $value);
			} elseif (is_object($value) && isset($value->{$_key})) {
				$key = $value->{$_key};
			} elseif (isset($value[$_key])) {
				$key = $value[$_key];
			}
			if ($key === null) {
				continue;
			}
			$grouped[$key][] = $value;
		}
		// Recursively build a nested grouping if more parameters are supplied
		// Each grouped array value is grouped according to the next sequential key
		if (func_num_args() > 2) {
			$args = func_get_args();
			foreach ($grouped as $key => $value) {
				$params = array_merge([ $value ], array_slice($args, 2, func_num_args()));
				$grouped[$key] = call_user_func_array('array_group_by', $params);
			}
		}
		return $grouped;
	}
