<div id="password-lost-form" class="widecolumn">
	<?php if ( $attributes['show_title'] ) : ?>
		<h2><?php _e( 'Forgot Your Password?', 'personalize-login' ); ?></h2>
	<?php endif; ?>
	
	<!-- Show errors if there are any -->
	<?php if ( count( $attributes['errors'] ) > 0 ) : ?>
		<?php foreach ( $attributes['errors'] as $error ) : ?>
			<p class="login-error">
				<?php echo $error; ?>
			</p>
		<?php endforeach; ?>
	<?php endif; ?>
	
	<?php if ( $attributes['lost_password_sent'] ) : ?>
		<p class="login-info">
			<?php _e( 'Check your email for a link to reset your password.', 'personalize-login' ); ?>
		</p>
	<?php endif; ?>
	
	
	<div class="loginContainer">
		<div class="titleIntro">
			<div class="loginTitle">
				<h2>Forgot Your Password?</h2>
			</div>
			<div class="loginIntro">
				<?php
					_e(
						"Enter your email address and we'll send you a link to reset your password.",
						'personalize_login'
					);
				?>
			</div>
		</div>
		<div class="ffcLoginForm">
			<?php $login_url = home_url( 'password-reset-sent' ); ?>
			<form id="lostpasswordform" action="<?php echo wp_lostpassword_url($login_url); ?>" method="post">
				<p class="form-row">
					<label for="user_login"><?php _e( 'Email', 'personalize-login' ); ?>
					<input type="text" name="user_login" id="user_login">
				</p>
		
				<p class="lostpassword-submit">
					<input type="submit" name="submit" class="lostpassword-button"
					       value="<?php _e( 'Reset Password', 'personalize-login' ); ?>"/>
				</p>
			</form>
		</div>
	</div>
</div>