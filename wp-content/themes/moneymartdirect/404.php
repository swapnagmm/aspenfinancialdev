<?php get_header(); ?>

<div class="loginContainer notFoundPage">
	<div class="titleIntro notFoundContainer">
	<!--If no results are found-->
		<div class="loginTitle"> 
			<h2 class="notFoundTitle"><?php esc_html_e('No Results Found','Divi'); ?></h2>
		</div>
		<div class="loginIntro">
			<p class="notFoundContent"><?php esc_html_e('The page you requested could not be found.','Divi'); ?></p>
		</div>
	</div>
</div>
<!--End if no results are found-->


<?php get_footer(); ?>
