<?php

require "mm_middleware.php";

if (is_ajax()) {
    if (isset($_POST["state_cd"]) && !empty($_POST["state_cd"])
        && isset($_POST["approved_amt"]) && !empty($_POST["approved_amt"])
        && isset($_POST["pay_freq"]) && !empty($_POST["pay_freq"])
        && isset($_POST["contract_apr"]) && !empty($_POST["contract_apr"])
        && isset($_POST["mm_application_nbr"]) && !empty($_POST["mm_application_nbr"])) { //Checks if needed values are set
        $approved_amt = $_POST["approved_amt"];
        $state_cd = $_POST["state_cd"];
        $pay_freq = $_POST["pay_freq"];
        $contract_apr = $_POST["contract_apr"];

        $mm_application_nbr = $_POST["mm_application_nbr"];
        error_log("mark: values passed to get_pricing_values: $approved_amt, $state_cd, $pay_freq, $contract_apr, $mm_application_nbr\n");
        //$tmp = $_SESSION['app_approved_apr'];
        //error_log("mark: apr in session: $tmp\n");

        $results = mm_get_pricing_values($state_cd, $approved_amt, $pay_freq, $contract_apr, $mm_application_nbr);

        $tmp = $results["return_value"];

        if ($tmp == 0)
        {
            header('Content-Type: application/json');
            echo json_encode($results);
        }
        else
        {
            error_log("mark: an error is being returned from get_pricing_values to the AJAX caller: tmp: $tmp\n");
            header('HTTP/1.1 500 Internal Server');
            header('Content-Type: application/json; charset=UTF-8');
            die(json_encode(array('message' => $results["An Error occurred, check log."])));
        }
        //echo json_encode($results);
    }
}

//Function to check if the request is an AJAX request
function is_ajax() {
    return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}

