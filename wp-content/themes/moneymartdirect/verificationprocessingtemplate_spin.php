<?php
/*
Template Name: VERIFICATION PROCESSING W/SPIN
 */

get_header();

require_once $_SERVER['DOCUMENT_ROOT'] . BASE_DIR . "/wp-content/themes/moneymartdirect/include/mm_system_config.php";

?>
<div id="agreementPage">
<?php

// Check if user is logged in
if (!is_user_logged_in()) {
    //$post_id = 646;
    $post_id = 923;
    $queried_post = get_post($post_id);
    //$title = $queried_post->post_title;
    //echo $title;
    $content = $queried_post->post_content;
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    echo $content;

/*
echo "<h2>Sign in</h2>";
wp_login_form( array( 'echo' => true ) );
 */
} else {

    //Attempt to restore a session
    global $current_user;
    //get_currentuserinfo();
    wp_get_current_user();
    $email_address = $current_user->user_email;
    mm_restore_session($email_address);
     $uw_decision = isset($_SESSION["app_uw_decision"]) ? $_SESSION["app_uw_decision"] : '';
     $uw_decision = 'Approved';
    
    //echo $_SESSION["app_vf_decision"];
    //Code Start - Added by Swapna on July 19th 2018 for story 320
    $app_manual_approval = 0;
    $app_preverif_lead_id = '';
    
    if($_SESSION["app_vf_decision"] == "ACCEPTED PENDING CUSTOMER DOCUMENTS"){
         $conn = mm_get_db_connection();
        $sql_string = "select * from mm_application where application_nbr=?";
        $stmt = $conn->prepare($sql_string);
        $stmt->bind_param('i', $_SESSION['app_application_nbr']);
        $stmt->execute();
        $rows = $stmt->get_result();
        foreach ($rows as $row) {
            $app_manual_approval = $row['manual_approval'];
            $app_preverif_lead_id = $row['preverif_lead_id'];
        }
    }
    //Code End - Added by Swapna on July 19th 2018 for story 320

    //Build the Objects as hidden for now
    ?>
		<div class="verificationContent pendingDocs" id="app_pend_customer_docs" style="display:none">
		<?php
$post_id = 733;
    $queried_post = get_post($post_id);
    $content = $queried_post->post_content;
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    echo $content;
    ?>
		</div>
		<div class="verificationContent pendingDocs" id="app_pend_customer_docs_illinois" style="display:none">
		<?php
        $post_id = 2224;
        if (SITE_URI_PREFIX == '') {
            // prod
            $post_id = 2213;
        }
    $queried_post = get_post($post_id);
    $content = $queried_post->post_content;
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    echo $content;
    ?>
		</div>
		
		<div class="verificationContent pendingReview" id="app_pend_agent_review" style="display:none">
		<?php
$post_id = 775;
    $queried_post = get_post($post_id);
    $content = $queried_post->post_content;
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    echo $content;
    ?>
		</div>
		<div class="verificationContent error" id = "app_error" style="display:none">
		<?php
$post_id = 735;
    $queried_post = get_post($post_id);
    $content = $queried_post->post_content;
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    echo $content;
    ?>
		</div>
		<div class="verificationContent accepted" id="app_accepted" style="display:none">
			<?php

    $post_id = 731;
    $queried_post = get_post($post_id);
    $content = $queried_post->post_content;
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    echo $content;

    ?>

			 <?php gravity_form(19, false, false, true, '', true);?>
			 <!--<div class="embedPDF" style="height:auto;" id="embedPDF"></div>-->

	</div>

	<div class="resultContent approved" id="app_refresh_verif" style="display:none">
		<?php
// $postid = url_to_postid( $url );
    $post_id = 1521;
    $queried_post = get_post($post_id);
    $content = $queried_post->post_content;
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    echo $content;
    ?>
		</div>


<?php
if ($uw_decision == '') {
        //The customer should never reach this page without the uw decision being set.  They either went directly to the page or hit a back button from the funding page.
        ?>
    <script type="text/javascript">    var x = document.getElementById('app_refresh_verif'); x.style.display = 'block';</script>


<?php
//Code - Modified by Swapna on July 19th 2018 for story 320
} else if (!isset($_SESSION["app_vf_decision"]) || ($app_manual_approval== '1' && $_SESSION["app_vf_decision"] == "ACCEPTED PENDING CUSTOMER DOCUMENTS")) {
         $_SESSION["app_vf_decision"] = "Processing";
         //echo "I get here<br>";
        ?>
         <div id="overlay" align="center">
	        <div class="loaderContainer">
		        <div class="loader"> </div>
		        <p style="color:white"><b>Please wait while we process your application</b></p>
		        <p style="color:white"><b>This may take up to 60 seconds</b></p>
        	</div>
            </div>

        <script type="text/javascript">
        <?php
        //Code - Modified by Swapna on July 19th 2018 for story 320
        $preverif_lead_id = (isset($_SESSION['app_preverif_lead_id'])) ? $_SESSION['app_preverif_lead_id'] : $app_preverif_lead_id;
        $application_nbr = (isset($_SESSION['app_application_nbr'])) ? $_SESSION['app_application_nbr'] : '';
        $requested_amt = (isset($_SESSION['app_requested_amt'])) ? $_SESSION['app_requested_amt'] : 0;
        $requested_deposit_type = (isset($_SESSION['requested_deposit_type'])) ? $_SESSION['requested_deposit_type'] : '';
        ?>

        var preverif_lead_id = "<?php echo $preverif_lead_id; ?>";
        var application_nbr = "<?php echo $application_nbr; ?>";
        var requested_amt = "<?php echo $requested_amt; ?>";
        var requested_deposit_type = "<?php echo $preverif_lead_id; ?>";
        //alert("debug on verify test one");
        var data = {
             "preverif_lead_id": preverif_lead_id,
            "application_nbr" : application_nbr,
            "requested_amt" : requested_amt,
            "requested_deposit_type" : requested_deposit_type
        };
        data = jQuery(this).serialize() + "&" + jQuery.param(data);

        //console.log("about to hit process_verifications.php");
        //console.log(data);
        jQuery.ajax({
            url: "<?php echo baseUrl(); ?>/wp-content/themes/moneymartdirect/include/process_verifications.php",
            type: "POST",
            data: data,
            //content-type: 'application/json',
            dataType: "text json",
                success: function(data) {
                // return_value = data.return_value;

                var  data2 = {
                    "application_nbr": application_nbr
                };

                vf_decision = data.vf_decision;
                vf_state = data.vf_state;

                if(vf_decision == "Accepted"){
                    //console.log("verification accepted");
                    // make this synchronous, so that the PDF is displayed
                    // (async function() {
                    //     let result;
                    //     try {
                    //         result = await jQuery.ajax({
                    //             url: "<?php //echo baseUrl(); ?>/wp-content/themes/moneymartdirect/include/create_loan_agreement.php",
                    //             type: 'POST',
                    //             dataType: "text json",
                    //             data: data2,
                    //         });
                    //         return result;
                    //     }
                    //     catch (error) {
                    //         jQuery("#app_error").show();
                    //         jQuery("#overlay").hide();
                    //     }
                    // })().then( (data) => {
                    //         document_path = data.return_path;
                    //         obj = "<p><font color=\"red\"><b>Loan Agreement below - If the document doesn't display, please refresh the page. </b></font></p><iframe src=\"https://docs.google.com/gview?url="+ document_path + "&embedded=true\" style=\"width:600px; height:4700px;\" frameborder=\"0\"></iframe>";
                    //         jQuery("#embedPDF").append(obj);
                    //         jQuery("#app_accepted").show();
                    //         jQuery('#overlay').hide();
                    // });
                    //console.log("about to hit create_loan_agreement.php");
                    //console.log(data2);
                    jQuery.ajax({
                        url: "<?php echo baseUrl(); ?>/wp-content/themes/moneymartdirect/include/create_loan_agreement.php",
                        type: 'POST',
                        dataType: "text json",
                        data: data2,
                        success: function (data) {
                            //console.log("success with create_loan_agreement");
                            document_path = data.return_path;
                            obj = "<p><font color=\"red\"><b>Loan Agreement below - If the document doesn't display, please refresh the page. </b></font></p><iframe src=\"https://docs.google.com/gview?url="+ document_path + "&embedded=true\" style=\"width:600px; height:4700px;\" frameborder=\"0\"></iframe>";
                            jQuery("#embedPDF").append(obj);
                            jQuery("#app_accepted").show();
                            jQuery('#overlay').hide();
                        },
                        error: function (xhr, status, errorThrown) {
                            //console.log("error with create_loan_agreement: status " + status + ", errorThrown " + errorThrown);
                            jQuery("#app_error").show();
                            jQuery("#overlay").hide();
                        }
                    });
                }else if(vf_decision == "ACCEPTED PENDING CUSTOMER DOCUMENTS"){
                    //console.log("decision accepted pending customer documents");
                    if(vf_state == 'IL'){
                        //console.log("state of IL");
                        jQuery('#app_pend_customer_docs_illinois').show();
                    }else{
                        //console.log("not state of IL");
                        jQuery('#app_pend_customer_docs').show();
                    }
                    jQuery('#overlay').hide();
                }else if(vf_decision == "ACCEPTED PENDING AGENT REVIEW"){
                    //console.log("decision accepted pending agent review");
                    jQuery('#app_pend_agent_review').show();
                    jQuery('#overlay').hide();
                }else{
                    //console.log("problem with the decision. vf_decision is " + vf_decision);
                    jQuery('#app_error').show();
                    jQuery('#overlay').hide();
                }


            },
            error: function(xhr, status, errorThrown) {
                //console.log("error with process_verifications: status " + status + ", errorThrown " + errorThrown);
                 jQuery("#app_error").show();
                 jQuery("#overlay").hide();
            }
        });
</script>

<?php
} else {

        ?>
   <div id="overlay" align="center" style="display:none">
	        <div class="loaderContainer">
		        <div class="loader"> </div>
		        <p style="color:white"><b>Please wait while we process your application</b></p>
		        <p style="color:white"><b>This may take up to 60 seconds</b></p>
        	</div>
            </div>
   <?php
//The VF Decision was already set so just
        $vf_decision = (isset($_SESSION['app_vf_decision'])) ? $_SESSION['app_vf_decision'] : '';
        $application_nbr = (isset($_SESSION['app_application_nbr'])) ? $_SESSION['app_application_nbr'] : '';
        ?>
  <script type = "text/javascript">
        var vf_decision = "<?php echo "$vf_decision"; ?>";
        var application_nbr = "<?php echo "$application_nbr"; ?>";

        var data2 = {
            "application_nbr" : application_nbr
        }
        //console.log("ELSE");
      if(vf_decision == "Processing"){
          jQuery('#app_refresh').show();
      }else if(vf_decision == "Accepted"){
          //console.log("vf_decision is Accepted");
          jQuery("#overlay").show();
          (async function() {
                        let result;
                        try {
                            //console.log("about to hit create_loan_agreement");
                            result = await jQuery.ajax({
                                url: "<?php echo baseUrl(); ?>/wp-content/themes/moneymartdirect/include/create_loan_agreement.php",
                                type: 'POST',
                                dataType: "text json",
                                data: data2,
                            });
                            return result;
                        }
                        catch (error) {
                            jQuery("#app_error").show();
                            jQuery("#overlay").hide();
                        }
                    })().then( (data) => {
                            document_path = data.return_path;
                            obj = "<p><font color=\"red\"><b>Loan Agreement below - If the document doesn't display, please refresh the page. </b></font></p><iframe src=\"https://docs.google.com/gview?url="+ document_path + "&embedded=true\" style=\"width:600px; height:4700px;\" frameborder=\"0\"></iframe>";
                            jQuery("#embedPDF").append(obj);
                            jQuery("#app_accepted").show();
                            jQuery('#overlay').hide();
                    });

                }else if(vf_decision == "ACCEPTED PENDING CUSTOMER DOCUMENTS"){
                    // if(vf_state == 'IL'){
                    //     jQuery('#app_pend_customer_docs_illinois').show();
                    // }else{
                        jQuery('#app_pend_customer_docs').show();
                    // }
                }else if(vf_decision == "ACCEPTED PENDING AGENT REVIEW"){
                    jQuery('#app_pend_agent_review').show();
                }else{
                    jQuery('#app_error').show();
                }
  </script>
  <?php
}
}

?>
</div>
<?php
get_footer();
?>



