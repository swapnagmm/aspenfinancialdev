

<?php




require "mm_middleware.php";

if (is_ajax()) {
  if (isset($_POST["preverif_lead_id"]) && !empty($_POST["preverif_lead_id"])) { //Checks if gf_lead_id value exists
 
  $preverif_lead_id = isset($_POST["preverif_lead_id"]) ? $_POST["preverif_lead_id"] : 0;
  $mm_application_nbr = isset($_POST["application_nbr"] )? $_POST["application_nbr"] : 0;
   $requested_amt = isset($_POST["requested_amt"] )? $_POST["requested_amt"] : 0;
    $requested_deposit_type = isset($_POST["requested_deposit_type"]) ? $_POST["requested_deposit_type"] : 0;
 
 $preverif_results = mm_process_verifications($preverif_lead_id, $mm_application_nbr, $requested_amt, $requested_deposit_type);
 echo json_encode($preverif_results);
  }
}

//Function to check if the request is an AJAX request
function is_ajax() {
  return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}

?>
