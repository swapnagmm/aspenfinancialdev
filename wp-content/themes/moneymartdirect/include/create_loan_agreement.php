<?php
require "mm_middleware.php";
require_once $_SERVER['DOCUMENT_ROOT'] . BASE_DIR . "/wp-content/themes/moneymartdirect/include/mm_system_config.php";

$la_results = '';

if (is_ajax()) {
  if (isset($_POST["application_nbr"]) && !empty($_POST["application_nbr"])) { //Checks if application_nbr value exists
  
     $application_nbr = isset($_POST["application_nbr"]) ? $_POST["application_nbr"] : 0;
     //error_log("in create_loan_agreement.php with app number: $application_nbr\n");
    $la_results = mm_generate_loan_agreement($application_nbr);
    $la_return_value = $la_results["return_value"];
     $document_path = $la_results["document_path"];
     //error_log("mark: document_path of generated PDF: $document_path\n");
    if($la_return_value == 0){
        //error_log("la_return_value is zero\n");
        $local_path = mm_get_log_path();
        $random_string = RandomToken(6);
	    $local_filepath = $local_path . $document_path;
	    $local_filepath = substr($local_filepath,0, strlen($local_filepath) -4) . "_$random_string.pdf";
	    $local_filepath = str_replace("customer_documents/", "", $local_filepath);
	    //error_log("mark: local_filepath: $local_filepath\n");
	    $temp_array = explode("logs/",$local_filepath);
	    $relative_filepath = $temp_array[1];
	    //error_log("mark: relative_filepath: $relative_filepath\n");
	    $return_path = baseUrl() . "/mm_logs/" . $relative_filepath;
        //error_log("mark: return_path: $return_path\n");
        //Download the loan document for display
        $result = mm_get_ftp_document($document_path, $local_filepath, "B");
        $return_message = $result["return_message"];
        if($result["return_value"] == 0){
            $la_results["return_path"] = $return_path;
            error_log("mark: returning the path to pdf: $return_path\n");
        }else{
            error_log("result is NOT zero\n");
            $document_path = '';
        }
    }
    else { error_log("la_return_value is NOT zero\n"); }
  }
   echo json_encode($la_results);
}

//Function to check if the request is an AJAX request
function is_ajax() {
  return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}

?>
