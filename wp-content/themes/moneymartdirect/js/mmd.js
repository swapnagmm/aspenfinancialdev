//jQuery('#input_14_51_4').val(jQuery('#input_14_52').val());

jQuery(document).ready(function ($) {
    "use strict";
    //alert('Ready');
    /* apply only to input fields with a class of gf_readonly */
    jQuery("li.gf_readonly input").attr("readonly", "readonly");
    
    // $("input:text").each(function ()
    // {
    //     // store default value
    //     var v = this.value;
    
    //     $(this).blur(function ()
    //     {
    //         // if input is empty, reset value to default 
    //         if (this.value.length == 0) this.value = v;
    //     }).focus(function ()
    //     {
    //         // when input is focused, clear its contents
    //         //this.value = "";
    //     }); 
    // });

    //Code added by Swapna on July 16th 2018 to capture host name and check for it in few places where ids are not matching w.r.t dev and prod
    var hostname = window.location.hostname;
    var baseURL = window.location.protocol + '//' + hostname + '/dev';

    //#region Default Focus
    $("#input_14_63").focus();

    //#endregion Default Focus
    
    //#region Support functions
    function isValidUSZip(sZip) {
        return /^\d{5}(-\d{4})?$/.test(sZip);
    }

    function isValidRouting(routing) {
        var checksumTotal = (7 * (parseInt(routing.charAt(0), 10) + parseInt(routing.charAt(3), 10) + parseInt(routing.charAt(6), 10))) +
          (3 * (parseInt(routing.charAt(1), 10) + parseInt(routing.charAt(4), 10) + parseInt(routing.charAt(7), 10))) +
          (9 * (parseInt(routing.charAt(2), 10) + parseInt(routing.charAt(5), 10) + parseInt(routing.charAt(8), 10)));
        var checksumMod = checksumTotal % 10;
        if (checksumMod !== 0) return false;
        return true;
    }
    
    function isVisible(id) {
        var element = $('#' + id);
        if (element.length > 0 && element.css('visibility') !== 'hidden' && element.css('display') !== 'none') {
            return true;
        } else {
            return false;
        }
    }

    function isVisibleByClassName(classname) {
        var element = $('.' + classname);
        //alert(element.length);
        //alert(element.css('visibility'));
        //alert(element.css('display'));
        if (element.length > 0 && element.css('visibility') !== 'hidden' && element.css('display') !== 'none') {
            return true;
        } else {
            return false;
        }
    }
    
    // WI spousal form
    jQuery("#choice_17_15_1").on('change',function() {
        if (state === 'WI') {
            jQuery('#input_17_13_4').val('Wisconsin');
            if(!jQuery(this).is(':checked')) {
                // unchecked, clear the values in the input fields
                jQuery('#input_17_13_1').val('');
                jQuery('#input_17_13_2').val('');
                jQuery('#input_17_13_3').val('');
                jQuery('#input_17_13_5').val('');
            } else {
                jQuery('#input_17_13_1').val(jQuery('#input_17_16').val());
                jQuery('#input_17_13_2').val(jQuery('#input_17_17').val());
                jQuery('#input_17_13_3').val(jQuery('#input_17_18').val());
                jQuery('#input_17_13_5').val(jQuery('#input_17_19').val());
            }
        }
                
    });

    var idleTimer;
    function resetTimer()
    {
        clearTimeout(idleTimer);
        idleTimer = setTimeout(whenUserIdle,600000);//setting for 1 mins for QA testing. Should be 600000 in PROD
    }

    $(function(){
        if($('#gform_14').length){
            resetTimer();
            $('#gform_14').bind('click keyup keypress keydown focus',resetTimer); //space separated events list that we want to monitor
        }
    });

    function whenUserIdle(){
        jQuery.ajax({
            url: baseURL + "/wp-content/themes/moneymartdirect/include/idle_timeout.php",
            type: "POST",
            dataType: "text json",
            success: function(result) {
                if(result.data === true){
                    alert('You have been idle for 10 minutes. Redirecting to home page.');
                    window.location.href = baseURL;
                }
            }
        });
    }

    //jQuery('#input_17_13_4').on('change', function () {
    //    jQuery('#input_17_13_4').val('Wisconsin');
    //});
    
    // submit for loan amount
    // $(".gform_wrapper form").submit(function(event) {
    //     if (gformSubmitted) {
    //         event.preventDefault();
    //     }
    //     else {
    //         gformSubmitted = true;
    //         $("input[type='submit']", this).val("Processing, please wait...");
    //     }
    // });
    
       
        // changes made by Swapna on June 27th 2018 to fix wisconsin state validation and submit button issue
         $("#gform_17").submit(function (e) {
            let form_has_error = ($('#input_17_4')[0].selectedIndex === 0 || $('#input_17_4')[0].selectedIndex === '') ? true : false; 

            if (form_has_error) {
                alert('Please choose an option for "How would you like to receive your money?"');
                window["gf_submitting_17"] = false;
                return false;
            }else{
                let wi_marital_status = $('#input_17_12');
                
                form_has_error = (wi_marital_status.length> 0 && wi_marital_status.css('visibility') !== 'hidden' && wi_marital_status.css('display')!== 'none' && wi_marital_status[0].selectedIndex === 0) ? true : false;
                if(form_has_error && state=='WI'){
                    alert('Please choose an option for "Married Status"');
                    window["gf_submitting_17"] = false;
                    return false;
                }else{
                    if(wi_marital_status[0].selectedIndex == '1'){
                        if($('#input_17_14_3').val() == "" || $('#input_17_14_6').val() == "" || $('#input_17_13_1').val() == ""  || $('#input_17_13_3').val() == ""  || $('#input_17_13_5').val() == ""){
                            if($('#input_17_14_3').val() == ""){
                                alert('Please enter your Spouse First Name');
                                window["gf_submitting_17"] = false;
                                return false;
                            }if($('#input_17_14_6').val() == ""){
                                alert('Please enter your Spouse Last Name');
                                window["gf_submitting_17"] = false;
                                return false;
                            }else if($('#input_17_13_1').val() == ""){
                                alert('Please enter Street Address');
                                window["gf_submitting_17"] = false;
                                return false;
                            }else if($('#input_17_13_3').val() == ""){
                                alert('Please enter City');
                                window["gf_submitting_17"] = false;
                                return false;
                            }else if($('#input_17_13_5').val() == ""){
                                alert('Please enter Zip Code');
                                window["gf_submitting_17"] = false;
                                return false;
                            }else{
                                window["gf_submitting_17"] = true;
                                return true; 
                            }
                            
                            
                        }else{
                            window["gf_submitting_17"] = true;
                            return true; 
                        }
                    }else{
                        window["gf_submitting_17"] = true;
                        return true; 
                    }
                   
                }
                
                
            }
         });
         
        //  $("#gform_17").submit(function (e) {
        //     let form_has_error = false;
        //     //alert($('#input_17_4')[0].selectedIndex);
        //     // let tmp = $('#input_17_4')[0].selectedIndex;
        //     // alert(tmp);
        //     // //alert($('#input_17_4')[0].selectedIndex === 0);
        //     // alert(tmp == 0);
        //     // alert(tmp === 0);
        //     // form_has_error = ($('#input_17_4')[0].selectedIndex === 0) ? true : false; 
        //     // alert(form_has_error);
        //     let wi_marital_status = $('#input_17_12');
        //     alert(wi_marital_status[0].selectedIndex);
        //     // form_has_error = (wi_marital_status.length && wi_marital_status[0].selectedIndex === 0) ? true : false;
             
        //     if (form_has_error) {
        //              e.preventDefault();
        //     }
        //  });
        
    
    //#endregion Support functions
    

    $(document).bind('gform_post_render', function () {
        //alert('In Bind'+jQuery('#gform_source_page_number_14').val());
        var form_has_error = false;
        // var pg_nbr = $('#gform_source_page_number_14').val();
        // if (pg_nbr==='5') {
        //     if (current_state!=='DE') {
        //         $('#field_14_118').hide();
        //         $('#field_14_119').hide();
        //     }
        // }

        //#region Step 2
        $("#input_14_51_5").on({
            //mouseenter: function () {
            //    $(this).css("background-color", "lightgray");
            //},
            // mouseleave: function(){
            //     $(this).css("background-color", "lightblue");
            // },
            // click: function(){
            //     $(this).css("background-color", "yellow");
            // }
            focus: function () {
                var tmp = $(this).parent().has('div');
                if (tmp.length > 0) {
                    tmp.find('div').remove();
                    form_has_error = false;
                }
            },
            blur: function () {
                if (!isValidUSZip($(this).val())) {
                    $(this).parent().append('<div class="gfield_description validation_message">Zip code entered is invalid.</div>');
                    form_has_error = true;
                }


            }
        });
        //#endregion Step 2
        //#region Step 3
        // Routing Number validation
        $("#input_14_63, #input_14_109").on({
            focus: function () {
                var tmp = $(this).parent().has('div');
                if (tmp.length > 0) {
                    tmp.find('div.validation_message').remove();
                    form_has_error = false;
                }
            },
            blur: function () {
                if ($(this).val().length != 9) {
                    $(this).parent().append('<div class="gfield_description validation_message">Routing number must be exactly 9 digits.</div>');
                    form_has_error = true;
                } else {
                    if (!isValidRouting($(this).val())) {
                        $(this).parent().append('<div class="gfield_description validation_message">Routing number is invalid.</div>');
                        form_has_error = true;
                    }
                }


            }
        });

        $('#gform_14').bind('click keyup keypress keydown focus',resetTimer); //space separated events list that we want to monitor

        //#endregion Step 3
        $("#gform_14").submit(function (e) {
            let pg_nbr_elem = $('#gform_source_page_number_14');
            if (pg_nbr_elem.length) {
                // if (pg_nbr_elem.val() === '4') {
                //     let y=$('#input_14_115').val().trim();
                //     let m=$('#input_14_116').val().trim();
                //     if((y===''&&m==='')||(y==='0'&&m==='0')||(y==='0'&&m==='')||(y===''&&m==='0')) {
                //         $('#input_14_115').parent().append('<div class="gfield_description validation_message">Both Years and Months cannot be zero.</div>');
                //         $('#input_14_116').parent().append('<div class="gfield_description validation_message">Both Years and Months cannot be zero.</div>');
                //         form_has_error = true;
                //     } else if ((y==='0'&&m>0)||(y>0&&m==='0')) {
                //         var tmp = $(this).parent().has('div');
                //         if (tmp.length > 0) {
                //             tmp.find('div.validation_message').remove();
                //             form_has_error = false;
                //         }
                //     }
                // }

                if (form_has_error) {
                    e.preventDefault();
                }
            }
        });// submit
        
        
        // DE Disclosure
        let tmp = isVisible('label_14_119_1');
        tmp ? $('#field_14_121').show() : $('#field_14_121').hide();
        
        // CA Disclosure
        /*let ca_sched_agreement = $("#label_14_122_1");
        if (ca_sched_agreement.length) {
            $("#label_14_122_1").html('I have read and agree to the California Schedule of Charges located <a href="'+location.protocol + '//' + location.host+'/wp-content/uploads/2018/06/CA-AFD_CA_ScheduleofCharges_5_28_18.pdf" name="CA-AFD_CA_ScheduleofCharges_5_28_18.pdf" target="_blank">here.</a>');
        }
        tmp = isVisible('label_14_122_1');
        tmp ? $('#field_14_123').show() : $('#field_14_123').hide();*/
        
         //Code Start - condition added by Swapna on July 16th 2018 to capture host name and check for if its dev or prod as the ids were not matching for california 
         /*if(hostname == "dev.moneymartdirect.com")
         {
             var label_name = 'label_14_122_1';
             var field_name = 'field_14_123';
         }
         else if(hostname == "aspenfinancialdirect.com")
         {
             var label_name = 'label_14_129_1';
             var field_name = 'field_14_121';
         }
         //Code End

        // CA Disclosure
        let ca_sched_agreement = $("#"+label_name);
        if (ca_sched_agreement.length) {
            $("#"+label_name).html('I have read and agree to the California Schedule of Charges located <a href="' + location.protocol + '//' + location.host + '/wp-content/uploads/2018/06/CA-AFD_CA_ScheduleofCharges_5_28_18.pdf" name="CA-AFD_CA_ScheduleofCharges_5_28_18.pdf" target="_blank">here.</a>');
        }

         tmp1 = isVisible(label_name);
             tmp1 ? $(field_name).show() : $(field_name).hide();*/


        let ca_sched_agreement = $(".agreecaliforniacheckbox");
        //alert(ca_sched_agreement.length);
        if (ca_sched_agreement.length) {
            var labelcalifornia = $(".agreecaliforniacheckbox").children('div').find('ul li label');
            //alert(labelcalifornia);
            labelcalifornia.html('I have read and agree to the California Schedule of Charges located <a href="' + location.protocol + '//' + location.host + '/wp-content/uploads/2018/06/CA-AFD_CA_ScheduleofCharges_5_28_18.pdf" name="CA-AFD_CA_ScheduleofCharges_5_28_18.pdf" target="_blank">here...</a>');
        }

        //tmp1 = isVisibleByClassName('agreecaliforniacheckbox');
        //alert(tmp1);
        // tmp1 ? $('.california_agreement_bar').show() : $('.california_agreement_bar').hide();

    }); // bind

});



